
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol



def molden2nmd (molden_inp_file,nmd_out_name):


    ## Reading the data from molden file
    w,ir,modes,r,atm_names=mol.w_modes_r_atm_names_from_molden(molden_inp_file)

    n_atm=len(r)
    n_vib=len(modes)

    nmd_out_file=nmd_out_name+".nmd"
    xyz_out_file=nmd_out_name+".xyz"

    mol.xyz_file (r,atm_names,xyz_out_file)


    with open(nmd_out_file,"w+") as file:
        ## writing atom names
        file.write("atomnames ")
        for name in atm_names:
            file.write(name+" "+name+" "+name+" ")
        file.write("\n")

        ## writing coordinates
        file.write("coordinates ")
        for i in range(0,n_atm):
            file.write(str(r[i,0])+"  "+str(r[i,1])+"  "+str(r[i,2])+" ")
        file.write("\n")

        ## writing modes
        for i in range(0,n_vib):
            file.write("mode "+str(i)+" ")
            for j in range(0,n_atm):
                file.write(str(modes[i,j,0])+"  "+str(modes[i,j,1])+"  "+str(modes[i,j,2])+" ")
            file.write("\n")




















###########################################################################%%


if __name__== "__main__":


    #inp_file="/home/ar612/Documents/Work/cp2k_organized/vibrational_analysis/aptreac2_b3lyp_vibrational_analysis/aptreac2_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"
    # nmd_out_name="/media/ar612/multimedia_ale/Documents/cp2k_organized/vibrational_analysis/25pctreac1_b3lyp_vibrational_analysis/25pctreac1_b3lyp_vibrational_analysis-VIBRATIONS-1"
    # inp_file=molden_inp_file
    molden_inp_file=sys.argv[1]
    nmd_out_name=sys.argv[2]

    molden2nmd (molden_inp_file,nmd_out_name)
