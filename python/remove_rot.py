#! /bin/python3

#%%
import numpy as np
import sys
import mol
import rmsd


file1=sys.argv[1]
file2=sys.argv[2]

file1="/home/ar612/Documents/Work/cp2k_organized/reac_path/pctreac1dftb_pctts1dftb_pctprod1dftb_band_40rep_dftb_reduced/pctreac1_complete_path.xyz"
file2="/home/ar612/Documents/Work/cp2k_organized/reac_path/pctreac1dftb_pctts1dftb_pctprod1dftb_band_40rep_dftb_reduced/pctreac1_complete_path.xyz"


e1,r1,atm_names1=mol.e_r_atm_names_from_xyz(file1)
e2,r2,atm_names2=mol.e_r_atm_names_from_xyz(file2)

r2_rot=mol.remove_trans_rot(r2,r1[0],range(len(atm_names1)))

## Writing the trajectory to the file
mol.write_trajectory (r2_rot,e2,atm_names1,file1.replace(".xyz","_rot.xyz"))
























###########################################################################%%


if __name__== "__main__":


    # r1_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/test/r20.xyz"
    # r2_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/test/pmf2.xyz"

    r1_xyz_file=sys.argv[1]
    r2_xyz_file=sys.argv[2]




    main(r1_xyz_file,r2_xyz_file)
