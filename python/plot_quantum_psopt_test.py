
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import glob

import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

def get_iter_time(inp_file):
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'Number of Iterations....:' in line:
               iter=int(line.split()[-1])
           if 'Runtime is:' in line and 'min' in line:
               time=float(line.split()[-2])
    return iter,time


def plot_overlap(file_pattern,lsolver_list,ncpu_list):

    ##%%
    nlsolver=len(lsolver_list)
    nncpu=len(ncpu_list)

    iter=np.zeros([nlsolver,nncpu])
    time=np.zeros([nlsolver,nncpu])

    for i,lsolver in enumerate(lsolver_list):
        for j,ncpu in enumerate(ncpu_list):

            print(lsolver,ncpu)

            file=file_pattern.replace("$$",str(lsolver)).replace("%%",str(ncpu))
            file=glob.glob(file)[0]

            iter_file,time_file=get_iter_time(file)

            iter[i,j]=iter_file
            time[i,j]=time_file


    ##%%
    fig, ax = plt.subplots()
    for i,lsolver in enumerate(lsolver_list):
        ax.plot(ncpu_list,iter[i],'o',label=lsolver)
    ax.set_title(name)
    ax.set_xlabel("ncpus")
    ax.set_ylabel("Iterations")
    ax.legend()
    ##%%

    ##%%
    fig, ax = plt.subplots()
    for i,lsolver in enumerate(lsolver_list):
        ax.plot(ncpu_list,time[i],'o',label=lsolver)
    ax.set_title(name)
    ax.set_xlabel("ncpus")
    ax.set_ylabel("Time (min)")
    ax.legend()
    ##%%

    ##%%
    fig, ax = plt.subplots()
    for i,lsolver in enumerate(lsolver_list):
        ax.plot(ncpu_list,time[i]/iter[i],'o',label=lsolver)
    ax.set_title(name)
    ax.set_xlabel("ncpus")
    ax.set_ylabel("Time (min)/Iteration")
    ax.legend()
    ##%%

if __name__== "__main__":

    lsolver_list=["ma27","ma57", "ma86", "ma97", "mumps"]
    ncpu_list=[4]

    name="12_emax"
    file_pattern="/home/ar612/Documents/Work/quantum_psopt/test/"+name+"/$$/%%/$$_%%_*.out"
    ##%%
    plot_overlap(file_pattern,lsolver_list,ncpu_list)
