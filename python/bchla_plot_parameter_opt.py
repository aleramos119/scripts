
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import islice
import glob
import quantum as quant
import shutil

##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%
abc=[chr(i) for i in range(97,123)]

def i_max(e_list,vmax):
    for i,e in enumerate(e_list):
        if e < vmax:
            i_max=i
    return i_max


def G(t,W,sig):
    if np.abs(t)<=W/2:
        return 1
    if np.abs(t)>W/2:
        return np.exp(-((np.abs(t)-W/2)**2)/(2*sig**2))

############## Ectract the ith element of a line matching the given pattern #################


def extract_pattern(inp_file,pattern,i,number=True):

    e=np.zeros([0])

    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if pattern in line:
               if number:
                   e=np.append(e,float(line.split()[i]))
               else:
                   e=np.append(e,line.split()[i])

    return e



def read_psopt_output(out_file):

    collocation=None
    nnodes=None
    solved=""
    solver=""
    niter=None
    cost=None
    end_cost=None
    run_cost=None
    local_err=None
    t=None

    with open(out_file) as file:
       for n, line in enumerate(file, 1):
           if "Collocation method:" in line:
               collocation=line.split()[-1]
           if "Number of nodes phase 1:" in line:
               nnodes=int(line.split()[-1])
           if "running with linear solver" in line:
               solver=line.split()[-1].replace('.','')
           if "Number of Iterations....:" in line:
               niter=int(line.split()[-1])
           if "Runtime is:" in line and "sec" in line:
               t=float(line.split()[-2])/60

           if "NLP solver reports:" in line and "The problem has been solved!" in line:
               solved="solved"
           if "EXIT: Solved To Acceptable Level." in line:
               solved="almost_solved"

    with open(out_file) as file:
        if solved=="solved" or solved=="almost_solved":
            for n, line in enumerate(file, 1):
                if "(unscaled) cost function value:" in line:
                    cost=float(line.split()[-1])
                if "Phase 1 endpoint cost function value:" in line:
                    end_cost=float(line.split()[-1])
                if "Phase 1 integrated part of the cost:" in line:
                    run_cost=float(line.split()[-1])
                if "Phase 1 maximum relative local error:" in line:
                    local_err=float(line.split()[-1])



    return collocation,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t


def read_irep_data_from_folder(dir):

    par_opt=True


    eta0_list_list=[]
    etaf_list_list=[]
    t0_list_list=[]
    tf_list_list=[]
    cost_list_list=[]
    end_cost_list_list=[]
    run_cost_list_list=[]
    niter_list_list=[]
    comp_time_list_list=[]
    i_list_list=[]


    eta_dir_list=glob.glob(dir+"/*")
    eta_dir_list=[eta_dir for eta_dir in eta_dir_list if os.path.isdir(eta_dir)]
    eta_dir_list.sort()

    project=dir.split("/")[-1]

    if "no_par_opt" in dir:
        par_opt=False



    for eta_dir in eta_dir_list:

        i_dir_list=glob.glob(eta_dir+"/*")
        i_dir_list=[i_dir for i_dir in i_dir_list if os.path.isdir(i_dir)]

        i_dir_list.sort()




        eta0_list=[]
        etaf_list=[]
        t0_list=[]
        tf_list=[]
        cost_list=[]
        end_cost_list=[]
        run_cost_list=[]
        niter_list=[]
        i_list=[]
        comp_time_list=[]



        name_list=eta_dir.split("/")[-1].split("_")
        indx_t0=name_list.index("t0")
        indx_eta0=name_list.index("eta0")




        for i_dir in i_dir_list:





            out_file_list=glob.glob(i_dir+"/reduced.out")
            out_file_list=[ out_file for out_file in out_file_list if "slurm" not in out_file and "ipopt" not in out_file ]
            out_file_list.sort()
            eta_file=i_dir+"/p.dat"
            time_file=i_dir+"/t.dat"



            if len(out_file_list) != 0:
                #print(out_file_list)
                colloc,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,comp_time=read_psopt_output(out_file_list[-1])

                niter_list.append(niter)
                comp_time_list.append(comp_time)





                if solved=="solved" or solved=="almost_solved":



                    t0=float(name_list[indx_t0+1])
                    t0_list.append(t0)

                    eta0=float(name_list[indx_eta0+1])
                    eta0_list.append(eta0)


                    if par_opt:
                        etaf=float(np.genfromtxt(eta_file))
                    else:
                        etaf=eta0

                    tf=float(np.genfromtxt(time_file)[-1])


                    i_list.append(int(i_dir.split("/")[-1]))
                    etaf_list.append(etaf)
                    tf_list.append(tf)
                    cost_list.append(cost)
                    end_cost_list.append(end_cost)
                    run_cost_list.append(run_cost)



        i_list_list.append(i_list)
        eta0_list_list.append(eta0_list)
        etaf_list_list.append(etaf_list)
        t0_list_list.append(t0_list)
        tf_list_list.append(tf_list)
        cost_list_list.append(cost_list)
        end_cost_list_list.append(end_cost_list)
        run_cost_list_list.append(run_cost_list)
        niter_list_list.append(niter_list)
        comp_time_list_list.append(comp_time_list)



    return project,i_list_list,eta0_list_list,etaf_list_list,cost_list_list,end_cost_list_list,run_cost_list_list,t0_list_list,tf_list_list,niter_list_list,comp_time_list_list





##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%



##################################################################################################################%%
################################ Ploting Collocation ##########################################################%%
##################################################################################################################%%
e0=0.0005764776761753108
#mu0=0.0295
mu0=1.657

e0_2_ps=mol.au2fs/(e0*1000)
ps_2_e0=1/e0_2_ps


input_dir=sys.argv[1]
input_dir="/home/ar612/Documents/Work/cavities/fixed_dipole/par_opt_eta_scan_tf_free/excitation_molecular_1/N1_5_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced"
input_dir_list=input_dir.split("/")


sys=input_dir_list[-2]


#sys_dir="/data/ar612/cavities/H_transfer/wc_w10_neta_11/"+sys
sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/"+sys




###%%
scan="eta_free"
#plt.rc('font', size=40)
marksize=20


i=0
#for eta_random_dir in eta_random_dir_list:
for i in range(1):

    name=input_dir_list[-1]

    dir=input_dir

    project_3,i_list_3,eta0_list_3,eta_list_3,cost_list_3,end_cost_list_3,run_cost_list_3,t0_list_3,tf_list_3,niter_list,comp_time_list=read_irep_data_from_folder(dir)




    tf_list_3=[ list(np.array(tf)*e0_2_ps) for tf in tf_list_3]
    t0_list_3=[ list(np.array(tf)*e0_2_ps) for tf in t0_list_3]


    optimal_par_list=[]
    optimal_cost_list=[]
    optimal_end_cost_list=[]
    optimal_run_cost_list=[]
    optimal_tf_list=[]
    optimal_etaf_list=[]
    optimal_i_list=[]



    if scan == "tf":
        par_list=list(np.arange(150, 400, 30, dtype=float)*e0_2_ps)
        read_par_list=t0_list_3

    if scan == "tf_free":
        par_list=list(np.arange(25, 30, 10, dtype=float)*e0_2_ps)
        read_par_list=t0_list_3

    if scan == "eta":
        par_list=list(np.arange(0.00, 0.055, 0.005, dtype=float))
        read_par_list=eta0_list_3

    if scan == "eta_free":
        par_list=list(np.arange(0.035, 0.036, 0.005, dtype=float))
        read_par_list=eta0_list_3


    for t,par_list in enumerate(read_par_list):
        min_val=np.inf
        i_min=0
        if len(par_list)!=0:
            par=par_list[0]
            for i,cost in enumerate(cost_list_3[t]):
                if cost_list_3[t][i] < min_val:
                    min_val=cost_list_3[t][i]
                    i_min=i




            optimal_par_list.append(par)
            optimal_cost_list.append(cost_list_3[t][i_min])
            optimal_end_cost_list.append(end_cost_list_3[t][i_min])
            optimal_run_cost_list.append(run_cost_list_3[t][i_min])
            optimal_tf_list.append(tf_list_3[t][i_min])
            optimal_etaf_list.append(eta_list_3[t][i_min])
            optimal_i_list.append(i_list_3[t][i_min])


    t0_name=glob.glob(dir+"/*")[0].split("_")[-1]


    if scan == "eta" or scan=="eta_free":
        for k in range(len(optimal_par_list)):
            mmax=max([ int(mdir.split("/")[-1]) for mdir in glob.glob(dir+"/eta0_"+"{:.3f}".format(optimal_par_list[k])+"_t0_"+t0_name+"/*") if os.path.isdir(mdir)])

            for m in range(1,mmax+1):
                if m!=optimal_i_list[k] and os.path.isdir(dir+"/eta0_"+"{:.3f}".format(optimal_par_list[k])+"_t0_"+t0_name+"/"+str(m)):
                    shutil.rmtree(dir+"/eta0_"+"{:.3f}".format(optimal_par_list[k])+"_t0_"+t0_name+"/"+str(m))



    #########################################################

    matrix=np.array([optimal_par_list,optimal_cost_list]).transpose()

    if scan=="tf" or scan=="tf_free":
        mol.write_matrix_to_file(matrix,"tf(ps)   total_cost","",dir+"/total_cost.txt")


    if scan=="eta" or scan=="eta_free":
        mol.write_matrix_to_file(matrix,"eta0   total_cost","",dir+"/total_cost.txt")



    #########################################################



    #########################################################
    matrix=np.array([optimal_par_list,optimal_end_cost_list]).transpose()

    if scan=="tf":
        mol.write_matrix_to_file(matrix,"tf(ps)   end_cost","",dir+"/end_cost.txt")

    if scan=="eta" or scan=="eta_free":
        mol.write_matrix_to_file(matrix,"eta0   end_cost","",dir+"/end_cost.txt")



    #########################################################


    #########################################################
    matrix=np.array([optimal_par_list,optimal_run_cost_list]).transpose()

    if scan=="tf":
        mol.write_matrix_to_file(matrix,"tf(ps)   run_cost","",dir+"/run_cost.txt")

    if scan=="eta" or scan=="eta_free":
        mol.write_matrix_to_file(matrix,"eta0   run_cost","",dir+"/run_cost.txt")


    #########################################################



    #########################################################    matrix=np.array([eta0_list_3,cost_list_3]).transpose()
    matrix=np.array([optimal_par_list,optimal_tf_list]).transpose()

    if scan=="tf":
        mol.write_matrix_to_file(matrix,"tf(ps)   tf","",dir+"/tf.txt")

    if scan=="eta" or scan=="eta_free":
        mol.write_matrix_to_file(matrix,"eta0   tf","",dir+"/tf.txt")


    #########################################################





    #########################################################
    matrix=np.array([optimal_par_list,optimal_etaf_list]).transpose()

    if scan=="tf":
        mol.write_matrix_to_file(matrix,"tf(ps)   etaf","",dir+"/etaf.txt")

    if scan=="eta" or scan=="eta_free":
        mol.write_matrix_to_file(matrix,"eta0   /home/ar612/Documents/Work/cavities/no_par_opt_tf_scan/excitation_molecular_1/Emax1_10/neta_11/etaf","",dir+"/etaf.txt")

    #########################################################

    mol.write_matrix_to_file(np.array([optimal_par_list,optimal_i_list]).transpose(),"i","",dir+"/opt_i.txt")
    #

####%%


#################################################################################%%
