"""A set of function usefull for working with quantum systems.
"""

import numpy as np
from scipy import interpolate
from scipy import integrate
import math
import scipy


def bra_ket_int(r,psi1,O,psi2):
    """A function to compute the integral \int \psi1*(r)O(r)\psi2(r)dr.

    Parameters
    ----------
    r : 1d array
        The grid points.
    psi1 : 1d array
        The values of psi1 at the respective grid points.
    O : 1d array
        The values of operator O at the respective grid points..
    psi2 : 1d array
        The values of psi2 at the respective grid points..

    Returns
    -------
    Complex number
        The value of the integral.

    """
    return integrate.simps(psi1.conj()*O*psi2,r)


def bra_ket_dis(psi1,O,psi2):
    """Computes the projection <psi1|O|psi2>.

    Parameters
    ----------
    psi1 : 1d array
        The representation of psi1 in some base.
    O : 2d array
        The representation of operator O in some base.
    psi2 : 1d array
        The representation of psi2 in some base.

    Returns
    -------
    Complex number
        The result of the projection

    """
    n=len(psi1)
    m=len(psi2)
    if n==len(O) and len(O[0])==m:
        return np.matmul(np.matmul(psi1.conj(),O),psi2)
    else:
        return 0

def spline(x_list,y_list,x1_list):
    """Given a set of grid points (x_list) and respective y values (y_lis), construct an interpolation and returns the values in the new grid point (x1_list).

    Parameters
    ----------
    x_list : 1d array
        Old grid points.
    y_list : 1d array
        Old y values.
    x1_list : 1d array
        New grid points.

    Returns
    -------
    1d array
        New y values.

    """
    f=interpolate.CubicSpline(x_list, y_list)
    y1_list=np.array([f(x1) for x1 in x1_list])
    return y1_list

def mat_rep(r,states1,O,states2):
    """Given 2 sets of eigenstates, construct the matrix representation of operator O in the given space.

    Parameters
    ----------
    r : 1d array
        The grid points.
    states1 : 2d array
        The first set of states at the respective grid points.
    O : 1d array
        The operator values at the respective grid points.
    states2 : type
        The second set of states at the respective grid points.

    Returns
    -------
    2d array
        The matrix representation of operator O.

    """
    n=len(states1) #Number of eigenstates1
    m=len(states2) #Number of eigenstates2

    O_mat=np.zeros([n,m])

    for i in range(n):
        for j in range(m):
            O_mat[i,j]=bra_ket_int(r,states1[i],O,states2[j])
    return O_mat

def deriv(r_list, psi):
    """Constructs the derivatives of the set of states psi at the given grid points.

    Parameters
    ----------
    r_list : 1d array
        The grid points.
    psi : 2d array
        The set of states at the given grid points.

    Returns
    -------
    2d array
        The set of state derivatives at the given grid points.

    """
    n=len(psi)
    l=len(r_list)

    psi_der=np.zeros([n,l])

    for i in range(n):
        f=interpolate.CubicSpline(r_list, psi[i]).derivative()
        psi_der[i]=np.array([ f(r)  for r in r_list])

    return psi_der

def populations(psi):
    return np.multiply(psi,np.conj(psi)).real

def vec_2_psi(vec):
    Ntot=len(vec)//2
    return vec[:Ntot]+1j*vec[Ntot:]

def psi_2_vec(psi):
    Ntot=len(psi)
    steps=len(psi[0])
    vec=np.zeros([2*Ntot,steps])

    vec[:Ntot]=psi.real
    vec[Ntot:]=psi.im

    return vec

def normalize_vec(vec):
    psi=vec_2_psi(vec)
    norm=bra_ket_dis(psi,np.identity(len(vec)//2),psi).real
    return vec/np.sqrt(norm)


def electronic_pop(psi,N_list):
    """Reads the output of irep2_psopt.

    Parameters
    ----------
    psi: 2d array
        eigenstates populations as function of time.

    N_list: 1d array
        list of integers of the number of eigenstates on each electronic state

    Returns
    -------
    2d array
        The electronic populations as a function of time.

    """

    nstates=len(psi)
    nsteps=len(psi[0])
    n_estates=len(N_list)

    Nsum_list=np.insert(np.cumsum(N_list),0,0)

    pop=populations(psi)

    e_pop=np.zeros([n_estates,nsteps])

    for i in range(n_estates):
        e_pop[i]=pop[Nsum_list[i],Nsum_list[i+1]]

    return e_pop



def H(n,x,m=0):
    """hermite function.

    Parameters
    ----------
    n: integer
        hermite order

    x: 1d array
        list of coordinate values

    Returns
    -------
    1d array
        list of function values

    """
    if m > n:
        return 0
    else:
        return (2**m)*(math.factorial(n)/math.factorial(n-m))*scipy.special.eval_hermite(n-m,x)


def psi_ho(n,x,m,w):
    """Harmonic oscilator eigenstates

    Parameters
    ----------
    n: integer
        eigenstate order

    x: 1d array
        list of coordinate values

    m: float
        mass of the particle

    w: float
        angular frequency of the HO

    Returns
    -------
    1d array
        list of function values

    """
    return (1/np.sqrt(float(math.factorial(n)*2**n)))*(m*w/np.pi)**(1/4)*np.exp(-0.5*m*w*x**2)*H(n,np.sqrt(m*w)*x)


def d2_psi_ho(n,x,m,w):
    """Second derivative of the harmonic oscilator eigenstates

    Parameters
    ----------
    n: integer
        eigenstate order

    x: 1d array
        list of coordinate values

    m: float
        mass of the particle

    w: float
        angular frequency of the HO

    Returns
    -------
    1d array
        list of function values

    """
    return (1/np.sqrt(math.factorial(n)*2**n))*(m*w/np.pi)**(1/4)*np.exp(-0.5*m*w*x**2)* (   (m*w)*H(n,np.sqrt(m*w)*x,2)   - 2* (m*w)**(3/2.)*x*H(n,np.sqrt(m*w)*x,1) + (m*w)*(m*w*x**2-1)*H(n,np.sqrt(m*w)*x)  )
