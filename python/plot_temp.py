
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol


def main(inp_file,y_label,cy,col,job):
    ##################################################################################################################%%
    #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1)

    transp=0.7
    if(job=="comp"):
        cv=mol.fast_load(inp_file,np.array([1,2,4,5]),1000).T
        #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=(1,2,4,5))
        plt.style.use(['ggplot'])
        plt.plot(cv[0]*mol.fs2ps,cv[1]*cy,linewidth=2.5,label="E_kin",color='tab:blue')
        plt.plot(cv[0]*mol.fs2ps,(cv[2]-cv[2,0])*cy,linewidth=2.5,alpha=transp,label="E_pot",color='tab:orange')
        plt.plot(cv[0]*mol.fs2ps,(cv[2]-cv[2,0]+cv[1])*cy,linewidth=2.5,alpha=transp,label="E_pot+E_kin",color='tab:green')
        plt.plot(cv[0]*mol.fs2ps,(cv[3]-cv[2,0])*cy,linewidth=2.5,label="Cons_qty",color='tab:red')
        plt.xlabel('t (ps)',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel(y_label,fontweight='bold')
        plt.grid(True)
        plt.legend(loc=8,ncol=8)
        plt.show()
    elif(job=="pot" or job=="cons"):
        cv=mol.fast_load(inp_file,np.array([1,col]),1000).T
        #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=(1,col))
        plt.style.use(['ggplot'])
        plt.plot(cv[0]*mol.fs2ps,(cv[1]-cv[1,0])*cy,linewidth=2.5,color='tab:blue')
        plt.xlabel('t (ps)',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel(y_label,fontweight='bold')
        plt.grid(True)
        plt.show()
    elif(job=="temp"):
        cv=mol.fast_load(inp_file,np.array([1,col]),1000).T
        #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=(1,col))
        plt.style.use(['ggplot'])
        avg_dat=mol.mean_list(cv[1]*cy) ## This list contains the average as a function of list index
        avg=avg_dat[-1]
        avg_list=np.full((len(cv[1])), avg)
        plt.plot(cv[0]*mol.fs2ps,cv[1]*cy,linewidth=2.5,color='tab:blue')
        plt.plot(cv[0]*mol.fs2ps,avg_list,linewidth=2.5,color='tab:red')
        plt.plot(cv[0]*mol.fs2ps,avg_dat,linewidth=2.5,color='tab:red')
        plt.text(1.01*cv[0,-1]*mol.fs2ps, avg, str(avg)[:5], horizontalalignment='left', size='large',color='tab:red')
        plt.xlabel('t (ps)',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel(y_label,fontweight='bold')
        plt.grid(True)
        plt.show()
    elif(job=="time"):
        cv=mol.fast_load(inp_file,np.array([0,col]),1000).T
        #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=(1,col))
        avg_dat=mol.mean_list(cv[1]*cy) ## This list contains the average as a function of list index
        avg=avg_dat[-1]
        plt.style.use(['ggplot'])
        avg_list=np.full((len(cv[1])), avg)
        plt.plot(cv[0],cv[1]*cy,linewidth=2.5,color='tab:blue')
        plt.plot(cv[0],avg_list,linewidth=2.5,color='tab:red')
        plt.plot(cv[0],avg_dat,linewidth=2.5,color='tab:red')
        plt.text(1.01*cv[0,-1], avg, str(avg)[:5], horizontalalignment='left', size='large',color='tab:red')
        plt.xlabel('steps',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel(y_label,fontweight='bold')
        plt.grid(True)
        plt.show()
    else:
        cv=mol.fast_load(inp_file,np.array([1,col]),1000).T
        #cv=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=(1,col))
        plt.plot(cv[0]*mol.fs2ps,cv[1]*cy,linewidth=2.5,color='tab:blue')
        plt.xlabel('t (ps)',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel(y_label,fontweight='bold')
        plt.grid(True)
        plt.show()







if __name__== "__main__":

    # inp_file="/cluster/data/ar612/cp2k_organized/meta_dyn/cv_reactant_meta_dyn_reduced/cv_reactant_meta_dyn-1.ener"
    # job="time"


    inp_file=sys.argv[1]
    job=sys.argv[2]

    if(job=="kin"):
        y_label="E_kin (kcal/mol)"
        cy=mol.Eh2kcalmol
        col=2
        comp=""
    if(job=="temp"):
        y_label="T (K)"
        cy=1
        col=3
        comp=""
    if(job=="pot"):
        y_label="E_pot (kcal/mol)"
        cy=mol.Eh2kcalmol
        col=4
        comp=""
    if(job=="cons"):
        y_label="Cons_qty (kcal/mol)"
        cy=1
        col=5
        comp=""
    if(job=="time"):
        y_label="Time/frame (s)"
        cy=1
        col=6
        comp=""
    if(job=="comp"):
        y_label="E (kcal/mol)"
        cy=mol.Eh2kcalmol
        col=4
        comp="comp"


    main(inp_file,y_label,cy,col,job)
