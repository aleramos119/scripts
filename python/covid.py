#%%
import numpy as np

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
#%matplotlib qt

class country:
  def __init__(self,name,time, pop, infect, death):
    self.name = name
    self.time = time
    self.pop = pop
    self.infect = infect
    self.death = death


##################################################################################################################%%
count_list=[]
count_list.append(country("Cuba",['23.04.2020','22.05.2020'],11326616,[1235.,1916.],[43,81]))
count_list.append(country("EEUU",['23.04.2020','22.05.2020'],331002651,[851195.,1625071.],[47798,96526]))
count_list.append(country("Spain",['23.04.2020','22.05.2020'],46754778,[213024.,280117.],[22157,27940]))
count_list.append(country("France",['23.04.2020','22.05.2020'],65273511,[159877.,181826.],[21340,28215] ))
count_list.append(country("Germany",['23.04.2020','22.05.2020'],83783942,[151175.,179381.],[5354,8325] ))
count_list.append(country("Italy",['23.04.2020','22.05.2020'],60461826,[187327.,228006.],[25085,32486] ))
count_list.append(country("China",['23.04.2020','22.05.2020'],1439323776,[82798.,82971.],[4632,4634] ))
count_list.append(country("Taiwan",['23.04.2020','22.05.2020'],23816775,[427.,441.],[6,7]))
count_list.append(country("South Korea",['23.04.2020','22.05.2020'],51269185,[10702.,11142.],[240,264]))
count_list.append(country("Chile",['23.04.2020','22.05.2020'],19116201,[11296.,61857.],[160,630]))
count_list.append(country("Brazil",['23.04.2020','22.05.2020'],212559417,[46348.,312074.],[2934,20112]))
#count_list.append(country("Argentina",['23.04.2020','22.05.2020'],45195774,[3288.,9931.],[159,419]))
count_list.append(country("Mexico",['23.04.2020','22.05.2020'],128932753,[10544.,59567.],[970,6510]))
count_list.append(country("El Salvador",['23.04.2020','22.05.2020'],6486205,[250.,1725.],[8,33]))
count_list.append(country("Costa Rica",['23.04.2020','22.05.2020'],5094118,[681.,903.],[6,10]))
count_list.append(country("Rusia",['23.04.2020','22.05.2020'],145934462,[62773.,326448.],[555,3249]))
count_list.append(country("Vietnam",['23.04.2020','22.05.2020'],97338579,[268.,324.],[0,0]))
count_list.append(country("Dominican Republic",['23.04.2020','22.05.2020'],10847910,[5543.,13989.],[265,456]))
count_list.append(country("Hong Kong",['23.04.2020','22.05.2020'],7496981,[1036.,1066.],[4,4]))
count_list.append(country("Singapore",['23.04.2020','22.05.2020'],5850342,[11178.,30426.],[12,23]))
count_list.append(country("Colombia",['23.04.2020','22.05.2020'],50882891,[4356.,18330.],[206,652]))
count_list.append(country("Venezuela",['23.04.2020','22.05.2020'],28435940,[298.,882.],[10,10]))
##################################################################################################################%%

##%%
fig=plt.figure(figsize=(16, 9))
ax = fig.add_subplot(111)
n=0
for count in count_list:
    ax.plot((count.infect[n]/count.pop)*100,(count.death[n]/count.infect[n])*100,'o',markersize=4,label=count.name)
n=1
plt.gca().set_prop_cycle(None)
for count in count_list:
    ax.plot((count.infect[n]/count.pop)*100,(count.death[n]/count.infect[n])*100,'o',markersize=7,label=count.name)
    ax.text((count.infect[n]/count.pop)*100,(count.death[n]/count.infect[n])*100, count.name, horizontalalignment='center',size='small')
plt.gca().set_prop_cycle(None)
for count in count_list:
    ax.plot([(count.infect[0]/count.pop)*100,(count.infect[1]/count.pop)*100],[(count.death[0]/count.infect[0])*100,(count.death[1]/count.infect[1])*100],'--',label=count.name)
ax.set_xlabel('Percentage of population infected')
ax.set_ylabel('Mortality rate')
plt.title("23.04.2020 ---------- 22.05.2020")
fig.savefig("covid.pdf", bbox_inches='tight')
fig.savefig("covid.png", bbox_inches='tight')
plt.show()
##%%
