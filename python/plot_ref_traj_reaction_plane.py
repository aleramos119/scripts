
#%%
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt
from itertools import islice
import os
import sys
import scipy.linalg as sla
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import rmsd
%matplotlib qt
plt.style.use(['ggplot'])

def main (ref_traj_inp_file,reaction_path_inp_file,sat,n_level):

    ##%%
    ### Extracting data from name
    name_list=ref_traj_inp_file.split("/")[-1].split("_")
    project_name="_".join(name_list[:-2])

    pdf_file=project_name+".pdf"
    ener_file=project_name+".ener"

    inx=name_list.index("nx")


    x_min=float(name_list[inx+1])
    x_max=float(name_list[inx+2])
    nx=int(name_list[inx+3])

    iny=name_list.index("ny")
    y_min=float(name_list[iny+1])
    y_max=float(name_list[iny+2])
    ny=int(name_list[iny+3])


    ##%%
    ## Reading trajectory energy for reaction plane
    e,r_irp,atm_names=mol.e_r_atm_names_from_xyz (reaction_path_inp_file)


    e_scan,r_scan,atm_names=mol.e_r_atm_names_from_xyz (ref_traj_inp_file)
    emin=min(e_scan)
    e_scan=e_scan-emin
    e_scan=e_scan*mol.Eh2kcalmol

    for i,x in enumerate(e_scan):
        if x > sat:
            e_scan[i]=sat

    # x=np.linspace(x_min, x_max, num=nx)
    # y=np.linspace(y_min, y_max, num=ny)
    # e_x_y=np.array(np.split(e,nx))





    r_scan=mol.remove_trans_rot(r_scan,r_irp[0])
    XSCAN=mol.flat(mol.cartesian2mass_weighted(r_scan,atm_names))


    ##%%
    ## Reading trajectory for statistic
    e,r_stat,atm_names=mol.e_r_atm_names_from_xyz (stat_traj_file)


    r_stat=mol.remove_trans_rot(r_stat,r_irp[0])
    XSTAT=mol.flat(mol.cartesian2mass_weighted(r_stat,atm_names))

    r_stat_rmsd=np.array([ rmsd.rmsd(r,r_irp[0]) for r in r_stat])


    rmsd.rmsd(r_irp[-1],r_irp[0])

    ##%%
    plt.plot(np.array(range(len(r_stat_rmsd)))*0.0005,r_stat_rmsd)
    plt.xlabel("t(ps)")
    plt.ylabel("rmsd (A)")
    ##%%


    ##%%
    ## Reading reaction path
    XR,XTS,XL,XIRP,e_irp,its,r_irp,atm_names=mol.XIRP_from_irp(reaction_path_inp_file)

    mol.write_trajectory(r_scan,np.array(range(len(r_scan))),atm_names,"scan.xyz")
    mol.write_trajectory(r_irp,np.array(range(len(r_irp))),atm_names,"irp.xyz")
    mol.write_trajectory(r_stat,np.array(range(len(r_stat))),atm_names,"stat.xyz")

    d1,d2,P1,P2,P12,I_P12=mol.d1_d2_P1_P2_P12_I_P12_from_irp(XR,XTS,XL)


    ## The projected reaction path
    XIRP_proj_2=np.array([ np.matmul(P12,x-XTS) + XTS for x in XIRP])
    D_XIRP_proj=XIRP-XIRP_proj_2

    d1_coord=[np.dot(d1.T,X-XTS)[0,0] for X in XIRP ]
    d2_coord=[np.dot(d2.T,X-XTS)[0,0] for X in XIRP ]

    d1_scan=[np.dot(d1.T,X-XTS)[0,0] for X in XSCAN ]
    d2_scan=[np.dot(d2.T,X-XTS)[0,0] for X in XSCAN ]

    d1_stat=[np.dot(d1.T,X-XTS)[0,0] for X in XSTAT ]
    d2_stat=[np.dot(d2.T,X-XTS)[0,0] for X in XSTAT ]
    ##%%


    ##%%
    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    plt.axis([x_min, x_max, y_min,y_max])
    plt.tricontourf(d1_scan,d2_scan,e_scan,n_level,cmap='rainbow',alpha=.55,vmin=0, vmax=sat)
    cbar=plt.colorbar()
    cbar.ax.set_title('kcal/mol')
    #C=ax.tricontourf(d1_scan,d2_scan,e_scan,n_level)
    ax.plot(d1_stat,d2_stat,'bo',markersize=0.2)
    ax.plot(d1_coord,d2_coord,'ro')
    ax.plot([d1_coord[i] for i in [0,its,-1]],[d2_coord[i] for i in [0,its,-1]],'bo')
    plt.text(d1_coord[0], d2_coord[0], "x_reac", horizontalalignment='right', size='large',color='black')
    plt.text(d1_coord[its], d2_coord[its], "x_ts", horizontalalignment='right', size='large',color='black')
    plt.text(d1_coord[-1], d2_coord[-1], "x_prod", horizontalalignment='right', size='large',color='black')
    #ax.clabel(C, inline=1, fontsize=10)
    plt.xlabel("d1",fontweight='bold')
    plt.ylabel("d2",fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    fig.savefig(pdf_file, bbox_inches='tight')
    ##%%



    ##%%
    h,px,py=np.histogram2d(d1_coord,d2_coord,bins=20,density=True)

    plt.style.use(['ggplot'])
    fig=plt.figure(figsize=(6,9))
    ax=plt.subplot(111)
    plt.contourf(px[:-1],py[:-1],h.T,10,cmap='rainbow_r',alpha=.55)
    cbar=plt.colorbar()
    cbar.ax.set_title('kcal/mol')
    C=ax.contour(px[:-1],py[:-1],h.T,10, colors='black', linewidths=0.6)
    ax.clabel(C, inline=1, fontsize=10)
    plt.xlabel("d1",fontweight='bold')
    plt.ylabel("d2",fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    ##%%




###########################################################################%%


if __name__== "__main__":

    ##%%
    reaction_path_inp_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/pctts1_irc.xyz"


    #b3lyp
    ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/reac_plane_pctts1_irc_nx_-6_12_60_ny_-4_12_60_b3lyp_eps_10e-6_ref_traj_reduced/reac_plane_pctts1_irc_nx_-6_12_60_ny_-4_12_60_b3lyp_eps_10e-6_ref_traj-pos-1_000_ordered.xyz"
    #dftb
    ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/pctts1_nx_-10_10_60_ny_-4_14_60_dftb_eps_10e-6_ref_traj_reduced/pctts1_nx_-10_10_60_ny_-4_14_60_dftb_eps_10e-6_ref_traj-pos-1.xyz"


    #Unconstrained trajectory
    stat_traj_file="/home/ar612/Documents/Work/cp2k_organized/ir_spec/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec_reduced/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec-pos-1.xyz"
    #Constrained trajectory
    stat_traj_file="/home/ar612/Documents/Work/cp2k_organized/ir_spec/0pctts1irc_1_thf_196_300K_30A_ir_spec_reduced/0pctts1irc_1_thf_196_300K_30A_ir_spec-pos-1.xyz"

    ref_traj_inp_file=sys.argv[1]
    traj_name=sys.argv[2]

    #reaction_path_inp_file=reaction_path_inp_file+traj_name



    sat=80
    n_level=70
    ##%%


    main(ref_traj_inp_file,reaction_path_inp_file,sat,n_level)
