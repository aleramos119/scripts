
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
from itertools import islice



#########################################################################%%
def plot_e(dist1,e1,xlabel):
    plt.plot(dist1,e1,'b-o',linewidth=2.5,label="b3lyp",color="tab:blue")
    #plt.plot(dist2,e2,'b-o',linewidth=2.5,label="dftb",color="tab:red")
    #plt.plot(dist3,e3,'bo',label="ock.",color="tab:green")
    plt.xlabel(xlabel,fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.ylabel('E (kcal/mol)',fontweight='bold')
    #plt.legend()
    plt.grid(True)
    plt.show()


############################################################################%%


def main (inp_file,job):
    #inp_file="/home/ar612/Documents/Work/cp2k_organized/geo_opt/2phenyl_b3lyp_geo_opt_reduced/2phenyl_b3lyp_geo_opt-pos-1.xyz"


    #e2,r2,atm_names2=mol.e_r_atm_names_from_xyz(xyz_inp_file2,n_atm)
    e1,r1,atm_names1,xyz_time,xyz_indx=mol.e_r_atm_names_from_xyz(inp_file)



    ## Expresing energies in kcal/mol and using the first image as reference
    e1=(e1-e1[0])*mol.Eh2kcalmol
    #e2=(e2-e2[0])*mol.Eh2kcalmol

    if job=="ener":
        dist1=np.array(range(0,len(e1)))
        xlabel="step"
    elif job=="reac_path":
        dr1,dist1=mol.path_dist(r1)
        xlabel="reac_path (A)"
    else:
        print("*********** ERROR *************   ener.py: invalid job")


    plot_e(dist1,e1,xlabel)



###########################################################################%%


if __name__== "__main__":


    # for i in range(10,40):
    #     xyz_inp_file_pattern="/home/ar612/Documents/cp2k_organized/reac_path/"+project1+"_reduced/"+project2+"-pos-Replica_nr_$$-1.xyz"
    #     xyz_inp_file=xyz_inp_file_pattern.replace("$$",str(i))
    #     main(xyz_inp_file)

    # inp_file="/data/ar612/cp2k_organized/geo_opt/pheny_cyclo_trimer_dftb_geo_opt/pheny_cyclo_trimer_dftb_geo_opt-pos-1.xyz"
    # job="ener"

    job=sys.argv[1]
    inp_file=sys.argv[2]


    main(inp_file,job)
