##%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import cycle
import numpy as np
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
from scipy.linalg import eigh
import scipy.sparse.linalg as sl
import scipy.sparse as spr
from scipy.integrate import simps
from scipy import interpolate
from functools import partial
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
import time
from skcuda import linalg
%load_ext Cython
##%%

####%%
%%cython

# cython: boundscheck=False
# cython: cdivision=True
# cython: wraparound=False
import numpy as np
cimport numpy as np

cpdef H_cont(double L, double [:] V, double m):
    """Compute the Hamiltonian NxN matrix in atomic units (hbar = 1; m = 1)
       according to Tannor's book, sec. 11.6.4

    Parameters
    ----------
    L : number
        Length of x-interval.
    V : array
        Sampled potential.

    Returns
    -------
    Hij : array
        Hamiltonian.
    """


    cdef int N = len(V)
    cdef np.ndarray[dtype=double, ndim=2] Hij = np.zeros([N, N])
    cdef double K, pi = np.pi
    cdef int i, j

    K = pi/(L/N)  # pi/dx

    for i in range(N):
        for j in range(i+1):
            if i == j:
                Hij[i, j] = 0.5*K**2/(3.*m) + V[i]
            else:
                Hij[i, j] = K**2* (-1.)**(j-i)/(m*pi**2*(j-i)**2 )
                Hij[j, i] = Hij[i, j]  # use Hermitian symmetry

    return Hij
#%%

##%%
def V_double(x,vmax,a):
    return vmax*((x/a)**2-1)**2

def V_harm(x,vmax,a):
    k=8*vmax*a**-2
    return (0.5*k)*x**2

def w(vmax,a,m,conv=1):
    return np.sqrt(8*vmax/(a**2*m))*conv
##%%

def plot_eigen_states(V,m,x0,xf,n_points,Emax,project,out_dir):

    ## Creating the potential list

    ##%%
    x_au=np.linspace(x0,xf,n_points,endpoint=False)
    L_au=xf-x0
    V_au=np.array([V(xi) for xi in x_au])
    m=mh*5
    ##%%
    plt.plot(V_au)
    ## Diagonalizing the Hamiltonian scipy.eigh
    ##%%
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    dt_H = time.time() - ti
    e_au,psi=eigh(Hij,lower=False)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%
    I=np.identity(len(V_au))
    inver=np.linalg.inv(Hij-e_au[0]*I)
    psi00=psi[:,0]
    res1=np.dot(Hij-e_au[0]*I,psi0)
    for i in range(200):
        psi0=np.dot(inver,psi0)
        psi0=psi0/np.linalg.norm(psi0)

    res2=np.dot(Hij-e_au[0]*I,psi0)

    plt.plot(res1,label="res1")
    plt.plot(res2,label="res2")
    plt.legend()
    ##%%
    plt.plot(psi0)
    ## Diagonalizing the Hamiltonian scipy.sparse.eigh
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)*1e+10
    Hij_csc = spr.csc_matrix(np.array(Hij))
    dt_H = time.time() - ti
    e_spar,psi_spar = sl.eigsh(Hij_csc, 2, sigma=0, which='LM',tol=1e-6)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    ## Diagonalizing the sympy
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    Hij_sympy = Matrix(Hij)
    dt_H = time.time() - ti
    psi_sympy=Hij_sympy.eigenvects()
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    ## Diagonalizing the Hamiltonian skcuda
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    Hij_gpu = gpuarray.to_gpu(np.array(Hij))
    dt_H = time.time() - ti
    psi_gpu, e_gpu = linalg.eig(Hij_gpu)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    e_au[:2]
    e_spar
    e_gpu[:2]
    ##%%
    plt.plot(x_au,psi_gpu.get()[0,:],label="gpu")
    plt.plot(x_au,psi[:,0],'o',label="cpu")
    plt.plot(x_au,psi_spar[:,0],'o',label="spar")
    plt.legend()
    ##%%

    conv=np.array([[1,1],[mol.au2A,mol.Eh2kcalmol],[mol.au2A,mol.Eh2cm_1]])
    conv_name=[["a0","Eh"],["A","kcal"],["A","cm-1"]]

    for j in range(len(conv)):

        x_conv=conv[j][0]
        e_conv=conv[j][1]

        e=e_au*e_conv
        V=V_au*e_conv
        x=x_au*x_conv
        psiT=psi.T

        fig=plt.figure(figsize=(16, 9))
        for i,ei in enumerate(e[:Emax]):
            plt.plot(x,-psiT[i]*(e[2]-e[1])*10+ei,label="E_"+str(i)+": "+str(ei),linewidth=2.5)
        plt.plot(x,V,label="V(x)",linewidth=2.5)
        plt.tick_params(labelsize=14)
        plt.xlabel('x ('+conv_name[j][0]+')',fontweight='bold')
        plt.ylabel('V ('+conv_name[j][1]+')',fontweight='bold')
        plt.xlim(x[0],x[-1])
        plt.ylim(-0.5*vmax*e_conv, 3*vmax*e_conv)
        plt.legend(loc=1)
        fig.savefig(out_dir+"/"+project+"_pot_eigenstates_"+conv_name[j][0]+"_"+conv_name[j][1]+".pdf", bbox_inches='tight')

        mol.write_matrix_to_file(e," E (" + conv_name[j][1] + ")"," ",out_dir+"/"+project+"_eigenenergies_"+conv_name[j][1]+".txt")
        mol.write_matrix_to_file(np.hstack((np.array([x]).T,psi))," "," ", out_dir+"/"+project+"_eigenstates_"+conv_name[j][0]+".txt")
##%%


def main(project_pattern,out_dir_pattern,n_points,V,x0,xf,mh,i0,i1,Emax):

    for i in range(i0,i1+1):

        project=project_pattern.replace("$$",str(i))
        out_dir=out_dir_pattern.replace("$$",str(i))
        m=mh*i

        plot_eigen_states(V,m,x0,xf,n_points,Emax,project,out_dir)


##%%

if __name__== "__main__":

    ##%%
    project_pattern="$$_hmass"
    out_dir_pattern=os.environ['QUANTICS_HOME']+"/control/double_well/eigen_states/"+project_pattern

    n_points=2**11

    a=2.0
    vmax=0.01
    V = partial(V_double, vmax=vmax, a=a)
    x0=-4
    xf=4

    mh=1837.18
    i0=5
    i1=5

    Emax=20


    ##%%


    main(project_pattern,out_dir_pattern,n_points,V,x0,xf,mh,i0,i1,Emax)
