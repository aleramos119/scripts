#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np


###################################### FES P ########################################%%

#mol_list=["aptreac1","aptprod1","aptreac2","aptts1","aptts2_irc","aptts1_irc"]
mol_list=["pcmreac1dftb","pcmts1dftb","pcmprod1dftb"]


for molecule in mol_list:

    mol1="/home/ar612/Documents/Work/cp2k_organized/xyz/$$.xyz".replace("$$",molecule)
    mol_out="/home/ar612/Documents/Work/cp2k_organized/xyz//$$_reorder.xyz".replace("$$",molecule)

    # mol1="/home/ar612/Documents/Work/cp2k_organized/xyz/pctts2dftb.xyz"
    # mol_out="/home/ar612/Documents/Work/cp2k_organized/xyz/pctts2dftb_order.xyz"

    e,r,atm_names=mol.e_r_atm_names_from_xyz(mol1)

    ref_order_phenyl=[0,1,2,3,4,5,6,7,8,9,10,11,12,13]
    natm_phenyl=len(ref_order_phenyl)
    ref_order_cyclo=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,14,16,18,17]
    natm_cyclo=len(ref_order_cyclo)


    # ref_order=ref_order_phenyl+[i+natm_phenyl for i in ref_order_cyclo]+[i+natm_phenyl+natm_cyclo for i in ref_order_cyclo]+[i+natm_phenyl+2*natm_cyclo for i in ref_order_cyclo]
    # #1phenyl3cyclo
    # #mol1_order=[0,8,1,9,10,32,12,28,14,29,13,31,11,30,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18]
    # #1phenyl6cyclo
    # #mol1_order=[0,8,1,9,10,32,12,28,14,29,13,31,11,30,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18,98,99,95,94,91,127,121,124,126,125,90,123,122,93,96,92,97,100,101,117,118,114,113,108,112,107,105,102,103,106,109,104,111,115,110,116,119,120,89,88,71,77,72,78,79,74,82,83,76,87,86,75,85,84,73,81,80]
    # #24pctreac1
    # mol1_order=[0,8,1,9,11,30,13,31,14,29,12,28,10,32,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18]


    ref_order=ref_order_phenyl+[i+natm_phenyl for i in ref_order_cyclo]
    mol1_order=[0,8,1,9,10,32,12,28,14,29,13,31,11,30,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18]



    nsteps=len(r)
    natm=len(r[0])

    final_r=np.zeros([nsteps,natm,3])
    final_atm_names=[" " for i in range(natm)]

    # ref_order.index(5)
    # mol1_order

    for i in range(nsteps):
        for j in range(natm):
            ordered_index=mol1_order[ref_order.index(j)]
            final_r[i,j]=r[i,ordered_index]
            final_atm_names[j]=atm_names[ordered_index]


    mol.write_trajectory(final_r,range(len(final_r)),final_atm_names,mol_out)
