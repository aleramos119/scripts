
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import glob
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
import subprocess

x=np.linspace(0,np.pi,50)

y=np.sin(x)**4

plt.plot(x,y,'o')
2*np.pi/(2150-1426)
T_fs=25
T_au=T_fs*mol.fs2au
w_au=2*np.pi/T_au
w_au
100/T_fs
#
# def read_fchk(fchk_file):
#
#     mass_0=0
#     mass_f=0
#     hess_0=0
#     hess_f=0
#
#     with open(fchk_file) as file:
#         ## Initializing arrays
#         for n, line in enumerate(file, 1):
#            if 'Number of atoms' in line:
#                n_atm=int(line.split()[-1])
#                n_dof=3*n_atm
#            if 'Real atomic weights' in line:
#                mass_list=np.zeros([0])
#                mass_0=n
#                n_mass_lines=int(np.ceil(n_atm/5.))
#                mass_f=n+n_mass_lines
#            if n > mass_0 and n <= mass_f:
#                mass_list=np.append(mass_list,np.array( [  float(number) for number in line.split()] ))*mol.amu2au_mass
#
#            if 'Cartesian Force Constants' in line:
#                n_fc=int(line.split()[-1])
#                fc_list=np.zeros([0])
#                hess_0=n
#                n_fc_lines=int(np.ceil(n_fc/5.))
#                hess_f=n+n_fc_lines
#            if n > hess_0 and n <= hess_f:
#                fc_list=np.append(fc_list,np.array( [  float(number) for number in line.split()] ))
#
#     hessian=np.zeros([n_dof,n_dof])
#     hessian_mass=np.zeros([n_dof,n_dof])
#
#
#
#     index=0
#     for i in range(n_dof):
#         for j in range(i+1):
#             hessian[i,j]=fc_list[index]
#             hessian[j,i]=hessian[i,j]
#
#
#             hessian_mass[i,j]=hessian[i,j]/np.sqrt(mass_list[int(np.floor(i/3))]*mass_list[int(np.floor(j/3))])
#             hessian_mass[j,i]=hessian_mass[i,j]
#             index=index+1
#
#
#
#     w,v=linalg.eigh(hessian_mass)
#
#     np.sqrt(np.sort(w))*mol.Eh2cm_1


def get_name_data(name):

    name_list=name.split("/")[-2].split("_")
    project_dir="/".join(name.split("/")[:-2])

    project_name="_".join(name_list[:-2])

    inx=name_list.index("nx")
    x_min=float(name_list[inx+1])
    x_max=float(name_list[inx+2])
    nx=int(name_list[inx+3])

    iny=name_list.index("ny")
    y_min=float(name_list[iny+1])
    y_max=float(name_list[iny+2])
    ny=int(name_list[iny+3])

    iemax=name_list.index("emax")
    emax=float(name_list[iemax+1])

    return project_dir,project_name,x_min,x_max,nx,y_min,y_max,ny,emax


def projected_out_normal_modes(modes_inp_file,d1,d2,n_cutoff=8):

    ener,w,redm,fcnstmDA,intes,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(modes_inp_file)
    w=w*mol.cm_12Eh

    P1=np.matmul(d1,d1.T)
    P2=np.matmul(d2,d2.T)
    I=np.identity(len(P2))
    P12=P1+P2    ##The projector in MWC representation
    I_P12=I-P12

    l=mol.freq2eigenval(w)
    n_vib=len(w)
    n_dim=len(q_mwc[:,0])
    K=np.matmul(q_mwc,np.matmul(np.diag(l),q_mwc.T)) ##The hessian in MWC representation

    n_cutoff=8

    K_P12=np.matmul(I_P12,np.matmul(K,I_P12))
    l_P12,Q_P12_full=np.linalg.eigh(K_P12)
    n0=[x[1] for x in sorted([(x,i) for (i,x) in enumerate(np.sqrt(abs(l_P12)))])[n_cutoff:] ] ## These are the indexes of the normal modes with  not vanishing eigenvalues
    n0=sorted(n0)

    ### These are the normal modes without rotatations, translations and d1 and d2, with their respective frequencies
    Q_P12=np.zeros([n_dim,len(n0)])
    for i in range(n_dim):
        for j in range(n_vib-2):
            Q_P12[i,j]=Q_P12_full[i,n0[j]]
    w_P12_au=np.array([ np.sqrt(np.abs(l_P12[i]))  for i in n0])


    ##%%
    ### Writing the d1 d2 and normal modes to file
    d1_d2_Q_P12_T=np.zeros([len(n0)+2,n_dim])
    d1_d2_Q_P12_T[0]=d1[:,0]
    d1_d2_Q_P12_T[1]=d2[:,0]
    for i in range(n_vib-2):
        d1_d2_Q_P12_T[i+2]=Q_P12[:,i]

    d1_d2_Q_P12_cart_T=mol.mass_weighted2cartesian(mol.no_flat(d1_d2_Q_P12_T),atm_names)

    project_dir="/".join(modes_inp_file.split("/")[:-1])
    mol.write_modes_2_nmd(project_dir+"/d1_d2_Q_P12.nmd",atm_names,r,d1_d2_Q_P12_cart_T)
    ##%%

    return Q_P12,w_P12_au



def plot_reac_path_projection(XIRP,XR,q_mwc_reac,cuttof):

    n_vib=len(q_mwc_reac[0])
    n_path=len(XIRP)

    DYk=np.zeros([n_vib,n_path])
    for i in range(n_vib):
        for j in range(n_path):
            DYk[i,j]=np.linalg.norm(np.matmul((XIRP-XR)[j].T,np.array([q_mwc_reac[:,i]]).T))*(mol.A2au)

    ##%%
    n0=[]
    for i in range(n_vib):
        if max(DYk[i]) > cuttof:
            plt.plot(DYk[i],"o",label=str(i+1))
            n0.append(i)
    plt.xlabel("reac_path")
    plt.ylabel(r"$\Delta Y_k (s)$")
    plt.legend()
    ##%%

    return n0


def plot_reac_plane_projection(P12,q_mwc_reac):

    n_vib=len(q_mwc_reac[0])

    DYk=np.zeros([n_vib])
    for i in range(n_vib):
        DYk[i]=np.linalg.norm(np.matmul(P12,np.array([q_mwc_reac[:,i]]).T))

    ##%%
    plt.bar(range(1,n_vib+1),DYk)
    plt.xlabel("mode")
    plt.ylabel(r"$reac_plane_projection$")
    ##%%



def complete_traj(x_list,y_list,d1_coord,d2_coord,traj_0,sat_scale=2):

    nx=len(x_list)
    ny=len(y_list)
    n_steps=len(traj_0)

    ## Fist I set the minimum to zero and define from there the saturation, that will be the default value for the matrix
    min_traj=min(traj_0)
    traj=traj_0-min_traj
    sat=max(traj)*sat_scale

    ## Then I define what it means to be close to the grid points
    err=0.1
    dx=(x_list[1]-x_list[0])*err
    dy=(y_list[1]-y_list[0])*err

    ## This is the matrix where I storage the potential values corresponding to the grid x_list,y_list
    mat=np.full([nx,ny],sat)

    ## This is the list of the values i,j, j*nx+i corresponding to the trajectory. It can also be used i*ny+j if the j index is the one that runs faster
    i_j_list=np.zeros([n_steps,3])

    ## I scan the trajectory and asigne the values to its corresponding place in the matrix
    for step in range(n_steps):
        i=np.where(abs(x_list-(d1_coord[step]))<dx)[0][0]
        j=np.where(abs(y_list-(d2_coord[step]))<dy)[0][0]
        mat[i,j]=traj[step]
        i_j_list[step,0]=i
        i_j_list[step,1]=j
        i_j_list[step,2]=int(j*nx+i)

    ## These are the corresponding list values from the matrix
    xxx=np.zeros(nx*ny)
    yyy=np.zeros(nx*ny)
    eee=np.zeros(nx*ny)

    for j,y in enumerate(y_list):
        for i,x in enumerate(x_list):
            xxx[j*nx+i]=x
            yyy[j*nx+i]=y
            eee[j*nx+i]=mat[i,j]

    return xxx,yyy,eee,i_j_list

def plot_traj_2d(xxx,yyy,eee):
    x_min=min(xxx)
    x_max=max(xxx)
    y_min=min(yyy)
    y_max=max(yyy)
    e_min=min(eee)
    e_max=max(eee)
    ##%%
    n_level=30
    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    plt.axis([x_min, x_max, y_min,y_max])
    plt.tricontourf(xxx,yyy,eee,n_level,cmap='rainbow',alpha=.55,vmin=e_min, vmax=e_max)
    cbar=plt.colorbar()
    C=ax.tricontour(xxx,yyy,eee,n_level,n_level, linewidths=0.3,colors='black')
    ax.clabel(C, inline=1, fontsize=10)
    plt.plot(xxx,yyy,'.')
    plt.xlabel("x",fontweight='bold')
    plt.ylabel("y",fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    #%%

def second_largest(numbers):
    minimum = float('-inf')
    first, second = minimum, minimum
    for n in numbers:
        if n > first:
            first, second = n, first
        elif first > n > second:
            second = n
    return second if second != minimum else None


def replace_pattern_file(inp_file,out_file,patterns,replaces):
    #input file
    fin = open(inp_file, "rt")
    #output file to write the result to
    fout = open(out_file, "wt")
    #for each line in the input file
    for line in fin:
        linef=line
        for i in range(len(patterns)):
            linef=linef.replace(patterns[i] , replaces[i])
        fout.write(linef)
    #close input and output files
    fin.close()
    fout.close()

def create_dir(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)


def write_complete_traj(eee,x_list,y_list,d1_coord,d2_coord,potfit_temp,chnpot_temp,dir_0,name):

    x_min=min(d1_coord)
    x_max=max(d1_coord)
    y_min=min(d2_coord)
    y_max=max(d2_coord)
    e_min=min(eee)
    e_max=second_largest(eee)
    nx=len(x_list)
    ny=len(y_list)

    dir=dir_0+"/"+name
    create_dir(dir)
    mol.write_matrix_to_file(np.array([x_list]).T,'','',dir+'/x_list.txt')
    mol.write_matrix_to_file(np.array([y_list]).T,'','',dir+'/y_list.txt')
    mol.write_matrix_to_file(np.array([eee]).T,'','',dir+'/'+name+'.txt')

    create_dir(dir+"/potfit")
    create_dir(dir+"/chnpot")
    replace_pattern_file(potfit_temp,dir+"/potfit.inp",["%name%","%e_min%","%e_max%","%nx%","%ny%"],[name,str(e_min),str(e_max),str(nx),str(ny)])
    replace_pattern_file(chnpot_temp,dir+"/chnpot.inp",["%x_min%","%x_max%","%y_min%","%y_max%"],[str(x_min),str(x_max),str(y_min),str(y_max)])

    potfit_command="potfit85 -w potfit.inp"
    chnpot_command="chnpot85 -w chnpot.inp"

    process = subprocess.run(potfit_command.split(), stdout=subprocess.PIPE,cwd=dir)
    process = subprocess.run(chnpot_command.split(), stdout=subprocess.PIPE,cwd=dir)



def main (n_steps,file_pattern):

    ##%%
    ### Reading reaction path
    XR,XTS,XL,XIRP,e_irp,its,atm_names=mol.XIRP_from_irp(reaction_path_inp_file)
    n_path=len(XIRP)
    n_atm=int(len(XR)/3)
    n_dim=len(XR)

    d1,d2,P1,P2,P12,I_P12=mol.d1_d2_P1_P2_P12_I_P12_from_irp(XR,XTS,XL)

    project_dir,project_name,x_min,x_max,nx,y_min,y_max,ny,emax=get_name_data(file_pattern)
    x_list=np.linspace(x_min, x_max, num=nx)
    y_list=np.linspace(y_min, y_max, num=ny)


    ### The projected reaction path and the differenece
    XIRP_proj_2=np.array([ np.matmul(P12,x-XTS) + XTS for x in XIRP])
    D_XIRP_proj=XIRP-XIRP_proj_2

    ## Computing projected and reaction path in cartesian coordinates
    xirp=mol.flat(mol.mass_weighted2cartesian(mol.no_flat(XIRP)[:,:,:,0],atm_names))
    xirp_proj_2=mol.flat(mol.mass_weighted2cartesian(mol.no_flat(XIRP_proj_2)[:,:,:,0],atm_names))



    ## Computing the deviation
    ##%%
    sigma_2=np.array([    (1/np.sqrt(n_atm))*np.sqrt(np.dot((xirp[i]-xirp_proj_2[i])[:,0],(xirp[i]-xirp_proj_2[i])[:,0]))    for i in range(0,len(XIRP))])
    dr,dist=mol.path_dist(mol.mass_weighted2cartesian(mol.no_flat(XIRP)[:,:,:,0],atm_names))
    plt.plot(dist,sigma_2,"o",label="2D")
    plt.xlabel("Reac_coord (A)")
    plt.ylabel(r'$\sigma (A)$')


    ##%%
    ## These are the projected out normal modes
    Q_P12,w_P12_au=projected_out_normal_modes(ts_modes_inp_file,d1,d2,n_cutoff=8)
    n_proj_out=len(Q_P12[0])
    d_P12_au=np.array([ np.sqrt(1/w)  for w in w_P12_au])



    ## Reading reactant normal modes and ploting the projection into the reaction path
    ener_reac,w_reac,redm_reac,fcnstmDA_reac,intes_reac,q_cart_reac,q_mwc_reac,f_au_reac,dip_au_reac,dip_der_au_reac,r_reac,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(reac_modes_inp_file)
    n_vib=len(w_reac)
    reac_nm_cutoff=0.5 ### This is the cutoff to select reac normal modes

    #These are the normal modes indexes that I consider important
    jl=plot_reac_path_projection(XIRP,XR,q_mwc_reac,reac_nm_cutoff)
    plot_reac_plane_projection(P12,q_mwc_reac)
    ##%%


    ### Y_jl are the selected normal modes that I consider important
    Y_jl=np.array([q_mwc_reac[:,i] for i in jl])
    ## This is the projection into the transition state projected out normal modes
    pjl_k=np.matmul(Y_jl,Q_P12)

    ## Ploting the projection
    for i in range(len(jl)):
        plt.plot(abs(pjl_k[i]),"o-",label=str(jl[i]))
    plt.xlabel(r"$Q_k$")
    plt.ylabel(r"$Y_{j_l}^{(f)}$")
    plt.legend()
    ##%%



    ##The cuttof to select relevant Q_P12 normal modes
    q_cutoff_list=np.linspace(0.1,0.1,1)
    lenk=[]
    for q_cutoff in q_cutoff_list:
        k=[]
        for i in range(n_proj_out):
            if np.linalg.norm(pjl_k[:,i]) > q_cutoff:
                k.append(i)
        lenk.append(len(k))



    plt.plot(q_cutoff_list,lenk,'o')
    q=np.array([Q_P12[:,i] for i in k]).T
    ## This is the projector into the space defined by the relevant coordinates
    B=[d1[:,0],d2[:,0]]
    for i in range(len(k)):
        B.append(q[:,i])
    B=np.array(B).T

    mol.write_modes_2_nmd("d1_d2_q.nmd",atm_names,mol.mass_weighted2cartesian(mol.no_flat([XTS])[:,:,:,0],atm_names)[0],mol.no_flat(B.T))

    ## This is the projector into the complementary space defined by the relevant coordinates
    B_QP12=[]
    for i in range(n_proj_out):
        if i not in k:
            B_QP12.append(Q_P12[:,i])
    B_QP12=np.array(B_QP12).T
    ##%%


    ## Computing the difference between projected reaction path in terms of normal modes
    DQj_au=np.zeros([n_vib-2,n_path])
    for i in range(n_vib-2):
        for j in range(n_path):
            DQj_au[i,j]=np.linalg.norm(np.matmul(Q_P12[:,i],D_XIRP_proj[j]))*(mol.A2au*np.sqrt(mol.amu2au_mass))


    DQjmax=np.array([ max(q)/d_P12_au[i] for i,q in enumerate(DQj_au)])

    for i in range(len(DQj_au)):
        plt.plot(DQj_au[i]/d_P12_au[i],label=str(i))
    plt.legend()

    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    ax.bar(range(3,n_vib-2+3),DQjmax)
    ax.set_xlabel("Qi")
    ax.set_ylabel(r"$\Delta Q_i^{(max)} / \delta_i $")
    fig.savefig("Fig_5.pdf", bbox_inches='tight')
    ##%%



    ##%%
    #### Defining the lists where I storage the Information needed for the hamiltonian
    Xplane=[]
    d1_coord=[]
    d2_coord=[]
    e_au=[]
    f=[]
    K=[]
    wk=[]
    dip=[]
    dip_der=[]



    ##%%
    ## Reading all the files
    for i in range(n_steps):
        out_file=file_pattern.replace("$$",str(i))
        out_file=glob.glob(out_file)[0]

        ener,w,redm,fcnstmDA,intes,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(out_file)
        l=mol.freq2eigenval(w*mol.cm_12Eh)
        K_full=np.matmul(q_mwc,np.matmul(np.diag(l),q_mwc.T)) ##The hessian in MWC representation

        ## Coordinates
        X=mol.flat(mol.cartesian2mass_weighted(np.array([r]),atm_names))[0]
        Xplane.append(X)
        d1_coord.append(np.dot(d1.T,X-XTS)[0,0])
        d2_coord.append(np.dot(d2.T,X-XTS)[0,0])

        ## Energy
        e_au.append(ener)

        ## forces
        f.append(np.matmul(q.T,f_au_flat))

        ## hessian
        K.append(np.matmul(q.T,np.matmul(K_full,q)))

        ## frequencies
        wk.append(np.matmul(B_QP12.T,np.matmul(K_full,B_QP12)).diagonal())

        ## Dipole
        dip.append(dip_au)
        dip_der.append(np.matmul(q.T,dip_der_au))






    ##Displacing and changing units of the array
    e_au=np.array(e_au)
    emin=min(e_au)
    e_au=e_au-emin
    e_kcal=e_au*mol.Eh2kcalmol
    sum_wk=np.array([sum(i) for i in wk])

    f=np.array(f)
    K=np.array(K)
    dip=np.array(dip)
    dip_der=np.array(dip_der)


    d1_coord_au=np.array(d1_coord)*mol.A2au*np.sqrt(mol.amu2au_mass)
    d2_coord_au=np.array(d2_coord)*mol.A2au*np.sqrt(mol.amu2au_mass)
    x_list_au=x_list*mol.A2au*np.sqrt(mol.amu2au_mass)
    y_list_au=y_list*mol.A2au*np.sqrt(mol.amu2au_mass)



    potfit_temp="/home/ar612/Documents/Work/quantics/potfit/potfit.temp"
    chnpot_temp="/home/ar612/Documents/Work/quantics/potfit/chnpot.temp"
    dir='/home/ar612/Documents/Work/quantics/potfit'

    xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,e_au)
    write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir,"U")

    xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,sum_wk)
    write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir,"wk")

    for i in range(len(f[0])):
        name="f"
        dir_f=dir+"/"+name
        name=name+"_"+str(i)
        xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,f[:,i])
        write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir_f,name)


    for i in range(len(K[0])):
        for j in range(len(K[0])):
            name="k"
            dir_f=dir+"/"+name
            name=name+"_"+str(i)+"_"+str(j)
            xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,K[:,i,j])
            write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir_f,name)



    xyz_list=["x","y","z"]
    for i,xyz in enumerate(xyz_list):
        name="mu"
        dir_f=dir+"/"+name
        name=name+"_"+xyz
        xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,dip[:,i],5)
        write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir_f,name)


    for i in range(len(K[0])):
        for j,xyz in enumerate(xyz_list):
            name="dmu_dq"
            dir_f=dir+"/"+name
            name=name+"_"+str(i)+"_"+str(j)
            xxx,yyy,eee,ij_list=complete_traj(x_list_au,y_list_au,d1_coord_au,d2_coord_au,dip_der[:,i,j])
            write_complete_traj(eee,x_list_au,y_list_au,d1_coord_au,d2_coord_au,potfit_temp,chnpot_temp,dir_f,name)

    ## KE matrix
    for i in range(12+2):
        op_list=list(np.full([12+3],"  1  "))
        op_list[i]=" KE  "
        op_list.insert(0,"1.0                 ")
        print('|'.join(op_list))


    ## Force matrix
    f_path_temp="f_$$ = natpot{potential/f/f_$$/chnpot}"
    for i in range(12):
        print(f_path_temp.replace("$$",str(i)))

    for i in range(12):
        op_list=list(np.full([12+3],"  1  "))
        op_list[0]="& f_"+str(i)+"    "
        dif=11-len(op_list[0])
        for ddd in range(dif):
            op_list[0]=op_list[0]+" "
        del op_list[1]
        op_list[i+1]="  q  "
        op_list.insert(0,"1.0                 ")
        print('|'.join(op_list))

    ## hesian matrix
    f_path_temp="k_$$_%% = natpot{potential/k/k_$$_%%/chnpot}"
    for i in range(12):
        for j in range(12):
            print(f_path_temp.replace("$$",str(i)).replace("%%",str(j)))

    for i in range(12):
        for j in range(12):
            op_list=list(np.full([12+3],"  1  "))
            op_list[0]="& k_"+str(i)+"_"+str(j)
            dif=11-len(op_list[0])
            for ddd in range(dif):
                op_list[0]=op_list[0]+" "
            del op_list[1]
            if i==j:
                op_list[i+1]=" q^2 "
            else:
                op_list[i+1]="  q  "
                op_list[j+1]="  q  "
            op_list.insert(0,"0.5                 ")
            print('|'.join(op_list))

    ## The primitive basis sections
    basis_pat="q$$   gauss   0.0   0.0d0   %%"
    for i in range(12):
        print(basis_pat.replace("$$",str(i)).replace("%%",str(d_P12_au[k[i]])))

    ## The primitive basis sections
    basis_pat="q$$   sin   150   -%%   %%  short"
    for i in range(12):
        print(basis_pat.replace("$$",str(i)).replace("%%",str(8*d_P12_au[k[i]])))


    basis_pat="q$$  = 1"
    for i in range(12):
        print(basis_pat.replace("$$",str(i)).replace("%%",str(8*d_P12_au[k[i]])))


    5*mol.fs2au
###########################################################################%%


if __name__== "__main__":

    ##%%

    project_name="aptts2_nx_-2_2_50_ny_-1.5_2.5_50_dftb_eps_10e-6_ref_traj-pos-1_emax_80"
    file_pattern="/home/ar612/Documents/Work/gaussian/scan_reac_plane/"+project_name+"_reduced/$$_"+project_name+"/[0-9]*.out"

    reaction_path_inp_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/aptts2_irc.xyz"
    ts_modes_inp_file="/home/ar612/Documents/Work/gaussian/geo_opt/aptts1_reduced/771450.out"
    reac_modes_inp_file="/home/ar612/Documents/Work/gaussian/geo_opt/aptreac1_reduced/771479.out"

    n_steps=1344

    main(n_steps,file_pattern)
