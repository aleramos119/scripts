##%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import cycle
import numpy as np
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
from scipy.linalg import eigh
from functools import partial
#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit
import time
#from skcuda import linalg
##%%


def RayleighQuotientIteration(A,v,mu,max_iter):
  I = np.identity(A.shape[0])
  A=np.float64(A)
  mui=np.empty(max_iter)
  for t in range(max_iter):
    v = np.linalg.solve(A - mu * I, v) #compute (mu*I - A)^(-1)v
    v /= np.linalg.norm(v)
    mui[t]=mu
    mu = np.dot(v, np.dot(A, v)) #compute Rayleigh quotient
  return (v, mui)


def PrecisionRayleighQuotientIteration(A,v0,lam,max_iter,sub_iter):
  I = np.identity(A.shape[0])
  A=np.float64(A)
  mui=np.empty(max_iter)
  mu=0
  v=np.copy(v0)

  for t in range(max_iter):
      if t % sub_iter == 0:
          Ap=(mu+lam)*np.outer(v,v)
      v = np.linalg.solve(A - Ap - mu * I, v) #compute (mu*I - A)^(-1)v
      v /= np.linalg.norm(v)
      mui[t]=mu
      mu = np.dot(v, np.dot(A - Ap, v)) #compute Rayleigh quotient
  return (v, mui)

####%%
def H_cont(L, V, m):
    """Compute the Hamiltonian NxN matrix in atomic units (hbar = 1; m = 1)
       according to Tannor's book, sec. 11.6.4

    Parameters
    ----------
    L : number
        Length of x-interval.
    V : array
        Sampled potential.

    Returns
    -------
    Hij : array
        Hamiltonian.
    """

    N = len(V)
    Hij = np.zeros([N, N],dtype=np.longdouble)

    K = np.longdouble(np.pi/(L/N))  # pi/dx

    for i in range(N):
        for j in range(i+1):
            if i == j:
                Hij[i, j] = 0.5*K**2/(3.*m) + V[i]
            else:
                Hij[i, j] = K**2* (-1.)**(j-i)/(m*np.pi**2*(j-i)**2 )
                Hij[j, i] = Hij[i, j]  # use Hermitian symmetry

    return Hij
#%%

##%%
def V_double(x,vmax,a):
    return vmax*((x/a)**2-1)**2

def V_harm(x,vmax,a):
    k=8*vmax*a**-2
    return (0.5*k)*x**2

def w(vmax,a,m,conv=1):
    return np.sqrt(8*vmax/(a**2*m))*conv
##%%

def plot_eigen_states(V,m,x0,xf,n_points,Emax,project,out_dir):

    ## Creating the potential list

    ##%%
    x_au=np.linspace(x0,xf,n_points,endpoint=False,dtype=np.longdouble)
    L_au=xf-x0
    V_au=np.array([V(xi) for xi in x_au],dtype=np.longdouble)
    m=mh*5
    ##%%
    len(x_au)
    ##%%
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    dt_H = time.time() - ti
    e_au,psi=eigh(Hij)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%
    Hij[1]
    np.dot(psi[:,1],psi[:,1])
    e_au[1]-e_au[0]
    e_au[1]
    max_iter=200
    e0=np.array([ e_au[0] for i in range(max_iter)])
    e1=np.array([ e_au[1] for i in range(max_iter)])
    #plt.plot(psi[:,0],label="res1")
    v,mui=PrecisionRayleighQuotientIteration(Hij,psi[:,1],e_au[1],max_iter,10)
    #plt.plot(v,label="res2")
    #plt.plot(e0,label="e0")
    #plt.plot(e1,label="e1")
    plt.plot(mui,'o',label="mui")
    plt.legend()
    plt.plot(v)
    plt.plot(psi[:,1])
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)*1e+10
    Hij_csc = spr.csc_matrix(np.array(Hij))
    dt_H = time.time() - ti
    e_spar,psi_spar = sl.eigsh(Hij_csc, 2, sigma=0, which='LM',tol=1e-6)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    ## Diagonalizing the sympy
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    Hij_sympy = Matrix(Hij)
    dt_H = time.time() - ti
    psi_sympy=Hij_sympy.eigenvects()
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    ## Diagonalizing the Hamiltonian skcuda
    ##%%
    linalg.init()
    ti = time.time()
    Hij=H_cont(L_au,V_au,m)
    Hij_gpu = gpuarray.to_gpu(np.array(Hij))
    dt_H = time.time() - ti
    psi_gpu, e_gpu = linalg.eig(Hij_gpu)
    dt_eig = time.time() - ti - dt_H
    print(dt_H,dt_eig)
    ##%%

    e_au[:2]
    e_spar
    e_gpu[:2]
    ##%%
    plt.plot(x_au,psi_gpu.get()[0,:],label="gpu")
    plt.plot(x_au,psi[:,0],'o',label="cpu")
    plt.plot(x_au,psi_spar[:,0],'o',label="spar")
    plt.legend()
    ##%%

    conv=np.array([[1,1],[mol.au2A,mol.Eh2kcalmol],[mol.au2A,mol.Eh2cm_1]])
    conv_name=[["a0","Eh"],["A","kcal"],["A","cm-1"]]

    for j in range(len(conv)):

        x_conv=conv[j][0]
        e_conv=conv[j][1]

        e=e_au*e_conv
        V=V_au*e_conv
        x=x_au*x_conv
        psiT=psi.T

        fig=plt.figure(figsize=(16, 9))
        for i,ei in enumerate(e[:Emax]):
            plt.plot(x,-psiT[i]*(e[2]-e[1])*10+ei,label="E_"+str(i)+": "+str(ei),linewidth=2.5)
        plt.plot(x,V,label="V(x)",linewidth=2.5)
        plt.tick_params(labelsize=14)
        plt.xlabel('x ('+conv_name[j][0]+')',fontweight='bold')
        plt.ylabel('V ('+conv_name[j][1]+')',fontweight='bold')
        plt.xlim(x[0],x[-1])
        plt.ylim(-0.5*vmax*e_conv, 3*vmax*e_conv)
        plt.legend(loc=1)
        fig.savefig(out_dir+"/"+project+"_pot_eigenstates_"+conv_name[j][0]+"_"+conv_name[j][1]+".pdf", bbox_inches='tight')

        mol.write_matrix_to_file(e," E (" + conv_name[j][1] + ")"," ",out_dir+"/"+project+"_eigenenergies_"+conv_name[j][1]+".txt")
        mol.write_matrix_to_file(np.hstack((np.array([x]).T,psi))," "," ", out_dir+"/"+project+"_eigenstates_"+conv_name[j][0]+".txt")
##%%


def main(project_pattern,out_dir_pattern,n_points,V,x0,xf,mh,i0,i1,Emax):

    for i in range(i0,i1+1):

        project=project_pattern.replace("$$",str(i))
        out_dir=out_dir_pattern.replace("$$",str(i))
        m=mh*i

        plot_eigen_states(V,m,x0,xf,n_points,Emax,project,out_dir)


##%%

if __name__== "__main__":

    ##%%
    project_pattern="$$_hmass"
    out_dir_pattern=os.environ['QUANTICS_HOME']+"/control/double_well/eigen_states/"+project_pattern

    n_points=2**3

    a=np.longdouble(2.0)
    vmax=np.longdouble(0.01)
    V = partial(V_double, vmax=vmax, a=a)
    x0=np.longdouble(-4)
    xf=np.longdouble(4)

    mh=np.longdouble(1837.18)
    i0=5
    i1=5



    Emax=20


    ##%%


    main(project_pattern,out_dir_pattern,n_points,V,x0,xf,mh,i0,i1,Emax)
