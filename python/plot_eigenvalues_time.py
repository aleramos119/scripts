
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import glob
import matplotlib.pyplot as plt
#%matplotlib qt
plt.style.use(['ggplot'])



def read_data_from_inp(inp_file):

    Hdiag=[]
    mudiag=[]

    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if "##eigen_energies_file" in line:
               Hdiag=line.split()[-2]
           if "##dipole_matrix_file" in line:
               mudiag=line.split()[-2]

    return Hdiag,mudiag



def main(H0,mu,t,Et):

    nsteps=len(t)
    nstates=len(H0)
    En_t=np.zeros([nsteps,nstates])

    for i in range(nsteps):
        e,v=np.linalg.eigh(H0-mu*Et[i])
        En_t[i]=e


    mol.write_matrix_to_file(En_t,"","","En_t.dat")

    fig1, ax1 = plt.subplots(1, 1,sharex=True,figsize=(16,9))
    for i in range(nstates):
        ax1.plot(t,En_t[:,i],'-',label="E_"+str(i))
    ax1.legend()
    fig1.savefig("En_t.pdf")
    plt.close("all")






if __name__== "__main__":

    #inp_file="/home/ar612/Documents/Work/irep/control/fermi_res_gamma_0.1_w1_2_w2_1_nx_3_ny_6/decoupling_10_02_intpot_10_intcost_0.1_T_160/decoupling_10_02_intpot_10_intcost_0.1_T_160_0.050/decoupling_10_02_intpot_10_intcost_0.1_T_160_0.050.inp"

    systems_dir="/home/ar612/Documents/Work/quantum_psopt/systems/"

    #inp_file="*.inp"

    inp_file=glob.glob("*inp")[0]
    print(inp_file)

    Hdiag_file,mudiag_file=read_data_from_inp(inp_file)
    Hdiag_file="/".join(Hdiag_file.split("/")[-2:])
    mudiag_file="/".join(mudiag_file.split("/")[-2:])

    Hdiag=np.genfromtxt(systems_dir+Hdiag_file)
    mudiag=np.genfromtxt(systems_dir+mudiag_file)

    t=np.genfromtxt("t.dat")
    Et=np.genfromtxt("u.dat")


    main(Hdiag,mudiag,t,Et)
