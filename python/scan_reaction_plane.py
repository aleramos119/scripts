
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
#import rmsd
%matplotlib qt
plt.style.use(['ggplot'])
############################################################################%%
## Returns the un-flatten position array
def no_flat(r,part=3):

    if len(r[0])%part != 0:
        print("*********** ERROR *************  no_flat: The length of each elemt in r must be devided by part (default=3). All the elements in r must have the same dimensions")
    else:
        return np.array([ np.array(np.array_split(x[:,0],len(x)/part)) for x in r])

def flat(r):
    return np.array([ np.array([x.flatten()]).T for x in r])




def main (modes_inp_file,m1,m2,disp_max):

    ##%%
    ######## Reading cartesian irp ##########
    traj_name="pctts1"
    e_irp,rirp,atm_names=mol.e_r_atm_names_from_xyz(reac_path_file)
    rirp=np.array([x-mol.center_of_mass(np.array([x]),atm_names) for x in rirp])

    its=mol.get_max_index(e_irp)

    rr=rirp[0]
    rts=rirp[its]
    rp=rirp[-1]

    n_atm=len(rr)

    ##%%
    ## Calculating mass weighted structures and transposing to be consistent with the 1 column vector convention
    XR=np.array([mol.cartesian2mass_weighted(np.array([rr]),atm_names)[0].flatten()]).T
    XTS=np.array([mol.cartesian2mass_weighted(np.array([rts]),atm_names)[0].flatten()]).T
    XL=np.array([mol.cartesian2mass_weighted(np.array([rp]),atm_names)[0].flatten()]).T
    XIRP=mol.cartesian2mass_weighted(rirp,atm_names)
    XIRP=flat(XIRP)

    ##%%
    ## Defining the reaction plane directions
    d1=(XR-XL)/np.linalg.norm((XR-XL))
    P1=np.matmul(d1,d1.T)

    d2=((XR-XTS)-np.matmul(P1,(XR-XTS)))/np.linalg.norm((XR-XTS)-np.matmul(P1,(XR-XTS)))
    P2=P1+np.matmul(d2,d2.T)

    d1_cart=mol.mass_weighted2cartesian(no_flat(np.array([d1])),atm_names)[0]
    d2_cart=mol.mass_weighted2cartesian(no_flat(np.array([d2])),atm_names)[0]


    ##%%
    ## Computing the projected reaction path and the difference
    XIRP_proj_2=np.array([ np.matmul(P2,x-XTS)  + XTS for x in XIRP])

    DIFF_2=(XIRP-XIRP_proj_2)*(mol.A2au*np.sqrt(mol.amu2au_mass))

    ## Computing projected and reaction path in cartesian coordinates
    xirp=flat(mol.mass_weighted2cartesian(no_flat(XIRP),atm_names))
    xirp_proj_2=flat(mol.mass_weighted2cartesian(no_flat(XIRP_proj_2),atm_names))



    ## Computing the deviation
    ##%%
    sigma_2=np.array([    (1/np.sqrt(n_atm))*np.sqrt(np.dot((xirp[i]-xirp_proj_2[i])[:,0],(xirp[i]-xirp_proj_2[i])[:,0]))    for i in range(0,len(XIRP))])
    dr,dist=mol.path_dist(rirp)
    plt.plot(dist,sigma_2,"o",label="2D")
    plt.xlabel("Reac_coord (A)")
    plt.ylabel(r'$\sigma (A)$')

    ##%%
    ## Maximum displacement for every direction
    x_max=10
    x_min=-10
    nx=60

    y_max=14
    y_min=-4
    ny=60

    x_list=np.linspace(x_min, x_max, num=nx)
    y_list=np.linspace(y_min, y_max, num=ny)



    ## The first extructure needed for append to work
    X=np.zeros([nx*ny,3*n_atm,1])
    X0=XTS

    ## Escaning through the displacements in both directions
    i=0
    for dx1 in x_list:
        for dx2 in y_list:
            X[i]=X0+dx1*d1+dx2*d2
            i=i+1

    r=mol.mass_weighted2cartesian(no_flat(X),atm_names)+[10,10,10]

    ## Naming the final file
    out_pattern="/home/ar612/Documents/Work/cp2k_organized/ref_traj/trajectory/"+traj_name+"_&&.xyz"
    out_file=out_pattern.replace("&&","nx"+"_"+str(x_min)+"_"+str(x_max)+"_"+str(nx)+"_"+"ny"+"_"+str(y_min)+"_"+str(y_max)+"_"+str(ny))

    ## Writing the trajectory to the file
    mol.write_trajectory (r,np.zeros(len(r)),atm_names,out_file)

    # ##%%
    # ## Reading cartesian structures
    #select = 0  ## Select if performs rotation to the molecule (1) or not (0)
    #er,rr,atm_names=mol.e_r_atm_names_from_xyz("/home/ar612/Documents/cp2k_organized/XYZ/ts.xyz",ener="no")
    # ets,rts,atm_names=mol.e_r_atm_names_from_xyz(rts_xyz_file,ener="no")
    # ep,rp,atm_names=mol.e_r_atm_names_from_xyz(rp_xyz_file,ener="no")
    #e_irp,rirp,atm_names=mol.e_r_atm_names_from_xyz(reac_path_file)
    #rirp=np.array([x-mol.center_of_mass(np.array([x]),atm_names) for x in rirp])
    #
    # ts_w,ts_modes,ts_r,atm_names=mol.w_modes_r_atm_names_from_molden(rts_vib_file)
    #
    # ## Transforming the readed normal modes matrix
    # ts_mass_modes=mol.cartesian2mass_weighted(ts_modes,atm_names) ## Mass weighted normal modes
    # ts_mass_modes=mol.normal(ts_mass_modes)  ## Normal normal modes
    # ts_mass_modes=np.array([x.flatten() for x in ts_mass_modes]).T ## Transposing to be consistent with the 1 column vector convention
    #
    #
    # k=np.array([ w**2 for w in ts_w])
    # hess_w=np.diag(k)
    #
    # hess_cart=np.matmul(ts_mass_modes,np.matmul(hess_w,ts_mass_modes.T))
    #
    #
    #
    #rr=rirp[0]
    #rpl=rirp[38]
    #rts=rirp[88]
    #rpr=rirp[41]
    #rp=rirp[-1]
    #
    #
    #
    # n_atm=len(rr)
    # ##%%
    #
    # ##%%
    # ## Centering the structures in the origin and comuting mass weighted vectors
    #
    # ## Calculating mass weighted structures and transposing to be consistent with the 1 column vector convention
    # XR=np.array([mol.cartesian2mass_weighted(np.array([rr]),atm_names)[0].flatten()]).T
    # XTS=np.array([mol.cartesian2mass_weighted(np.array([rts]),atm_names)[0].flatten()]).T
    # XL=np.array([mol.cartesian2mass_weighted(np.array([rp]),atm_names)[0].flatten()]).T
    # XIRP=mol.cartesian2mass_weighted(rirp,atm_names)
    # XIRP=flat(XIRP)
    #
    #
    #
    # XPL=np.array([mol.cartesian2mass_weighted(np.array([rpl]),atm_names)[0].flatten()]).T
    # XPR=np.array([mol.cartesian2mass_weighted(np.array([rpr]),atm_names)[0].flatten()]).T
    #
    #
    # ##%%
    # ## Defining the reaction plane directions
    # d1=(XR-XL)/np.linalg.norm((XR-XL))
    # P1=np.matmul(d1,d1.T)
    #
    # d2=((XR-XTS)-np.matmul(P1,(XR-XTS)))/np.linalg.norm((XR-XTS)-np.matmul(P1,(XR-XTS)))
    # P2=P1+np.matmul(d2,d2.T)
    #
    # d3=((XPL-XPR)-np.matmul(P2,(XPL-XPR)))/np.linalg.norm((XPL-XPR)-np.matmul(P2,(XPL-XPR)))
    # P3=P2+np.matmul(d3,d3.T)
    #
    # d3_t=((XPR-XTS)-np.matmul(P2,(XPR-XTS)))/np.linalg.norm((XPR-XTS)-np.matmul(P2,(XPR-XTS)))
    # P3_t=P2+np.matmul(d3_t,d3_t.T)
    #
    # d4_t=((XPL-XTS)-np.matmul(P3_t,(XPL-XTS)))/np.linalg.norm((XPL-XTS)-np.matmul(P3_t,(XPL-XTS)))
    # P4_t=P3_t+np.matmul(d4_t,d4_t.T)
    #
    # ##%%
    # ## The complementary space hessian
    # HESS_2_cart=np.matmul((np.identity(len(P2))-P2).T,np.matmul(hess_cart,(np.identity(len(P2))-P2)))
    # HESS_3_cart=np.matmul((np.identity(len(P3))-P3).T,np.matmul(hess_cart,(np.identity(len(P3))-P3)))
    # HESS_4_cart=np.matmul((np.identity(len(P4_t))-P4_t).T,np.matmul(hess_cart,(np.identity(len(P4_t))-P4_t)))
    #
    # ##%%
    # ## The Complementary space normal modes
    # K_2,TS_mode_2=np.linalg.eigh(HESS_2_cart)
    # K_2[:3]=-K_2[:3] ##The first 3 values are cero,but they are negative
    # W_2_cm_1 = np.array([ np.sqrt(k) for k in K_2])
    # W_2_au=W_2_cm_1*mol.cm_12Eh
    #
    # K_3,TS_mode_3=np.linalg.eigh(HESS_3_cart)
    # K_3[:2]=-K_3[:2] ##The first 3 values are cero,but they are negative
    # W_3_cm_1 = np.array([ np.sqrt(k) for k in K_3])
    # W_3_au=W_3_cm_1*mol.cm_12Eh
    #
    # K_4,TS_mode_4=np.linalg.eigh(HESS_4_cart)
    # K_4[:5]=-K_4[:5] ##The first 3 values are cero,but they are negative
    # W_4_cm_1 = np.array([ np.sqrt(k) for k in K_4])
    # W_4_au=W_4_cm_1*mol.cm_12Eh
    #
    #
    #
    #
    # ##%%
    # d1_cart=mol.mass_weighted2cartesian(np.array([d1]),atm_names)[0]
    # d2_cart=mol.mass_weighted2cartesian(np.array([d2]),atm_names)[0]
    # d3_cart=mol.mass_weighted2cartesian(np.array([d3]),atm_names)[0]
    # d4_cart=mol.mass_weighted2cartesian(np.array([d4]),atm_names)[0]
    #
    # ##%%
    # ## Computing the projected reaction path and the rmsd
    # XIRP_proj_2=np.array([ np.matmul(P2,x-XTS)  + XTS for x in XIRP])
    # XIRP_proj_3=np.array([ np.matmul(P3,x-XTS)  + XTS for x in XIRP])
    # XIRP_proj_4=np.array([ np.matmul(P4_t,x-XTS)  + XTS for x in XIRP])
    #
    # ##%%
    # ## The difference between the projected and the reaction path in au
    # DIFF_2=(XIRP-XIRP_proj_2)*(mol.A2au*np.sqrt(mol.amu2au_mass))
    # DIFF_3=(XIRP-XIRP_proj_3)*(mol.A2au*np.sqrt(mol.amu2au_mass))
    # DIFF_4=(XIRP-XIRP_proj_4)*(mol.A2au*np.sqrt(mol.amu2au_mass))
    #
    # ## The projections along the complementary normal modes
    # dQ_2_au=np.array([ max([ np.matmul(mode,x[:,0]) for x in DIFF_2[36:45]])  for mode in TS_mode_2.T[8:]])
    # dQ_3_au=np.array([ max([ np.matmul(mode,x[:,0]) for x in DIFF_3[36:45]])  for mode in TS_mode_3.T[9:]])
    # dQ_4_au=np.array([ max([ np.matmul(mode,x[:,0]) for x in DIFF_4[36:45]])  for mode in TS_mode_4.T[10:]])
    #
    # ## The harmonic oscillator standard deviation
    # d_2=np.array([ np.sqrt(1./w) for w in W_2_au[8:]])
    # d_3=np.array([ np.sqrt(1./w) for w in W_3_au[9:]])
    # d_4=np.array([ np.sqrt(1./w) for w in W_4_au[10:]])
    #
    # ## The relative displacemets
    # rel_2=np.array([ dQ_2_au[i]/d_2[i] for i in range(len(d_2))])
    # rel_3=np.array([ dQ_3_au[i]/d_3[i] for i in range(len(d_3))])
    # rel_4=np.array([ dQ_4_au[i]/d_4[i] for i in range(len(d_4))])
    #
    # ##%%
    # plt.bar(np.array(range(len(rel_2)))+8+0.2,rel_2,width=0.3,align="center",alpha=0.5,label="2D")
    # plt.bar(np.array(range(len(rel_3)))+9+0.5,rel_3,width=0.3,align="center",alpha=0.5,label="3D")
    # plt.bar(np.array(range(len(rel_4)))+10+0.8,rel_4,width=0.3,align="center",alpha=0.5,label="4D")
    # plt.xlabel("modes",fontweight='bold')
    # plt.ylabel("$\Delta Q_i/\delta_i$",fontweight='bold')
    # plt.tick_params(labelsize=14)
    # plt.legend()
    # ##%%
    # xirp=flat(mol.mass_weighted2cartesian(no_flat(XIRP),atm_names))
    # xirp_proj_2=flat(mol.mass_weighted2cartesian(no_flat(XIRP_proj_2),atm_names))
    # xirp_proj_3=flat(mol.mass_weighted2cartesian(no_flat(XIRP_proj_3),atm_names))
    # xirp_proj_4=flat(mol.mass_weighted2cartesian(no_flat(XIRP_proj_4),atm_names))
    #
    # sigma_2=np.array([    (1/np.sqrt(n_atm))*np.sqrt(np.dot((xirp[i]-xirp_proj_2[i])[:,0],(xirp[i]-xirp_proj_2[i])[:,0]))    for i in range(0,len(XIRP))])
    # sigma_3=np.array([    (1/np.sqrt(n_atm))*np.sqrt(np.dot((xirp[i]-xirp_proj_3[i])[:,0],(xirp[i]-xirp_proj_3[i])[:,0]))    for i in range(0,len(XIRP))])
    # sigma_4=np.array([    (1/np.sqrt(n_atm))*np.sqrt(np.dot((xirp[i]-xirp_proj_4[i])[:,0],(xirp[i]-xirp_proj_4[i])[:,0]))    for i in range(0,len(XIRP))])
    #
    # ##%% Ploting the rmsd%%
    # plt.plot(sigma_2[36:45],"o",label="2D")
    # plt.plot(sigma_3[36:45],"o",label="3D")
    # plt.plot(sigma_4[36:45],"o",label="4D")
    # plt.xlabel("step",fontweight='bold')
    # plt.ylabel("$\sigma^2$",fontweight='bold')
    # plt.legend()
    # plt.tick_params(labelsize=14)
    # plt.legend()
    # ##plt.plot(sigma_4,"o")%%
    #
    #
    #
    #
    # mol.write_trajectory(no_flat(xirp_proj_3),e_irp,atm_names,"/home/ar612/test_3.xyz")
    #
    #
    #
    #
    # ##%%
    # ## Maximum displacement for every direction
    # x_max=2
    # x_min=-6
    # nx=50
    #
    # y_max=2
    # y_min=-3
    # ny=50
    #
    # x_list=np.linspace(x_min, x_max, num=nx)
    # y_list=np.linspace(y_min, y_max, num=ny)
    #
    #
    #
    # ## The first extructure needed for append to work
    # r=np.zeros([1,n_atm,3])
    #
    # r0=rts
    #
    # ## Escaning through the displacements in both directions
    # for dx1 in x_list:
    #     for dx2 in y_list:
    #         r=np.append(r,[r0+dx1*d1_cart+dx2*d2_cart+[10,10,10]],axis=0)
    #
    #
    # ## Removing the first dummy structure
    # r=r[1:]
    #
    # ## Naming the final file
    # out_file=out_pattern.replace("&&",str(nx)+"_"+str(ny))
    #
    # ## Writing the trajectory to the file
    # mol.write_trajectory (r,np.zeros(len(r)),atm_names,out_file)
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #




###########################################################################%%


if __name__== "__main__":


    # rr_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/37_r20_ts_p_80_rep_movie.xyz"
    # rts_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/39_r20_ts_p_80_rep_movie.xyz"
    # rp_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/43_r20_ts_p_80_rep_movie.xyz"

    reac_path_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/pctts1_irc.xyz"
    rts_vib_file="/home/ar612/Documents/cp2k_organized/vibrational_analysis/39_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis/39_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"


    main(modes_inp_file,m1,m2,disp_max)
