
#%%
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
plt.style.use(['ggplot'])
from matplotlib import cm
from numpy import linspace
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
from scipy import interpolate
%matplotlib qt
plt.style.use(['ggplot'])
############################################%%

def plot_e(dist,e,project,dh):
    ##%%
    ## Maximum and minimum values of the scale
    max=1
    min=0

    ## Creating an array of colors map scaled from 0 to 1 and a norm instance to get the correct scale in the colorbar
    map=cm.rainbow(dh/max)
    norm = matplotlib.colors.Normalize(vmin=min,vmax=max)

    ## Here I define the grid to set the relative sizes between plot and color bar
    gs = gridspec.GridSpec(1, 8)

    fig=plt.figure(figsize=(10, 8))
    ax1=fig.add_subplot(gs[0, :7])
    for i in range(0,len(dh)):
        ax1.plot(dist[i],e[i],'b-o',linewidth=2.5,color=map[i] )
    ax1.set_xlabel(project+"(A)",fontweight='bold')
    ax1.tick_params(labelsize=14)
    ax1.set_ylabel('E (kcal/mol)',fontweight='bold')
    ax1.grid(True)
    ax2=fig.add_subplot(gs[0, 7:8])
    matplotlib.colorbar.ColorbarBase(ax2, cmap='rainbow',norm=norm, drawedges=False, label=r'$\frac{|\Delta r_H|}{|\Delta R|}$')
    fig.show()
    ##%%
    return fig,ax1,ax2


def plot_comparisson(dist,dist_int,e_gas,e_solv,e_solv_error,e_solv_min,free_ener_inp_file,std_free_ener_inp_file,project,n_images):
    plt.plot(dist,e_gas,'b-o',linewidth=2,label="reaction path in gas phase")
    plt.errorbar(dist, e_solv, yerr=e_solv_error,linewidth=2,fmt='r-o',capsize=3,label="same reaction path with solvent (averaged)")
    plt.errorbar(dist_int, free_ener_inp_file, yerr=std_free_ener_inp_file,linewidth=2,fmt='m-o',capsize=3,label="free energy")
    plt.plot(dist,e_solv_min,'g-o',linewidth=2,label="same reaction path with solvent (minimum)")
    plt.xlabel(project+"(A)",fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.ylabel('E (kcal/mol)',fontweight='bold')
    plt.grid(True)
    plt.style.use(['ggplot'])
    plt.legend(loc=3,ncol=1)
    plt.savefig(project+"_comp.pdf", bbox_inches='tight')
    plt.show()

def plot_dy(dist,e,dh,dy,w,inp_modes,project):


    ## Creating interpolations for the data
    min_dist=np.min(dist)
    max_dist=np.max(dist)
    dist_int=np.linspace(min_dist, max_dist,1000)
    dh_int=interpolate.interp1d(dist, dh, kind='quadratic')
    e_int=interpolate.interp1d(dist, e, kind='quadratic')

    ## Ploting the energy profile of the reaction
    fig,ax1,ax2=plot_e(dist_int,e_int(dist_int),project,dh_int(dist_int))

    ## Ploting the normal modes
    palette = plt.get_cmap('Set1')

    ## Making blue energy axes
    color = 'blue'
    ax1.set_ylabel('E (kcal/mol)', color=color,fontweight='bold')  # we already handled the x-label with ax1
    ax1.tick_params(axis='y', labelcolor=color,labelsize=14)


    color = 'red'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_xlabel(project+"(A)",fontweight='bold')
    ax2.set_ylabel('dy', color=color,fontweight='bold')
    for i in inp_modes:
        ax2.plot(dist[1:], dy[i],linewidth=1.,color=palette(i/12),marker='', alpha=0.9, label=str(i+1)+": " +str(w[i]))
        plt.text(dist[-1]+0.1, dy[i,-1], str(i+1), horizontalalignment='left', size='large', color=palette(i/12))
        plt.text(dist[1]-1, dy[i,0], str(i+1), horizontalalignment='left', size='large', color=palette(i/12))
    ax2.tick_params(axis='x',labelsize=14)
    ax2.tick_params(axis='y', labelcolor=color,labelsize=14)
    ax2.legend()

    plt.savefig(project+"_vibra.pdf", bbox_inches='tight')
    plt.show()

def struct_gen(r0,c,y):

    n=len(r0)
    n_vib=len(y)
    dr=np.ceros([n,3])

    for i in range(0,n_vib):
        dr=dr+c[i]*y[i]

    r=r0+dr

    return r


def spline(r,e,dr=2):

    n_frames=len(r)
    n_atm=len(r[0])
    dim=len(r[0,0])

    dr=2

    x=np.arange(0, 20)
    xnew=np.arange(0, 20,1./dr)[:-1]

    f=CubicSpline(x, e)

    e_int=f(xnew)


    r_int=np.zeros([dr*n_frames-1,n_atm,dim])

    for i in range(0,n_atm):
        for j in range(0,dim):

            y=r[::,i,j]
            f= CubicSpline(x, y)

            r_int[::,i,j]=f(xnew)



    return e_int,r_int


def projections(modes,dr):

    n_images=len(dr)
    n_vib=len(modes)
    ## Creating the Matrix where I store the projection of the normal modes for each reaction path step
    dy=np.empty([n_vib,n_images])

    ## Calculating the projections. The displacement dr are normalize
    for i in range(0,n_vib):
        for j in range(0,n_images):
            dy[i,j]=np.dot(modes[i].flatten(),dr[j].flatten())

    return dy

############################################################################%%


def main (reac_path_file,normal_modes_file,project):

    ## Reading reaction path from movie file and normal modes
    e,r,atm_names=mol.e_r_atm_names_from_xyz(reac_path_file)
    w,modes,r0,atm_names=mol.w_modes_r_atm_names_from_molden(normal_modes_file)


    ## Expresing energies in kcal/mol and using the first image as reference
    e=(e-e[0])*mol.Eh2kcalmol
    modes=mol.normal(modes)

    ## Extracting number of atoms and some other info
    n_images=len(r)
    n_atm=len(r0)
    n_vib=len(w)

    ## Calculating the displacementes of each image of the reaction path with respect to the first image. The first displacement will be cero
    dr,dist=mol.path_dist(r)
    min_dist=np.min(dist)
    max_dist=np.max(dist)



    ## Calculating the relation between the hydrogen displacement and the rest of the molecule as a function of the reaction path
    disp_atm=27 ## Hydrogen atom
    dh=np.array([ np.linalg.norm(dr[i,disp_atm])/np.linalg.norm(dr[i]) for i in range(0,n_images-1)])
    dh=np.append(dh,dh[-1])

    ## Calculating the relation between the hydrogen displacement and the rest of the molecule as a function of the reaction path
    # disp_atm=26 ## Oxigen atom
    # dh=np.array([ np.linalg.norm(dr[i,disp_atm]) for i in range(0,n_images-1)])
    # dh=np.append(dh,dh[-1])

    #plt.plot(dh,"o")

    ## Creating interpolations for the data
    dist_int=np.linspace(min_dist, max_dist,1000)
    dh_int=interpolate.interp1d(dist, dh, kind='quadratic')
    e_int=interpolate.interp1d(dist, e, kind='quadratic')

    ## Ploting the energy profile of the reaction
    #fig,ax1,ax2=plot_e(dist_int,e_int(dist_int),project,dh_int(dist_int))

    ## Calculating projections
    dr_mass=mol.cartesian2mass_weighted(dr,atm_names)
    modes_mass=mol.cartesian2mass_weighted(modes,atm_names)
    modes_mass_normal=mol.normal(modes_mass)
    modes_normal=mol.normal(modes)
    # mat=mol.projection_matrix(modes_mass_normal)
    # plt.matshow(mat)

    dy=projections(modes_normal,dr)


    ## Calculating the maximum projection of each mode along the reaction path
    max_pr=np.zeros(n_vib)
    for i in range(0,n_vib):
        max_pr[i]=np.max(abs(dy[i,image_0:image_f]))
    plt.plot(max_pr,'o')

    ## Defining the cuttof and calculating the important modes
    cuttof=0.15
    inp_modes=np.zeros(0)


    for i in range(0,n_vib):
        if np.max(abs(dy[i,image_0:image_f])) > cuttof:
            inp_modes=np.append(inp_modes,i)

    inp_modes=[int(i) for i in inp_modes]
    # dy[22]=-dy[22]
    # dy[24]=-dy[24]
    # dy[92]=-dy[92]
    # dy[7]=-dy[7]
    plot_dy(dist,e,dh,dy,w,inp_modes,project)












###########################################################################%%


if __name__== "__main__":


    reac_path_file="/home/ar612/Documents/cp2k_organized/reac_path/r20_ts_p_80_rep/r20_ts_p_80_rep_movie.xyz"
    normal_modes_file="/home/ar612/Documents/cp2k_organized/vibrational_analysis/37_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis/37_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"
    important_modes_file="/home/ar612/Documents/cp2k_organized/reac_path/r20_ts_p_80_rep/r20_ts_p_80_rep_important_modes.xyz"
    project="r20_ts_p_80_rep"

    image_0=38
    image_f=41




    main(xyz_inp_file,reac_path,n_atm,job)
