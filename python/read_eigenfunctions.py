
#%%
import numpy as np
from numpy import linalg
from scipy import interpolate
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import quantum as quan
from scipy import integrate





############### Reading the data #######################
##%%
eval1=np.genfromtxt("/home/ar612/Documents/Work/HD+/generator/u1_eigenvalues.dat")[:,1]
eval2=np.genfromtxt("/home/ar612/Documents/Work/HD+/generator/u2_eigenvalues.dat")[:,1]

dat1=np.genfromtxt("/home/ar612/Documents/Work/HD+/generator/u1_eigenstates.dat")
dat2=np.genfromtxt("/home/ar612/Documents/Work/HD+/generator/u2_eigenstates.dat")
##%%
############### Reading the data #######################
plt.plot(dat1[:,0],dat1[:,500],'o-')



############## Selecting the eigenstates ################
##%%
first1=25
imax1=500   ## Maximum 460
di1=1
select1=[i for i in range(first1)]+[i for i in range(first1,imax1,di1)]
select1_1=[i+1 for i in select1]

first2=5
imax2=500  ## Maximum 460
di2=1
select2=[i for i in range(first2)]+[i for i in range(first2,imax2,di2)]
select2_1=[i+1 for i in select2]


e1=eval1[select1]
e2=eval2[select2]

r1=dat1[:,0]
psi1=dat1[:,select1_1].T
psi1_der=quan.deriv(r1,psi1)
N1=len(psi1)
len1=len(r1)

r2=dat2[:,0]
psi2=dat2[:,select2_1].T
psi2_der=quan.deriv(r2,psi2)
N2=len(psi2)
len2=len(r2)
Ntot=N1+N2
N1
N2
Ntot
##%%

############## Selecting the eigenstates ################

plt.plot(r1,psi1[600],'o-')
plt.plot(r1,psi1_der[600],'o-')


mat1=np.zeros([N1,N1])

for i in range(N1):
    for j in range(N1):
        mat1[i,j]=bra_ket_int(r1,psi1[i],np.ones(len1),psi1[j])


plt.matshow(mat1)



np.array(range(100,101))


##%%

#### This lists contains the first and final relevant index of the projections into the eigenstates
minindx1=[]
minindx2=[]
maxindx1=[]
maxindx2=[]

i_20=1990   ### The index corresponding to 20au distance
di_30=2810  ### The index displacement so the gaussian is centered at 30au distance

state_list=[psi1[0]  ,  np.array([ psi1[0][i-2810] for i in range(len1)])  ,  np.array([ np.sqrt(1/20)*np.heaviside(r-20,np.sqrt(1/20)) for r in r1]) ]
state_list_name=["ground","disp_ground","heaviside"]

state_vect_1=[]
state_vect_2=[]

for state in state_list:

    cn1=[0]*N1
    for i in range(N1):
        cn1[i]=quan.bra_ket_int(r1,psi1[i],np.ones(len1),state)

    cn2=[0]*N2
    for i in range(N2):
        cn2[i]=quan.bra_ket_int(r1,psi2[i],np.ones(len2),state)

    indx1=[i for i in range(len(cn1)) if abs(cn1[i]) > 0.01 ]
    minindx1.append(min(indx1))
    maxindx1.append(max(indx1))


    indx2=[i for i in range(len(cn2)) if abs(cn2[i]) > 0.01 ]
    minindx2.append(min(indx2))
    maxindx2.append(max(indx2))

    psi_vec_1=np.array(cn1+[0]*N2+[0]*Ntot)
    psi_vec_2=np.array([0]*N1+cn2+[0]*Ntot)

    state_vect_1.append(quan.normalize_vec(psi_vec_1))
    state_vect_2.append(quan.normalize_vec(psi_vec_2))

    reconstructed_state1=np.zeros([len1])
    for i in range(N1):
        reconstructed_state1+=cn1[i]*psi1[i]

    reconstructed_state2=np.zeros([len2])
    for i in range(N2):
        reconstructed_state2+=cn2[i]*psi2[i]

    plt.plot(r1,state,'-',label='psi')
    plt.plot(r1,reconstructed_state1,'-',label='psi1')
    plt.plot(r1,reconstructed_state2,'-',label='psi2')
    plt.legend()


# plt.plot(minindx1,'o-',label="minindx1")
# plt.plot(maxindx1,'o-',label="maxindx1")
# plt.plot(minindx2,'o-',label="minindx2")
# plt.plot(maxindx2,'o-',label="maxindx2")
# plt.legend()


##%%
######### Reading Operators and calculating matrices ###############
surface_dir="/home/ar612/Documents/Work/HD+/surfaces"

d11_file=surface_dir+"/D11-R.dat"
d12_file=surface_dir+"/D12-R.dat"
d22_file=surface_dir+"/D22-R.dat"
q11_file=surface_dir+"/Q11-R.dat"
q12_file=surface_dir+"/Q12-R.dat"
q21_file=surface_dir+"/Q21-R.dat"
q22_file=surface_dir+"/Q22-R.dat"
p12_file=surface_dir+"/P12-R.dat"


d11_dat=np.genfromtxt(d11_file)
d12_dat=np.genfromtxt(d12_file)
d22_dat=np.genfromtxt(d22_file)
q11_dat=np.genfromtxt(q11_file)
q12_dat=np.genfromtxt(q12_file)
q21_dat=np.genfromtxt(q21_file)
q22_dat=np.genfromtxt(q22_file)
p12_dat=np.genfromtxt(p12_file)


d11_r1=quan.spline(d11_dat[:,0],d11_dat[:,1],r1)
d12_r1=quan.spline(d12_dat[:,0],d12_dat[:,1],r1)
d22_r1=quan.spline(d22_dat[:,0],d22_dat[:,1],r1)
q11_r1=quan.spline(q11_dat[:,0],q11_dat[:,1],r1)
q12_r1=quan.spline(q11_dat[:,0],q12_dat[:,1],r1)
q21_r1=quan.spline(q21_dat[:,0],q21_dat[:,1],r1)
q22_r1=quan.spline(q22_dat[:,0],q22_dat[:,1],r1)
p12_r1=quan.spline(p12_dat[:,0],p12_dat[:,1],r1)
p21_r1=quan.spline(p12_dat[:,0],p12_dat[:,1],r1)   ### I'm asuming that P12(R) is equal to P21(R)


d11_au=quan.mat_rep(r1,psi1,d11_r1,psi1)
d12_au=quan.mat_rep(r1,psi1,d12_r1,psi2)
d22_au=quan.mat_rep(r1,psi2,d22_r1,psi2)
q11_au=quan.mat_rep(r1,psi1,q11_r1,psi1)
q12_au=quan.mat_rep(r1,psi1,q12_r1,psi2)
q21_au=quan.mat_rep(r1,psi2,q21_r1,psi1)
q22_au=quan.mat_rep(r1,psi2,q22_r1,psi2)
p12_au=quan.mat_rep(r1,psi1,p12_r1,psi2_der)
p21_au=quan.mat_rep(r1,psi2,p21_r1,psi1_der)
h11_au=np.diag(e1)
h22_au=np.diag(e2)
##%%
######### Reading Operators and calculating matrices ###############
plt.plot(r1,p12_r1,'o-')
plt.plot(r1,d12_r1,'o-')
plt.plot(r1,q12_r1,'o-')
plt.plot(r1,psi1[0])
plt.plot(r1,final_state)
plt.plot(r1,'o')
final_state=np.array([ psi1[0][i-2810] for i in range(len(r1))])

########## Rescaling #############
##%%
e0=h11_au[1,1]-h11_au[0,0]
mu0=max(d11_au.flatten())

d11=d11_au/mu0
d12=d12_au/mu0
d21=d12_au.T/mu0
d22=d22_au/mu0

q11=q11_au/e0
q12=q12_au/e0
q21=q21_au/e0
q22=q22_au/e0

p12=p12_au/e0
p21=p21_au/e0

h11=h11_au/e0
h22=h22_au/e0
##%%
########## Rescaling #############

########## Writing matrices to file ############
##%%
matrix_dir="/home/ar612/Documents/Work/HD+/matrices"
sys_dir=matrix_dir+"/mu0_"+str(mu0)+"_e0_"+str(e0)+"_N1_"+str(N1)+"_N2_"+str(N2)+"_first1_"+str(first1)+"_imax1_"+str(imax1)+"_di1_"+str(di1)+"_first2_"+str(first2)+"_imax2_"+str(imax2)+"_di2_"+str(di2)
os.makedirs(sys_dir,exist_ok=True)

mol.write_matrix_to_file(np.c_[r1,psi1.T],"","",sys_dir+"/psi1.txt")
mol.write_matrix_to_file(np.c_[r2,psi2.T],"","",sys_dir+"/psi2.txt")
mol.write_matrix_to_file(e1,"","",sys_dir+"/e1.txt")
mol.write_matrix_to_file(e2,"","",sys_dir+"/e2.txt")


for i in range(len(state_list)):
    mol.write_matrix_to_file([state_vect_1[i]],'','',sys_dir+"/cp_"+state_list_name[i]+"_1.txt")
    mol.write_matrix_to_file([state_vect_2[i]],'','',sys_dir+"/cp_"+state_list_name[i]+"_2.txt")



mol.write_matrix_to_file(d11,"","",sys_dir+"/d11.txt")
mol.write_matrix_to_file(d12,"","",sys_dir+"/d12.txt")
mol.write_matrix_to_file(d21,"","",sys_dir+"/d21.txt")
mol.write_matrix_to_file(d22,"","",sys_dir+"/d22.txt")

mol.write_matrix_to_file(q11,"","",sys_dir+"/q11.txt")
mol.write_matrix_to_file(q12,"","",sys_dir+"/q12.txt")
mol.write_matrix_to_file(q21,"","",sys_dir+"/q21.txt")
mol.write_matrix_to_file(q22,"","",sys_dir+"/q22.txt")

mol.write_matrix_to_file(p12,"","",sys_dir+"/p12.txt")
mol.write_matrix_to_file(p21,"","",sys_dir+"/p21.txt")

mol.write_matrix_to_file(h11,"","",sys_dir+"/h11.txt")
mol.write_matrix_to_file(h22,"","",sys_dir+"/h22.txt")
##%%
########## Writing matrices to file ############

select1
select2
def to_matrix(file):
    mat_read=np.genfromtxt(file)

    disp=int(mat_read[0,1])

    tot=len(mat_read)

    n=int(mat_read[-1,0])+int(not disp)
    m=int(mat_read[-1,1])+int(not disp)

    mat=np.empty([n,m])

    for l in range(tot):
        i=int(mat_read[l,0])-disp
        j=int(mat_read[l,1])-disp
        val=mat_read[l,2]
        mat[i,j]=val

    return mat


def select_index(e1,e2):

    nindx=len(e1)
    n2=len(e2)

    index_list_2=[-1]*nindx

    for j in range(nindx):
        min=float("inf")
        for i in range(n2):
            if abs(e2[i]-e1[j]) < min:
                index_list_2[j]=i
                min=abs(e2[i]-e1[j])

    return index_list_2




psi1_john=np.genfromtxt("/home/ar612/Documents/Work/HD+/eigenstates/wavefunctions-pot1.dat")
psi2_john=np.genfromtxt("/home/ar612/Documents/Work/HD+/eigenstates/wavefunctions-pot2.dat")
ene1_john=np.genfromtxt("/home/ar612/Documents/Work/HD+/coupling_terms/ene1.dat")[:,1]
ene2_john=np.genfromtxt("/home/ar612/Documents/Work/HD+/coupling_terms/ene2.dat")[:,1]
d12_john=to_matrix("/home/ar612/Documents/Work/HD+/coupling_terms/D12.dat")
p12_john=to_matrix("/home/ar612/Documents/Work/HD+/coupling_terms/P12.dat")

indx_john1=select_index(e1,ene1_john)
indx_john2=select_index(e2,ene2_john)

a=np.array([[1,2,3],
            [4,5,6],
            [7,8,9]])

a[[1,2]][:,[0,1]]

indx_john1
indx_john2
plt.matshow(d12_au)
plt.matshow(d12_john[indx_john1][:,indx_john2])

max(d12_au.flatten())
min(d12_au.flatten())
max(d12_john[indx_john1][:,indx_john2].flatten())
min(d12_john[indx_john1][:,indx_john2].flatten())

d11_au[:4,:4]

-d11_john[:4,:4]

plt.matshow(d12_john)
plt.matshow(d12_au)

max(d12_john.flatten())
max(d12_au.flatten())
##%%
i=9
plt.plot(psi2_john[:,0],psi2_john[:,indx_john2[i]+1],'-o',label='john_'+str(i))
plt.plot(r2,-psi2[i],'-o',label='mine_'+str(i))
plt.legend()
##%%
