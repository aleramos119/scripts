import numpy as np
##%matplotlib qt
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol


def main(inp_file_pattern,n,stride=1):

    plt.style.use(['ggplot'])
    fig=plt.figure(figsize=(10,10))
    ax=fig.add_subplot(111)
    for i in range(0,n,stride):
        inp_file=inp_file_pattern.replace("$$",str(i))
        x,fes=np.genfromtxt(inp_file,unpack= True,usecols=(0,1))
        plt.plot(x,fes*mol.J2cal,label=str(i),linewidth=(3-0.5)*(i+1)/n)
    plt.xlabel("colvar",fontweight='bold')
    plt.ylabel("F (kcal/mol)",fontweight='bold')
    plt.legend()
    plt.show()


if __name__== "__main__":

    inp_file_pattern="fes_$$.dat"
    n=int(sys.argv[1])

    main(inp_file_pattern,n)
