
#from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider
#import matplotlib.animation as animation
import sys
%matplotlib qt



def main(inp_file):

    ## Loading file columns
    xlist,ylist,zlist,fes=np.genfromtxt(inp_file,unpack= True, skip_header = 0,usecols={0,1,2,3})

    ## Defining max and min values
    fes_max=np.max(fes)
    fes_min=np.min(fes)
    xmin=min(x)
    xmax=max(x)

    ymin=min(y)
    ymax=max(y)

    zmin=min(z)
    zmax=max(z)

    for i,fesi in enumerate(fes):
        if fesi > sat:
            fes[i]=sat

    


    #### Initial value of z_val
    initial_z_val = 0
    z_val = initial_z_val

    ### Generating the data
    indx=[i for i, n in enumerate(z) if z_val-0.001 < n < z_val+0.001]

    ### Creating the subfigures
    fig=plt.figure(1,figsize=(10,8))
    plt.style.use(['ggplot'])






    ###Ploting the first figure
    plt.subplot(111)
    plt.axis([xmin, xmax, ymin,ymax])
    plt.scatter(x[indx],y[indx],c=fes[indx],marker="s",s=23,vmin=fes_min, vmax=fes_max,cmap=plt.get_cmap("rainbow"),alpha=.55)
    #cb=plt.colorbar()
    plt.title("z= "+str(z_val)+" A")
    plt.xlabel("x (A)",fontweight='bold')
    plt.ylabel("y (A)",fontweight='bold')
    plt.tick_params(labelsize=14)


    ## Set slide axes
    axz_val = plt.axes([0.25, .02, 0.50, 0.02])
    # Slider
    sz_val = Slider(axz_val, 'z (A)', -4, 4, valinit=initial_z_val,valstep=0.1)

    def update(val):
        # amp is the current value of the slider
        z_val = sz_val.val
        # update indx
        indx=[i for i, n in enumerate(z) if z_val-0.001 < n < z_val+0.001]
        # make plot again
        plt.subplot(111)
        plt.cla()
        plt.axis([-5, 5, -5, 5])
        plt.scatter(x[indx],y[indx],c=fes[indx],marker="s",s=23,vmin=fes_min, vmax=fes_max-150,cmap=plt.get_cmap("rainbow"),alpha=.55)
        #cb=plt.colorbar()
        plt.title("z= "+str(z_val)+" A")
        plt.xlabel("x (A)",fontweight='bold')
        plt.ylabel("y (A)",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        # redraw canvas while idle
        fig.canvas.draw_idle()

    # call update function on slider value change
    sz_val.on_changed(update)

    plt.show()



if __name__== "__main__":

    inp_file="/home/ar612/Documents/cp2k_organized/ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_10_10_eps_10e-6_ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_10_10_eps_10e-6_ref_traj_ener.txt"
    sat=300
    #inp_file=sys.argv[1]
    main(inp_file)







# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
#
# # x = [1,2,3,2]
# # y = [1,2,3,1]
# # z = [1,2,3,1]
# # c = [0.1,0.2,0.3,0.4]
#
# ax.scatter(x, y, z, c=fes, cmap=plt.get_cmap("rainbow"),marker="s",alpha=0.3,edgecolors='face')
# plt.xlabel('x (A)',fontweight='bold')
# plt.ylabel('y (A)',fontweight='bold')
# #plt.zlabel('z (A)',fontweight='bold')
# plt.show()



# fig = plt.figure()
# ax = fig.add_subplot(111)
#
# fig.subplots_adjust(left=0.25, bottom=0.25)
#
#
# z_val=-4
#
# indx=[i for i, n in enumerate(z) if n == z_val]
#
#
# plt.style.use(['ggplot'])
# plt.scatter(x[indx],y[indx],c=fes[indx],marker="s",s=10,vmin=fes_min, vmax=fes_max,cmap=plt.get_cmap("rainbow"),alpha=.55)
# plt.colorbar()
# plt.title("z= "+str(z_val)+" A")
# plt.xlabel("x (A)",fontweight='bold')
# plt.ylabel("y (A)",fontweight='bold')
# plt.tick_params(labelsize=14)
# plt.grid(True)
# plt.show()
#
#
# z_slider_ax  = fig.add_axes([0.25, 0.15, 0.65, 0.03], axisbg=axis_color)
# z_slider = Slider(amp_slider_ax, 'Amp', -4, 4, valinit=-4)
#
#
# def sliders_on_changed(val):
#     line.set_ydata(signal(z_slider.val))
# #     fig.canvas.draw_idle()
# # z_slider.on_changed(sliders_on_changed)
#
#
#
#
#
#






# plt.figure(1)                # the first figure
# plt.subplot(221)             # the first subplot in the first figure
# plt.plot([1, 2, 3])
# plt.subplot(264)             # the second subplot in the first figure
# plt.plot([4, 5, 6])
# plt.plot([4, 5, 6],[5,4,5])
#
#
# plt.figure(2)                # a second figure
# plt.plot([4, 5, 6])          # creates a subplot(111) by default
#
# plt.figure(1)                # figure 1 current; subplot(212) still current
# plt.subplot(221)             # make subplot(211) in figure1 current
# plt.title('Easy as 1, 2, 3')
# plt.title('test') # subplot 211 title
# plt.plot([4, 5, 6],[5,4,5])
# plt.show()
