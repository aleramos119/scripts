#%%
import sys
sys.path.append('$PYDIR')

import mol
import numpy as np
from itertools import islice

def main():
    mol.reduce_periodic_file (sys.argv[1],sys.argv[2],int(sys.argv[3]),int(sys.argv[4]),step_i=1,step_f=10000000)
#########################################################%%

if __name__== "__main__":
     main()
