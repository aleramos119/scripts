#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import numpy as np
from itertools import islice
from timeit import default_timer as timer
%matplotlib qt
import matplotlib.pyplot as plt
plt.style.use(['ggplot'])

from scipy import integrate





#########################################################%%

if __name__== "__main__":

        system="double_well"
        time=15000

        hmass_list=range(1,11)


        psopt_ctrl_file_pattern=os.environ['OCTGAUSS_HOME']+"/control/"
        quantics_ctrl_file_pattern=os.environ['QUANTICS_HOME']+"/control/"

        psopt_ctrl_file_pattern=psopt_ctrl_file_pattern+"double_well/14T_2/$$_hmass/"
        quantics_ctrl_file_pattern=quantics_ctrl_file_pattern+"double_well/14T_2/$$_hmass/$$_hmass/"

        efield_name="efield.txt"
        state_name="x.dat"
        time_name="t.dat"

        qdq_name="qdq_1_1.pl"

        ##Making the plots

        fig, ax = plt.subplots(5, 2, sharex='col')


        for hmass in hmass_list[5:10]:
        #for hmass in hmass_list[0:5]:
            dm=6
            #dm=1
            ##Reading PSOPT data
            psopt_ctrl_file_dir=psopt_ctrl_file_pattern.replace("$$",str(hmass))
            psopt_efield_file=psopt_ctrl_file_dir+efield_name
            psopt_state_file=psopt_ctrl_file_dir+state_name
            psopt_time_file=psopt_ctrl_file_dir+time_name


            efield=mol.fast_load(psopt_efield_file,[0,1])
            t=mol.fast_load(psopt_time_file,[0]).flatten()
            x=mol.fast_load(psopt_state_file,[2]).flatten()

            #int_field=integrate.simps(efield[:,1],efield[:,0])

            cum_int=integrate.cumtrapz(efield[:,1],efield[:,0],initial=0)
            #
            # plt.plot(efield[:,0],cum_int)

            ##Reading quantics data
            quantics_ctrl_file_dir=quantics_ctrl_file_pattern.replace("$$",str(hmass))
            qdq_file=quantics_ctrl_file_dir+qdq_name

            qdq=mol.fast_load(qdq_file,[0,1])


            ####################################################%%
            ax[hmass-dm,0].plot(efield[:,0],efield[:,1],'-',label="psopt_control "+str(hmass)+"mH",color="tab:red")
            #ax2 = ax[hmass-dm,0].twinx()
            #ax2.plot(efield[:,0],cum_int,'-',label="integral "+str(hmass)+"mH",color="tab:blue")
            ax[hmass-dm,0].legend()
            ax[hmass-dm,1].plot(t,x,'-',label="psopt_state "+str(hmass)+"mH")
            ax[hmass-dm,1].plot(qdq[:,0]*mol.fs2au,qdq[:,1],'-',label="quantics_state "+str(hmass)+"mH")
            ax[hmass-dm,1].legend()
            plt.show()
            ####################################################%%














        dh_file="/home/ar612/Documents/cp2k_organized/pmf/r20_ts_p_80_rep/solv/39_r20_ts_p_80_rep_solv_pmf/39_r20_ts_p_80_rep_solv_pmf__dH__39.txt"
        dH_list=mol.fast_load(dh_file,[0]).T[0]

        avg=dH_list[0]
        dH_avg=np.zeros([len(dH_list)])
        dH_avg[0]=avg
        for i in range(1,len(dH_list)):
            avg=i*avg/(i+1)+dH_list[i]/(i+1)
            dH_avg[i]=avg


        plt.plot(dH_avg)



    efield=mol.fast_load(psopt_efield_file,[0,1])
    t=mol.fast_load("/data/ar612/oct_gauss/control/double_well/10T_2/8_hmass/t.dat",[0]).flatten()
    x=mol.fast_load("/data/ar612/oct_gauss/control/double_well/10T_2/8_hmass/x.dat",[3]).flatten()

    m=16534.62
    vmax=0.01
    a=2

    sig=np.pi*np.sqrt(m*a**2/(8*vmax))
    g=np.array([ np.exp(-0.5*((i -t[-1]/2)/(sig/4))**2) for i in t])

    plt.plot(t,x)
    plt.plot(t,10*g)
