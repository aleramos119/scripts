#! /bin/python3

#%%
import numpy as np
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol



rest_file="/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctreac1_1_thf_196_300K_30A_qmmm-1.restart"



def read_r_from_restart(rest_file):

    l0=0
    l1=0

    natm=0

    with open(rest_file) as file:
       for i, line in enumerate(file, 0):
           if "&COORD" in line:
               l0=i
           elif "&END COORD" in line:
               l1=i


    natm=l1-l0-1

    atm_names=np.genfromtxt(rest_file,skip_header=l0+1,max_rows=natm,usecols={0},dtype=str)
    r=np.genfromtxt(rest_file,skip_header=l0+1,max_rows=natm,usecols={1,2,3})
    mol_name=np.genfromtxt(rest_file,skip_header=l0+1,max_rows=natm,usecols={4},dtype=str)
    mol_number=np.genfromtxt(rest_file,skip_header=l0+1,max_rows=natm,usecols={5})

    return atm_names,r,mol_name,mol_number



def write_r_cp2k_inp (file_dir,r):

    n=len(r)  ## Numer of atoms

    dim=len(r[0])  ## This is the dimension of each element of r

    file_lines = open(file_dir, 'r').readlines()  ## Reading the file into an array of lines


    l0=0
    l1=0

    natm=0

    with open(rest_file) as file:
       for i, line in enumerate(file, 0):
           if "&COORD" in line:
               l0=i



    for i in range(0,n):
       temp=file_lines[l0+i].split()
       temp[1:1+dim]=r[i]
       temp=np.append(temp,"\n")
       file_lines[l0+i]="   ".join(temp)

    with open(file_dir,'w') as file:
        file.writelines(file_lines)




############################################################%%
rest_file=sys.argv[1]
d=sys.argv[2]

atm_names,r,mol_name,mol_number=read_r_from_restart(rest_file)

atm_pairs=[[14,15],[33,34],[52,53]]

for pair in atm_pairs:
    vec=(r[pair[0]]-r[pair[1]])/np.linalg.norm(r[pair[0]]-r[pair[1]])
    r[pair[0]]=r[pair[1]]+d*vec


write_r_cp2k_inp (rest_file,r)
