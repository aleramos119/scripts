
#%%
import numpy as np
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
import matplotlib.pyplot as plt
%matplotlib qt


#
# def mass_weight (y,n_vib,atm_names,n_atm):
#     for i in range(0,n_vib):
#         for j in range(0,n_atm):
#             y[i,j]=y[i,j]*np.sqrt(mol.mass[atm_names[j]])
#
#     return y
#
# def normal (y,n_vib):
#     for i in range(0,n_vib):
#         y[i]=y[i]/np.linalg.norm(y[i].flatten())
#     return y
#
#
#
# def projection_matrix (y,n_vib):
#     y_ij=np.empty([n_vib,n_vib])
#
#     for i in range(0,n_vib):
#         for j in range(0,n_vib):
#             y_ij[i,j]=np.dot(y[i].flatten(),y[j].flatten())
#
#
#     return y_ij
#
#
#
#
# ###################################################################%%
# def projections(dr,n_images,y,n_vib):
#     ## Creating the Matrix where I store the projection of the normal modes for each reaction path step
#     dy=np.empty([n_images,n_vib])
#
#     ## Calculating the projections. The displacement dr are normalize
#     for i in range(0,n_images):
#         for j in range(0,n_vib):
#             dy[i,j]=np.dot(dr[i].flatten(),y[j].flatten())
#
#     return dy
#


def vel_gen(c,y):

    n=len(y[0])
    n_vib=len(y)
    v=np.zeros([n,3])

    for i in range(0,n_vib):
        v=v+c[i]*y[i]

    return v


def flat(traj):
    return np.array([ r.flatten() for r in traj])

def modes2Q(modes,atm_names):
    return mol.flat(mol.cartesian2mass_weighted(modes,atm_names)).T


def modes_modes_name_r_atm_names_from_nmd (inp_file):

    ## These are some variables to check whether I already asigned the initial and final lines for the coordinates and frequancy sections
    n_atm=0
    r=[]
    atm_names=[]
    mode_name_list=[]
    modes=[]




    ## Reading the file line by line
    with open(inp_file) as file:
        for n, line in enumerate(file):
            if 'atomnames' in line:
                atm_names=np.genfromtxt(inp_file, skip_header = n,max_rows=1,dtype=str)[1::3]
            if 'coordinates' in line:
                r=np.genfromtxt(inp_file, skip_header = n,max_rows=1,dtype=float)[1:]
                n_atm=len(r)//3
                r=np.array(np.split(r,n_atm))
            if 'mode' in line:
                mode_name_list.append(int(line.split()[1]))
                modes.append(np.array(np.split(np.genfromtxt(inp_file, skip_header = n,max_rows=1,dtype=float)[2:],n_atm)))


    modes=np.array(modes)

    return modes,mode_name_list,r,atm_names

############################################################################%%

def main (vel_list,select_modes_file):

    modes,modes_name_list,r,atm_names=modes_modes_name_r_atm_names_from_nmd (select_modes_file)
    modes=mol.normal(modes)

    v=vel_gen(vel_list,modes)





    mol.write_matrix_to_file(v,"&VELOCITY","&END VELOCITY",vel_out_file)













###########################################################################%%


if __name__== "__main__":

    select_modes_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/0pctts1irc_select_cc.nmd"
    vel_out_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/vel.out"

    modes,modes_name_list,r,atm_names=modes_modes_name_r_atm_names_from_nmd (select_modes_file)
    modes=mol.normal(modes)
    Q=modes2Q(modes,atm_names)[0]

    w_211=0.0161891140539

    n=6

    V=np.sqrt((2*n+1)*w_211/(np.sum(Q[:,4]**2)*mol.amu2au_mass))



    vel_list=np.array([0, 0, 0, 0, 1, 0])*V













###%%
e=mol.extract_pattern("/home/ar612/Documents/Work/cp2k_organized/reac_path/max_dr_0.0002_pctreac2_pctts2_pctprod2_band_40rep_dftb_2411805.out","BAND TOTAL ENERGY [au]        =",5)



plt.plot(e-e[0],"o")
