
#%%
import sys


def read_matrix_from_file(file_name, nformat):
    with open(file_name, 'r') as fd_read:
        ret_mxt = [list(map(nformat, line.split())) for line in fd_read]
    return ret_mxt

def write_matrix_to_file(nmatrix, file_name):
    save_str = '\n'.join(['   '.join(map(str, i)) for i in nmatrix])
    with open(file_name, 'w') as fd_write:
        fd_write.write(save_str)


def reduce_matrix_file(matrix_file,max_n,max_m,out_file):

    matrix=read_matrix_from_file(matrix_file,float)

    final_matrix=[row[:max_m] for row in matrix[:max_n]]

    write_matrix_to_file(final_matrix,out_file)






if __name__== "__main__":

    # state_index_file="/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/cp_rest_states_index.txt"
    # eigenvec_file="/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_10_ny_20/eigenvec.txt"
    # emax=18
    # vec="row"

    matrix_file=sys.argv[1]
    max_n=int(sys.argv[2])
    max_m=int(sys.argv[3])
    out_file=sys.argv[4]



    reduce_matrix_file(matrix_file,max_n,max_m,out_file)
