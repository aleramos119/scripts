
#%%
import numpy as np
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import quantum as quan
from matplotlib.widgets import Slider
import time
from scipy import integrate


def t_psi_irep2_prop(file):
    """Reads the output of irep2_prop.

    Parameters
    ----------
    file : string
        The file output from irep2_prop.

    Returns
    -------
    1d array, 2d array
        Time steps and the wavefunction at each step.

    """
    dat=np.genfromtxt(file).T
    t=dat[0]

    Ntot=(len(dat)-1)//2

    psin=dat[1:Ntot+1]+1j*dat[Ntot+1:]

    return t,psin


def psi_irep2_psopt(psi_file):
    """Reads the output of irep2_psopt.

    Parameters
    ----------
    psi_file : string
        The file output from irep2_psopt.

    Returns
    -------
    2d array
        The wavefunction at each time step.

    """

    dat=np.genfromtxt(psi_file).T

    Ntot=(len(dat))//2

    psin=dat[:Ntot]+1j*dat[Ntot:]

    return psin








def eigen_r(psi1_file):
    """Reads the grid points and eigenfunctions.

    Parameters
    ----------
    psi1_file : string
        File where grid points are in the first column and the rest of columns are the eigenstates.

    Returns
    -------
    1d array, 2d array
        Grid points and eigenfunctions.

    """

    psi1r_dat=np.genfromtxt(psi1_file)

    r1=psi1r_dat[:,0]

    psi1r=psi1r_dat[:,1:]

    return r1,psi1r




def electronic_pop(psi,N_list):
    """Reads the output of irep2_psopt.

    Parameters
    ----------
    psi: 2d array
        eigenstates populations as function of time.

    N_list: 1d array
        list of integers of the number of eigenstates on each electronic state

    Returns
    -------
    2d array
        The electronic populations as a function of time.

    """

    nstates=len(psi)
    nsteps=len(psi[0])
    n_estates=len(N_list)

    Nsum_list=np.insert(np.cumsum(N_list),0,0)

    pop=quan.populations(psi)

    e_pop=np.zeros([n_estates,nsteps])


    for i in range(n_estates):
        e_pop[i]=np.sum(pop[Nsum_list[i]:Nsum_list[i+1]],axis=0)

    return e_pop

#
#
# def slide(x,y_list,xlimits,ylimits,labels,axlabels=['x','y','z'],marker='-'):
#
#     ###Ploting the first figure
#     ##%%
#     cmap = plt.get_cmap("tab10")
#     fig, ax = plt.subplots(2, 1,figsize=(12,9),gridspec_kw={'height_ratios': [10,1]})
#     for i,y in enumerate(y_list):
#         ax[0].plot(x,y[0],marker,label=labels[i],color=cmap(i))
#     # ax[0].legend()
#     # ax[0].set_xlim(xlimits[0],xlimits[1])
#     # ax[0].set_ylim(ylimits[0],ylimits[1])
#     # ax[0].set_xlabel(axlabels[0],fontweight='bold')
#     # ax[0].set_ylabel(axlabels[1],fontweight='bold')
#
#
#     # Slider
#     sz_val = Slider(ax[1], axlabels[2], 0, len(y)-1, valinit=0,valstep=1)
#
#     def update(val):
#         z_val = sz_val.val
#
#         for line in ax[0].get_lines(): # ax.lines:
#             line.remove()
#         for i,y in enumerate(y_list):
#             #ax[0].lines[j].remove()
#             print(i)
#             #ax[0].plot(x,x**2,marker,color=cmap(i))
#
#     sz_val.on_changed(update)
#
#
#
#



###########%%
system="mu0_8.659702836473983_e0_0.00871590046354287_N1_30_N2_10_first1_25_imax1_500_di1_100_first2_5_imax2_500_di2_100"

wunit=0.00871590046354287
muunit=8.659702836473983
tunit=1/wunit
Eunit=wunit/muunit

N1=30
N2=10
step=10

root_dir="/home/ar612/Documents/Work/HD+"

heaviside_1=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_heaviside_1.txt")
heaviside_2=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_heaviside_2.txt")
ground_1=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_ground_1.txt")
ground_2=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_ground_2.txt")
disp_ground_1=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_disp_ground_1.txt")
disp_ground_2=psi_irep2_psopt(root_dir+"/matrices/"+system+"/cp_disp_ground_2.txt")

##%%
list=["20","40","60","80"]
psif1=heaviside_1
psif2=heaviside_2
folder="init_cost_0.2_nnodes_500_ground_2_heaviside_1_T_2_umax_100_reduced"
fig, ax = plt.subplots(figsize=(16,9))
for i in list:
    #t,psin=t_psi_irep2_prop("/home/ar612/Documents/git_kraken/irep2_prop/results/t_Yt_0.txt")
    psin=psi_irep2_psopt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+i+"/x.dat")
    t=np.genfromtxt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+i+"/t.dat")*tunit*mol.au2fs
    cf1=abs(np.matmul(psif1,psin))**2
    cf2=abs(np.matmul(psif2,psin))**2
    ax.plot(t,cf1,'-o',label="psif1_"+i)
    ax.plot(t,cf2,'-',label="psif2_"+i)
    ax.set_title(folder)
ax.legend()




##%%
rest="20"


t=np.genfromtxt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+rest+"/t.dat")*tunit*mol.au2fs
u=np.genfromtxt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+rest+"/u.dat")*Eunit


fig, ax = plt.subplots(figsize=(16,9))
ax.plot(t,u,'o-')
ax.set_xlabel("t(fs)")
ax.set_ylabel("E(au)")



##%%
epop=electronic_pop(psin,[N1,N2])

fig, ax = plt.subplots(figsize=(16,9))

ax.plot(t,epop[0],label="e_pop_0")
ax.plot(t,epop[1],label="e_pop_1")
ax.legend()
##%%









####%%
######## This section is to plot the energy representation of the wf
psi1n_2=np.multiply(psi1n,np.conj(psi1n)).real
psi2n_2=np.multiply(psi2n,np.conj(psi2n)).real


cmap = plt.get_cmap("tab10")

fig0, ax0 = plt.subplots(3, 1,figsize=(12,27))
for i in range(N1):
    ax0[0].plot(t,psi1n_2[i],'-',label=r'${|C_'+str(i)+'^{(1)}|}^2$',color=cmap(i))
ax0[0].set_ylabel(r'${|C_n^{(1)}|}^2$',fontweight='bold')
ax0[0].legend()
for i in range(N2):
    ax0[1].plot(t,psi2n_2[i],'--',label=r'${|C_'+str(i)+'^{(2)}|}^2$',color=cmap(i))
ax0[1].set_ylabel(r'${|C_n^{(2)}|}^2$',fontweight='bold')
ax0[1].legend()

ax0[2].plot(t_field,e_field,'-')
ax0[2].set_xlabel("t (w12^(1))",fontweight='bold')
ax0[2].set_ylabel(r'E(t)',fontweight='bold')
###########%%









###%%
#### This section is to plot the space representation of the wf


###########%%
step=10
N1=30
N2=10

wunit=0.00871590046354287
muunit=8.659702836473983
tunit=1/wunit
Eunit=wunit/muunit

#system="mu0_8.659702836473983_e0_0.00871590046354287_N1_500_N2_500_first1_25_imax1_500_di1_1_first2_5_imax2_500_di2_1"
#t_dat,psin=t_psi_irep2_prop("/home/ar612/Documents/Work/HD+/irep_prop/ground_2_forward_40fs/t_Yt_0.txt")


folder="init_cost_0_nnodes_500_ground_2_heaviside_2_T_2_umax_100_reduced"
psin=psi_irep2_psopt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+rest+"/x.dat")
t_dat=np.genfromtxt(root_dir+"/irep2/test/"+folder+"/"+folder+"_0.00000"+rest+"/t.dat")

psi1r_dat=np.genfromtxt("/home/ar612/Documents/Work/HD+/matrices/"+system+"/psi1.txt")
psi2r_dat=np.genfromtxt("/home/ar612/Documents/Work/HD+/matrices/"+system+"/psi2.txt")

t=t_dat*tunit*mol.au2fs

psi1n=psin[:N1].T
psi2n=psin[N1:].T

psi1n_pop=quan.populations(psi1n)
psi2n_pop=quan.populations(psi2n)


r1=psi1r_dat[:,0]
r2=psi2r_dat[:,0]

psi1r=psi1r_dat[:,1:]
psi2r=psi2r_dat[:,1:]



psi1rt=np.matmul(psi1r,psi1n.T)
psi2rt=np.matmul(psi2r,psi2n.T)

psi1rt_2=quan.populations(psi1rt).T
psi2rt_2=quan.populations(psi2rt).T


##%%


x_list=[r1,r2]
y_list=[psi1rt_2,psi2rt_2]
xlimits=[0,30]
ylimits=[0,2]
labels=['1','2']
axlabels=['r',r'$|\psi(r)|^2$','t_step']
marker='-'


cmap = plt.get_cmap("tab10")
fig, ax = plt.subplots(2, 1,figsize=(12,9),gridspec_kw={'height_ratios': [10,1]})
for i,y in enumerate(y_list):
    ax[0].plot(x_list[i],y[0],marker,label=labels[i],color=cmap(i))


ax[0].legend()
ax[0].set_xlim(xlimits[0],xlimits[1])
ax[0].set_ylim(ylimits[0],ylimits[1])
ax[0].set_xlabel(axlabels[0],fontweight='bold')
ax[0].set_ylabel(axlabels[1],fontweight='bold')


# Slider
sz_val = Slider(ax[1], axlabels[2], 0, len(y)-1, valinit=0,valstep=1)

def update(val):
    z_val = sz_val.val

    for line in ax[0].get_lines(): # ax.lines:
        line.remove()
    for i,y in enumerate(y_list):
        #ax[0].lines[j].remove()
        ax[0].plot(x_list[i],y[z_val],marker,color=cmap(i))

sz_val.on_changed(update)
###%%


20*mol.fs2au/tunit

dt=30000*0.0005*tunit*mol.au2fs
dt
40/dt
