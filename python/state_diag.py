#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 11:17:23 2022

@author: eric fischer
"""

from importlib import reload
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import os
import mol
import quantum as quant
%matplotlib qt
plt.style.use(['ggplot'])
import math
quant = reload(quant)



out_dir="/home/ar612/Documents/Work/cavities/H_transfer"
data_dir="/home/ar612/Documents/Work/cavities/H_transfer/data"

e0=0.0005764776761753108
mu0=0.0025174068699653565


e0_2_ps=mol.au2fs/(e0*1000)
ps_2_e0=1/e0_2_ps



eta="0.035"
sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_10_wc_w10_e0_0.0005764776761753108_mu0_0.0025174068699653565_neta_11/eta_"+eta

h11=np.genfromtxt(sys_dir+"/h11.txt")
d11=np.genfromtxt(sys_dir+"/d11.txt")
t11=np.genfromtxt(sys_dir+"/t11.txt")

if eta == "0.035":
    t_list=np.genfromtxt("/home/ar612/Documents/Work/cavities/par_opt/excitation_molecular_1/Emax1_10/neta_11/eta0_0.035_random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/3/eta0_0.035_t0_25/t.dat")
    u=np.genfromtxt("/home/ar612/Documents/Work/cavities/par_opt/excitation_molecular_1/Emax1_10/neta_11/eta0_0.035_random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/3/eta0_0.035_t0_25/u.dat")
if eta == "0.015":
    t_list=np.genfromtxt("/home/ar612/Documents/Work/cavities/no_par_opt_eta_scan_tf_free/excitation_molecular_1/Emax1_10/neta_11/random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/2/eta0_0.015_t0_25/t.dat")
    u=np.genfromtxt("/home/ar612/Documents/Work/cavities/no_par_opt_eta_scan_tf_free/excitation_molecular_1/Emax1_10/neta_11/random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/2/eta0_0.015_t0_25/u.dat")


nt=len(t_list)

nstates=len(h11)


ener=np.zeros([nt,nstates])


for i,t in enumerate(t_list):
    ener[i]=np.linalg.eigvalsh(h11-d11*u[i], UPLO='L')


ener=ener-ener[0,0]
ener=ener/ener[0,1]

##%%
plt.rc('font', size=20)
for i in range(3):
    for j in range(i):
        plt.plot(t_list*e0_2_ps,ener[:,i]-ener[:,j],label="e_"+str(i)+"-e"+str(j))
plt.xlabel("t(ps)")
plt.ylabel("e(w10)")
plt.title("eta_"+eta)
plt.legend()
