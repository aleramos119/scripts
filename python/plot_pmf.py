#%%
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules/')
import mol
import numpy as np
from itertools import islice
from timeit import default_timer as timer
%matplotlib qt
import matplotlib.pyplot as plt

def form_dD(r,dr,atm_list):

    n_images=len(r)
    n_atm=len(atm_list)
    n_cons=(n_atm**2-n_atm)/2


    D=np.zeros([n_images,n_cons])
    dD=np.zeros([n_images-1,n_cons])



    for k in range(0,n_images):

        n=0
        for i,img_i in enumerate(atm_list):
            for j,img_j in enumerate(atm_list):
                if img_i < img_j:
                    D[k,n]=np.linalg.norm(r[k,i]-r[k,j])  ## Here I calculate the elements of the distance matrix

                    if k < n_images-1:
                        dD[k,n]=np.sum( (r[k,i]-r[k,j])*(dr[k,i]-dr[k,j]) ) / D[k,n] ## Here I calculate the elements of the variation of the distance matrix
                                                                                         ## as a function of the variations of the position vectors

                    n=n+1

    return D,dD

def dD_lag_mult_projection(dD,lag_mult):

    T=len(lag_mult) ## The number of time steps

    dH_dchi=np.empty([T]) ## In this array I storage the force along the reaction path for each time step

    ## Calculating the projection
    for j in range(0,T):
        dH_dchi[j]=np.dot(dD,lag_mult[j])

    return dH_dchi

def compute_free_ener(dH_along_path,std_along_path,n_images):

    free_ener=np.zeros([n_images])
    std_free_ener=np.zeros([n_images])


    sum=0
    free_ener[0]=0
    std_free_ener[0]=0
    for i in range(1,n_images):
        sum= sum + dH_along_path[i-1]
        free_ener[i]=sum
        std_free_ener[i]=np.sqrt(np.sum(std_along_path[0:i]**2))


    return free_ener,std_free_ener







#########################################################%%

if __name__== "__main__":


        ####################################################%%
        cp2k_dir="/home/ar612/Documents/cp2k_organized"

        job="solv"

        name_append="_pmf"

        reac_path_file="&&/reac_path/r20_ts_p_80_rep/r20_ts_p_80_rep_movie.xyz"
        free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_free_ener.txt"
        std_free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_std_free_ener.txt"
        dh_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!__dH__$$.txt"



        reac_path_file=reac_path_file.replace("&&",cp2k_dir)
        free_ener_out_file=free_ener_out_file.replace("&&",cp2k_dir)
        std_free_ener_out_file=std_free_ener_out_file.replace("&&",cp2k_dir)
        dh_file_pattern=dh_file_pattern.replace("&&",cp2k_dir)



        reac_path_file=reac_path_file.replace("**",job)
        free_ener_out_file=free_ener_out_file.replace("**",job)
        std_free_ener_out_file=std_free_ener_out_file.replace("**",job)
        dh_file_pattern=dh_file_pattern.replace("**",job)



        reac_path_file=reac_path_file.replace("!!",name_append)
        free_ener_out_file=free_ener_out_file.replace("!!",name_append)
        std_free_ener_out_file=std_free_ener_out_file.replace("!!",name_append)
        dh_file_pattern=dh_file_pattern.replace("!!",name_append)


        ## Here I read the geometries of the reaction path to calculate latter dr and dist
        e_solv,r_solv,atm_names_solv=mol.e_r_atm_names_from_xyz (reac_path_file)

        im_list=np.array(range(30,46))-1 ##The list of images that I will use to compute free Energy
        atm_list=np.array([1,9,2,28,27])-1 ##List of atom that are going to use to construct D and dD

        ## Keping only frames and atoms that I'm interested in
        e_solv=e_solv[im_list]
        pmf_solv=np.genfromtxt(free_ener_out_file,unpack= True, usecols={0})
        atm_names_solv=atm_names_solv[atm_list]
        r_solv=r_solv[im_list][:,atm_list]






        n_atm=len(atm_names_solv)
        n_images=len(e_solv)
        dr,dist=mol.path_dist(r_solv)
        n_cons=(n_atm**2-n_atm)/2  ## Number of constraint in the simulation. All distances between atoms

        ####################################################%%


        ####################################################%%
        job="equi"

        name_append="_5_atm_cons_pmf"

        reac_path_file="&&/reac_path/r20_ts_p_80_rep/r20_ts_p_80_rep_movie.xyz"
        free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_free_ener.txt"
        std_free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_std_free_ener.txt"
        dh_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!__dH__$$.txt"



        reac_path_file=reac_path_file.replace("&&",cp2k_dir)
        free_ener_out_file=free_ener_out_file.replace("&&",cp2k_dir)
        std_free_ener_out_file=std_free_ener_out_file.replace("&&",cp2k_dir)
        dh_file_pattern=dh_file_pattern.replace("&&",cp2k_dir)



        reac_path_file=reac_path_file.replace("**",job)
        free_ener_out_file=free_ener_out_file.replace("**",job)
        std_free_ener_out_file=std_free_ener_out_file.replace("**",job)
        dh_file_pattern=dh_file_pattern.replace("**",job)



        reac_path_file=reac_path_file.replace("!!",name_append)
        free_ener_out_file=free_ener_out_file.replace("!!",name_append)
        std_free_ener_out_file=std_free_ener_out_file.replace("!!",name_append)
        dh_file_pattern=dh_file_pattern.replace("!!",name_append)

        ## Here I read the geometries of the reaction path to calculate latter dr and dist
        e_gas,r_gas,atm_names_gas=mol.e_r_atm_names_from_xyz (reac_path_file)


        ## Keping only frames and atoms that I'm interested in
        e_gas=e_gas[im_list]
        pmf_gas=np.genfromtxt(free_ener_out_file,unpack= True, usecols={0})
        atm_names_gas=atm_names_gas[atm_list]
        r_gas=r_gas[im_list][:,atm_list]

        ####################################################%%
        plt.style.use(['ggplot'])
        plt.plot(dist,pmf_gas,'-o',label="gas phase")
        plt.plot(dist,pmf_solv,'-o',label="solvated phase")
        plt.xlabel("reac_path (A)",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel('E (kcal/mol)',fontweight='bold')
        plt.grid(True)
        plt.legend()
        plt.show()
        ####################################################%%



        dh_file="/home/ar612/Documents/cp2k_organized/pmf/r20_ts_p_80_rep/solv/39_r20_ts_p_80_rep_solv_pmf/39_r20_ts_p_80_rep_solv_pmf__dH__39.txt"
        dH_list=mol.fast_load(dh_file,[0]).T[0]

        avg=dH_list[0]
        dH_avg=np.zeros([len(dH_list)])
        dH_avg[0]=avg
        for i in range(1,len(dH_list)):
            avg=i*avg/(i+1)+dH_list[i]/(i+1)
            dH_avg[i]=avg


        plt.plot(dH_avg)
