#! /bin/python3
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
import mol
#%matplotlib qt


def lagrange(x,t,f):
    N=len(t)
    px=0
    for k in range(N):
        Lk=1
        for i in range(N):
            if(i!=k):
                Lk=Lk*(x-t[i])/(t[k]-t[i])
        px=px+f[k]*Lk
    return px


def main(t_file,u_file):
    ##################################################################################################################%%

    tk=np.genfromtxt(t_file)
    uk=np.genfromtxt(u_file)

    t=np.linspace(tk[0],tk[-1],10000)
    u=np.array([lagrange(i,tk,uk) for i in t])

    t_name_list=t_file.split(".")
    t_name_list[-2]=t_name_list[-2]+"_lagrange"
    t_final_file='.'.join(t_name_list)

    u_name_list=u_file.split(".")
    u_name_list[-2]=u_name_list[-2]+"_lagrange"
    u_final_file='.'.join(u_name_list)

    mol.write_matrix_to_file(np.array([t]).T,"","",t_final_file)
    mol.write_matrix_to_file(np.array([u]).T,"","",u_final_file)

    plt.plot(t,u,'-')
    plt.plot(tk,uk,'o')
    plt.tick_params(labelsize=14)
    plt.grid(True)
    plt.show()



if __name__== "__main__":

    # u_file="/home/ar612/Documents/Work/irep/test/collocation/fermi_res_gamma_0.5_w1_2_w2_1_nx_3_ny_6_T_90/Legendre/Legendre_140/u.dat"
    # t_file="/home/ar612/Documents/Work/irep/test/collocation/fermi_res_gamma_0.5_w1_2_w2_1_nx_3_ny_6_T_90/Legendre/Legendre_140/t.dat"

    t_file="t.dat"
    u_file="u.dat"


    main(t_file,u_file)
