
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np

#%%



############################################################################%%


def main (inp_file,out_file):


    ## Reading cartesian normal modes and calculating mass_weighted normal modes
    r,atm_names=mol.r_atm_names_from_g96(inp_file)

    atm_names_xyz = [mol.remove_digits(name) for name in atm_names]


    mol.write_trajectory(np.array([r]),np.zeros([1]),atm_names_xyz,out_file)



###########################################################################%%


if __name__== "__main__":

    # inp_file="/home/ar612/Documents/cp2k_organized/XYZ/thf.g96"
    #
    # out_file="/home/ar612/Documents/cp2k_organized/XYZ/thf.xyz"

    inp_file=sys.argv[1]
    out_file=sys.argv[2]

    main(inp_file,out_file)
