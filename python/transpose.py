#! /usr/bin/python3
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol


def main (inp_file):
    mat=np.genfromtxt(inp_file)
    mol.write_matrix_to_file(mat.T,"","",inp_file,format='%.20e')

###########################################################################%%


if __name__== "__main__":

    ##%%
    inp_file=sys.argv[1]

    main(inp_file)
