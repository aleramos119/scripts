
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
%matplotlib qt
plt.style.use(['ggplot'])
from matplotlib.widgets import Slider



############################################################################%%
ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/pctreac1_vib_52_vib_204_205_-206_nx_-1_1_60_ny_-0.5_4_60_dftb_eps_10e-6_ref_traj/pctreac1_vib_52_vib_204_205_-206_nx_-1_1_60_ny_-0.5_4_60_dftb_eps_10e-6_ref_traj-pos-1.xyz"

m1=52
m2=204

x_max=1
x_min=-1
nx=60

y_max=4
y_min=-0.5
ny=60



sat=150


e,r,atm_names=mol.e_r_atm_names_from_xyz (ref_traj_inp_file)

e=e*mol.Eh2kcalmol
emin=min(e)
e=e-emin


for i,x in enumerate(e):
    if x > sat:
        e[i]=sat


x=np.linspace(x_min, x_max, num=nx)
y=np.linspace(y_min, y_max, num=ny)




e_x_y=np.asarray(np.split(e,nx))



##%%

l=20
fig=plt.figure(figsize=(10,10))
ax=plt.subplot(111)
plt.axis([y_min,y_max,x_min, x_max])
plt.contourf(y,x,e_x_y,l,cmap='rainbow',alpha=.55,vmin=0, vmax=sat)
cbar=plt.colorbar()
cbar.ax.set_title('kcal/mol')
C=ax.contour(y,x,e_x_y,l, colors='black', linewidths=0.6)
ax.clabel(C, inline=1, fontsize=10)
plt.xlabel("w_"+str(m2),fontweight='bold')
plt.ylabel("w_"+str(m1),fontweight='bold')
plt.grid(True)
plt.tick_params(labelsize=14)


##%%











###########################################################################%%



    ##%%

    main(modes_inp_file,m1,m2,n,sat)
