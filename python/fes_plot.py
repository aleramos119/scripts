
#%%

import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])


def read_fes_data(fes_file):

    nthf0=-1
    dim=2
    name_list=[]
    nbin_list=[]
    periodic_list=[]

    with open(fes_file) as file:
       for n, line in enumerate(file, 1):
           if "FIELDS" in line:
               dim=int((len(line.split())-3)/2)
               name_list=line.split()[2:2+dim]
               nbin_list=[]
               periodic_list=[]
           for i,name in enumerate(name_list):
               if name in line and "nbins" in line:
                   nbin_list.append(int(line.split()[-1]))
               if name in line and "periodic" in line:
                   periodic_list.append(line.split()[-1])
           if n > dim*4:
               break

    dat=mol.fast_load(fes_file,range(0,dim+1))

    return dim,name_list,nbin_list,periodic_list,dat

def main(fes_file,n_level):
    ##################################################################################################################%%
    dim,name_list,nbin_list,periodic_list,dat=read_fes_data(fes_file)

    x=dat[:,0]
    y=dat[:,1]
    fes=dat[:,2]

    ##%%
    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    plt.tricontourf(x,y,fes,n_level,cmap='rainbow',alpha=.55)
    cbar=plt.colorbar()
    C=ax.tricontour(x,y,fes,n_level, linewidths=0.3,colors='black')
    ax.clabel(C, inline=1, fontsize=10)
    #plt.plot(x,y,'.')
    plt.xlabel(name_list[0],fontweight='bold')
    plt.ylabel(name_list[1],fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    plt.show()
    fig.savefig("fes_"+name_list[0]+"_"+name_list[1]+".pdf", bbox_inches='tight')
    #%%





if __name__== "__main__":

    fes_file="/home/ar612/Documents/Work/practice/p_hnco1_coch2_meta_dyn_1_reduced/fes.dat"
    fes_file='/home/ar612/Documents/Work/quantics/test/potfit/potfit_test/data.xyz'
    fes_file=sys.argv[1]
    n_level=int(30)



    main(fes_file,n_level)
