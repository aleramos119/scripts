##%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib qt
plt.style.use(['ggplot'])
from scipy.integrate import simps
##%%


##%%
def V_double(x,vmax,a):
    return vmax*((x/a)**2-1)**2

def V_harm(x,vmax,a):
    k=8*vmax*a**-2
    return (0.5*k)*x**2

def w(vmax,a,m,conv=1):
    return np.sqrt(8*vmax/(a**2*m))*conv

def mu_lin(q,x):
    return q*x

def mu_quad(q,x):
    return q*x**2

##%%

def expect(psi,x):
    nodes=len(psi)
    Emax=int(len(psi[0])/2)

    expec_x=np.zeros(nodes)

    for i in range(nodes):
        expec_x[i]=0
        for n in range(Emax):
            for m in range(Emax):
                expec_x[i]=expec_x[i] + ( psi[i,n]*psi[i,m] +  psi[i,n+Emax]*psi[i,m+Emax] )*x[n,m]

    return expec_x


def main(project_name,Emax):


    psi=np.genfromtxt(psi_file,dtype=np.float)
    t=np.genfromtxt(t_file,dtype=np.float)
    tfs=t*mol.au2fs
    x=np.genfromtxt(x_file,dtype=np.float)
    x2=np.genfromtxt(x2_file,dtype=np.float)

    Emax=int(len(psi[0])/2)

    expect_x=expect(psi,x)
    expect_x2=expect(psi,x2)

    sigma=np.sqrt(expect_x2 - expect_x**2)


    matrix=np.array([tfs,expect_x,sigma]).T

    out_file="/".join(psi_file.split("/")[:-1]) + "/qdq_1_1.pl"

    mol.write_matrix_to_file(matrix,"","",out_file)


    #plt.matshow(muij_lin)
    #muij_lin[1,1]
##%%

if __name__== "__main__":

    ##%%
    t_file="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/t.dat"
    psi_file="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/x.dat"
    x_file="/home/ar612/Documents/Work/fgh/double_well/eigen_states/1_hmass/1_hmass_muij_qx.txt"
    x2_file="/home/ar612/Documents/Work/fgh/double_well/eigen_states/1_hmass/1_hmass_muij_qx2.txt"


    ##%%


    main(project_name,Emax)
