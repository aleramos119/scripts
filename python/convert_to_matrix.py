
#%%

import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

file="/home/ar612/Documents/Work/HD+/coupling_terms/ene1.dat"

##%%
def to_matrix(file):
    mat_read=np.genfromtxt(file)

    disp=int(mat_read[0,1])

    tot=len(mat_read)

    n=int(mat_read[-1,0])+int(not disp)
    m=int(mat_read[-1,1])+int(not disp)

    mat=np.empty([n,m])

    for l in range(tot):
        i=int(mat_read[l,0])-disp
        j=int(mat_read[l,1])-disp
        val=mat_read[l,2]
        mat[i,j]=val

    return mat


def e_to_matrix(file):
    mat_read=np.genfromtxt(file)

    return np.diag(mat_read[:,1])


##%%

matrix_dir="/home/ar612/Documents/Work/HD+/matrices"
couplings_dir="/home/ar612/Documents/Work/HD+/coupling_terms"
d11_file="/home/ar612/Documents/Work/HD+/coupling_terms/D11.dat"
d12_file="/home/ar612/Documents/Work/HD+/coupling_terms/D12.dat"
d22_file="/home/ar612/Documents/Work/HD+/coupling_terms/D22.dat"
p12_file="/home/ar612/Documents/Work/HD+/coupling_terms/P12_real.dat"
e1_file="/home/ar612/Documents/Work/HD+/coupling_terms/ene1.dat"
e2_file="/home/ar612/Documents/Work/HD+/coupling_terms/ene2.dat"



##%%
d11_au=to_matrix(d11_file)
d12_au=to_matrix(d12_file)
d22_au=to_matrix(d22_file)
p12_au=to_matrix(p12_file)
e1_au=e_to_matrix(e1_file)
e2_au=e_to_matrix(e2_file)
##%%
e0=e1_au[1,1]-e1_au[0,0]
#mu0=max(d11_au[:500,:500].flatten())
mu0=1

d11=d11_au/mu0
d12=d12_au/mu0
d22=d22_au/mu0

p12=p12_au/e0

e1=e1_au/e0
e2=e2_au/e0
##%%
sys_dir=matrix_dir+"/mu0_"+str(mu0)+"_e0_"+str(w21)
os.mkdir(sys_dir)
##%%
sys_dir=matrix_dir+"/mu0_"+str(mu0)+"_e0_"+str(w21)
mol.write_matrix_to_file(d11,"","",sys_dir+"/D11.dat")
mol.write_matrix_to_file(d12,"","",sys_dir+"/D12.dat")
mol.write_matrix_to_file(d12.T,"","",sys_dir+"/D21.dat")
mol.write_matrix_to_file(d22,"","",sys_dir+"/D22.dat")
mol.write_matrix_to_file(p12,"","",sys_dir+"/P12.dat")
mol.write_matrix_to_file(p12.T,"","",sys_dir+"/P21.dat")
mol.write_matrix_to_file(p12,"","",sys_dir+"/Q12.dat")
mol.write_matrix_to_file(p12.T,"","",sys_dir+"/Q21.dat")
mol.write_matrix_to_file(e1,"","",sys_dir+"/ene1.dat")
mol.write_matrix_to_file(e2,"","",sys_dir+"/ene2.dat")
