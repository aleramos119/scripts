
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
import mol


def main(inp_file):
    ##################################################################################################################%%


    dat=np.genfromtxt(inp_file)
    maxi=max(abs(dat).flatten())

    ax = plt.subplot()
    cbar=ax.matshow(dat,vmax=maxi,vmin=-maxi)
    plt.colorbar(cbar)
    plt.show()


if __name__== "__main__":

    inp_file="/home/ar612/Documents/Work/pyemma/test/colvar_diff/correlation_tica1.txt"

    inp_file=sys.argv[1]

    main(inp_file)
