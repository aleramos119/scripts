
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/ar612/Documents/Python_modules/')
import mol
from scipy import interpolate
from scipy import optimize
from scipy import integrate
from matplotlib import cm
import matplotlib
plt.style.use(['ggplot'])
import matplotlib.gridspec as gridspec



def find_roots(f,val,int,res=2000):
    list=np.array([[i,f(i)-val] for i in np.linspace(int[0],int[1],res)])
    x0=np.array([list[i,0]  for i in range(1,len(list)) if (np.sign(list[i-1,1])-np.sign(list[i,1]))!=0])
    x1=np.array([optimize.newton(lambda x: e_int(x)-val, x0=x)   for x in x0 ])
    return x1

def theta(e,e_int,m,int):
    q1,q2=find_roots(e_int,e,int)
    i=integrate.quad(lambda x: np.sqrt(2*m*np.abs(e_int(x)-e)),q1,q2)
    return i

def Psc(e,theta_int,vclas,vmax,err):
    if e < vclas:
        res=0
    elif vclas <= e < vmax-err:
        #res=0
        res=1/(1.+np.exp(2*theta_int(e)))
    else:
        res=1./2
    return res


def integ(f,xi,xf):
    i=integrate.quad(f,xi,xf)
    return i





#########################################################################%%
def plot_e(dist,e,project,dh):
    ##%%
    ## Maximum and minimum values of the scale
    max=np.max(dh)
    min=np.min(dh)

    ## Creating an array of colors map scaled from 0 to 1 and a norm instance to get the correct scale in the colorbar
    map=cm.rainbow(dh/max)
    norm = matplotlib.colors.Normalize(vmin=min,vmax=max)

    ## Ploting
    %matplotlib qt
    plt.style.use(['ggplot'])

    ## Here I define the grid to set the relative sizes between plot and color bar
    gs = gridspec.GridSpec(1, 8)

    fig=plt.figure(figsize=(10, 8))
    ax1=fig.add_subplot(gs[0, :7])
    for i in range(0,len(dh)):
        ax1.plot(dist[i],e[i],'b-o',linewidth=2.5,color=map[i] )
    ax1.set_xlabel(project+"(a0)",fontweight='bold')
    ax1.tick_params(labelsize=14)
    ax1.set_ylabel('E (Eh)',fontweight='bold')
    ax1.grid(True)
    ax2=fig.add_subplot(gs[0, 7:8])
    matplotlib.colorbar.ColorbarBase(ax2, cmap='rainbow',norm=norm, drawedges=False, label=r'$\frac{|\Delta r_h|}{|\Delta R|}$')
    fig.savefig(project+"_ener.pdf", bbox_inches='tight')
    fig.show()
    ##%%
    



############################################################################%%


def main (xyz_inp_file,project,n_atm,vclas_scale,vmax_scale,mass_scale):

    ###%%
    vclas_scale=1
    vmax_scale=1
    mass_scale=2

    e,r_a,atm_names=mol.e_r_atm_names_from_xyz(xyz_inp_file,n_atm)
    ## Expresing distances in au
    k_au=mol.k*mol.J2Eh
    r=r_a*mol.A2au

    ## Scaling the energy
    e=(e-e[0])
    e[:19]=e[:19]*vclas_scale
    e[19]=e[19]*vmax_scale
    vclas=e[18]
    n_images=len(e)

    e_kcalmol=e*mol.Eh2kcalmol






    #Calculating distance along the reac path
    dr,dist=mol.path_dist(r)
    dr_a,dist_a=mol.path_dist(r_a)

    min_dist=0
    max_dist=np.max(dist)

    min_dist_a=0
    max_dist_a=np.max(dist_a)


    ## Calulating the relation between the displacements of h and the rest of atoms
    #dh=np.array([ np.linalg.norm(dr[i,disp_atm])/np.linalg.norm(dr[i,np.arange(n_atm)!=disp_atm]) for i in range(0,n_images-1)])
    disp_atm=27
    dh=np.array([ np.linalg.norm(dr[i,disp_atm])/np.linalg.norm(dr[i]) for i in range(0,n_images-1)])
    dh=np.append(dh,dh[-1])



    #Interpolating the reaction path profile
    dh_int=interpolate.interp1d(dist, dh, kind='quadratic')

    e_int=interpolate.interp1d(dist, e, kind='quadratic')
    e_int_kcalmol=interpolate.interp1d(dist_a, e_kcalmol, kind='quadratic')

    dist_new=np.linspace(min_dist, max_dist,1000)
    dist_new_a=np.linspace(min_dist_a, max_dist_a,1000)

    vmax=np.max(e_int(dist_new))
    ##%%

    ###%%
    fig=plt.figure(figsize=(10, 8))
    plt.figure(fig.number)
    plt.plot(dist_new_a,e_int_kcalmol(dist_new_a),label=str(vclas_scale)+"_"+str(vmax_scale)+"_"+str(mass_scale))
    plt.plot(dist_a,e_kcalmol,'o')
    plt.xlabel("reac_pat (A)")
    plt.ylabel(r"$E (kcal/mol)$")
    plt.legend(title="(vclas_scale)_(vmax_scale)_(mass_scale)")
    plt.savefig("/home/ar612/"+project+"_reac_path.pdf", bbox_inches='tight')
    plt.show()
    ###%%

    #plot_e(dist_new,e_int(dist_new),project,dh_int(dist_new))



    ##Evaluating theta function an interpolating it
    ###%%
    err=0.000004
    mass=1836*mass_scale
    t=np.array([[ener,theta(ener,e_int,mass,[min_dist,max_dist])[0]] for ener in np.linspace(vclas,vmax-err,50)])
    theta_int=interpolate.interp1d(t[:,0], t[:,1], kind='quadratic')
    e_new=np.linspace(vclas, vmax-err,1000)
    ## Ploting theta function
    ###%%
    fig2=plt.figure(figsize=(10, 8))
    plt.figure(fig2.number)
    plt.plot(e_new,theta_int(e_new),label=str(vclas_scale)+"_"+str(vmax_scale)+"_"+str(mass_scale))
    plt.plot(t[:,0], t[:,1],'o')
    plt.xlabel(r'$E (E_h)$')
    plt.ylabel(r'$\theta$(E)')
    plt.legend(title="(vclas_scale)_(vmax_scale)_(mass_scale)")
    plt.savefig("/home/ar612/"+project+"_theta.pdf", bbox_inches='tight')
    plt.show()
    ###%%



    # ## Evaluating and ploting Psc
    # for i in range(800,810,10):
    #     psc=np.array([[x,Psc(x,theta_int,vclas,vmax,err)*np.e**(-x/(k_au*i))] for x in np.linspace(0, 0.1,1000)])
    #     plt.plot(psc[:,0],psc[:,1],label=str(i))
    #     plt.legend()


    ## Evaluating and ploting int
    ##%%
    xi=vclas
    xf=0.1
    kqm=np.array([ [   T,  integ( lambda E: Psc(E,theta_int,vclas,vmax,err)*np.e**(-E/(k_au*T))          ,xi,xf)[0]  ]     for T in np.linspace(100, 600,100)])

    ### Calculating derivative
    kqm_der=np.array([[kqm[i,0], (  np.log(kqm[i+1,1]) - np.log(kqm[i,1])   )/(  1/(k_au*kqm[i+1,0]) - 1/(k_au*kqm[i,0])  ) ] for i in range(0,len(kqm)-1)])
    ##%%

    ##%%
    #fig3, ax = plt.subplots(figsize=(10, 8))
    plt.figure(fig3.number)
    ax.plot(1/(k_au*kqm[:,0]),np.log(kqm[:,1]),'-',label=str(vclas_scale)+"_"+str(vmax_scale)+"_"+str(mass_scale))
    ax.set_xlabel(r'$1/kT \quad (E_h^{-1})$')
    ax.set_ylabel(r'$\log(k) $')
    ax.set_xticks(np.append(range(500,3000+1,1000),np.array([1/(k_au*316),1/(k_au*279)])), minor=False)
    plt.legend(title="(vclas_scale)_(vmax_scale)_(mass_scale)")
    plt.savefig("/home/ar612/"+project+"_arrhenius.pdf", bbox_inches='tight')
    plt.show()
    ###%%


    ##%%
    #fig4, ax2 = plt.subplots(figsize=(10, 8))
    plt.figure(fig4.number)
    ax2.plot(kqm_der[:,0],-mol.Eh2kcalmol*kqm_der[:,1],'-',label=str(vclas_scale)+"_"+str(vmax_scale)+"_"+str(mass_scale))
    ax2.set_xlabel(r'$T (K) $')
    ax2.set_ylabel(r'$E_a (kcal/mol) $')
    ax2.set_xticks(np.append(range(100,600+1,150),np.array([316,279])), minor=False)
    plt.legend(title="(vclas_scale)_(vmax_scale)_(mass_scale)")
    plt.savefig("/home/ar612/"+project+"_Ea.pdf", bbox_inches='tight')
    plt.show()
    ###%%




###########################################################################%%


if __name__== "__main__":

    project="r20_ts1_p_band_b3lyp_40_rep"

    n_atm=33
    %matplotlib qt


    xyz_inp_file="/home/ar612/Documents/cp2k_organized/reac_path/"+project+"/"+project+"_movie.xyz"


    main(xyz_inp_file,project,n_atm)
