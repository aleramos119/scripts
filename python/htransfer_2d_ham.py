#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 11:17:23 2022

@author: eric fischer
"""

from importlib import reload
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import os
import mol
import quantum as quant
%matplotlib qt
plt.style.use(['ggplot'])
import math
quant = reload(quant)

au_to_fs=2.41888e-2
au_to_eV=27.2
au_to_cm=219474.63068
ang_to_au = 1.8897261
au_to_debye = 1/0.393456


out_dir="/home/ar612/Documents/Work/cavities/H_transfer"
data_dir="/home/ar612/Documents/Work/cavities/H_transfer/data"

e0=0.0005764776761753108
mu0=1.657


states = 31 # selected nr of states
N1=states
N2=1



wc="w10"

if wc=="w10":
    cavity_freq_0 = 0.0005764776761752717 # w10 = 0.0005764776761752717, w20 = 0.0040185785553936235
    trans_dip_10 = 0.04170531670450174 # d10 = 0.04170531670450174, d20 = 0.010498464836670468

if wc=="w20":
    cavity_freq_0 = 0.0040185785553936235 # w10 = 0.0005764776761752717, w20 = 0.0040185785553936235
    trans_dip_10 = 0.010498464836670468 # d10 = 0.04170531670450174, d20 = 0.010498464836670468



size_transfer_grid = 71
size_cavity_grid = 71


#-----------Read Data/ Define Grids ------------------
eta_list = np.linspace(0.000,0.05,11)

neta=len(eta_list)


cavity_freq_list=np.array([0.5,1.0])



for cavity_freq in cavity_freq_list:
    wc="w10_var_"+str(cavity_freq)
    cavity_freq=cavity_freq*cavity_freq_0

    if cavity_freq == 1.0:
        mixed_states=[1,2]
    if cavity_freq == 0.5:
        mixed_states=[2,3]

    for eta_indx,eta in enumerate(eta_list):

        state_grid = np.arange(0, states)

        sys="N1_"+str(N1)+"_wc_"+wc+"_e0_"+str(e0)+"_mu0_"+str(mu0)+"_neta_"+str(neta)
        final_dir=out_dir+"/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/eta_"+"{:.3f}".format(eta)
        pot_surf_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/pot_surf"
        dip_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/dipole"
        eigen_ener_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/eigen_ener"
        eigen_states_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/eigen_states"+"/eta_"+"{:.3f}".format(eta)
        eigen_states_projection_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/eigen_states_projection"+"/eta_"+"{:.3f}".format(eta)
        displaced_eigen_states_projection_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/displaced_eigen_states_projection"+"/eta_"+"{:.3f}".format(eta)

        q_elements_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/q_elements"
        xc_elements_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/xc_elements"





        os.makedirs(final_dir,exist_ok='True')
        os.makedirs(pot_surf_plot_dir,exist_ok='True')
        os.makedirs(dip_plot_dir,exist_ok='True')
        os.makedirs(eigen_ener_plot_dir,exist_ok='True')
        os.makedirs(eigen_states_plot_dir,exist_ok='True')
        os.makedirs(eigen_states_projection_plot_dir,exist_ok='True')
        os.makedirs(displaced_eigen_states_projection_plot_dir,exist_ok='True')

        os.makedirs(q_elements_plot_dir,exist_ok='True')
        os.makedirs(xc_elements_plot_dir,exist_ok='True')






        # Read Potential/ Dipole fct/ Dipole Self-Energy on grid

        disc_transfer_grid = np.loadtxt(data_dir+'/transfer_pot.dat', usecols=(0,), dtype=float)
        disc_transfer_pot = np.loadtxt(data_dir+'/transfer_pot.dat', usecols=(1,), dtype=float)
        disc_transfer_dip = np.loadtxt(data_dir+'/proton_dipole_minQ.dat', usecols=(1,), dtype=float)
        disc_transfer_dse = np.loadtxt(data_dir+'/proton_dse_minQ.dat', usecols=(1,), dtype=float)


        spline_transfer_pot = interp1d(disc_transfer_grid, disc_transfer_pot, kind='cubic')
        spline_transfer_dip = interp1d(disc_transfer_grid, disc_transfer_dip, kind='cubic')
        spline_transfer_dse = interp1d(disc_transfer_grid, disc_transfer_dse, kind ='cubic')

        # H-Transfer 1D system
        transfer_mass = 1914.028 #m_e
        cavity_coupling = cavity_freq/trans_dip_10*eta

        # Transfer DVR grid/ Htransfer PES

        transfer_min = disc_transfer_grid[0]
        transfer_max = disc_transfer_grid[len(disc_transfer_grid)-1]
        transfer_grid = np.linspace(transfer_min, transfer_max, num=size_transfer_grid, endpoint=True)
        transfer_increment = np.abs(transfer_grid[1]-transfer_grid[0])

        transfer_pot  = spline_transfer_pot(transfer_grid)
        transfer_dip  = spline_transfer_dip(transfer_grid)
        transfer_dse  = spline_transfer_dse(transfer_grid)

        # cavity-grid size (odd)
        cavity_min = -500
        cavity_max = 500
        cavity_grid = np.linspace(cavity_min, cavity_max, num=size_cavity_grid, endpoint=True)
        cavity_increment = np.abs(cavity_grid[1]-cavity_grid[0])



        #----------- Define Hamiltonian ------------------

        # Colbert-Miller DVR KEO & Potential functions

        def colbert_miller_keo(grid, grid_increment, mass):
            keo = np.zeros([len(grid), len(grid)], dtype = float)
            pre_factor = 1/(2*mass*grid_increment**2)
            for i in range(0, len(grid)):
                for j in range(0, len(grid)):
                    if i == j:
                        keo[i,j] = pre_factor*(np.pi**2)/3
                    else:
                        keo[i,j] = pre_factor*2*(-1)**(i-j)/(i-j)**2
            return keo


        def harmonic_pot(grid, frequency,x0=0):
            ho_pot = np.zeros([len(grid), len(grid)], dtype = float)
            for j in range(len(grid)):
                ho_pot[j,j] = 0.5*(frequency**2)*(grid[j]+x0)**2

            return ho_pot



        # Grid representation Hamiltonian terms

        transfer_keo = colbert_miller_keo(transfer_grid, transfer_increment, transfer_mass)
        cavity_keo   = colbert_miller_keo(cavity_grid, cavity_increment, 1.0)

        transfer_pot = transfer_pot*np.identity(size_transfer_grid)
        transfer_dip = transfer_dip*np.identity(size_transfer_grid)
        transfer_dse = transfer_dse*np.identity(size_transfer_grid)

        cavity_pot = harmonic_pot(cavity_grid, cavity_freq)

        mu0_dip=1.68
        x0=np.sqrt(2/cavity_freq)*mu0_dip*eta/trans_dip_10

        displaced_cavity_pot = harmonic_pot(cavity_grid, cavity_freq,x0)


        cavity_coord = cavity_grid*np.identity(size_cavity_grid)

        cavity_transfer_pes = np.zeros([size_transfer_grid, size_cavity_grid], dtype = float)
        for ix in range(0, size_transfer_grid):
            for iy in range(0, size_cavity_grid):
                cavity_transfer_pes[ix,iy] = transfer_pot[ix, ix] + cavity_pot[iy, iy] + np.sqrt(2.0*cavity_freq)*cavity_coupling*transfer_dip[ix,ix]*cavity_coord[iy,iy] + (cavity_coupling**2)/cavity_freq*transfer_dse[ix,ix]


        min_pot=min(cavity_transfer_pes.flatten())

        ##%%
        plt.figure()
        plt.contour(cavity_transfer_pes-min_pot, levels=50,  colors='black', alpha=0.8,vmin=0,vmax=0.02,\
                             extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], \
                                          transfer_grid[0], transfer_grid[size_transfer_grid-1]])
        plt.imshow(cavity_transfer_pes-min_pot, cmap="RdPu", aspect = 'auto',vmin=0,vmax=0.01,\
               extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1],\
                            transfer_grid[size_transfer_grid-1], transfer_grid[0]],\
                            interpolation = 'spline16')
        cbar=plt.colorbar()
        plt.xlabel('$x_c/\sqrt{m_e}a_0$', fontsize=18)
        plt.ylabel('$q/a_0$', fontsize=18)
        cbar.set_label(r'$V(Eh)$')
        plt.savefig(pot_surf_plot_dir+"/pot_surf_eta_"+"{:.3f}".format(eta)+".pdf", dpi=1000)
        plt.close()
        ##%%

        # 1D Transfer Hamiltonian
        transfer_hamiltonian = transfer_keo + transfer_pot
        cavity_hamiltonian = cavity_keo + cavity_pot
        displaced_cavity_hamiltonian = cavity_keo + displaced_cavity_pot


        # 2D cavity-Transfer Hamiltonian

        cavity_transfer_keo = np.kron(transfer_keo, np.identity(size_cavity_grid)) + np.kron(np.identity(size_transfer_grid), cavity_keo)

        cavity_transfer_pot = np.kron(transfer_pot, np.identity(size_cavity_grid)) + np.kron(np.identity(size_transfer_grid), cavity_pot)

        cavity_transfer_coupling = np.sqrt(2.0*cavity_freq)*cavity_coupling*np.kron(transfer_dip, cavity_coord)

        cavity_transfer_dse = (cavity_coupling**2)/cavity_freq*np.kron(transfer_dse, np.identity(size_cavity_grid))

        cavity_transfer_hamiltonian = cavity_transfer_keo + cavity_transfer_pot + cavity_transfer_coupling + cavity_transfer_dse

        zero_order_cavity_transfer_hamiltonian = cavity_transfer_keo + cavity_transfer_pot

        cavity_transfer_dipole = np.kron(transfer_dip, np.identity(size_cavity_grid))
        cavity_displacement_trans = np.sqrt(2*cavity_freq)*np.kron(np.identity(size_transfer_grid), cavity_coord)

        #----------- Solve Hamiltonian ------------------

        # 1D Transfer Hamiltonian Eigensystem

        eval_transfer_sys, evec_transfer_sys = np.linalg.eigh(transfer_hamiltonian)
        eval_cavity, evec_cavity = np.linalg.eigh(cavity_hamiltonian)
        displaced_eval_cavity, displaced_evec_cavity = np.linalg.eigh(displaced_cavity_hamiltonian)








#         # ##%%
#         # plt.figure()
#         # plt.plot(eval_transfer_sys[0:states]-min_pot, 'd')
#         # plt.title('Transfer Eigenvalues')
#         # plt.xlabel('State No.')
#         # plt.ylabel('Energy/cm^-1')
#         # ##%%
#
#         # fig, axes = plt.subplots(1,1,figsize = (8,6))
#         # for i in range(5):
#         #     axes.plot(transfer_grid, evec_transfer_sys[:,i] , label="psi"+str(i))
#         # axes.legend(loc=1)
#         # axes.set_xlabel('s')
#
#         ##%%
#
        # 2D Cavity-Transfer Hamiltonian Eigensystem

        ##%%
        eval_cavity_transfer_sys, evec_cavity_transfer_sys = np.linalg.eigh(cavity_transfer_hamiltonian)
        eval_zero_order_cavity_transfer_sys, evec_zero_order_cavity_transfer_sys = np.linalg.eigh(zero_order_cavity_transfer_hamiltonian)


        #Reshape states vec-to- matrix
        evec_cavity_transfer_list = []
        for i in range(0, states):
            evec_cavity_transfer_mat = np.reshape(evec_cavity_transfer_sys[:, i], (size_transfer_grid,size_cavity_grid),order='C')
            evec_cavity_transfer_list.append(evec_cavity_transfer_mat)






        max_evec_transfer=10
        max_evec_cavity=25


        # phi_chi_xy=np.zeros([max_evec_transfer,max_evec_cavity,size_transfer_grid,size_cavity_grid])
        #
        # for ii in range(max_evec_transfer):
        #     for jj in range(max_evec_cavity):
        #         phi_chi_xy[ii,jj]=np.einsum('i,j',evec_transfer_sys[:,ii],evec_cavity[:,jj])
        #
        # phi_chi_m=np.zeros([max_evec_transfer,max_evec_cavity,states])
        #
        # for ii in range(max_evec_transfer):
        #     for jj in range(max_evec_cavity):
        #         for kk in range(states):
        #             phi_chi_m[ii,jj,kk]=np.einsum('ij,ij',phi_chi_xy[ii,jj],evec_cavity_transfer_list[kk])
        #
        # for kk in range(states):
        #     plt.imshow(abs(phi_chi_m[:,:,kk]),origin='lower')#,cmap="RdBu_r")
        #     plt.colorbar()
        #     plt.title(r'$|'+str(kk)+r'\rangle$')
        #     plt.xlabel(r'$|\chi\rangle$'+"_cavity", fontsize=18)
        #     plt.ylabel(r'$|\phi\rangle$'+"_system", fontsize=18)
        #     plt.savefig(eigen_states_projection_plot_dir+'/state_'+str(kk)+'.png', dpi=1000)
        #     plt.close()
        #     mol.write_matrix_to_file(phi_chi_m[:,:,kk],"","",eigen_states_projection_plot_dir+'/state_'+str(kk)+'.txt')
        #
        #
        #
        # q_xy=np.repeat(np.array([transfer_grid]).T, size_cavity_grid, axis=1)
        # q_matrix=np.zeros([states,states])
        #
        # for ii in range(states):
        #     for jj in range(states):
        #         q_matrix[ii,jj]=np.einsum('ij,ij,ij',evec_cavity_transfer_list[ii],q_xy,evec_cavity_transfer_list[jj])
        #
        # plt.imshow(q_matrix,origin='lower',vmin=-0.7,vmax=0.7,cmap="RdBu_r")#,cmap="RdBu_r")
        # plt.colorbar()
        # plt.xlabel(r'$|m\rangle$', fontsize=18)
        # plt.ylabel(r'$|n\rangle$', fontsize=18)
        # plt.savefig(q_elements_plot_dir+"/q_mat_"+"{:.3f}".format(eta)+".png", dpi=1000)
        # plt.close()
        # mol.write_matrix_to_file(q_matrix,"","",q_elements_plot_dir+"/q_mat_"+"{:.3f}".format(eta)+".txt")
        #
        #
        #
        # xc_xy=np.repeat([cavity_grid], size_transfer_grid, axis=0)
        #
        # xc_matrix=np.zeros([states,states])
        #
        # for ii in range(states):
        #     for jj in range(states):
        #         xc_matrix[ii,jj]=np.einsum('ij,ij,ij',evec_cavity_transfer_list[ii],xc_xy,evec_cavity_transfer_list[jj])
        #
        # plt.imshow(xc_matrix,origin='lower',vmin=-0.7,vmax=0.7,cmap="RdBu_r")#,cmap="RdBu_r")
        # plt.colorbar()
        # plt.xlabel(r'$|m\rangle$', fontsize=18)
        # plt.ylabel(r'$|n\rangle$', fontsize=18)
        # plt.savefig(xc_elements_plot_dir+"/xc_mat_"+"{:.3f}".format(eta)+".png", dpi=1000)
        # plt.close()
        # mol.write_matrix_to_file(xc_matrix,"","",xc_elements_plot_dir+"/xc_mat_"+"{:.3f}".format(eta)+".txt")








        phi_chi_xy=np.zeros([max_evec_transfer,max_evec_cavity,size_transfer_grid,size_cavity_grid])

        for ii in range(max_evec_transfer):
            for jj in range(max_evec_cavity):
                phi_chi_xy[ii,jj]=np.einsum('i,j',evec_transfer_sys[:,ii],displaced_evec_cavity[:,jj])

        phi_chi_m=np.zeros([max_evec_transfer,max_evec_cavity,states])

        for ii in range(max_evec_transfer):
            for jj in range(max_evec_cavity):
                for kk in range(states):
                    phi_chi_m[ii,jj,kk]=np.einsum('ij,ij',phi_chi_xy[ii,jj],evec_cavity_transfer_list[kk])

        for kk in range(states):
            plt.imshow(abs(phi_chi_m[:,:,kk]),origin='lower')#,cmap="RdBu_r")
            plt.colorbar()
            plt.title(r'$|'+str(kk)+r'\rangle$')
            plt.xlabel(r'$|\chi\rangle$'+"_cavity", fontsize=18)
            plt.ylabel(r'$|\phi\rangle$'+"_system", fontsize=18)
            plt.savefig(displaced_eigen_states_projection_plot_dir+'/state_'+str(kk)+'.png', dpi=1000)
            plt.close()
            mol.write_matrix_to_file(phi_chi_m[:,:,kk],"","",displaced_eigen_states_projection_plot_dir+'/state_'+str(kk)+'.txt')









        print("eta is "+"{:.3f}".format(eta))




#
#
#
#
#
#
#
#
#
#         sym_state=np.abs(evec_cavity_transfer_list[mixed_states[0]]+evec_cavity_transfer_list[mixed_states[1]])
#
#
#         ##Computes the integral before and after the barrier
#         sum_0=np.sum([np.sum(xxx) for xxx in sym_state][:(size_transfer_grid-1)//2])
#         sum_1=np.sum([np.sum(xxx) for xxx in sym_state][(size_transfer_grid-1)//2:])
#
#         ### This line is to guarantee the correct symetry for the superposition of states 1 and 2
#         if sum_0 > sum_1:
#             evec_cavity_transfer_sys[:,mixed_states[1]]=-evec_cavity_transfer_sys[:,mixed_states[1]]
#
#         #---------------------- Cavity-Transfer Dipole Moments ------------------------
#
#         cavity_transfer_dipole_moments = np.zeros([states, states], dtype = float)
#         for i in range(states):
#             for j in range(states):
#                 cavity_transfer_dipole_moments[i,j] = np.einsum('i,ij,j', evec_cavity_transfer_sys[:,i],cavity_transfer_dipole, evec_cavity_transfer_sys[:,j])
#
#
#
#
#
#         # cavity_transfer_dipole_moments = np.zeros([states, states], dtype = float)
#         # for i in range(states):
#         #     for j in range(i+1):
#         #         if np.einsum('i,ij,j', evec_cavity_transfer_sys[:,i],cavity_transfer_dipole, evec_cavity_transfer_sys[:,j]) < 0:
#         #             evec_cavity_transfer_sys[:,j]=-evec_cavity_transfer_sys[:,j]
#         #         cavity_transfer_dipole_moments[i,j] = np.einsum('i,ij,j', evec_cavity_transfer_sys[:,i],cavity_transfer_dipole, evec_cavity_transfer_sys[:,j])
#         #         cavity_transfer_dipole_moments[j,i] = cavity_transfer_dipole_moments[i,j]
#
#
#
#
#
#
#         sym_state=np.abs(evec_cavity_transfer_list[mixed_states[0]]+evec_cavity_transfer_list[mixed_states[1]])
#
#         ##Computes the integral before and after the barrier
#         sum_0=np.sum([np.sum(xxx) for xxx in sym_state][:(size_transfer_grid-1)//2])
#         sum_1=np.sum([np.sum(xxx) for xxx in sym_state][(size_transfer_grid-1)//2:])
#
#         ### This line is to guarantee the correct symetry for the superposition of states 1 and 2
#         if sum_1 > sum_0:
#             symetry='symetric'
#         else:
#             symetry='antisymetric'
#
#
#
#
#         ##%%
#         plt.figure()
#         plt.plot(eval_cavity_transfer_sys[0:states], 'o')
#         plt.title('Cavity-Transfer Eigenvalues')
#         plt.xlabel('State No.')
#         plt.ylabel('Energy/au')
#         plt.ylim(0.004,0.03)
#         plt.savefig(eigen_ener_plot_dir+"/eigen_ener_eta_"+"{:.3f}".format(eta)+".pdf", dpi=1000)
#         plt.close()
#         ##%%
#         # Projection
#
#         OH_state_amplitudes = np.zeros([states], dtype = float)
#         SH_state_amplitudes = np.zeros([states], dtype = float)
#         for i in range(states):
#             OH_state_amplitudes[i] = np.dot(evec_zero_order_cavity_transfer_sys[:,0], evec_cavity_transfer_sys[:,i])**2
#             SH_state_amplitudes[i] = np.dot(evec_zero_order_cavity_transfer_sys[:,1], evec_cavity_transfer_sys[:,i])**2
#
#         # ##%%
#         # fig, axes = plt.subplots()
#         # axes.stem(eval_cavity_transfer_sys[0:states]*au_to_cm, SH_state_amplitudes, label="OH")
#         # axes.legend(loc=1)
#         # plt.show()
#         # ##%%
#
#         #---------------------- Cavity-Transfer Hybrid States ------------------------
#
#
#
#
#         for ix in range(0, states):
#             plt.figure()
#             plt.contour(cavity_transfer_pes, levels=50,  colors='black', alpha=0.4,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#             plt.contour(evec_cavity_transfer_list[ix], levels=6,  colors='white', alpha=0.4, extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#             plt.imshow(evec_cavity_transfer_list[ix], cmap="RdBu_r", aspect = 'auto',vmin=-0.15,vmax=0.15,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1],transfer_grid[size_transfer_grid-1], transfer_grid[0]],interpolation = 'spline16')
#             plt.colorbar()
#             plt.title('P'+str(ix)+': '+str(round(eval_cavity_transfer_sys[ix]*au_to_cm,2))+' cm$^{-1}$')
#             plt.xlabel('$x_c/\sqrt{m_e}a_0$', fontsize=18)
#             plt.ylabel('$q/a_0$', fontsize=18)
#             plt.savefig(eigen_states_plot_dir+'/state_'+str(ix)+'.pdf', dpi=1000)
#             plt.close()
#
#         ##%%
#         plt.contour(cavity_transfer_pes, levels=50,  colors='black', alpha=0.4,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#         plt.contour(evec_cavity_transfer_list[mixed_states[0]]+evec_cavity_transfer_list[mixed_states[1]], levels=6,  colors='white', alpha=0.4, extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#         plt.imshow(evec_cavity_transfer_list[mixed_states[0]]+evec_cavity_transfer_list[mixed_states[1]], "RdBu_r", aspect = 'auto',vmin=-0.15,vmax=0.15,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1],transfer_grid[size_transfer_grid-1], transfer_grid[0]],interpolation = 'spline16')
#         plt.title(r'$|1\rangle + |2\rangle$')
#         plt.colorbar()
#         plt.xlabel('$x_c/\sqrt{m_e}a_0$', fontsize=18)
#         plt.ylabel('$q/a_0$', fontsize=18)
#         plt.savefig(eigen_states_plot_dir+'/symetric_1_2.pdf', dpi=1000)
#         plt.close()
#         ##%%
#
#
#         ##%%
#         plt.contour(cavity_transfer_pes, levels=50,  colors='black', alpha=0.4,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#         plt.contour(evec_cavity_transfer_list[mixed_states[0]]-evec_cavity_transfer_list[mixed_states[1]], levels=6,  colors='white', alpha=0.4, extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]])
#         plt.imshow(evec_cavity_transfer_list[mixed_states[0]]-evec_cavity_transfer_list[mixed_states[1]], cmap="RdBu_r", aspect = 'auto',vmin=-0.15,vmax=0.15,extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1],transfer_grid[size_transfer_grid-1], transfer_grid[0]],interpolation = 'spline16')
#         plt.title(r'$|1\rangle - |2\rangle$')
#         plt.colorbar()
#         plt.xlabel('$x_c/\sqrt{m_e}a_0$', fontsize=18)
#         plt.ylabel('$q/a_0$', fontsize=18)
#         plt.savefig(eigen_states_plot_dir+'/antisymetric_1_2.pdf', dpi=1000)
#         plt.close()
#         ##%%
#
#
#         mol.write_matrix_to_file(cavity_transfer_pes,"","",eigen_states_plot_dir+"/pes.txt")
#         mol.write_matrix_to_file(evec_cavity_transfer_list[mixed_states[0]]+evec_cavity_transfer_list[mixed_states[1]],"","",eigen_states_plot_dir+"/symetric.txt")
#         mol.write_matrix_to_file(evec_cavity_transfer_list[mixed_states[0]]-evec_cavity_transfer_list[mixed_states[1]],"","",eigen_states_plot_dir+"/antisymetric.txt")
#         for i in range(5):
#             mol.write_matrix_to_file(evec_cavity_transfer_list[i],"","",eigen_states_plot_dir+"/state_"+str(i)+".txt")
#
#
#
#
#         mol.write_matrix_to_file([[cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]]],"extent = [cavity_grid[0], cavity_grid[size_cavity_grid-1], transfer_grid[0], transfer_grid[size_transfer_grid-1]]","",eigen_states_plot_dir+"/grid.txt")
#
#
#
#
#         ##%%
#         plt.figure()
#         plt.imshow(np.abs(cavity_transfer_dipole_moments))
#         plt.colorbar()
#         plt.savefig(dip_plot_dir+'/trans_dip_eta_'+"{:.3f}".format(eta)+".pdf", dpi=1000)
#         plt.close()
#         ##%%
#
#
#         # nstates_cavity=100
#         #
#         # ### Matrix containing the cavity eigenstates m in the y representation
#         # y_m=np.zeros([size_cavity_grid,nstates_cavity])
#         #
#         # for i in range(nstates_cavity):
#         #     y_m[:,i]=quant.psi_ho(i,cavity_grid,1,cavity_freq)
#         #
#         #
#         # n_m_alpha=np.zeros(size_transfer_grid,nstates_cavity,states)
#         #
#         # for n in range(size_transfer_grid):
#         #     for m in range(nstates_cavity):
#         #         for alpha in range(states):
#         #             for i in range(size_transfer_grid):
#         #                 for j in range(size_cavity_grid):
#         #                     n_m_alpha[]
#
#
#         cavity_emission_transfer_moments = np.zeros([states, states], dtype = float)
#         for i in range(states):
#             for j in range(states):
#                 if i != j:
#                     cavity_emission_transfer_moments[i,j] = np.dot(evec_cavity_transfer_sys.transpose()[i],np.dot(cavity_displacement_trans, evec_cavity_transfer_sys[:,j]))
#
#         emission_diagonal=np.zeros([states, states])
#
#         for i in range(states):
#             sum=0
#             for j in range(i):
#                 sum=sum+cavity_emission_transfer_moments[i,j]**2
#             emission_diagonal[i,i]=sum
#
#         #plt.plot(emission_diagonal.diagonal(),'o-')
#         #
#         eval_mat=np.diag(eval_cavity_transfer_sys[0:states]/e0)
#         dip_mat=cavity_transfer_dipole_moments/mu0
#
#         ground_state=np.zeros(2*N1)
#         ground_state[0]=1
#
#         state1=np.zeros(2*N1)
#         state1[1]=1
#
#         state2=np.zeros(2*N1)
#         state2[2]=1
#
#         state3=np.zeros(2*N1)
#         state3[3]=1
#
#         mix_symetric=np.zeros(2*N1)
#         mix_symetric[mixed_states[0]]=1
#         mix_symetric[mixed_states[1]]=1
#
#         mix_antisymetric=np.zeros(2*N1)
#         mix_antisymetric[mixed_states[0]]=1
#         mix_antisymetric[mixed_states[1]]=-1
#
#         mol.write_matrix_to_file(eval_mat[:N1,:N1],"","",final_dir+"/h11.txt")
#         mol.write_matrix_to_file(dip_mat[:N1,:N1],"","",final_dir+"/d11.txt")
#         mol.write_matrix_to_file(emission_diagonal,"","",final_dir+"/t11.txt")
#         mol.write_matrix_to_file([ground_state],"","",final_dir+"/state_0.txt")
#         mol.write_matrix_to_file([state1],"","",final_dir+"/state_1.txt")
#         mol.write_matrix_to_file([mix_symetric],"","",final_dir+"/symetric.txt")
#         mol.write_matrix_to_file([mix_antisymetric],"","",final_dir+"/antisymetric.txt")
#
#         mol.write_matrix_to_file([state1],"","",final_dir+"/molecular_1.txt")
#
#
#         if eta ==0:
#             mol.write_matrix_to_file([state2],"","",final_dir+"/molecular_1.txt")
#         if eta!=0 and symetry=='symetric':
#             mol.write_matrix_to_file([mix_symetric],"","",final_dir+"/molecular_1.txt")
#         if eta!=0 and symetry=='antisymetric':
#             mol.write_matrix_to_file([mix_antisymetric],"","",final_dir+"/molecular_1.txt")
#
#
#
#
#
#         mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/d12.txt")
#         mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/d21.txt")
#         mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/d22.txt")
#
#         mol.write_matrix_to_file(np.zeros([N1,N1]),"","",final_dir+"/q11.txt")
#         mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/q12.txt")
#         mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/q21.txt")
#         mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/q22.txt")
#
#         mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/p12.txt")
#         mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/p21.txt")
#
#         mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/h22.txt")
#
#
#
#
#
#
# amatransfer_pot=np.genfromtxt("/home/ar612/Documents/Work/cavities/H_transfer/data/transfer_pot.dat")
# x=transfer_pot[:,0]
# pot=transfer_pot[:,1]
#
# h11_list=[[eval_transfer_sys[i]]*len(x) for i in range(len(eval_transfer_sys))]
#
#
# for i in range(10):
#     if i == 0:
#         plt.plot(x,h11_list[i],label="E0="+str(eval_transfer_sys[0]))
#     else:
#         plt.plot(x,h11_list[i],label="E"+str(i)+"-E0="+str(eval_transfer_sys[i]-eval_transfer_sys[0]))
# plt.plot(x,pot)
# plt.legend()
