
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import scipy
import numpy as np
import matplotlib.pyplot as plt
import rmsd
%matplotlib qt
plt.style.use(['ggplot'])

def lorentzian_shape(w,intes,width):
    nmodes=len(w)
    x=np.linspace(w[0]-10*width,w[-1]+10*width,1000)
    y=np.zeros(len(x))
    for i in range(nmodes):
        arg=(x-w[i])/(width/2)
        y=y + intes[i]/(1+(arg)**2)

    return x,y

def find_index(w_list,w_search):
    index=0
    dist=np.inf

    for i,w in enumerate(w_list):
        if abs(w-w_search) < dist:
            index=i
            dist=abs(w-w_search)

    return index
###############################################################################%%
################### Static spectrum ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1=cp2k_dir+"/vibrational_analysis/aptprod1_b3lyp_vibrational_analysis/aptprod1_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"
    inp_file_2=cp2k_dir+"/vibrational_analysis/aptreac2_b3lyp_vibrational_analysis/aptreac2_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"

    w1,int1,modes1,r1,atm_names1=mol.w_modes_r_atm_names_from_molden (inp_file_1)
    w2,int2,modes2,r2,atm_names2=mol.w_modes_r_atm_names_from_molden (inp_file_2)

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w1, int1,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="apt_prod1")
    ax.stem(w2, int2,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="apt_reac2")
    ax.set_xlim([1400,1700])
    ax.legend()

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/vibrational_analysis/alpha_phenyl_tropolone_1400_1700_zoom.pdf", bbox_inches='tight')



    ###%%

###############################################################################%%
################### Static spectrum ###########################################%%
###############################################################################%%




###############################################################################%%
################### 1phenyl1cyclo 1phenyl3cyclo  comparisson ##################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1_83381.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1_83384.out"
    inp_file_3=cp2k_dir+"/ir_spec/r3_thf_114_ir_spec_reduced/abs_after_secs.txt"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/1phenyl1cyclo_1_thf_198_300K_30A_ir_spec_reduced/spectr_17000.dat"
    inp_file_5="/home/ar612/Documents/Work/cp2k_organized/ir_spec/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec_reduced/spectr_17000.dat"


    w,redm,f,intes,modes,r,atm_names=mol.w_redm_f_intes_modes_r_atm_names_from_gauss(inp_file_1)
    w2,redm2,f2,intes2,modes2,r2,atm_names2=mol.w_redm_f_intes_modes_r_atm_names_from_gauss(inp_file_2)
    exp=np.genfromtxt(inp_file_3)
    theo1=np.genfromtxt(inp_file_4)
    theo2=np.genfromtxt(inp_file_5)


    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    plt.plot(exp[0,1:],exp[1,1:],linewidth=2.5,label="Experiment")
    plt.plot(theo1[:,0],theo1[:,1],linewidth=2.5,label="Dynamic: 1 PHI + 1 CH-ol + 198 THF at 300K : blyp")
    plt.plot(theo2[:,0],theo2[:,1],linewidth=2.5,label="Dynamic: 1 PHI + 3 CH-ol + 196 THF at 300K : blyp")
    #ax.stem(w2, intes2/100,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 PHI + 3 CH-ol : b3lyp/6-31++g(d,p)")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([1000,4000])
    ax.set_ylim([0,4])

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/pctreac_experiment_static_dynamic_spectra.pdf", bbox_inches='tight')



    ###%%

###############################################################################%%
################### Static and travis spectrum ###########################################%%
###############################################################################%%




###############################################################################%%
################### Static spectrum ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/phoxide_reduced/phoxide_83749.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/phosphine1_reduced/phosphine1_83747.out"
    inp_file_3="/home/ar612/Documents/Work/gaussian/geo_opt/phosphine2_reduced/phosphine2_83748.out"

    w1,redm1,f1,intes1,modes1,r1,atm_names1=mol.w_redm_f_intes_modes_r_atm_names_from_gauss (inp_file_1)
    w2,redm2,f2,intes2,modes2,r2,atm_names2=mol.w_redm_f_intes_modes_r_atm_names_from_gauss (inp_file_2)
    w3,redm3,f3,intes3,modes3,r3,atm_names3=mol.w_redm_f_intes_modes_r_atm_names_from_gauss (inp_file_3)

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w1, intes1,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="phoxide")
    ax.stem(w2, intes2,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="phosphine1")
    ax.stem(w3, intes3,linefmt='C4-', markerfmt=' ',use_line_collection=True,label="phosphine2")
    ax.set_xlim([0,3900])
    ax.legend()

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/vibrational_analysis/phoxide_phosphine.pdf", bbox_inches='tight')



    ###%%








###############################################################################%%
################### aptreac1 and aptprod1  comparisson ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/aptreac1_reduced/771479.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/aptprod1_reduced/771480.out"
    inp_file_3="/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptreac1_thf_114_300K_ir_spec_reduced/spectr_25000.dat"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptprod1_thf_114_300K_ir_spec_reduced/spectr_25000.dat"

    ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_prod,redm,fcnstmDA,intes_prod,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    theo_reac=np.genfromtxt(inp_file_3)
    theo_prod=np.genfromtxt(inp_file_4)

    max_reac=max(intes_reac)
    max_prod=max(intes_prod)
    intes_reac=intes_reac/max_reac
    intes_prod=intes_prod/max_prod

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_reac / b3lyp/6-31++g(d,p)")
    ax.stem(w_prod, intes_prod,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_prod / b3lyp/6-31++g(d,p)")
    plt.plot(theo_reac[:,0],theo_reac[:,1],color="tab:red",linewidth=2.5,label="Dynamic: 1 Trop_reac + 114 THF at 300K")
    plt.plot(theo_prod[:,0],theo_prod[:,1],color="tab:blue",linewidth=2.5,label="Dynamic: 1 Trop_prod + 114 THF at 300K")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([200,3500])
    ax.set_ylim([0,0.8])

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptreac1_aptprod1_ir_spec.pdf", bbox_inches='tight')



    ###%%
    E=0.001
    w=0.00608
    tmax=100*mol.fs2au
    t=np.linspace(0,tmax,200)
    dt=t[1]-t[0]
    f=2*np.pi/dt
    e=E*np.sin(w*t)*np.sin(t*np.pi/tmax)**2

    dat=np.genfromtxt("/home/ar612/Documents/Work/quantics/test/4_hmass/efield.txt")

    plt.plot(t,e)
    plt.plot(dat[:,0],dat[:,1])

    np.sqrt(8*0.01/(4*7348.72))

    f_dat=2*np.pi/(dat[1,0]-dat[0,0])
    plt.magnitude_spectrum(e,f)
    plt.magnitude_spectrum(dat[:,1],f_dat)


    20000*mol.au2fs









###############################################################################%%
######### naphthoic acid dimer experiment theory  comparisson #################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/aptreac1_reduced/771479.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/aptprod1_reduced/771480.out"
    inp_file_3="/home/ar612/Downloads/napthoic_acid_in_CHCl3-concentration_dependence_FTIR.txt"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/ntad1_1_chl_195_300K_30A_ir_spec_reduced/spectr_25000.dat"
    inp_file_5="/home/ar612/Downloads/napthoic_acid_in_CDCl3.txt"

    # ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    # ener,w_prod,redm,fcnstmDA,intes_prod,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    exp=np.genfromtxt(inp_file_3)
    exp_d=np.genfromtxt(inp_file_5)
    theo=np.genfromtxt(inp_file_4)

    exp.shape

    conc=["1.5mM","3mM","6.25mM","12.5mM","25mM","50mM","100mM"]

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    # ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_reac / b3lyp/6-31++g(d,p)")
    # ax.stem(w_prod, intes_prod,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_prod / b3lyp/6-31++g(d,p)")
    for i in range (1,8):
        plt.plot(exp[:,0],exp[:,i],linewidth=2.5,label="Experiment (CHCl3) "+conc[i-1])
    plt.plot(exp_d[:,0],exp_d[:,1],color="red",linewidth=2.5,label="Experiment (CDCl3)")
    plt.plot(theo[:,0],theo[:,1],color="blue",linewidth=2.5,label="Dynamic (BLYP): 1 dimmer + 195 CHCl3")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper right')
    ax.set_xlim([1000,1800])
    ax.set_ylim([0,1])
    #%%
    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/naphthoic_acid_ir_spec_1000_1800.pdf", bbox_inches='tight')






###############################################################################%%
######### naphthoic acid dimer and monomer  comparisson #################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/aptreac1_reduced/771479.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/aptprod1_reduced/771480.out"
    inp_file_3="/home/ar612/Downloads/napthoic_acid_in_CHCl3-concentration_dependence_FTIR.txt"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/ntad1_1_chl_195_300K_30A_ir_spec_reduced/spectr_25000.dat"
    inp_file_5="/home/ar612/Documents/Work/cp2k_organized/ir_spec/nta_1_chl_199_300K_30A_ir_spec_reduced/spectr_25000.dat"

    # ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    # ener,w_prod,redm,fcnstmDA,intes_prod,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    exp=np.genfromtxt(inp_file_3)
    theo_dimer=np.genfromtxt(inp_file_4)
    theo_mon=np.genfromtxt(inp_file_5)

    exp.shape

    conc=["1.5mM","3mM","6.25mM","12.5mM","25mM","50mM","100mM"]

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    # ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_reac / b3lyp/6-31++g(d,p)")
    # ax.stem(w_prod, intes_prod,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_prod / b3lyp/6-31++g(d,p)")
    for i in range (1,7):
        plt.plot(exp[:,0],exp[:,i],linewidth=2.5,label="Experiment (CHCl3) "+conc[i-1])
    plt.plot(theo_dimer[:,0],theo_dimer[:,1],color="blue",linewidth=2.5,label="Dynamic (BLYP): 1 dimmer + 195 CHCl3")
    plt.plot(theo_mon[:,0],theo_mon[:,1],color="red",linewidth=2.5,label="Dynamic (BLYP): 1 monomer + 199 CHCl3")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper right')
    ax.set_xlim([500,3800])
    ax.set_ylim([0,1])
    #%%
    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/nta_dimmer_monomer_experiment_ir_spec.pdf", bbox_inches='tight')



###############################################################################%%
######### naphthoic acid dimer and monomer comparisson #################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/ntad1_reduced/148915.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/nta_reduced/148914.out"
    inp_file_3="/home/ar612/Downloads/napthoic_acid_in_CHCl3-concentration_dependence_FTIR.txt"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/ntad1_1_chl_195_300K_30A_ir_spec_reduced/spectr_25000.dat"
    inp_file_5="/home/ar612/Documents/Work/cp2k_organized/ir_spec/nta_1_chl_199_300K_30A_ir_spec_reduced/spectr_25000.dat"

    ener,w_dimmer,redm,fcnstmDA,intes_dimmer,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_monomer,redm,fcnstmDA,intes_monomer,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    exp=np.genfromtxt(inp_file_3)
    theo_dimer=np.genfromtxt(inp_file_4)
    theo_mon=np.genfromtxt(inp_file_5)


    conc=["1.5mM","3mM","6.25mM","12.5mM","25mM","50mM","100mM"]

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w_dimmer, intes_dimmer/1400,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: nta_dimmer : blyp/6-31++g(d,p)")
    ax.stem(w_monomer, intes_monomer/700,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: nta_monomer : blyp/6-31++g(d,p)")
    # for i in range (1,7):
    #     plt.plot(exp[:,0],exp[:,i],linewidth=2.5,label="Experiment (CHCl3) "+conc[i-1])
    plt.plot(theo_dimer[:,0],theo_dimer[:,1],color="red",linewidth=2.5,label="Dynamic (BLYP): 1 dimmer + 195 CHCl3")
    plt.plot(theo_mon[:,0],theo_mon[:,1],color="blue",linewidth=2.5,label="Dynamic (BLYP): 1 monomer + 199 CHCl3")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper right')
    ax.set_xlim([500,3800])
    ax.set_ylim([0,1])
    #%%
    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/nta_dimmer_monomer_experiment_ir_spec.pdf", bbox_inches='tight')









###############################################################################%%
################### aptreac1 and aptprod1  comparisson ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    ###%%###%%
    inp_file_1="/home/ar612/Documents/Work/quantics/potfit/aptts1/ir_thermal_elec/test_muxy_50000fs_0.1fs_50K_reduced/myspectra50_tau_600/sum1"
    inp_file_2="/home/ar612/Documents/Work/quantics/potfit/aptts1/ir_thermal_elec/test_muxy_50000fs_0.1fs_300K_reduced/myspectra300_tau_400/sum4"
    inp_file_3="/home/ar612/Documents/Work/quantics/potfit/aptts1/ir_thermal_elec/test_muxy_50000fs_0.1fs_1000K_reduced/myspectra1000_tau_200/sum4"

    spec1=np.genfromtxt(inp_file_1)
    spec2=np.genfromtxt(inp_file_2)
    spec3=np.genfromtxt(inp_file_3)


    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    plt.plot(spec1[:,0],spec1[:,3]/(max(spec1[:,3])),linewidth=2.5,label="2D: 50K")
    plt.plot(spec2[:,0],spec2[:,3]/(max(spec2[:,3])),linewidth=2.5,label="2D: 300K")
    #plt.plot(spec3[:,0],spec3[:,3]/(max(spec3[:,3])),linewidth=2.5,label="2D: 1000K")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper right')
    ax.set_xlim([50,3500])
    #ax.set_ylim([0,0.8])

    fig.savefig("/home/ar612/Documents/Work/quantics/potfit/aptts1/ir_thermal_elec/2d_ir_spectra.pdf", bbox_inches='tight')



    ##%%
    inp_file_pat="/home/ar612/Documents/Work/quantics/potfit/aptts1/ir_thermal_elec/test_muxy_50000fs_0.1fs_300K_reduced/reac_plane_prop-300-$$/expectation"
    n=6

    sumr=0
    sumi=0
    sumabs=0
    for i in range(n):
        #print(np.genfromtxt(inp_file_pat.replace("$$",str(i+1))).shape)
        t=np.genfromtxt(inp_file_pat.replace("$$",str(i+1)))[:,0]
        expr=np.genfromtxt(inp_file_pat.replace("$$",str(i+1)))[:,2]
        expi=np.genfromtxt(inp_file_pat.replace("$$",str(i+1)))[:,3]
        sumr=sumr+expr
        sumi=sumi+expi
        sumabs=sumabs+np.sqrt(expr**2+expi**2)

    sumr=sumr/n
    sumi=sumi/n
    sumabs=sumabs/n

    plt.plot(t,sumr,label="sumr")
    plt.plot(t,sumi,label="sumi")
    plt.plot(t,sumabs,label="sumabs")
    plt.plot(t,np.sqrt(sumi**2+sumr**2),label="|<mu(t)mu(0)>|")
    plt.legend()

    #%%









###############################################################################%%
######### ure1 and ure2 static spectrum comparisson #################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/ure1_reduced/231486.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/ure2_reduced/231487.out"

    ener,w_1,redm,fcnstmDA,intes_1,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_2,redm,fcnstmDA,intes_2,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)



    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w_1, intes_1,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: ure_1 : b3lyp/6-31++g(d,p)")
    ax.stem(w_2, intes_2,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: ure_2 : b3lyp/6-31++g(d,p)")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper right')
    ax.set_xlim([0,3800])
    #ax.set_ylim([0,800])
    #%%
    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/ure1_ure2_static_ir_spec.pdf", bbox_inches='tight')






###############################################################################%%
################### 1phenyl1cyclo_1_thf_198_300K_30A_ir_spec_reduced and aptprod1  comparisson ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/aptreac1_reduced/771479.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/aptprod1_reduced/771480.out"
    inp_file_3="/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptreac1_thf_114_300K_ir_spec_reduced/spectr_25000.dat"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptprod1_thf_114_300K_ir_spec_reduced/spectr_25000.dat"

    ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_prod,redm,fcnstmDA,intes_prod,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    theo_reac=np.genfromtxt(inp_file_3)
    theo_prod=np.genfromtxt(inp_file_4)

    max_reac=max(intes_reac)
    max_prod=max(intes_prod)
    intes_reac=intes_reac/max_reac
    intes_prod=intes_prod/max_prod

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_reac / b3lyp/6-31++g(d,p)")
    ax.stem(w_prod, intes_prod,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 Trop_prod / b3lyp/6-31++g(d,p)")
    plt.plot(theo_reac[:,0],theo_reac[:,1],color="tab:red",linewidth=2.5,label="Dynamic: 1 Trop_reac + 114 THF at 300K")
    plt.plot(theo_prod[:,0],theo_prod[:,1],color="tab:blue",linewidth=2.5,label="Dynamic: 1 Trop_prod + 114 THF at 300K")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([200,3500])
    ax.set_ylim([0,0.8])

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/aptreac1_aptprod1_ir_spec.pdf", bbox_inches='tight')
##%%



###############################################################################%%
################### pctdreac1 pctdprod1  pctdprod2 comparisson ################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/pctdreac1/277879.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/pctdprod1/277880.out"
    inp_file_3="/home/ar612/Documents/Work/gaussian/geo_opt/pctdprod2/277881.out"
    inp_file_4=cp2k_dir+"/ir_spec/r3_thf_114_ir_spec_reduced/abs_after_secs.txt"



    ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_prod1,redm,fcnstmDA,intes_prod1,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)
    ener,w_prod2,redm,fcnstmDA,intes_prod2,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_3)

    exp=np.genfromtxt(inp_file_4)


    find_index(w_prod1,1062)
    w_prod1[103]

    nmodes=len(w_reac)

    x=np.linspace(200,3500,1000)
    width=20
    y_reac=np.zeros(len(x))
    y_prod1=np.zeros(len(x))
    y_prod2=np.zeros(len(x))
    for i in range(nmodes):
        arg_reac=(x-w_reac[i])/(width/2)
        arg_prod1=(x-w_prod1[i])/(width/2)
        arg_prod2=(x-w_prod2[i])/(width/2)
        y_reac=y_reac+ intes_reac[i]/(1+(arg_reac)**2)
        y_prod1=y_prod1+ intes_prod1[i]/(1+(arg_prod1)**2)
        y_prod2=y_prod2+ intes_prod2[i]/(1+(arg_prod2)**2)


    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    #ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdreac1 / b3lyp/6-31++g(d,p)")
    #ax.stem(w_prod1, intes_prod1,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdprod1 / b3lyp/6-31++g(d,p)")
    #ax.stem(w_prod2, intes_prod2,linefmt='C6-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdprod2 / b3lyp/6-31++g(d,p)")
    #plt.plot(exp[0,1:],exp[1,1:]*1257/3.29,color="tab:gray",linewidth=2.5,label="Experiment")
    plt.plot(x,y_reac,color="tab:red",linewidth=2.5,label="Static: 1 pctdreac1 / b3lyp/6-31++g(d,p)")
    plt.plot(x,y_prod1+700,color="tab:blue",linewidth=2.5,label="Static: 1 pctdprod1 / b3lyp/6-31++g(d,p)")
    plt.plot(x,y_prod2+1400,color="tab:orange",linewidth=2.5,label="Static: 1 pctdprod2 / b3lyp/6-31++g(d,p)")


    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([200,3500])
    ax.set_ylim([0,2500])

    #fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/pctdreac_pctdprod1_pctdprod2_lorentzian.pdf", bbox_inches='tight')
##%%




###############################################################################%%
################### pctreac1 pctprod1  pctprod2 comparisson ################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/pctdreac1_reduced/285765.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/pctdprod1_reduced/285759.out"
    inp_file_3="/home/ar612/Documents/Work/gaussian/geo_opt/pctdprod2_reduced/285761.out"
    inp_file_4=cp2k_dir+"/ir_spec/r3_thf_114_ir_spec_reduced/abs_after_secs.txt"



    ener,w_reac,redm,fcnstmDA,intes_reac,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_prod1,redm,fcnstmDA,intes_prod1,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)
    ener,w_prod2,redm,fcnstmDA,intes_prod2,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_3)

    exp=np.genfromtxt(inp_file_4)


    find_index(w_prod1,1062)
    w_prod1[103]

    nmodes=len(w_reac)

    x=np.linspace(200,4500,1000)
    width=20
    y_reac=np.zeros(len(x))
    y_prod1=np.zeros(len(x))
    y_prod2=np.zeros(len(x))
    for i in range(nmodes):
        arg_reac=(x-w_reac[i])/(width/2)
        arg_prod1=(x-w_prod1[i])/(width/2)
        arg_prod2=(x-w_prod2[i])/(width/2)
        y_reac=y_reac+ intes_reac[i]/(1+(arg_reac)**2)
        y_prod1=y_prod1+ intes_prod1[i]/(1+(arg_prod1)**2)
        y_prod2=y_prod2+ intes_prod2[i]/(1+(arg_prod2)**2)


    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    #ax.stem(w_reac, intes_reac,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdreac1 / b3lyp/6-31++g(d,p)")
    #ax.stem(w_prod1, intes_prod1,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdprod1 / b3lyp/6-31++g(d,p)")
    #ax.stem(w_prod2, intes_prod2,linefmt='C6-', markerfmt=' ',use_line_collection=True,label="Static: 1 pctdprod2 / b3lyp/6-31++g(d,p)")
    #plt.plot(exp[0,1:],exp[1,1:]*1257/3.29,color="tab:gray",linewidth=2.5,label="Experiment")
    plt.plot(x,y_reac,color="tab:red",linewidth=2.5,label="Static: 1 pctdreac1 / b3lyp/6-31++g(d,p)")
    plt.plot(x,y_prod1+700,color="tab:blue",linewidth=2.5,label="Static: 1 pctdprod1 / b3lyp/6-31++g(d,p)")
    plt.plot(x,y_prod2+1400,color="tab:orange",linewidth=2.5,label="Static: 1 pctdprod2 / b3lyp/6-31++g(d,p)")


    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([200,4500])
    ax.set_ylim([0,2500])

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/pctreac_pctprod1_pctprod2_lorentzian.pdf", bbox_inches='tight')
##%%





###############################################################################%%
################### pctreac1_pctreac2_stattic_spectra ###########################################%%
###############################################################################%%
    cp2k_dir=os.environ['CP2K_HOME']
    inp_file_1="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1/pctreac1_277308.out"
    inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac2/pctreac2_277309.out"

    inp_file_3="/home/ar612/Documents/Work/cp2k_organized/ir_spec/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec_reduced/abs_after_secs.txt"
    inp_file_4="/home/ar612/Documents/Work/cp2k_organized/ir_spec/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec_reduced/spectr_17000.dat"


    ener,w_reac1,redm,fcnstmDA,intes_reac1,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_1)
    ener,w_reac2,redm,fcnstmDA,intes_reac2,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(inp_file_2)

    # max_reac1=max(intes_reac1)
    # max_reac2=max(intes_reac2)
    # intes_reac1=intes_reac1/max_reac1
    # intes_reac2=intes_reac2/max_reac2

    xreac1,yreac1=lorentzian_shape(w_reac1,intes_reac1,20)
    xreac2,yreac2=lorentzian_shape(w_reac2,intes_reac2,20)


    exp=np.genfromtxt(inp_file_3)
    theo=np.genfromtxt(inp_file_4)

    find_index(w_reac2,3506)

    ###%%
    fig, ax = plt.subplots(figsize=(16,9))
    plt.rc('font', size=20)
    plt.plot(exp[0,1:],exp[1,1:]/max(exp[1,1:]),color="tab:red",linewidth=2.5,label="Experiment")
    plt.plot(theo[:,0],theo[:,1]/max(theo[:,1])*2,color="tab:blue",linewidth=2.5,label="Dynamic: 1 PHI + 3 CH-ol + 196 THF at 300K : blyp")
    plt.plot(xreac1,yreac1/max(yreac1)+0.5,color="tab:pink",linewidth=2.5,label="Static: 1 pctreac1 / b3lyp/6-31++g(d,p)")
    plt.plot(xreac2,yreac2/max(yreac1)+1,color="tab:orange",linewidth=2.5,label="Static: 1 pctreac2 / b3lyp/6-31++g(d,p)")

    # ax.stem(w_reac1, intes_reac1,linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Static: pctreac1 / b3lyp/6-31++g(d,p)")
    # ax.stem(w_reac2, intes_reac2,linefmt='C1-', markerfmt=' ',use_line_collection=True,label="Static: pctreac2 / b3lyp/6-31++g(d,p)")
    plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
    plt.tick_params(labelsize=14)
    ax.legend(loc='upper left')
    ax.set_xlim([200,4000])
    ax.set_ylim([-0.25,2.5])

    fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/pctreac1_pctreac2_stattic_spectra.pdf", bbox_inches='tight')
##%%





def flat(traj):
    return np.array([ r.flatten() for r in traj])

def modes2Q(modes,atm_names):
    return flat(mol.normal(mol.cartesian2mass_weighted(modes,atm_names))).T


def reorder(r,atm_names):
    ref_order_phenyl=[0,1,2,3,4,5,6,7,8,9,10,11,12,13]
    natm_phenyl=len(ref_order_phenyl)
    ref_order_cyclo=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,15,14,16,18,17]
    natm_cyclo=len(ref_order_cyclo)


    ref_order=ref_order_phenyl+[i+natm_phenyl for i in ref_order_cyclo]+[i+natm_phenyl+natm_cyclo for i in ref_order_cyclo]+[i+natm_phenyl+2*natm_cyclo for i in ref_order_cyclo]
    #1phenyl3cyclo
    #mol1_order=[0,8,1,9,10,32,12,28,14,29,13,31,11,30,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18]
    #1phenyl6cyclo
    #mol1_order=[0,8,1,9,10,32,12,28,14,29,13,31,11,30,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18,98,99,95,94,91,127,121,124,126,125,90,123,122,93,96,92,97,100,101,117,118,114,113,108,112,107,105,102,103,106,109,104,111,115,110,116,119,120,89,88,71,77,72,78,79,74,82,83,76,87,86,75,85,84,73,81,80]
    #24pctreac1
    mol1_order=[0,8,1,9,11,30,13,31,14,29,12,28,10,32,60,61,57,56,51,55,50,48,45,46,49,52,47,54,58,53,59,62,63,41,42,38,37,34,70,64,67,69,68,33,66,65,36,39,35,40,43,44,27,26,2,15,3,16,17,5,20,21,7,25,24,6,23,22,4,19,18]


    nsteps=len(r)
    natm=len(r[0])

    final_r=np.zeros([nsteps,natm,3])
    final_atm_names=[" " for i in range(natm)]

    # ref_order.index(5)
    # mol1_order

    for i in range(nsteps):
        for j in range(natm):
            ordered_index=mol1_order[ref_order.index(j)]
            final_r[i,j]=r[i,ordered_index]
            final_atm_names[j]=atm_names[ordered_index]


    return final_r,final_atm_names


def Ur1_r0(rtraj,r1,r0):

    r1_center=r1-rmsd.centroid(r1)
    r0_center=r0-rmsd.centroid(r0)

    Ur1_r0 = rmsd.kabsch(r1_center,r0_center)

    rfinal=rtraj

    for i,r in enumerate(rtraj):
        rfinal[i]=np.dot(rtraj[i],Ur1_r0)

    return rfinal
###############################################################################%%
#########  static spectrum comparisson ###################%%
###############################################################################%%
cp2k_dir=os.environ['CP2K_HOME']
inp_file_0="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/conf00-normal-modes.mldf"
inp_file_3="/home/ar612/Documents/Work/normcor-1.0/b3lyp_8step_0pctts1irc/conf00-normal-modes.mldf"
inp_file_1="/home/ar612/Documents/Work/normcor-1.0/1phenyl3cyclo_1_thf_196_300K_30A_ir_spec/conf00-normal-modes.mldf"
inp_file_2="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1_83384.out"

w_0,intes_0,modes_0,r_0,atm_names_0=mol.w_modes_r_atm_names_from_molden (inp_file_0)
w_3,intes_3,modes_3,r_3,atm_names_3=mol.w_modes_r_atm_names_from_molden (inp_file_3)
w_1,intes_1,modes_1,r_1,atm_names_1=mol.w_modes_r_atm_names_from_molden (inp_file_1)
w_2,redm2,f2,intes_2,modes_2,r_2,atm_names_2=mol.w_redm_f_intes_modes_r_atm_names_from_gauss(inp_file_2)
modes_2,atm_names_2=reorder(modes_2,atm_names_2)

modes_1=Ur1_r0(modes_1,r_1,r_0)
modes_2=Ur1_r0(modes_2,r_2,r_0)



Q_0=modes2Q(modes_0,atm_names_0)
Q_3=modes2Q(modes_3,atm_names_3)
Q_1=modes2Q(modes_1,atm_names_1)
Q_2=modes2Q(modes_2,atm_names_2)


n_vib=len(Q_2[0])
n_vib

proj=np.zeros([n_vib,n_vib])
for i in range(n_vib):
    for j in range(n_vib):
        proj[i,j]=np.dot(Q_0[:,i+6],Q_2[:,j])

plt.matshow(proj)

np.dot(Q_0[:,-1],Q_2[:,-1])

###%%
fig, ax = plt.subplots(figsize=(16,9))
plt.rc('font', size=20)
ax.stem(w_0, [1 for i in range(len(w_0))],linefmt='C0-', markerfmt=' ',use_line_collection=True,label="Dynamic: pctreac_1_normcoord : b3lyp/6-31++g(d,p)")
#ax.stem(w_3, [1 for i in range(len(w_3))],linefmt='C2-', markerfmt=' ',use_line_collection=True,label="Dynamic: 8_step_pctreac_1_normcoord : b3lyp/6-31++g(d,p)")
ax.stem(w_1, [1 for i in range(len(w_1))],linefmt='C2-', markerfmt=' ',use_line_collection=True,label="Dynamic: 1phenyl3cyclo_1_thf_196_300K_30A_ir_spec : b3lyp/6-31++g(d,p)")
#ax.stem(w_2, [1 for i in range(len(w_2))],linefmt='C2-', markerfmt=' ',use_line_collection=True,label="Static: pctreac_1_static : b3lyp/6-31++g(d,p)")
plt.xlabel('wave_number $(cm^{-1})$',fontweight='bold')
plt.tick_params(labelsize=14)
ax.legend(loc='upper right')
ax.set_xlim([0,4800])
#ax.set_ylim([0,800])
#%%
fig.savefig("/home/ar612/Documents/Work/cp2k_organized/ir_spec/pctreac1_molden_static_ir_spec.pdf", bbox_inches='tight')
