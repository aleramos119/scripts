
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol



#
# def read_fchk(fchk_file):
#
#     mass_0=0
#     mass_f=0
#     hess_0=0
#     hess_f=0
#
#     with open(fchk_file) as file:
#         ## Initializing arrays
#         for n, line in enumerate(file, 1):
#            if 'Number of atoms' in line:
#                n_atm=int(line.split()[-1])
#                n_dof=3*n_atm
#            if 'Real atomic weights' in line:
#                mass_list=np.zeros([0])
#                mass_0=n
#                n_mass_lines=int(np.ceil(n_atm/5.))
#                mass_f=n+n_mass_lines
#            if n > mass_0 and n <= mass_f:
#                mass_list=np.append(mass_list,np.array( [  float(number) for number in line.split()] ))*mol.amu2au_mass
#
#            if 'Cartesian Force Constants' in line:
#                n_fc=int(line.split()[-1])
#                fc_list=np.zeros([0])
#                hess_0=n
#                n_fc_lines=int(np.ceil(n_fc/5.))
#                hess_f=n+n_fc_lines
#            if n > hess_0 and n <= hess_f:
#                fc_list=np.append(fc_list,np.array( [  float(number) for number in line.split()] ))
#
#     hessian=np.zeros([n_dof,n_dof])
#     hessian_mass=np.zeros([n_dof,n_dof])
#
#
#
#     index=0
#     for i in range(n_dof):
#         for j in range(i+1):
#             hessian[i,j]=fc_list[index]
#             hessian[j,i]=hessian[i,j]
#
#
#             hessian_mass[i,j]=hessian[i,j]/np.sqrt(mass_list[int(np.floor(i/3))]*mass_list[int(np.floor(j/3))])
#             hessian_mass[j,i]=hessian_mass[i,j]
#             index=index+1
#
#
#
#     w,v=linalg.eigh(hessian_mass)
#
#     np.sqrt(np.sort(w))*mol.Eh2cm_1


def main (ref_traj_inp_file,emax):


    ##%%
    ## Reading reaction path energy
    e,r,atm_names=mol.e_r_atm_names_from_xyz (ref_traj_inp_file)
    emin=min(e)
    e=e-emin
    e=e*mol.Eh2kcalmol

    index=np.zeros([0])
    e_final=np.zeros([0])

    for i,x in enumerate(e):
        if x < emax:
            e_final=np.append(e_final,x)
            index=np.append(index,i)

    index=np.array([int(i) for i in index])

    r_final=np.array([r[i]  for i in index])


    name_0='.'.join(ref_traj_inp_file.split("/")[-1].split(".")[:-1])
    traj_name=name_0+"_emax_"+str(emax)


    ## Naming the final file
    out_file="/home/ar612/Documents/Work/gaussian/scan_reac_plane/trajectory/"+traj_name+".xyz"

    ## Writing the trajectory to the file
    mol.write_trajectory (r_final,e_final,atm_names,out_file)



###########################################################################%%


if __name__== "__main__":

    ##%%
    ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/aptts2_nx_-2_2_50_ny_-1.5_2.5_50_dftb_eps_10e-6_ref_traj/aptts2_nx_-2_2_50_ny_-1.5_2.5_50_dftb_eps_10e-6_ref_traj-pos-1.xyz"
    emax=80


    ref_traj_inp_file=sys.argv[1]
    emax=sys.argv[2]

    main(ref_traj_inp_file,emax)
