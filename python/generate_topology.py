
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import read_ifp
import numpy as np
from itertools import islice

#
# def mass_list(atm_names_sys):
#     return np.array([ mol.mass[atm] for atm in atm_names_sys])
# def xyz_name2g96_name(xyz_name):
#
#     g96_names=[]
#     atom_types=[]
#     atom_count=[]
#
#     for name in xyz_name:
#         if name in atom_types:
#             index=atom_types.index(name)
#             atom_count[index]+=1
#             g96_names.append(name+str(atom_count[index]))
#         else:
#             atom_types.append(name)
#             atom_count.append(1)
#             g96_names.append(name+str(1))
#
#     return np.array(g96_names)
#
# def g96_name2xyz_name(g96_name):
#     return np.array([mol.remove_digits(name) for name in g96_name])
#
# def types_list(xyz_name,select,sol_name):
#
#     atom_types_list=[]
#     for name in xyz_name:
#         if name == "O" and select==sol_name:
#             atom_types_list.append(1)
#         if name == "C":
#             atom_types_list.append(2)
#         if name == "H":
#             atom_types_list.append(3)
#         if name == "O" and select=="sys":
#             atom_types_list.append(4)
#         if name == "N":
#             atom_types_list.append(5)
#
#     return np.array(atom_types_list)
# def read_topology(top_file):
#
#     nmat=-10
#     cgm_line=0
#     n_atm=0
#     with open(top_file) as file:
#        for n, line in enumerate(file, 1):
#            if 'NMAT' in line:
#                nmat=n
#            if n==nmat+1:
#                n_atm=int(line.split()[0])
#                cg=np.zeros(n_atm)
#                ine=np.zeros(n_atm)
#                icg=np.zeros(n_atm)
#                jne=[]
#            if 'CGM' in line:
#                cgm_line=n
#            if n>cgm_line and n<=cgm_line+n_atm:
#                i=n-cgm_line-1
#                split=line.split()
#
#                cg[i]=float(split[4])
#                icg[i]=int(split[5])
#                ine[i]=int(split[6])
#                jne.append(np.array([int(k) for k in split[7:]]))
#     return cg,icg,ine,jne
#
# def read_info_packmol(packmol_inp_file,sol_name):
#     nsol0=-1
#     with open(packmol_inp_file) as file:
#        for n, line in enumerate(file, 1):
#            if 'output' in line:
#                packmol_out=line.split()[-1]
#            if 'structure' in line and sol_name in line:
#                nsol0=n
#            if n==nsol0+1:
#                n_sol=int(line.split()[-1])
#     return n_sol,packmol_out




def write_title(top_out,block_list):
    ff_list=[ block.ff for block in block_list]
    with open(top_out, 'w') as f:
        f.write("########################################################\n")
        f.write("######################  TITLE  #########################\n")
        f.write("########################################################\n")
        f.write("TITLE\n")
        f.write("This is the topology file "+top_out+" in G96 format. Force field " + " ".join(ff_list) + "\n")
        f.write("END\n")

def write_topphyscon(top_out,FPEPSI,HBAR):
    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("######################  TOPPHYSCON  ####################\n")
        f.write("########################################################\n")
        f.write("TOPPHYSCON\n")
        f.write("# FPEPSI: 1.0/(4.0*PI*EPS0) (EPS0 is the permitivity of vacuum)\n")
        f.write("0.1389354E+03\n")
        f.write("# HBAR: Planck's constant HBAR = H/(2* PI)\n")
        f.write("0.6350780E-01\n")
        f.write("END\n")

def write_topversion(top_out):
    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   TOPVERSION    #####################\n")
        f.write("########################################################\n")
        f.write("TOPVERSION\n")
        f.write("1.7\n")
        f.write("END\n")

def write_atomtypename(top_out,block_list,ifp):



    lj_atom_type_code=list(set([iacm for block in block_list for iacm in block.IACM]))
    lj_atom_type=[ifp.TYPE[i-1] for i in lj_atom_type_code]

    NRATT=len(lj_atom_type)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   ATOMTYPENAME    ###################\n")
        f.write("########################################################\n")
        f.write("ATOMTYPENAME\n")
        f.write("#Van de Waals atom type sequence and name block\n")
        f.write("#NRATT number of van der Waals atom types\n")
        f.write(str(NRATT)+"\n\n")
        for type in lj_atom_type:
            f.write(type+"\n")
        f.write("END\n")

    return lj_atom_type_code

def write_ljparmeters(top_out,lj_atom_type_code,ifp):

    lj_index=[code-1 for code in lj_atom_type_code]

    NRATT=len(lj_atom_type_code)
    NRATT2=NRATT*(NRATT+1)//2

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   LJPARAMETERS    ###################\n")
        f.write("########################################################\n")
        f.write("LJPARAMETERS\n")
        f.write("#Van de Waals (Lennard-Jones) interaction block\n")
        f.write("# NRATT2= NRATT*(NRATT+1)/2 number of LJ interaction types\n")
        f.write(str(NRATT2)+"\n")

        f.write("#It's assumed that i<j and that j appears in ascending order\n")
        f.write("#IAC JAC     C12              C6            CS12          CS6\n")

        for j in range(NRATT):
            for i in range(j+1):
                C6ij=ifp.SQRT_C6[lj_index[i]]*ifp.SQRT_C6[lj_index[j]]   ## No sqrt is needed because sqrt(C6) parameters are stored in the .ifp file. The formula (2.22) in gromos "Force Field and Topology Data Set" is wrong.
                select_c12ii=ifp.SELECT_SQRT_C12[lj_index[i]][lj_index[j]]-1 ##select_c12ii is 0, 1 or 2 depending on the matrix element (I,J)
                select_c12jj=ifp.SELECT_SQRT_C12[lj_index[j]][lj_index[i]]-1 ##It is similar to select_c12ii but uses matrix elemt (J,I) instead of (I,J). Table 8, from DOI:10.1002/jcc.20090

                C12ij=ifp.SQRT_C12[lj_index[i]][select_c12ii]*ifp.SQRT_C12[lj_index[j]][select_c12jj] ## No sqrt is needed because sqrt(C12) parameters are stored in the .ifp file. The formula (2.23) in gromos "Force Field and Topology Data Set" is wrong.

                CS6ij=ifp.SQRT_CS6[lj_index[i]]*ifp.SQRT_CS6[lj_index[j]]
                CS12ij=ifp.SQRT_CS12[lj_index[i]]*ifp.SQRT_CS12[lj_index[j]]
                f.write("  %i   %i   %.6E   %.6E   %.6E   %.6E \n"%(i+1,j+1,C12ij,C6ij,CS12ij,CS6ij))
        f.write("END\n")

def write_resname(top_out,block_file_list):

    residue_name=[block_file.split("/")[-1].split(".")[-2].split("_")[-2].upper() for block_file in block_file_list]

    NRAA2=len(residue_name)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   RESNAME    #####################\n")
        f.write("########################################################\n")
        f.write("RESNAME\n")
        f.write("#Solute residue sequence and name block\n")
        f.write("#NRAA2 number of residues\n")
        f.write(str(NRAA2)+"\n")
        f.write("#AANM: residue names\n")
        for residue in residue_name:
            f.write(residue+"\n")
        f.write("END\n")

    return residue_name

def write_soluteatom(top_out,lj_atom_type_code,nmol_list,block_list,ifp,residue_name):

    nblocks=len(block_list)

    g96_atm_names_list=[block.ANM for block in block_list]
    natm_list=[len(atm_names) for atm_names in g96_atm_names_list]

    NRP_total=sum([nmol_list[i]*natm_list[i] for i in range(nblocks)])

    lj_atom_type=[ifp.TYPE[i-1] for i in lj_atom_type_code]

    atom_name_list=[]
    residue_name_list=[]


    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("###################  SOLUTEATOM    #####################\n")
        f.write("########################################################\n")
        f.write("SOLUTEATOM\n")
        f.write("#Solute atom information block\n")
        f.write("#Number of atoms\n")
        f.write(str(NRP_total)+"\n")
        f.write("# ATNM: atom number  MRES: residue number  PANM: atom name of solute atom\n")
        f.write("# IAC: integer (van der Waals) atom type code  MASS: mass of solute atom\n")
        f.write("#CG: charge of solute atom  IGC: charge group code (0 or 1)\n")
        f.write("#INE: number of excluded atoms for non-bonded interactions\n")
        f.write("#JNE: indexes of atoms excluded from non-bonded interactions for atom i (i<j and j in ascending order)\n")
        f.write("# JNE14: indexes of atoms included for 1-4 LJ interactions for atom i (i<j and j in ascending order)\n")
        f.write(" %6s  %6s  %6s  %6s  %10s  %10s  %6s  %6s  %-30s  %6s  %-40s \n" % ("#ATNM","MRES","PANM","IAC","MASS","CG","IGC","INE","JNE","INE14","JNE14"))

        n=0
        for k in range(nblocks):
            for i in range(nmol_list[k]):
                ncomp=n
                for j in range(natm_list[k]):

                    atnm=n+1
                    mres=k+1
                    panm=g96_atm_names_list[k][j]
                    atom_name_list.append(panm)
                    residue_name_list.append(residue_name[k])
                    iac=lj_atom_type.index(ifp.TYPE[block_list[k].IACM[j]-1])+1
                    mass=ifp.ATMAS[ifp.N.index(block_list[k].MASS[j])]
                    cg=block_list[k].CGM[j]
                    igc=1
                    ine=block_list[k].MAE[j]
                    jne=" ".join([str(msae + ncomp)  for msae in block_list[k].MSAE[j]])
                    ine14=0
                    jne14=""

                    f.write(" %6i  %6i  %6s  %6i  %10.4f  %10.4f  %6i  %6i  %-40s   \n" % (atnm,mres,panm,iac,mass,cg,igc,ine,jne))
                    f.write("                                                                                                           %3i       %20s  \n" % (ine14,jne14))
                    n=n+1


        f.write("END\n")

        return atom_name_list,residue_name_list



def write_bondtype(top_out,block_list,ifp):

    bond_code=list(set([mcb_nb for block in block_list for mcb_nb in block.MCB_NB]))

    NBTY=len(bond_code)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   BONDTYPE   #####################\n")
        f.write("########################################################\n")
        f.write("BONDTYPE\n")
        f.write("#Bond interaction type block\n")
        f.write("# NBTY: number of covalent bond types\n")
        f.write(str(NBTY)+"\n")
        f.write("# CB: force constant  B0: Bond lenght\n")
        for code in bond_code:
            f.write("%15e   %15e \n" % (ifp.CB[code-1],ifp.B0[code-1]))
        f.write("END\n")

    return bond_code

def write_bondh(top_out,nmol_list,block_list,bond_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]



        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NB[k] for k in range(len(block.MCB_NB))  if block.ANM[block.I_NB[k][0]-1][0] == "H" or block.ANM[block.I_NB[k][1]-1][0] == "H" ]  for block in block_list]
        h_code_list=[[bond_code.index(block.MCB_NB[k])+1  for k in range(len(block.MCB_NB))  if block.ANM[block.I_NB[k][0]-1][0] == "H" or block.ANM[block.I_NB[k][1]-1][0] == "H" ]  for block in block_list ]


        NBONH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BONDH   ########################\n")
            f.write("########################################################\n")
            f.write("BONDH\n")
            f.write("#Solute bonds involving H atoms\n")
            f.write("# NBONH: number of bonds involving H atoms in solute\n")
            f.write(str(NBONH)+"\n")
            f.write("#  IBH, JBH: atom sequence numbers of atoms forming a bond  ICBH: bond type code\n")
            f.write("#    IBH     JBH     ICBH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i \n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")

def write_bond(top_out,nmol_list,block_list,bond_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]


        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NB[k] for k in range(len(block.MCB_NB))  if block.ANM[block.I_NB[k][0]-1][0] != "H" and block.ANM[block.I_NB[k][1]-1][0] != "H" ]  for block in block_list]
        h_code_list=[[bond_code.index(block.MCB_NB[k])+1  for k in range(len(block.MCB_NB))  if block.ANM[block.I_NB[k][0]-1][0] != "H" and block.ANM[block.I_NB[k][1]-1][0] != "H" ]  for block in block_list ]


        NBON=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BOND   ########################\n")
            f.write("########################################################\n")
            f.write("BOND\n")
            f.write("#Solute bonds involving H atoms\n")
            f.write("# NBON: number of bonds NOT involving H atoms in solute\n")
            f.write(str(NBON)+"\n")
            f.write("#  IB, JB: atom sequence numbers of atoms forming a bond  ICB: bond type code\n")
            f.write("#    IB     JB     ICB\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i \n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")



def write_bondangletype(top_out,block_list,ifp):

    bond_angle_code=list(set([mcb_nba for block in block_list for mcb_nba in block.MCB_NBA]))

    NBTTY=len(bond_angle_code)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   BONDANGLETYPE   ################\n")
        f.write("########################################################\n")
        f.write("BONDANGLETYPE\n")
        f.write("#Bond angle interaction type block\n")
        f.write("# NBTTY: number of covalent bond angle types\n")
        f.write(str(NBTTY)+"\n")
        f.write("# CT: Force constant  T0: Ideal bond angle\n")
        for code in bond_angle_code:
            f.write("%15e   %15e \n" % (ifp.CT[code-1],ifp.T0[code-1]))
        f.write("END\n")

    return bond_angle_code

def write_bondangleh(top_out,nmol_list,block_list,bond_angle_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NBA[k] for k in range(len(block.MCB_NBA))  if block.ANM[block.I_NBA[k][0]-1][0] == "H" or block.ANM[block.I_NBA[k][1]-1][0] == "H" or block.ANM[block.I_NBA[k][2]-1][0] == "H" ]  for block in block_list]
        h_code_list=[[bond_angle_code.index(block.MCB_NBA[k])+1 for k in range(len(block.MCB_NBA))  if block.ANM[block.I_NBA[k][0]-1][0] == "H" or block.ANM[block.I_NBA[k][1]-1][0] == "H" or block.ANM[block.I_NBA[k][2]-1][0] == "H" ]  for block in block_list]


        NTHEH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BONDANGLEH   ##################\n")
            f.write("########################################################\n")
            f.write("BONDANGLEH\n")
            f.write("#Solute bonds angles involving H atoms\n")
            f.write("# NTHEH: number of bonds involving H atoms in solute\n")
            f.write(str(NTHEH)+"\n")
            f.write("#  IBTH, JTH, KTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i   %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")

def write_bondangle(top_out,nmol_list,block_list,bond_angle_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NBA[k] for k in range(len(block.MCB_NBA))  if block.ANM[block.I_NBA[k][0]-1][0] != "H" and block.ANM[block.I_NBA[k][1]-1][0] != "H" and block.ANM[block.I_NBA[k][2]-1][0] != "H" ]  for block in block_list]
        h_code_list=[[bond_angle_code.index(block.MCB_NBA[k])+1 for k in range(len(block.MCB_NBA))  if block.ANM[block.I_NBA[k][0]-1][0] != "H" and block.ANM[block.I_NBA[k][1]-1][0] != "H" and block.ANM[block.I_NBA[k][2]-1][0] != "H" ]  for block in block_list]


        NTHEH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BONDANGLE   ##################\n")
            f.write("########################################################\n")
            f.write("BONDANGLE\n")
            f.write("#Solute bonds angles NOT involving H atoms\n")
            f.write("# NTHEH: number of bonds NOT involving H atoms in solute\n")
            f.write(str(NTHEH)+"\n")
            f.write("#  IBTH, JTH, KTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i   %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")




def write_impdihedraltype(top_out,block_list,ifp):

    bond_imp_dih_code=list(set([mcb_nb for block in block_list for mcb_nb in block.MCB_NIDA]))

    NQTY=len(bond_imp_dih_code)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   IMPDIHEDRALTYPE   ##############\n")
        f.write("########################################################\n")
        f.write("IMPDIHEDRALTYPE\n")
        f.write("#Improper (harmonic) dihedral angle interaction type block\n")
        f.write("# NQTY: number of covalent improper (harmonic) dihedral angle types\n")
        f.write(str(NQTY)+"\n")
        f.write("# CP: Force constant  B0: Ideal improper dihedral angle\n")
        for code in bond_imp_dih_code:
            f.write("%15e   %15e \n" % (ifp.CQ[code-1],ifp.Q0[code-1]))
        f.write("END\n")

    return bond_imp_dih_code

def write_impdihedralh(top_out,nmol_list,block_list,bond_imp_dih_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NIDA[k] for k in range(len(block.MCB_NIDA))  if block.ANM[block.I_NIDA[k][0]-1][0] == "H" or block.ANM[block.I_NIDA[k][1]-1][0] == "H" or block.ANM[block.I_NIDA[k][2]-1][0] == "H" or block.ANM[block.I_NIDA[k][3]-1][0] == "H"]  for block in block_list]
        h_code_list=[[bond_imp_dih_code.index(block.MCB_NIDA[k])+1 for k in range(len(block.MCB_NIDA))  if block.ANM[block.I_NIDA[k][0]-1][0] == "H" or block.ANM[block.I_NIDA[k][1]-1][0] == "H" or block.ANM[block.I_NIDA[k][2]-1][0] == "H" or block.ANM[block.I_NIDA[k][3]-1][0] == "H"]  for block in block_list]

        NQHIH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   IMPDIHEDRALH   ################\n")
            f.write("########################################################\n")
            f.write("IMPDIHEDRALH\n")
            f.write("#Improper (harmonic) dihedral angle interaction type block\n")
            f.write("# NQHIH: number of covalent improper (harmonic) dihedral angles involving H\n")
            f.write(str(NQHIH)+"\n")
            f.write("#  IBTH, JTH, KTH, LTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH     LTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i    %5i    %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_index_list[k][j][3]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")

def write_impdihedral(top_out,nmol_list,block_list,bond_imp_dih_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NIDA[k] for k in range(len(block.MCB_NIDA))  if block.ANM[block.I_NIDA[k][0]-1][0] != "H" and block.ANM[block.I_NIDA[k][1]-1][0] != "H" and block.ANM[block.I_NIDA[k][2]-1][0] != "H" and block.ANM[block.I_NIDA[k][3]-1][0] != "H"]  for block in block_list]
        h_code_list=[[bond_imp_dih_code.index(block.MCB_NIDA[k])+1 for k in range(len(block.MCB_NIDA))  if block.ANM[block.I_NIDA[k][0]-1][0] != "H" and block.ANM[block.I_NIDA[k][1]-1][0] != "H" and block.ANM[block.I_NIDA[k][2]-1][0] != "H" and block.ANM[block.I_NIDA[k][3]-1][0] != "H"]  for block in block_list]

        NQHIH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   IMPDIHEDRAL   ################\n")
            f.write("########################################################\n")
            f.write("IMPDIHEDRAL\n")
            f.write("#Improper (harmonic) dihedral angle interaction type block\n")
            f.write("# NQHI: number of covalent improper (harmonic) dihedral angles NOT involving H\n")
            f.write(str(NQHIH)+"\n")
            f.write("#  IBTH, JTH, KTH, LTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH     LTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i    %5i    %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_index_list[k][j][3]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")




def write_dihedraltype(top_out,block_list,ifp):

    bond_dih_code=list(set([mcb_nb for block in block_list for mcb_nb in block.MCB_NDA]))

    NQTY=len(bond_dih_code)

    with open(top_out, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   DIHEDRALTYPE   ##############\n")
        f.write("########################################################\n")
        f.write("DIHEDRALTYPE\n")
        f.write("#Dihedral angle interaction type block\n")
        f.write("# NQTY: number of covalent dihedral angle types\n")
        f.write(str(NQTY)+"\n")
        f.write("# CP: Force constant  B0: Ideal dihedral angle\n")
        for code in bond_dih_code:
            f.write("%15e   %15f   %5i\n" % (ifp.CP[code-1],ifp.PD[code-1],ifp.NP[code-1]))
        f.write("END\n")

    return bond_dih_code

def write_dihedralh(top_out,nmol_list,block_list,bond_dih_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NDA[k] for k in range(len(block.MCB_NDA))  if block.ANM[block.I_NDA[k][0]-1][0] == "H" or block.ANM[block.I_NDA[k][1]-1][0] == "H" or block.ANM[block.I_NDA[k][2]-1][0] == "H" or block.ANM[block.I_NDA[k][3]-1][0] == "H"]  for block in block_list]
        h_code_list=[[bond_dih_code.index(block.MCB_NDA[k])+1 for k in range(len(block.MCB_NDA))  if block.ANM[block.I_NDA[k][0]-1][0] == "H" or block.ANM[block.I_NDA[k][1]-1][0] == "H" or block.ANM[block.I_NDA[k][2]-1][0] == "H" or block.ANM[block.I_NDA[k][3]-1][0] == "H"]  for block in block_list]

        NQHIH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   DIHEDRALH   ################\n")
            f.write("########################################################\n")
            f.write("DIHEDRALH\n")
            f.write("#Dihedral angle interaction type block\n")
            f.write("# NQHIH: number of covalent dihedral angles involving H\n")
            f.write(str(NQHIH)+"\n")
            f.write("#  IBTH, JTH, KTH, LTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH     LTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i    %5i    %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_index_list[k][j][3]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")

def write_dihedral(top_out,nmol_list,block_list,bond_dih_code):

        nblocks=len(block_list)
        natm_list=[len(block.ANM) for block in block_list]

        ### This line selects which of the bond codes envolve hydrogen atoms
        h_index_list=[[block.I_NDA[k] for k in range(len(block.MCB_NDA))  if block.ANM[block.I_NDA[k][0]-1][0] != "H" and block.ANM[block.I_NDA[k][1]-1][0] != "H" and block.ANM[block.I_NDA[k][2]-1][0] != "H" and block.ANM[block.I_NDA[k][3]-1][0] != "H"]  for block in block_list]
        h_code_list=[[bond_dih_code.index(block.MCB_NDA[k])+1 for k in range(len(block.MCB_NDA))  if block.ANM[block.I_NDA[k][0]-1][0] != "H" and block.ANM[block.I_NDA[k][1]-1][0] != "H" and block.ANM[block.I_NDA[k][2]-1][0] != "H" and block.ANM[block.I_NDA[k][3]-1][0] != "H"]  for block in block_list]

        NQHIH=sum([nmol_list[k]*len(h_code_list[k]) for k in range(nblocks)])

        with open(top_out, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   DIHEDRAL   ################\n")
            f.write("########################################################\n")
            f.write("DIHEDRAL\n")
            f.write("#Dihedral angle interaction type block\n")
            f.write("# NQHI: number of covalent dihedral angles NOT involving H\n")
            f.write(str(NQHIH)+"\n")
            f.write("#  IBTH, JTH, KTH, LTH: atom sequence numbers of atoms forming a bond  ICTH: bond angle type code\n")
            f.write("#    IBTH     JTH     KTH     LTH    ICTH\n")
            disp=0
            for k in range(nblocks):
                for i in range (nmol_list[k]):
                    for j in range(len(h_code_list[k])):
                        f.write(" %5i    %5i    %5i    %5i    %5i\n" % (h_index_list[k][j][0]+disp,h_index_list[k][j][1]+disp,h_index_list[k][j][2]+disp,h_index_list[k][j][3]+disp,h_code_list[k][j]))
                    disp=disp+natm_list[k]
            f.write("END\n")



def write_solventatom(output_file):

        NRAM=0

        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   SOLVENTATOM   ####################\n")
            f.write("########################################################\n")
            f.write("SOLVENTATOM\n")
            f.write("#NRAM: Number of solvent atoms in one molecule\n")
            f.write(str(NRAM)+"\n")
            f.write("END\n")


def write_g96_coordenates(residue_name_list,atom_name_list,r_full,nmol_list,block_list,g96_output_file):

    r_full=r_full*mol.A2nm

    nblocks=len(block_list)
    natm_list=[len(block.ANM) for block in block_list]

    with open(g96_output_file, 'w') as f:
        f.write("TITLE\n")
        f.write("This is the coordinate file for "+g96_output_file + " in g96 format\n")
        f.write("END\n")
        f.write("POSITION\n")
        n=0
        nmol=0
        for k in range(nblocks):
            for i in range(nmol_list[k]):
                for j in range(natm_list[k]):
                    f.write(" %6i  %6s  %6s  %6i  %15f  %15f  %15f \n" % (nmol+1,residue_name_list[n],atom_name_list[n],n+1,r_full[n,0],r_full[n,1],r_full[n,2]))
                    n=n+1
                nmol=nmol+1
        f.write("END\n")

############################################################################%%


def main (nmol_list,block_file_list,ifp_file,full_sys_xyz,top_out):

    ## Sys data
    block_list=[read_ifp.build_block(block_file) for block_file in block_file_list]

    ## Parameter file
    ifp=read_ifp.ifp(ifp_file)



    ##%%
    write_title(top_out,block_list)

    write_topphyscon(top_out,block_list[-1].FPEPSI,block_list[-1].HBAR)

    write_topversion(top_out)


    lj_atom_type_code=write_atomtypename(top_out,block_list,ifp)

    write_ljparmeters(top_out,lj_atom_type_code,ifp)

    residue_name=write_resname(top_out,block_file_list)

    atom_name_list,residue_name_list=write_soluteatom(top_out,lj_atom_type_code,nmol_list,block_list,ifp,residue_name)

    bond_code=write_bondtype(top_out,block_list,ifp)

    write_bondh(top_out,nmol_list,block_list,bond_code)

    write_bond(top_out,nmol_list,block_list,bond_code)

    bond_angle_code=write_bondangletype(top_out,block_list,ifp)

    write_bondangleh(top_out,nmol_list,block_list,bond_angle_code)

    write_bondangle(top_out,nmol_list,block_list,bond_angle_code)

    bond_imp_dih_code=write_impdihedraltype(top_out,block_list,ifp)

    write_impdihedralh(top_out,nmol_list,block_list,bond_imp_dih_code)

    write_impdihedral(top_out,nmol_list,block_list,bond_imp_dih_code)
    #
    bond_dih_code=write_dihedraltype(top_out,block_list,ifp)

    write_dihedralh(top_out,nmol_list,block_list,bond_dih_code)

    write_dihedral(top_out,nmol_list,block_list,bond_dih_code)

    write_solventatom(top_out)

    e_full,r_full,atm_names_full=mol.e_r_atm_names_from_xyz(full_sys_xyz,ener="no")
    r_full=r_full[-1]
    full_sys_list=full_sys_xyz.split(".")
    full_sys_list[-1]="g96"
    g96_output_file=".".join(full_sys_list)

    write_g96_coordenates(residue_name_list,atom_name_list,r_full,nmol_list,block_list,g96_output_file)
###########################################################################%%


if __name__== "__main__":

    # nmol_list=[1,3]
    # block_file_list=["/home/ar612/Documents/Work/cp2k_organized/top/apt_g96.top","/home/ar612/Documents/Work/cp2k_organized/top/chl_g96.top"]
    #
    # ifp_file="/home/ar612/Documents/Work/cp2k_organized/top/54a7_ATB.ifp"
    # full_sys_xyz="/home/ar612/Documents/Work/cp2k_organized/equilibration/mm/apt_1_chl_3_300K_30A_test1_md/apt_1_chl_3.xyz"
    # top_out="/home/ar612/Documents/Work/cp2k_organized/equilibration/mm/apt_1_chl_3_300K_30A_test1_md/apt_1_chl_3_g96.top"


    narg=len(sys.argv)
    nblocks=(narg-4)//2


    nmol_list=[ int(sys.argv[2*i+1]) for i in range(nblocks)]
    block_file_list=[ sys.argv[2*i+2] for i in range(nblocks)]


    ifp_file=sys.argv[-3]
    full_sys_xyz=sys.argv[-2]
    top_out=sys.argv[-1]

    


    main(nmol_list,block_file_list,ifp_file,full_sys_xyz,top_out)
