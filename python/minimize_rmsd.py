
#%%
import numpy as np
import sys
import mol
import rmsd


############################################################################%%
## The functions transforms r2 to minimize rmsd respect to r1
def minimize_rmsd (r1,r2):

    n_atm1=len(r1)
    n_atm2=len(r2)

    ## Checking that both structures have the same number of atoms
    if n_atm1 != n_atm2:
        print("****** ERROR ******* minimize_rmsd: The structures have not the same number of atoms")
    else:
        cent_r1 = rmsd.centroid(r1)
        cent_r2 = rmsd.centroid(r2)

        r1_centered = r1-cent_r1
        r2_centered = r2-cent_r2

        ## Rotating r2 to fit r1
        U = rmsd.kabsch(r2_centered, r1_centered)
        r2_centered=np.dot(r2_centered,U)

        ## Translating to match the center of mass of r1
        r2_centered += cent_r1

        return r2_centered



def main (r1_xyz_file,r2_xyz_file):


    ##%%
    ## Reading cartesian structures
    e1,r1,atm_names=mol.e_r_atm_names_from_xyz(r1_xyz_file,ener="no")
    e2,r2,atm_names=mol.e_r_atm_names_from_xyz(r2_xyz_file,ener="no")

    r1=r1[0]
    r2=r2[0]

    r2_trans=minimize_rmsd(r1,r2)

    ## Writing the trajectory to the file
    mol.write_trajectory (np.array([r2_trans]),np.array(np.zeros(1)),atm_names,r2_xyz_file)
























###########################################################################%%


if __name__== "__main__":


    # r1_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/test/r20.xyz"
    # r2_xyz_file="/home/ar612/Documents/cp2k_organized/XYZ/test/pmf2.xyz"

    r1_xyz_file=sys.argv[1]
    r2_xyz_file=sys.argv[2]




    main(r1_xyz_file,r2_xyz_file)
