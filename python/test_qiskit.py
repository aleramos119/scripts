
##%%
import numpy as np
import qiskit
from qiskit import(IBMQ,
  QuantumCircuit,
  execute,
  Aer)
from qiskit.visualization import plot_histogram
##%%

qiskit.__qiskit_version__
##%%

# Use Aer's qasm_simulator
simulator = Aer.get_backend('qasm_simulator')

# Create a Quantum Circuit acting on the q register
circuit = QuantumCircuit(2, 2)

# Add a H gate on qubit 0
circuit.h(0)

# Add a CX (CNOT) gate on control qubit 0 and target qubit 1
circuit.cx(0, 1)

# Map the quantum measurement to the classical bits
circuit.measure([0,1],[0,1])

# Execute the circuit on the qasm simulator
job = execute(circuit, simulator, shots=10000)

# Grab results from the job
result = job.result()

# Returns counts
counts = result.get_counts(circuit)
print("\nTotal count for 00 and 11 are:",counts)

plot_histogram(counts)
# Draw the circuit
circuit.draw()





## To run in IBMQ
from qiskit.tools.monitor import job_monitor
IBMQ.save_account('89906eb25d9d73641a3bed22a82a42312cd134d1b9335e0736d312b29400dc2c895855ad590e19ef5e69b1e2ed6c2dde334bc1ab70b999afbaaff2dc7703d9f3',overwrite=True)
provider = IBMQ.load_account()

device = provider.get_backend('ibmq_valencia')
job = execute(circuit, backend = device)

job_monitor(job)


job.

result = job.result()

counts= result.get_counts(circuit)

print (counts)


plot_histogram(counts)
