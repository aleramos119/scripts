#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])

%matplotlib qt

import numpy as np

xyz="/home/ar612/Documents/Work/cp2k_organized/equilibration/mm/1phenyl6cyclo_1_thf_1_300K_30A_md/1phenyl6cyclo_1_thf_1_300K_30A_md-pos-1.xyz"
xyz="/home/ar612/Documents/Work/cp2k_organized/equilibration/mm/1phenyl6cyclo_1_thf_1_300K_30A_md/1phenyl6cyclo_1_thf_1.xyz"

#%%
e,r,atm_names=mol.e_r_atm_names_from_xyz(xyz,ener="n")

natm=len(r[0])

dist=[]

for i in range(natm):
    for j in range(i):
        dist.append(np.linalg.norm(r[0,i]-r[0,j]))


min(dist)
plt.hist(dist)

#%%
