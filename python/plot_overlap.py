
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

def get_xf(inp_file):
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'xf' in line and 'pxf' not in line:
               xf=float(line.split()[0])
    return xf


def plot_overlap(project_name,cross_pattern,oct_states_pattern,qdq_pattern,project_pattern,out_dir_pattern,final_out_dir,t0,tf,m0,mf,eig0,eigf,a,vmax,mH):

    t_list=range(t0,tf+1)
    m_list=list(range(m0,mf+1))

    final_overlap=np.zeros([len(t_list),len(m_list)])
    err=np.zeros([len(t_list),len(m_list)])

    for it,t in enumerate(t_list):
        for jm,m in enumerate(m_list):
            inp_file_pattern=out_dir_pattern+"/"+project_pattern+"/"+cross_pattern
            out_file_pattern=out_dir_pattern+"/"+project_pattern+"_overlap.pdf"
            out_file=out_file_pattern.replace("$$",str(m)).replace("%%",str(t))

            qdq_file=qdq_pattern.replace("$$",str(m)).replace("%%",str(t))
            oct_states_file=oct_states_pattern.replace("$$",str(m)).replace("%%",str(t))

            xf=get_xf(xf_file_pattern.replace("$$",str(m)).replace("%%",str(t)))
            oct_x=np.genfromtxt(oct_states_file)[-1,-2]
            quant_x=np.genfromtxt(qdq_file)[-1,1]

            err[it,jm]=np.abs(xf-quant_x)/a



    T_3_2=np.array([ (3/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_4_2=np.array([ (4/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_5_2=np.array([ (5/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_6_2=np.array([ (6/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_7_2=np.array([ (7/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])

    ##%%
    lsize=40
    plt.rc('font', size=lsize)
    t=(np.append(t_list,t_list[-1]+1)-0.5)*1000
    m=np.append(m_list,m_list[-1]+1)-0.5
    fig=plt.figure(figsize=(16, 9))
    ax = fig.add_subplot(111)
    bar=ax.pcolormesh(t,m,err.T,vmin=0,vmax=2,alpha=0.9,cmap='coolwarm')
    ax.plot(T_3_2[2:],m_list[2:],color="tab:green",linewidth=5)
    ax.plot(T_4_2[1:],m_list[1:],color="tab:red",linewidth=5)
    ax.plot(T_5_2,m_list,color="tab:green",linewidth=5)
    ax.plot(T_6_2,m_list,color="tab:red",linewidth=5)
    ax.plot(T_7_2[:-1],m_list[:-1],color="tab:green",linewidth=5)
    fig.colorbar(bar)
    ax.set_xlabel(r'$t_f ~(au)$',size=lsize)
    #ax.set_xlabel('T_2',fontweight='bold')
    ax.set_ylabel(r'$m ~(m_H)$',size=lsize)
    ax.tick_params(labelsize=lsize)
    fig.savefig(final_out_dir+project_name+".pdf", bbox_inches='tight')
    ##%%

if __name__== "__main__":

    #quatics_dir="/home/ar612/Documents/quantics"
    ##%%
    quatics_dir=os.environ['QUANTICS_HOME']
    oct_dir=os.environ['OCTGAUSS_HOME']

    os.environ['QUANTICS_HOME']
    cross_pattern="cross_eig_&&"
    project_pattern="$$_hmass"
    time_pattern="%%dT"

    project_name="hermite_2000_k_int_scl_optimal"
    subdir="/control/double_well/"+project_name+"/"


    final_out_dir=quatics_dir+subdir

    out_dir_pattern=quatics_dir+subdir+time_pattern+"/"+project_pattern
    oct_states_pattern=oct_dir+subdir+time_pattern+"/"+project_pattern+"/x.dat"
    xf_file_pattern=oct_dir+subdir+time_pattern+"/"+project_pattern+"/"+project_pattern+".inp"
    qdq_pattern=out_dir_pattern+"/"+project_pattern+"/qdq_1_1.pl"

    a=2
    vmax=0.01
    mH=1837.18

    t0=5
    tf=20

    m0=1
    mf=10

    eig0=1
    eigf=40
    ##%%
    plot_overlap(project_name,cross_pattern,oct_states_pattern,qdq_pattern,project_pattern,out_dir_pattern,final_out_dir,t0,tf,m0,mf,eig0,eigf,a,vmax,mH)
