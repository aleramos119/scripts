## This is a script to generate a xyz trajectory, given the forward and reverse trajectory of gaussian irc output files
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np





###########################################################################%%


if __name__== "__main__":

    ## Reading the forward and reverse trajectory of gaussian irc output files
    e0phe,point0phe,Ephe,Hphe,Gphe=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/phenyl_reduced/83888.out")
    e0cyclo,point0cyclo,Ecyclo,Hcyclo,Gcyclo=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/cyclo_reduced/84070.out")
    e0r3,point0r3,Er3,Hr3,Gr3=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/r3_reduced/83885.out")
    e01phe2cycl,point01phe2cycl,E1phe2cycl,H1phe2cycl,G1phe2cycl=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/1phenyl2cyclo_reduced/83887.out")
    e02phe1cycl,point02phe1cycl,E2phe1cycl,H2phe1cycl,G2phe1cycl=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/2phenyl1cyclo_reduced/85328.out")
    e01phe3cycl,point01phe3cycl,E1phe3cycl,H1phe3cycl,G1phe3cycl=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1_83384.out")
    e02phe,point02phe,E2phe,H2phe,G2phe=mol.thermo_from_gauss("/home/ar612/Documents/Work/gaussian/geo_opt/2phenyl_reduced/84071.out")

    ##%%
    (Er3-Ephe-Ecyclo)*mol.Eh2kcalmol
    (Hr3-Hphe-Hcyclo)*mol.Eh2kcalmol
    (Gr3-Gphe-Gcyclo)*mol.Eh2kcalmol

    3*(Er3-Ephe-Ecyclo)*mol.Eh2kcalmol
    3*(Hr3-Hphe-Hcyclo)*mol.Eh2kcalmol
    3*(Gr3-Gphe-Gcyclo)*mol.Eh2kcalmol
    ##%%

    (E1phe2cycl-Ephe-2*Ecyclo)*mol.Eh2kcalmol
    (H1phe2cycl-Hphe-2*Hcyclo)*mol.Eh2kcalmol
    (G1phe2cycl-Gphe-2*Gcyclo)*mol.Eh2kcalmol

    (E2phe1cycl-2*Ephe-Ecyclo)*mol.Eh2kcalmol
    (H2phe1cycl-2*Hphe-Hcyclo)*mol.Eh2kcalmol
    (G2phe1cycl-2*Gphe-Gcyclo)*mol.Eh2kcalmol

    (E1phe2cycl-Ephe-2*Ecyclo)*mol.Eh2kcalmol + (E2phe1cycl-2*Ephe-Ecyclo)*mol.Eh2kcalmol
    (H1phe2cycl-Hphe-2*Hcyclo)*mol.Eh2kcalmol + (H2phe1cycl-2*Hphe-Hcyclo)*mol.Eh2kcalmol
    (G1phe2cycl-Gphe-2*Gcyclo)*mol.Eh2kcalmol + (G2phe1cycl-2*Gphe-Gcyclo)*mol.Eh2kcalmol

    ##%%


    (E1phe3cycl-Ephe-3*Ecyclo)*mol.Eh2kcalmol
    (H1phe3cycl-Hphe-3*Hcyclo)*mol.Eh2kcalmol
    (G1phe3cycl-Gphe-3*Gcyclo)*mol.Eh2kcalmol

    (E2phe-2*Ephe)*mol.Eh2kcalmol
    (H2phe-2*Hphe)*mol.Eh2kcalmol
    (G2phe-2*Gphe)*mol.Eh2kcalmol

    (E1phe3cycl-Ephe-3*Ecyclo)*mol.Eh2kcalmol + (E2phe-2*Ephe)*mol.Eh2kcalmol
    (H1phe3cycl-Hphe-3*Hcyclo)*mol.Eh2kcalmol + (H2phe-2*Hphe)*mol.Eh2kcalmol
    (G1phe3cycl-Gphe-3*Gcyclo)*mol.Eh2kcalmol + (G2phe-2*Gphe)*mol.Eh2kcalmol

    ##%%
