#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import numpy as np



def bonded_atm_constraint (plumed_file,xyz_inp_file):


    bond_cut=1.6  ## Bond length cuttof
    var=0.2   ## The lenght of the bond variation that I will allowd (To be added to bond equilibrium distance)

    ## Reading positions from the reac_path file
    e,r,atm_names=mol.e_r_atm_names_from_xyz(xyz_inp_file)

    ## Pair of atoms with a distance less that bond_len in the first frame of the reac_path
    pair0=[[i,j] for i,x in enumerate(r[0]) for j,y in enumerate(r[0]) if np.linalg.norm(x-y)<bond_cut and i < j]

    ## From par0 I keep the ones where bond is not breaked
    final_pair=[pair  for pair in pair0 if all(np.linalg.norm(dr)<bond_cut for dr in r[:,pair[0]]-r[:,pair[1]])]

    ## I calculate the maximum bond distance for final_par pair of atoms
    max_bond=[max([np.linalg.norm(dr) for dr in r[:,pair[0]]-r[:,pair[1]]])+var  for pair in final_pair]


    with open(plumed_file,"w+") as file:
        cont=0
        for i,j in final_pair:
            colvar="bond"+str(cont)
            label="uwall_"+colvar
            file.write(colvar+":  DISTANCE ATOMS="+str(i+1)+","+str(j+1)+" NOPBC \n")
            file.write("UPPER_WALLS ARG="+colvar+" AT="+str(round(max_bond[cont],2))+" KAPPA=6000.0 EXP=6 EPS=1 OFFSET=0 LABEL="+label+" \n \n")
            cont=cont+1



#########################################################%%

if __name__== "__main__":



    plumed_file="/home/ar612/Documents/git_kraken/scripts/python/test/plumed_bond.inp"
    xyz_inp_file="/home/ar612/Documents/Work/cp2k_organized/reac_path/pcmreac1dftb_pcmts1dftb_pcmprod1dftb_band_40rep_dftb_reduced/pcmreac1dftb_pcmts1dftb_pcmprod1dftb_band_40rep_dftb_movie.xyz"
    #excluded=[[]]

    # plumed_file=sys.argv[1]
    # xyz_inp_file=sys.argv[2]


	bonded_atm_constraint (plumed_file,xyz_inp_file)
