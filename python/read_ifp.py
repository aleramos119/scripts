
 # ifp_file="/home/ar612/Downloads/cp2k/top/54a7_ATB.ifp"
 # top_file="/home/ar612/Downloads/cp2k/top/thf_g96.top"
 # N,ATMAS,ATMASN,ICB,CB,B0,ICT,CT,T0,ICQ,CQ,Q0,ICP,CP,PD,NP,IAC,TYPE,SQRT_C6,SQRT_C12,SQRT_CS6,SQRT_CS12,SELECT_SQRT_C12=data_from_ifp(ifp_file)
 # ff,FPEPSI,HBAR,NRNE,RNME,NMAT,NLIN,ATOM,ANM,IACM,MASS,CGM,ICGM,MAE,MSAE,NB,I_NB,MCB_NB,NBA,I_NBA,MCB_NBA,NIDA,I_NIDA,MCB_NIDA,NDA,I_NDA,MCB_NDA=data_from_top(top_file)
 #


class build_block:
    def __init__(self,top_file):

        FORCEFIELD="no"
        TOPPHYSCON="no"
        LINKEXCLUSIONS="no"
        MTBUILDBLSOLUTE="no"

        self.phy_const=[]
        self.ATOM=[]
        self.ANM=[]
        self.IACM=[]
        self.MASS=[]
        self.CGM=[]
        self.ICGM=[]
        self.MAE=[]
        self.MSAE=[]

        self.I_NB=[]
        self.MCB_NB=[]

        self.I_NBA=[]
        self.MCB_NBA=[]

        self.I_NIDA=[]
        self.MCB_NIDA=[]

        self.I_NDA=[]
        self.MCB_NDA=[]




        self.NMAT=-1*float('-inf')
        self.NB=-1*float('-inf')
        self.NBA=-1*float('-inf')
        self.NIDA=-1*float('-inf')
        self.NDA=-1*float('-inf')

        i_mtbuild=0
        with open(top_file) as file:
           for n, line in enumerate(file, 1):

               line_split=line.split()

               if "FORCEFIELD" in line:
                   FORCEFIELD="yes"
               if "END" in line and FORCEFIELD=="yes":
                   FORCEFIELD="no"

               if "TOPPHYSCON" in line:
                   TOPPHYSCON="yes"
               if "END" in line and TOPPHYSCON=="yes":
                   TOPPHYSCON="no"

               if "LINKEXCLUSIONS" in line:
                   LINKEXCLUSIONS="yes"
               if "END" in line and LINKEXCLUSIONS=="yes":
                   LINKEXCLUSIONS="no"

               if "MTBUILDBLSOLUTE" in line:
                   MTBUILDBLSOLUTE="yes"
               if "END" in line and MTBUILDBLSOLUTE=="yes":
                   MTBUILDBLSOLUTE="no"


               if FORCEFIELD=="yes" and "FORCEFIELD" not in line and "END" not in line and "#" not in line_split[0]:
                   self.ff=line_split[0]

               if TOPPHYSCON=="yes" and "TOPPHYSCON" not in line and "END" not in line and "#" not in line_split[0]:
                   self.phy_const.append(float(line_split[0]))

               if LINKEXCLUSIONS=="yes" and "LINKEXCLUSIONS" not in line and "END" not in line and "#" not in line_split[0]:
                   self.NRNE=int(line_split[0])


               if MTBUILDBLSOLUTE=="yes" and "MTBUILDBLSOLUTE" not in line and "END" not in line and "#" not in line_split[0]:
                   line_split=line.split()

                   if i_mtbuild==0:
                       self.RNME=line_split[0]

                   if i_mtbuild==1:
                       self.NMAT=int(line_split[0])
                       self.NLIN=int(line_split[1])

                   if i_mtbuild>=2 and i_mtbuild < 2 + self.NMAT:
                       self.ATOM.append(int(line_split[0]))
                       self.ANM.append(str(line_split[1]))
                       self.IACM.append(int(line_split[2]))
                       self.MASS.append(int(line_split[3]))
                       self.CGM.append(float(line_split[4]))
                       self.ICGM.append(int(line_split[5]))
                       self.MAE.append(int(line_split[6]))
                       self.MSAE.append([int(elemt) for elemt in line_split[7:]])

                   if i_mtbuild == 2 + self.NMAT:
                       self.NB=int(line_split[0])

                   if i_mtbuild == 2 + self.NMAT + self.NB + 1:
                       self.NBA=int(line_split[0])

                   if i_mtbuild == 2 + self.NMAT + self.NB + self.NBA + 2:
                       self.NIDA=int(line_split[0])

                   if i_mtbuild == 2 + self.NMAT + self.NB + self.NBA + self.NIDA + 3:
                       self.NDA=int(line_split[0])



                   if i_mtbuild>=2 + self.NMAT + 1 and i_mtbuild < 2 + self.NMAT + self.NB + 1:
                       self.I_NB.append([int(elemt) for elemt in line_split[:2]])
                       self.MCB_NB.append(int(line_split[2]))

                   if i_mtbuild>=2 + self.NMAT + self.NB + 2 and i_mtbuild < 2 + self.NMAT + self.NB + self.NBA + 2:
                       self.I_NBA.append([int(elemt) for elemt in line_split[:3]])
                       self.MCB_NBA.append(int(line_split[3]))

                   if i_mtbuild>=2 + self.NMAT + self.NB + self.NBA + 3 and i_mtbuild < 2 + self.NMAT + self.NB + self.NBA + self.NIDA + 3:
                       self.I_NIDA.append([int(elemt) for elemt in line_split[:4]])
                       self.MCB_NIDA.append(int(line_split[4]))

                   if i_mtbuild>=2 + self.NMAT + self.NB + self.NBA + self.NIDA + 4 and i_mtbuild < 2 + self.NMAT + self.NB + self.NBA + self.NIDA + self.NDA + 4:
                       self.I_NDA.append([int(elemt) for elemt in line_split[:4]])
                       self.MCB_NDA.append(int(line_split[4]))

                   i_mtbuild=i_mtbuild+1

        self.FPEPSI=self.phy_const[0]
        self.HBAR=self.phy_const[1]




class ifp:

    def __init__(self, ifp_file):

        MASSATOMTYPECODE="no"
        BONDTYPECODE="no"
        BONDANGLETYPECOD="no"
        IMPDIHEDRALTYPEC="no"
        DIHEDRALTYPECODE="no"
        SINGLEATOMLJPAIR="no"
        MIXEDATOMLJPAIR="no"
        N_TYPE="no"

        self.N=[]
        self.ATMAS=[]
        self.ATMASN=[]

        self.ICB=[]
        self.CB=[]
        self.B0=[]

        self.ICT=[]
        self.CT=[]
        self.T0=[]

        self.ICQ=[]
        self.CQ=[]
        self.Q0=[]


        self.ICP=[]
        self.CP=[]
        self.PD=[]
        self.NP=[]

        self.IAC=[]
        self.TYPE=[]
        self.SQRT_C6=[]
        self.SQRT_C12=[]
        self.SQRT_CS6=[]
        self.SQRT_CS12=[]
        self.SELECT_SQRT_C12=[]

        with open(ifp_file) as file:
           for n, line in enumerate(file, 1):

               line_split=line.split()

               if "MASSATOMTYPECODE" in line:
                   MASSATOMTYPECODE="yes"
               if "END" in line and MASSATOMTYPECODE=="yes":
                   MASSATOMTYPECODE="no"

               if "BONDTYPECODE" in line:
                   BONDTYPECODE="yes"
               if "END" in line and BONDTYPECODE=="yes":
                   BONDTYPECODE="no"

               if "BONDANGLETYPECOD" in line:
                   BONDANGLETYPECOD="yes"
               if "END" in line and BONDANGLETYPECOD=="yes":
                   BONDANGLETYPECOD="no"

               if "IMPDIHEDRALTYPEC" in line:
                   IMPDIHEDRALTYPEC="yes"
               if "END" in line and IMPDIHEDRALTYPEC=="yes":
                   IMPDIHEDRALTYPEC="no"

               if "DIHEDRALTYPECODE" in line:
                   DIHEDRALTYPECODE="yes"
               if "END" in line and DIHEDRALTYPECODE=="yes":
                   DIHEDRALTYPECODE="no"

               if "SINGLEATOMLJPAIR" in line:
                   SINGLEATOMLJPAIR="yes"
               if "END" in line and SINGLEATOMLJPAIR=="yes":
                   SINGLEATOMLJPAIR="no"

               if "MIXEDATOMLJPAIR" in line:
                   MIXEDATOMLJPAIR="yes"
               if "END" in line and MIXEDATOMLJPAIR=="yes":
                   MIXEDATOMLJPAIR="no"


               if MASSATOMTYPECODE=="yes" and "MASSATOMTYPECODE" not in line and "END" not in line and "#" not in line_split[0]:
                   self.N.append(int(line_split[0]))
                   self.ATMAS.append(float(line_split[1]))
                   self.ATMASN.append(str(line_split[2]))

               if BONDTYPECODE=="yes" and "BONDTYPECODE" not in line and "END" not in line and "#" not in line_split[0]:
                   self.ICB.append(int(line_split[0]))
                   self.CB.append(float(line_split[1]))
                   self.B0.append(float(line_split[2]))

               if BONDANGLETYPECOD=="yes" and "BONDANGLETYPECOD" not in line and "END" not in line and "#" not in line_split[0]:
                   self.ICT.append(int(line_split[0]))
                   self.CT.append(float(line_split[1]))
                   self.T0.append(float(line_split[2]))

               if IMPDIHEDRALTYPEC=="yes" and "IMPDIHEDRALTYPEC" not in line and "END" not in line and "#" not in line_split[0]:
                   self.ICQ.append(int(line_split[0]))
                   self.CQ.append(float(line_split[1]))
                   self.Q0.append(float(line_split[2]))

               if DIHEDRALTYPECODE=="yes" and "DIHEDRALTYPECODE" not in line and "END" not in line and "#" not in line_split[0]:
                   self.ICP.append(int(line_split[0]))
                   self.CP.append(float(line_split[1]))
                   self.PD.append(float(line_split[2]))
                   self.NP.append(int(line_split[3]))

               if SINGLEATOMLJPAIR=="yes" and "SINGLEATOMLJPAIR" not in line and "END" not in line and "#" not in line_split[0]:

                  if N_TYPE=="yes":
                      if len(line_split)>1:
                          if line_split[1][0].isalpha():
                              self.IAC.append(int(line_split[0]))
                              self.TYPE.append(str(line_split[1]))
                              self.SQRT_C6.append(float(line_split[2]))
                              self.SQRT_C12.append([float(elemt) for elemt in line_split[3:]])

                          if len(line_split)==2 and not line_split[0].isdigit():
                              self.SQRT_CS6.append(float(line_split[0]))
                              self.SQRT_CS12.append(float(line_split[1]))

                          if line_split[1].isdigit():
                              self.SELECT_SQRT_C12.append([int(elemt) for elemt in line_split])
                      if len(line_split)==1:
                          self.SELECT_SQRT_C12.append([int(elemt) for elemt in line_split])


                  if len(line_split)==1 and N_TYPE=="no":
                      N_TYPE="yes"
                      self.n_type=int(line_split[0])



        FLATT_SELECT_SQRT_C12=[item for sublist in self.SELECT_SQRT_C12 for item in sublist]
        self.SELECT_SQRT_C12=[FLATT_SELECT_SQRT_C12[i:i + self.n_type] for i in range(0, len(FLATT_SELECT_SQRT_C12), self.n_type)]


##%%
