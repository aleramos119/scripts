
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol
import os
import seaborn as sns


def main(plot_names,inp_files,n,main_folder,scale):
    ##################################################################################################################%%

    ener=np.zeros([1,n])
    time=np.zeros([])


    for i in inp_files:
        ener=np.append(ener,np.array(np.genfromtxt(i, skip_header = 1,usecols=4),ndmin=2),axis=0)
        time=np.append(time,np.mean(np.genfromtxt(i, skip_header = 2,usecols=6)))


    ener=ener[1:]
    time=time[1:]

    plt.style.use('ggplot')
    palette = plt.get_cmap('Spectral')


    #for i in range(0,len(ener)):
        #print(palette(i*4))



    #f = plt.figure(figsize=(9, 5))
    for i in range(0,len(ener)):
        #scale=10
        plt.plot((ener[i]-ener[i,0])*mol.Eh2kcalmol,linewidth=2.5,color=palette(i*scale),label=plot_names[i])
        plt.text(20, (ener[i]-ener[i,0])[-1]*mol.Eh2kcalmol, str(time[i])[:5]+" s", horizontalalignment='left', size='large', color=palette(i*scale))
        plt.xlabel('reac_path_image',fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.ylabel("E(kcal/mol)",fontweight='bold')
        plt.grid(True)
        plt.legend(loc=9,ncol=3,bbox_to_anchor=(0.5, 1.4))
        plt.savefig(main_folder+"/energy_time_comp.pdf", bbox_inches='tight')









if __name__== "__main__":

    main_folder="/cluster/data/ar612/cp2k_organized/reac_path/func_comp"
    data_name="r19_ts_p_e_dens-1.ener"

    n=20

    folder_list=os.listdir(main_folder)




    inp_files=np.empty([])
    plot_names=np.empty([])


    for folder in folder_list:
        inp_files=np.append(inp_files, main_folder+"/"+folder+"/"+data_name)
        plot_names=np.append(plot_names,folder[14:])

    plot_names=plot_names[1:]
    inp_files=inp_files[1:]


    main(plot_names,inp_files,n,main_folder,40)
