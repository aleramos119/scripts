import numpy as np
##%matplotlib qt
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol


def main(inp_file_pattern,i0,i1,di,col_1,col_2,mark,size):
    plt.style.use(['ggplot'])
    fig=plt.figure(figsize=(10,10))
    ax=fig.add_subplot(111)

    if(col_2 == 0):
        for i in range(i0,i1+1,di):
            inp_file=inp_file_pattern.replace("##",str(i))
            dat=mol.fast_load(inp_file,np.array([col_1]),1000).T
            ax.plot(dat[0],mark,markersize=size,label=str(i))

    if(col_2 != 0):
        for i in range(i0,i1+1,di):
            inp_file=inp_file_pattern.replace("##",str(i))
            dat=mol.fast_load(inp_file,np.array([col_1,col_2]),1000).T
            ax.plot(dat[0],dat[1],mark,markersize=size,label=str(i))

    plt.xlabel(inp_file_pattern,fontweight='bold')
    plt.legend()
    plt.show()


if __name__== "__main__":

    inp_file_pattern=sys.argv[1]
    i0=int(sys.argv[2])
    i1=int(sys.argv[3])

    di=int(sys.argv[4])

    col_1=int(sys.argv[5])
    col_2=int(sys.argv[6])
    mark=sys.argv[7]


    if mark == "-":
        size=0.4
    if mark != "-":
        size = 2


    main(inp_file_pattern,i0,i1,di,col_1,col_2,mark,size)
