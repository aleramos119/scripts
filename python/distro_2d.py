
#%%
import numpy as np
#%matplotlib qt
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/ar612/Documents/Python_modules/')
import mol


def main(inp_file,col_1,col_2,temp=298):
    ##################################################################################################################%%
    #d1,d2=np.genfromtxt(inp_file,unpack= True, skip_header = 1,skip_footer=2,usecols=(col_1,col_2))

    t,d1,d2=mol.fast_load(inp_file,np.array([0,col_1,col_2]),1000).T

    h,px,py=np.histogram2d(d1,d2,bins=100,density=True)

    k=mol.k*mol.J2kcalmol

    temp=298
    h=-k*temp*np.log(h)

    n_cont=20

    ##################################################################################################################%%
    plt.style.use(['ggplot'])
    fig=plt.figure(figsize=(20,10))
    ax=plt.subplot(121)
    plt.contourf(px[:-1],py[:-1],h.T,n_cont,cmap='rainbow_r',alpha=.55)
    cbar=plt.colorbar()
    cbar.ax.set_title('kcal/mol')
    C=ax.contour(px[:-1],py[:-1],h.T,[0.5,1,1.5,2,2.5,3], colors='black', linewidths=0.6)
    ax.clabel(C, inline=1, fontsize=10)
    plt.xlabel(col_1,fontweight='bold')
    plt.ylabel(col_2,fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)

    plt.style.use(['ggplot'])
    ax=plt.subplot(122)
    plt.plot(t,d1,'o',markersize=0.05,alpha=0.7,color='tab:blue')
    plt.xlabel("t (ps)",fontweight='bold')
    plt.ylabel("CO_x (A)",fontweight='bold')
    plt.tick_params(labelsize=14)

    plt.show()
##################################################################################################################%%

if __name__== "__main__":

    # inp_file="/media/ar612/Disco local/colvar"
    # col_1=10
    # col_2=11


    inp_file=sys.argv[1]
    col_1=int(sys.argv[2])
    col_2=int(sys.argv[3])

    main(inp_file,col_1,col_2)
