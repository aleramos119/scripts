
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])

#%%

############################################################################%%


gaussian_inp_file="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1/pctreac1_277308.out"
out_pattern="/home/ar612/Documents/Work/cp2k_organized/ref_traj/trajectory/pctreac1_vib_52_vib_204_205_-206_nx_xmin_xmax_60_ny_ymin_ymax_60.xyz"


n1=52
n2=204
n3=205
n4=206


## Reading cartesian normal modes and calculating mass_weighted normal modes
ener,w,redm,fcnstmDA,intes,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r0,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(gaussian_inp_file)


n_atm=len(r0)
n_vib=len(w)


xmax=1
xmin=-1
nx=60
x_list=np.linspace(xmin, xmax, num=nx)


ymax=4
ymin=-0.5

ymax=4
ymin=0
ny=1
y_list=np.linspace(ymin, ymax, num=ny)


np.linalg.norm(r0[52]-r0[53])

## The first extructure needed for append to work
r=np.zeros([1,n_atm,3])

## Escaning through the displacements in both directions
for dx1 in x_list:
    dx1=0
    for dx2 in y_list:
        r=np.append(r,[r0+dx1*q_cart[n1]+dx2*(q_cart[n2]-q_cart[n4])/3],axis=0)

## Removing the first dummy structure
r=r[1:]


dx2=1.4297

r=np.append(r,[r0+dx2*(q_cart[n2]-q_cart[n4])/3],axis=0)
r1=r0+dx2*(q_cart[n2]-q_cart[n4])/3

np.linalg.norm(r1[52]-r1[53])


out_file="/home/ar612/Documents/Work/cp2k_organized/xyz/pctreac1_oh_dist_1.05.xyz"
out_file=out_pattern.replace("xmin",str(xmin)).replace("xmax",str(xmax)).replace("ymin",str(ymin)).replace("ymax",str(ymax))
mol.write_trajectory(r,np.zeros(len(r)),atm_names,out_file)


## Writing the trajectory to the file
mol.write_trajectory(np.array([r0]),np.zeros(1),atm_names,out_file)


###########################################################################%%


if __name__== "__main__":


    modes_inp_file=os.environ['GAUSSIAN_HOME']+"/ts_opt/r39_r20_ts_p_80_rep/39_r20_ts_p_80_rep_movie_ts_opt_60800.out"
    out_pattern=os.environ['CP2K_HOME']+"/ref_traj/trajectory/39_r20_ts_p_80_rep_vib_$$_n_&&.xyz"


    main(modes_inp_file,m1,m2,disp_max)
