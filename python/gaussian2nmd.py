
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol


###
# nmd_out_name="test_h2o"
# gaussian_inp_file=inp_file
# inp_file="/home/ar612/Documents/Work/gaussian/geo_opt/h2o/771432.out"




def gaussian2nmd (gaussian_inp_file,nmd_out_name):


    ## Reading the data from molden file
    ener,w,redm,fcnstmDA,intes,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names=mol.ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss(gaussian_inp_file)
    #w,redm2,f2,intes_2,q_cart,r,atm_names=mol.w_redm_f_intes_modes_r_atm_names_from_gauss(gaussian_inp_file)

    n_atm=len(r)
    n_vib=len(w)

    nmd_out_file=nmd_out_name+".nmd"
    rcm=mol.center_of_mass(np.array([r]),atm_names)

    mol.write_modes_2_nmd(nmd_out_file,atm_names,r,q_cart,range(n_vib))
    #mol.write_modes_2_nmd(nmd_out_name+"_dip.nmd",["H"],rcm,np.array([[dip_au]]))














###########################################################################%%


if __name__== "__main__":


    inp_file="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1_83384.out"
    gaussian_inp_file=inp_file
    nmd_out_name="/home/ar612/Documents/Work/gaussian/geo_opt/pctreac1_reduced/pctreac1"
    # inp_file=molden_inp_file
    gaussian_inp_file=sys.argv[1]
    nmd_out_name=sys.argv[2]

    gaussian2nmd (gaussian_inp_file,nmd_out_name)
