
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import glob
import scipy
from scipy import special
import math
import matplotlib.pyplot as plt
from scipy.integrate import simps
%matplotlib qt


def plot_traj_2d(xxx,yyy,eee):
    x_min=min(xxx)
    x_max=max(xxx)
    y_min=min(yyy)
    y_max=max(yyy)
    e_min=min(eee)
    e_max=max(eee)

    n_level=30
    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    plt.axis([x_min, x_max, y_min,y_max])
    plt.tricontourf(xxx,yyy,eee,n_level,cmap='coolwarm',alpha=.55,vmin=e_min, vmax=e_max)
    cbar=plt.colorbar()
    C=ax.tricontour(xxx,yyy,eee,n_level,n_level, linewidths=0.3,colors='black')
    ax.clabel(C, inline=1, fontsize=10)
    #plt.plot(xxx,yyy,'.')
    plt.xlabel("x",fontweight='bold')
    plt.ylabel("y",fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    plt.show()



# def V(q1,q2,k1,k2,gamma):
#     return 0.5*k2*q2**2

def V(q1,q2,k1,k2,gamma):
    return 0.5*k1*q1**2 + 0.5*k2*q2**2 + gamma*q1*q2**2

def mu(q1,q2):
    return q1+q2

def H(n,x,m=0):
    if m > n:
        return 0
    else:
        return (2**m)*(math.factorial(n)/math.factorial(n-m))*scipy.special.eval_hermite(n-m,x)


def psi_ho(n,x,m,w):
    return (1/np.sqrt(math.factorial(n)*2**n))*(m*w/np.pi)**(1/4)*np.exp(-0.5*m*w*x**2)*H(n,np.sqrt(m*w)*x)

def d2_psi_ho(n,x,m,w):
    return (1/np.sqrt(math.factorial(n)*2**n))*(m*w/np.pi)**(1/4)*np.exp(-0.5*m*w*x**2)* (   (m*w)*H(n,np.sqrt(m*w)*x,2)   - 2* (m*w)**(3/2.)*x*H(n,np.sqrt(m*w)*x,1) + (m*w)*(m*w*x**2-1)*H(n,np.sqrt(m*w)*x)  )


def simps_2d(x,y,F):
    return simps([simps(fy,y) for fy in F],x)

def find_value(path,variable,place=-2,separator="_"):
    name_list=path.split("/")[place].split(separator)
    return name_list[name_list.index(variable)+1]


##%%
gamma_list=[1]

for gamma in gamma_list:
    ham_name="fermi_res"
    nx=2
    ny=3
    m1=1
    w1=2
    m2=1
    w2=1


    k1=m1*w1**2
    k2=m2*w2**2

    xmax=5
    n=300
    q1=np.linspace(-xmax,xmax,n)
    q2=np.linspace(-xmax,xmax,n)





    H_final=np.zeros([nx*ny,ny*nx])
    mu_final=np.zeros([nx*ny,ny*nx])
    Kx=np.zeros([nx,nx])
    Ky=np.zeros([ny,ny])

    Vmat=np.zeros([len(q1),len(q2)])
    mumat=np.zeros([len(q1),len(q2)])
    for i,qq1 in enumerate(q1):
        for j,qq2 in enumerate(q2):
            Vmat[i,j]=V(qq1,qq2,k1,k2,gamma)
            mumat[i,j]=mu(qq1,qq2)


    for i in range(nx):
        for j in range(nx):
            #overlapx[i,j]=simps(psi_ho(i,q1,m1,w1)*psi_ho(j,q1,m1,w1),q1)
            Kx[i,j]=simps(-psi_ho(i,q1,m1,w1)*d2_psi_ho(j,q1,m1,w1)/(2*m1),q1)

    for i in range(ny):
        for j in range(ny):
            #overlapy[i,j]=simps(psi_ho(i,q2,m2,w2)*psi_ho(j,q2,m2,w2),q2)
            Ky[i,j]=simps(-psi_ho(i,q2,m2,w2)*d2_psi_ho(j,q2,m2,w2)/(2*m2),q2)




    for i in range(nx):
        for j in range(ny):
            ## Runs through every state in consecutive order
            k=i*ny+j
            if True:
                print(i,j,k)
                for ii in range(nx):
                    for jj in range(ny):
                        ## Runs through every state in consecutive order
                        kk=ii*ny+jj

                        if kk >= k: ## I only fill the upper half of the matrices since they are simetric
                            ## I create empty matrices to store the product of the potential (and dipole) and the eigenstates. This is the integrand of the double integral
                            Vmat=np.zeros([len(q1),len(q2)])
                            mumat=np.zeros([len(q1),len(q2)])

                            ### Runs through every point in the grid and evaluates the product of the potential (and dipole) and the eigenstates
                            for iii,qq1 in enumerate(q1):
                                for jjj,qq2 in enumerate(q2):
                                    Vmat[iii,jjj]=V(qq1,qq2,k1,k2,gamma)*psi_ho(i,qq1,m1,w1)*psi_ho(j,qq2,m2,w2)*psi_ho(ii,qq1,m1,w1)*psi_ho(jj,qq2,m2,w2)
                                    mumat[iii,jjj]=mu(qq1,qq2)*psi_ho(i,qq1,m1,w1)*psi_ho(j,qq2,m2,w2)*psi_ho(ii,qq1,m1,w1)*psi_ho(jj,qq2,m2,w2)

                            H_final[k,kk]=Kx[i,ii]*np.identity(ny)[j,jj]+Ky[j,jj]*np.identity(nx)[i,ii]+simps_2d(q1,q2,Vmat)
                            mu_final[k,kk]=simps_2d(q1,q2,mumat)

    for k in range(nx*ny-2):
        for kk in range(k):
            H_final[k,kk]=H_final[kk,k]
            mu_final[k,kk]=mu_final[kk,k]

    out_dir="/home/ar612/Documents/Work/quantum_psopt/systems/"+ham_name+"_gamma_"+str(gamma)+"_w1_"+str(w1)+"_w2_"+str(w2)+"_nx_"+str(nx)+"_ny_"+str(ny)+"/"
    H_file=out_dir+"hij.txt"
    mu_file=out_dir+"muij.txt"


    os.makedirs(out_dir)
    mol.write_matrix_to_file(H_final,"","",H_file)
    mol.write_matrix_to_file(mu_final,"","",mu_file)



##%%
# w1=2
# w2=0.99
# [ w1*(i+0.5) for i in range(3)]
# [ w2*(i+0.5) for i in range(5)]
#
q2
##%%
n1=3
n2=6
qq1=np.zeros(n**2)
qq2=np.zeros(n**2)
ee=np.zeros(n**2)
psiii=np.zeros(n**2)
k=0
for i,x in enumerate(q1):
    for j,y in enumerate(q2):
        qq1[k]=x
        qq2[k]=y
        # if y == 10.:
        #     #ee[k]=V(x,y,k1,k2,gamma)
        psiii[k]=psi_ho(n1,x,m1,w1)*psi_ho(n2,y,m2,w2)
        k=k+1
q2[40]
#plot_traj_2d(qq1,qq2,ee)
plot_traj_2d(qq1,qq2,psiii)
fig=plt.figure(figsize=(10,10))
ax=plt.subplot(111)
ax.plot(q1,psiii[40::n])
##%%
#
#
#
#
# ##%%
# plt.plot(q2,0.5*k2*q2**2)
# for n in range(10):
#     En=w2*(n+0.5)
#     plt.plot(q2,psi_ho(n,q2,m2,w2)+En,'.',label=str(n))
# plt.legend()
# ##%%
#




##%%



index1(0,1,ny)+1


index2(6,ny)


m=18371.80
tf=17000
u_0001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.0001/u.dat")
t_0001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.0001/t.dat")
x_0001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.0001/x.dat")

-0.0001*simps(x_0001[:,3]*x_0001[:,3]/(2*m),t_0001)
0.3*simps(u_0001*u_0001/(np.sin(np.pi*t_0001/tf)**2+0.005),t_0001)


u_001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.001/u.dat")
t_001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.001/t.dat")
x_001=np.genfromtxt("/home/ar612/Documents/Work/oct_gauss/k_int_scl_test/p0_2_17T_10hmass/k_int_0.001/x.dat")

-0.001*simps(x_001[:,3]*x_001[:,3]/(2*m),t_001)
0.3*simps(u_001*u_001/(np.sin(np.pi*t_001/tf)**2+0.005),t_001)


##%%
plt.plot(t_001,x_001[:,3],label="001  int="+str(simps(x_001[:,3]*x_001[:,3]/(2*m),t_001)))
plt.plot(t_0001,x_0001[:,3],label="0001  int="+str(simps(x_0001[:,3]*x_0001[:,3]/(2*m),t_0001)))
plt.legend()
##%%




### Diagonalizing the hamiltonian
hij_path="/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.5_w1_2_w2_1_nx_3_ny_6/hij.txt"
hij=np.genfromtxt(hij_path)
ny=int(find_value(hij_path,"ny"))
e,v=np.linalg.eigh(hij)

e1_list=np.array([ [1,w1*(i+0.5)] for i in range(2)])
e2_list=np.array([ [2,w2*(i+0.5)+0.5] for i in range(3)])
ecoup_list=np.array([ [3,e[i]-0.5] for i in range(6)])

plt.plot(e1_list[:,0],e1_list[:,1],"o")
plt.plot(e2_list[:,0],e2_list[:,1],"o")
plt.plot(ecoup_list[:,0],ecoup_list[:,1],"o")



nk=len(e)
hij_eig=np.diag(e)
muij_eig=np.zeros([nk,nk])


##%%
state=1
n=300
qq1=np.zeros(n**2)
qq2=np.zeros(n**2)
ee=np.zeros(n**2)
psiii=np.zeros(n**2)
l=0
for i,x in enumerate(q1):
    for j,y in enumerate(q2):
        qq1[l]=x
        qq2[l]=y
        psiii[l]=0
        for k in range(nk):
            n1,n2=index2(k,ny)
            psiii[l]=psiii[l]+v[k,state]*psi_ho(n1,x,m1,w1)*psi_ho(n2,y,m2,w2)
        l=l+1

#plot_traj_2d(qq1,qq2,ee)
plot_traj_2d(qq1,qq2,psiii)
##%%

for k in range(nk):
    for kk in range(nk):
        i,j=index2(k,ny)
        ii,jj=index2(kk,ny)






#############################################################################################################################%%
############################################# Exact Fermi Resonance Hamiltonian #############################################%%
#############################################################################################################################%%

def index1(i,j,ny):
    return i*ny+j

def index2(k,ny):
    i=int(k/ny)
    j=k%ny
    return i,j

index1(3,6,20)

def dK(i,j):
    if (i==j):
        return 1
    else:
        return 0

def q(m,n,lam=1):
    return ( np.sqrt(n+1)*dK(m,n+1)  +  np.sqrt(n)*dK(m,n-1)  )/(lam*np.sqrt(2))

def q_2(m,n,lam=1):
    return ( np.sqrt((n+1)*(n+2))*dK(m,n+2)  + (2*n+1)*dK(m,n) +  np.sqrt(n*(n-1))*dK(m,n-2)  )/(2*lam**2)

def d2_q(m,n,lam=1):
    return lam**2*( np.sqrt((n+1)*(n+2))*dK(m,n+2)  - (2*n+1)*dK(m,n) +  np.sqrt(n*(n-1))*dK(m,n-2)  )/2

def H0(i1,i2,j1,j2,m1,w1,m2,w2,gamma):
    k1=m1*w1**2
    k2=m2*w2**2
    lam1=np.sqrt(m1*w1)
    lam2=np.sqrt(m2*w2)

    H01=-d2_q(i1,j1,lam1)/(2*m1) + k1*q_2(i1,j1,lam1)/2
    H02=-d2_q(i2,j2,lam2)/(2*m2) + k2*q_2(i2,j2,lam2)/2

    return H01*dK(i2,j2) + H02*dK(i1,j1) +  gamma*q(i1,j1,lam1)*q_2(i2,j2,lam2)


def mu0(i1,i2,j1,j2,m1,w1,m2,w2):
    lam1=np.sqrt(m1*w1)
    lam2=np.sqrt(m2*w2)

    return q(i1,j1,lam1)*dK(i2,j2) + q(i2,j2,lam2)*dK(i1,j1)




##%%
#gamma_list=[0.05,0.1,0.15,0.2]
gamma_list=[0.1]
for gamma in gamma_list:
    ham_name="test"
    m1=1
    w1=2
    m2=1
    w2=1
    #gamma=0.5


    nx=20
    ny=40
    n=nx*ny
    emax=9

    H=np.zeros([n,n])
    mu=np.zeros([n,n])

    for k in range(n):
        for kk in range(n):
            i1,i2=index2(k,ny)
            j1,j2=index2(kk,ny)
            H[k,kk]=H0(i1,i2,j1,j2,m1,w1,m2,w2,gamma)
            mu[k,kk]=mu0(i1,i2,j1,j2,m1,w1,m2,w2)

    e,v=np.linalg.eigh(H)

    Hdiag=np.diag(e)
    mudiag=np.matmul(v.T,np.matmul(mu,v))

    out_dir="/home/ar612/Documents/Work/quantum_psopt/systems/"+ham_name+"_gamma_"+str(gamma)+"_w1_"+str(round(w1,3))+"_w2_"+str(round(w2,3))+"_m1_"+str(round(m1,3))+"_m2_"+str(round(m2,3))+"_nx_"+str(nx)+"_ny_"+str(ny)+"/"
    H_file=out_dir+"H.txt"
    mu_file=out_dir+"mu.txt"

    Hdiag_file=out_dir+"Hdiag.txt"
    mudiag_file=out_dir+"mudiag.txt"

    eigenvec_file=out_dir+"eigenvec.txt"


    os.makedirs(out_dir,exist_ok='True')
    mol.write_matrix_to_file(H,"","",H_file)
    mol.write_matrix_to_file(mu,"","",mu_file)
    mol.write_matrix_to_file(Hdiag,"","",Hdiag_file)
    mol.write_matrix_to_file(mudiag,"","",mudiag_file)
    mol.write_matrix_to_file(v,"","",eigenvec_file)


    v_2=v**2
    state_list=["" for i in range(nx*ny)]
    for i in range(nx*ny):
        index_list=np.where(v_2[:,i]>0.2)[0]
        state_list[i]="|"+"{:>02d}".format(i)+r"$\rangle$:"
        for ind,index in enumerate(index_list):
            index2(9,ny)[0]
            state_list[i]=state_list[i] + r"   $| \langle $"+"{:1d}".format(index2(index,ny)[0])+","+"{:1d}".format(index2(index,ny)[1])+"|" +"{:02d}".format(i)+r"$\rangle |^2 =$" + "{0:.2f}".format(v_2[index,i])


    e1_list=np.array([ [1,w1*(i+0.5)] for i in range(nx)])
    e2_list=np.array([ [2,w2*(i+0.5)] for i in range(ny)])
    ecoup_list=np.array([ [3,Hdiag[i,i]] for i in range(emax)])

    e1_list[:,1]=e1_list[:,1]-min(e1_list[:,1])
    e2_list[:,1]=e2_list[:,1]-min(e2_list[:,1])
    ecoup_list[:,1]=ecoup_list[:,1]-min(ecoup_list[:,1])

    xmin=0.8
    xmax=1.2
    line=3.5
    lsize1=35
    lsize2=30
    fig=plt.figure(figsize=(16,9))
    plt.rc('font', size=lsize1)
    plt.hlines(e1_list[:,1],xmin,xmax,linewidth=line,color="tab:red")
    plt.hlines(e2_list[:,1],xmin+1,xmax+1,linewidth=line,color="tab:blue")
    plt.hlines(ecoup_list[:,1],xmin+2,xmax+2,linewidth=line,color="tab:purple")
    plt.xlim(0.5,13)
    for i,state in enumerate(state_list[:emax]):
        #print(state)
        #plt.text(3.2, Hdiag[i,i]-0.5-1, str(i), horizontalalignment='left',verticalalignment='center', size='small',color='black')
        plt.text(3.85,(ecoup_list[-1,1]-ecoup_list[0,1])*i/(len(ecoup_list)-1) , state,horizontalalignment="left",verticalalignment='center', size=lsize2,color='black')
    #plt.tick_params(axis='x', which='both', bottom=False, labelbottom=False, top=False)
    plt.grid(False)
    plt.xticks([1,2,3,4],["(a)","(b)","(c)","(d)"])
    plt.ylabel(r'$E(\hbar \omega_2)$')
    plt.ylim(-ecoup_list[1,1]/4,ecoup_list[emax-1,1]+ecoup_list[1,1]/4)
    fig.savefig(out_dir+"/"+ham_name+"_gamma_"+str(gamma)+"_w1_"+str(round(w1,3))+"_w2_"+str(round(w2,3))+"_m1_"+str(round(m1,3))+"_m2_"+str(round(m2,3))+"_nx_"+str(nx)+"_ny_"+str(ny)+"_states.pdf")
    plt.close(fig)
#%%




rest_state=np.genfromtxt("/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/cp_rest_states.txt")
eigenvec=np.genfromtxt("/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_10_ny_20/eigenvec.txt")




hdiag1=np.genfromtxt("/home/ar612/Documents/Work/quantum_psopt/systems/test_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_10_ny_20/Hdiag.txt").diagonal()
hdiag2=np.genfromtxt("/home/ar612/Documents/Work/quantum_psopt/systems/test_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_20_ny_40/Hdiag.txt").diagonal()


plt.plot(hdiag1)
plt.plot(hdiag2)

plt.hlines(hdiag1,0.5,1.5,color="tab:red")
for i,state in enumerate(hdiag1):
    plt.text(1.5,hdiag1[i],"E_"+str(i),horizontalalignment="left",verticalalignment='center',color='black')



diff=[np.abs(hdiag1[i]-hdiag2[i]) for i in range(200)]


plt.plot(diff)
