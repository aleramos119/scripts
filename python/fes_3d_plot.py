
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol
from mpl_toolkits.mplot3d import axes3d


def main(inp_file,x_label,cx,y_label,cy):
    ##################################################################################################################%%
    data=np.genfromtxt(inp_file,unpack= True, skip_header = 0)



    ############################## Contour Plot co dist and oh dih ####################################################%%
    x=np.asarray(np.array_split(data[0]*cx,100))
    y=np.asarray(np.array_split(data[1]*cy,100))
    z=np.asarray(np.array_split(data[2]*mol.Eh2kcalmol,100))


    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(x,y,z, cmap="autumn_r", lw=0.5, rstride=2, cstride=2, alpha=0.5)
    #ax.contourf(x,y,z,10, lw=3, cmap="autumn_r", linestyles="solid", offset=-80, alpha=0.5)
    C=plt.contour(x,y,z, 5, colors='black', linewidths=.6)
    plt.clabel(C, inline=1, fontsize=10)
    plt.xlabel('OH-HN (A)',fontweight='bold')
    plt.ylabel('CO (A)',fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.grid(True)
    plt.show()
    ###################################################################################################################








if __name__== "__main__":
    if(sys.argv[3]=="ang"):
        cx=180/np.pi
    if(sys.argv[3]=="dist"):
        cx=mol.au2A
    if(sys.argv[5]=="ang"):
        cy=180/np.pi
    if(sys.argv[5]=="dist"):
        cy=mol.au2A
    main(sys.argv[1],sys.argv[2],cx,sys.argv[4],cy)
