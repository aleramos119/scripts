
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
import mol
#%matplotlib qt



def main(inp_file,col):
    ##################################################################################################################%%
    dat=np.genfromtxt(inp_file,unpack= True, skip_header = 1,skip_footer=1,usecols=col)


    std=np.std(dat)

    avg_dat=mol.mean_list(dat) ## This list contains the average as a function of list index
    avg=avg_dat[-1]

    avg_list=np.full((len(dat)), avg) ## This list contains only the final average


    h,px=np.histogram(dat,bins=100,density=True)
    k=mol.k*mol.J2kcalmol
    temp=298
    fes=-k*temp*np.log(h)


    plt.style.use(['ggplot'])
    fig=plt.figure(figsize=(20,10))
    ax=plt.subplot(131)
    ax.title.set_text('Probability Density')
    plt.bar(px[:-1],h,alpha=0.7,color='tab:blue',align='edge',width=px[1]-px[0])
    plt.text(0.6,0.8, "Avg: "+str(avg)[:], horizontalalignment='left', size='large',color='tab:red',transform=ax.transAxes)
    plt.text(0.6,0.7, "Std: "+str(std)[:], horizontalalignment='left', size='large',color='tab:red',transform=ax.transAxes)
    plt.xlabel(inp_file,fontweight='bold')
    plt.tick_params(labelsize=14)



    ax=plt.subplot(132)
    ax.title.set_text('Free Energy (kcal/mol)')
    plt.bar(px[:-1],fes,alpha=0.7,color='tab:blue',align='edge',width=px[1]-px[0])
    plt.xlabel(inp_file,fontweight='bold')

    ax=plt.subplot(133)
    ax.title.set_text('Time Dependence (kcal/mol)')
    plt.plot(dat,'o',markersize=0.5,alpha=0.7,color='tab:blue')
    plt.plot(avg_list,'o',markersize=0.5,alpha=0.7,color='tab:red')
    plt.plot(avg_dat,'o',markersize=0.5,alpha=0.7,color='tab:red')
    plt.text(1.01*len(dat), avg, str(avg)[:], horizontalalignment='left', size='large',color='tab:red')
    plt.xlabel(inp_file,fontweight='bold')
    plt.tick_params(labelsize=14)

    plt.show()


if __name__== "__main__":

    # inp_file="/home/ar612/Documents/Work/cp2k_organized/well_temp/0pctts1irc_1_thf_196_300K_30A_free_well_temp_reduced/colvar"
    # col=10
    # dt=8*0.5/1000  ##ps
    #
    # t,ccc1,doh2,doh3,diff4=np.genfromtxt(inp_file,unpack= True, skip_header = 1,skip_footer=1)
    # t=t*dt
    #
    #
    # std=np.std(ccc1)
    #
    # avg_dat=mol.mean_list(ccc1) ## This list contains the average as a function of list index
    # avg=avg_dat[-1]
    #
    # avg_list=np.full((len(ccc1)), avg) ## This list contains only the final average
    #
    #
    # h,px=np.histogram(ccc1,bins=100,density=True)
    # k=mol.k*mol.J2kcalmol
    # temp=298
    # fes=-k*temp*np.log(h)
    #
    #
    # ##%%
    # plt.style.use(['ggplot'])
    # fig=plt.figure(figsize=(20,10))
    # ax=plt.subplot(121)
    # ax.title.set_text('Probability Density')
    # plt.bar(px[:-1],h,alpha=0.7,color='tab:blue',align='edge',width=px[1]-px[0])
    # plt.text(0.6,0.8, "Avg: "+str(avg)[:], horizontalalignment='left', size='large',color='tab:red',transform=ax.transAxes)
    # plt.text(0.6,0.7, "Std: "+str(std)[:], horizontalalignment='left', size='large',color='tab:red',transform=ax.transAxes)
    # plt.xlabel("Dihedral (Degrees)",fontweight='bold')
    # plt.tick_params(labelsize=14)
    #
    # ax=plt.subplot(122)
    # plt.plot(t,ccc1,'o',markersize=0.5,alpha=0.7,color='tab:blue')
    # plt.plot(t,avg_list,'o',markersize=0.5,alpha=0.7,color='tab:red')
    # plt.plot(t,avg_dat,'o',markersize=0.5,alpha=0.7,color='tab:red')
    # plt.text(1.01*t[-1], avg, str(avg)[:], horizontalalignment='left', size='large',color='tab:red')
    # plt.xlabel("t (ps)",fontweight='bold')
    # plt.ylabel("Dihedral (Degrees)",fontweight='bold')
    # plt.tick_params(labelsize=14)
    #
    # plt.show()
    # ##%%
    # plt.plot(t,diff4,'o',markersize=0.5,alpha=0.7,color='tab:blue')
    # plt.xlabel("t (ps)",fontweight='bold')
    # plt.ylabel("d1 (H,O) - d2(H,O) (A)  (Product is negative)",fontweight='bold')
    # plt.tick_params(labelsize=14)
    # ##%%
    # ##%%
    # plt.plot(t,ccc1,'o',markersize=0.5,alpha=0.7,color='tab:blue')
    # plt.xlabel("t (ps)",fontweight='bold')
    # plt.ylabel("Dihedral (Degrees)",fontweight='bold')
    # plt.tick_params(labelsize=14)
    # ##%%
    # ##%%
    # plt.plot(ccc1,diff4,'o',markersize=1.5,alpha=0.7,color='tab:blue')
    # plt.text(1.01*t[-1], avg, str(avg)[:], horizontalalignment='left', size='large',color='tab:red')
    # plt.xlabel("Dihedral (Degrees)",fontweight='bold')
    # plt.ylabel("d1 (H,O) - d2(H,O) (A)  (Product is negative)",fontweight='bold')
    # plt.tick_params(labelsize=14)
    # ##%%
    # # col=0
    # # col_2=0


    inp_file=sys.argv[1]
    col=int(sys.argv[2])

    main(inp_file,col)




#
# cum=np.cumsum(h)*(px[1]-px[0])
#
#
# plt.bar(px[:-1],cum,alpha=0.7,color='tab:blue',align='edge',width=px[1]-px[0])
# plt.plot(h,cum[:-1])
