
#%%
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt
import os
import sys
from scipy import interpolate
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import rmsd
%matplotlib qt
plt.style.use(['ggplot'])


def main (ref_traj_inp_file,reaction_path_inp_file,sat,n_level):

    ##%%
    ## Reading trajectory energy for reaction plane
    XR,XTS,XL,XIRP,e_irp,its,r_irp,atm_names=mol.XIRP_from_irp(reaction_path_inp_file)


    ## Reading trajectory for statistic
    e_stat,r_stat,atm_names=mol.e_r_atm_names_from_xyz (stat_traj_file)

    nframes=len(r_stat)

    natm=len(atm_names)
    imp_atms=[0,1,2,14,15,33,34,52,53]
    weights=np.zeros([natm,3])
    weights[imp_atms]=np.array([1,1,1])

    r_stat=mol.remove_trans_rot(r_stat,r_irp[its])
    #XSTAT=mol.flat(mol.cartesian2mass_weighted(r_stat,atm_names))

    r_stat_rmsd=np.array([ rmsd.rmsd(np.multiply(weights,r),np.multiply(weights,r_irp[0])) for r in r_stat])





    rmsd_r0_rts=rmsd.rmsd(np.multiply(weights,r_irp[0]),np.multiply(weights,r_irp[its]))
    rmsd_r0_rp=rmsd.rmsd(np.multiply(weights,r_irp[0]),np.multiply(weights,r_irp[-1]))

    nframes

    i=0
    for d in  r_stat_rmsd:
        if d < rmsd_r0_rts:
            i=i+1

    i/nframes*100

    ##%%
    plt.plot(np.array(range(nframes))*0.005,r_stat_rmsd,label="rmsd(r(t),r_ts)")
    plt.plot(np.array(range(nframes))*0.005,[rmsd_r0_rts for i in range(nframes)],label="rmsd(r_reac,r_ts)")
    plt.legend()
    plt.xlabel("t(ps)")
    plt.ylabel("rmsd (A)")
    ##%%


    n=0
    dmin=1.5
    for d in r_stat_rmsd:
        if d < dmin:
            n=n+1

    n_atm=len(atm_names)
    r_close=np.zeros([n,n_atm,3])
    j=0
    for i,r in enumerate(r_stat):
        if r_stat_rmsd[i] < dmin:
            r_close[j]=r
            j=j+1


    mol.write_trajectory(r_close,np.zeros([len(r_close)]),atm_names,"r_close.xyz")

###########################################################################%%

if __name__== "__main__":

    ##%%
    reaction_path_inp_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/pctts1_irc.xyz"



    #Unconstrained trajectory
    stat_traj_file="/home/ar612/Documents/Work/cp2k_organized/well_temp/0pctts1irc_1_thf_196_300K_30A_free_well_temp_reduced/0pctts1irc_1_thf_196_300K_30A_free_well_temp-pos-1.xyz"




    main(ref_traj_inp_file,reaction_path_inp_file,sat,n_level)
