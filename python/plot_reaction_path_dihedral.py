
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
%matplotlib qt
plt.style.use(['ggplot'])

def read_dihedral_atoms(dihedral_atoms_file,job):

    with open(dihedral_atoms_file) as file:
       for n, line in enumerate(file, 1):
           if job+"_path_step" in line:
               step=int(line.split()[-1])
           if job+"_angle" in line:
               a0=float(line.split()[-4])
               af=float(line.split()[-3])
               na=int(line.split()[-2])
               angle=np.linspace(a0,af,na,endpoint=False)
               if "deg" in line:
                   angle=np.radians(angle)
           if job+"_r0" in line:
               r0=[float(i) for i in line.split()[-3:]]
           if job+"_dihedral" in line:
               dihedral=[int(i) for i in line.split()[-4:]]
           if job+"_atoms" in line:
               atoms=[int(i) for i in line.split()[1:]]
    return step,angle,r0,dihedral,atoms


def main (ref_traj_inp_file,sat,n_level):

    ##%%
    ## Reading reaction path energy
    e,r,atm_names=mol.e_r_atm_names_from_xyz (ref_traj_inp_file)

    ### Extracting data from name
    name_list=ref_traj_inp_file.split("/")[-1].split("_")
    project_name="_".join(name_list[:-2])

    pdf_file=project_name+".pdf"
    ener_file=project_name+".ener"

    inx=name_list.index("nirc")
    nx=int(name_list[inx+1])


    iny=name_list.index("nangle")
    y_min=float(name_list[iny+1])
    y_max=float(name_list[iny+2])
    ny=int(name_list[iny+3])




    dr,x=mol.path_dist(r[:nx])
    x_min=x[0]
    x_max=x[-1]
    y=np.linspace(y_min, y_max, num=ny,endpoint=False)
    y_min=y[0]
    y_max=y[-1]



    ##%%
    ## Reading reaction path energy
    emin=min(e)
    e=e-emin
    mol.write_matrix_to_file(np.array(np.split(e,nx)),"","",ener_file)
    e=e*mol.Eh2kcalmol

    for i,ei in enumerate(e):
        if ei > sat:
            e[i]=sat

    e_x_y=np.array(np.split(e,ny))


    ##%%
    ### Read reaction path
    e_irp,rirp,atm_names=mol.e_r_atm_names_from_xyz(reaction_path_inp_file)
    dr_full,x_full=mol.path_dist(rirp)
    its=88

    step,angle_list,r0,dih_atms,atoms=read_dihedral_atoms(dihedral_atoms_file,job)

    dih_full=np.degrees(np.array([ mol.dihedral(rirp[i,dih_atms[0]],rirp[i,dih_atms[1]],rirp[i,dih_atms[2]],rirp[i,dih_atms[3]]) for i in range(len(rirp))]))



    ##%%
    fig=plt.figure(figsize=(10,10))
    ax=plt.subplot(111)
    plt.axis([x_min, x_max, y_min,y_max])
    plt.contourf(x,y,e_x_y,n_level,cmap='rainbow',alpha=.55,vmin=0, vmax=sat)
    cbar=plt.colorbar()
    cbar.ax.set_title('kcal/mol')
    C=ax.contour(x,y,e_x_y,n_level,cmap='rainbow', linewidths=0.6)
    ax.clabel(C, inline=1, fontsize=10)
    ax.plot(x_full,dih_full,'ro')
    ax.plot([x_full[i] for i in [0,its,-1]],[dih_full[i] for i in [0,its,-1]],'bo')
    plt.text(x_full[0], dih_full[0], "x_reac", horizontalalignment='left', size='large',color='black')
    plt.text(x_full[its], dih_full[its], "x_ts", horizontalalignment='left', size='large',color='black')
    plt.text(x_full[-1], dih_full[-1], "x_prod", horizontalalignment='left', size='large',color='black')
    plt.xlabel("Reac_coord (A)",fontweight='bold')
    plt.ylabel("Dihedral (Degrees)",fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)
    fig.savefig(pdf_file, bbox_inches='tight')
    ##%%



    ##%%
    fig, ax = plt.subplots()
    for i,angle in enumerate(y):
        ax.plot(x,e_x_y[i],'-',label=str(angle))
    #ax.set_title("Iterations")
    ax.set_xlabel("Reac_Path (A)")
    ax.set_ylabel("E(kcal/mol)")
    ax.legend()
    ##%%













###########################################################################%%


if __name__== "__main__":

    ##%%
    ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/aptts1_irc_nirc_41_nangle_20_a_b3lyp_eps_10e-6_ref_traj/aptts1_irc_nirc_41_nangle_-180_0_20_a_b3lyp_eps_10e-6_ref_traj-pos-1.xyz"
    reaction_path_inp_file="/home/ar612/Documents/Work/gaussian/irp/aptts1_irc.xyz"
    dihedral_atoms_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/dihedral_atoms"
    job="a"
    # ref_traj_inp_file="aptts1_gauss_nx_-1.5_1.5_50_ny_-1_1.7_50_eps_10e-8_ref_traj-pos-1.xyz"

    ref_traj_inp_file=sys.argv[1]



    sat=14
    n_level=40
    ##%%


    main(ref_traj_inp_file,sat,n_level)
