
import numpy as np
import sys
sys.path.append('$PYTHON_SCRIPTS')
import mol
from itertools import islice


# ### These are the position of the atoms from which I calculate the new coordinates vectors.
# ### These position are taken from the file r19.xyz
# pn=np.array([7.022079, 11.467340, 8.252784])
# pc1=np.array([7.938811, 12.208834, 8.041826])
# pc2=np.array([6.659386, 10.383466, 9.063626])
#d/1.40$ ^C
# ### Here I calculate the vectors conecting the three atoms
# dnc1=pc1-pn
# dnc2=pc2-pn
#
# ### Calculating the new coordinate vectors
# ez=dnc1/np.linalg.norm(dnc1)
# ez
#
# ex=np.cross(ez,dnc2)
# ex=ex/np.linalg.norm(ex)
# ex
#
# ey=np.cross(ez,ex)
# ey
#
# ### Chequing if they are perpendicular
# np.dot(ex,ey)


def main(inp_file,out_file_pattern,dist_list,n_atm,r0,phi_ind,chol_ind):

    e,r_init,atm_names=mol.e_r_atm_names_from_xyz(inp_file,n_atm,ener="no")
    r_init=r_init[0]
    r_fin=np.zeros(r_init.shape)





    ### Indexes for carbon and oxigen atoms before the separation
    c_ind=9
    o_ind=0

    r_c=r_init[c_ind]
    r_o=r_init[o_ind]

    di=np.linalg.norm(r_o-r_c)
    n_co=(r_o-r_c)/di


    for dist in dist_list:

        dr_phi=n_co*(di-dist)/2
        dr_chol=-n_co*(di-dist)/2


        r_fin[phi_ind]=r_init[phi_ind]+dr_phi
        r_fin[chol_ind]=r_init[chol_ind]+dr_chol

        out_file=out_file_pattern.replace("$$",str("%.2f" % dist))
        mol.xyz_file(r_fin,atm_names,out_file)


if __name__== "__main__":

    #dist_list=np.append(np.append(np.arange(1.36,2.01,0.04),2.5),3.)
    dist_list=np.array([1.37])

    phi_ind=np.array([1,2,9,10,11,12,13,14,15,28,29,30,31,32])
    chol_ind=np.array([0,3,4,5,6,7,8,16,17,18,19,20,21,22,23,24,25,26,27])

    inp_file="/cluster/data/ar612/cp2k_organized/XYZ/r14.xyz"

    out_file_pattern="/cluster/data/ar612/cp2k_organized/umbrella/scripts/co_dist_init_struct/r$$.xyz"

    n_atm=33

    r0=[10,10,10] ## Where I want the center of mass to be


    main(inp_file,out_file_pattern,dist_list,n_atm,r0,phi_ind,chol_ind)



    mol.k*mol.J2kcalmol*298/(0.05**2)

    200/(0.74**2)
