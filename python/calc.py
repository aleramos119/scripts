
#%%

import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

from scipy import integrate


def f(x,y0,y1,x0,sigma):
    return y0+(y1-y0)/(1+np.exp(-(x-x0)/sigma))

def f(w,k):
    return k/((k/2)**2+(w)**2)


x=np.linspace(0,200,200)


plt.plot(x,f(x,1,0,100,5),'o-')

kappa10=2.53*mol.cm_12Eh/0.0005764776761753108
kappa20=17.64*mol.cm_12Eh/0.0005764776761753108

kappa10
kappa20



1/kappa10
