
#%%
import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['seaborn-whitegrid'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import islice
import glob
%matplotlib qt

import quantum as quant





t_decay=1/(0.02*5.31398126420660976699e-01)
t_oscilation=86.1


##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%
abc=[chr(i) for i in range(97,123)]

def plot1(x,y,xlabel,ylabel,lsize,ax,plotlabel=" ",mark="-"):
    plt.rc('font', size=lsize)
    ax.plot(x,y,mark,linewidth=10.0,label=plotlabel)
    ax.set_xlabel(xlabel,size=lsize)
    ax.set_ylabel(ylabel,size=lsize)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2.5)
        ax.spines[axis].set_color("black")
    ax.grid(False)
    ax.tick_params(labelsize=size)
    ax.tick_params(direction='out', length=10, width=4)
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


def i_max(e_list,vmax):
    for i,e in enumerate(e_list):
        if e < vmax:
            i_max=i
    return i_max


def G(t,W,sig):
    if np.abs(t)<=W/2:
        return 1
    if np.abs(t)>W/2:
        return np.exp(-((np.abs(t)-W/2)**2)/(2*sig**2))










def t_psi_from_irep_psopt(file):
    dat=np.genfromtxt(file)

    t=dat[:,0]
    vec=dat[:,1:].transpose()

    psi=quant.vec_2_psi(vec)

    return t,psi

def psi_sch_2_psi_int(t,psi,e):

    ntime=len(t)
    npsi=len(psi)

    psi_int=np.zeros([npsi,ntime],dtype=np.complex_)

    for i in range(npsi):
        for j,tt in enumerate(t):
            psi_int[i,j]=np.exp(1j*e[i]*tt)*psi[i,j]

    return psi_int






###%%
states_name_list=["molecular_1","state_1","state_2"]
for state_name in states_name_list:

    file="/home/ar612/Documents/git_kraken/irep2_prop/results/t_Yt_0_"+state_name+".txt"
    sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_5_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/eta_0.050/"

    e_file=sys_dir+"h11.txt"
    sym_file=sys_dir+"symetric.txt"
    ant_file=sys_dir+"antisymetric.txt"


    e=np.genfromtxt(e_file).diagonal()
    t,psi=t_psi_from_irep_psopt(file)
    psi_sch=psi_sch_2_psi_int(t,psi,-e)

    ntime=len(t)
    npsi=len(psi)


    ####################################################
    state_file=sys_dir+state_name+".txt"

    state_vec=np.genfromtxt(state_file)
    state_psi=quant.vec_2_psi(quant.normalize_vec(state_vec))

    pop=np.zeros([ntime])

    for j in range(ntime):
        pop[j]=np.abs(np.dot(state_psi.conjugate(),psi_sch[:,j]))**2




    #######################################################




    # sym_vec=np.genfromtxt(sym_file)
    # sym_psi=quant.vec_2_psi(quant.normalize_vec(sym_vec))
    #
    # ant_vec=np.genfromtxt(ant_file)
    # ant_psi=quant.vec_2_psi(quant.normalize_vec(ant_vec))
    #
    #
    #
    # pop=np.zeros([npsi+2,ntime])
    #
    #
    # for j in range(ntime):
    #     pop[npsi,j]=np.abs(np.dot(sym_psi.conjugate(),psi_sch[:,j]))**2
    #     pop[npsi+1,j]=np.abs(np.dot(ant_psi.conjugate(),psi_sch[:,j]))**2
    #
    #     for k in range(npsi):
    #         pop[k,j]=np.abs(psi_sch[k,j])**2


    ##%%



    plt.plot(t,pop,'o-',label=state_name)
    plt.legend(loc="upper right")


    # plt.rc('font', size=20)
    # plt.plot(t,pop[npsi],'o-',label="1+2")
    # plt.plot(t,pop[npsi+1],'o-',label="1-2")
    #
    # sum=np.zeros(ntime)
    # for i in range(npsi):
    #     sum=sum+pop[i]
    #     if max(pop[i]) > 0.1:
    #         plt.plot(t,pop[i],'o-',label=str(i))
    #
    # plt.plot(t,sum,'o-',label="Total")
    #
    # plt.legend(loc="center left")
    ##%%
