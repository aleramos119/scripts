
#%%
import numpy as np
import glob
from itertools import islice
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

def plot_e(dist1,e1,dist2,e2,xlabel):
    plt.plot(dist1,e1,'b-o',linewidth=2.5,label="path 1",color="tab:blue")
    plt.plot(dist2,e2,'b-o',linewidth=2.5,label="path 2",color="tab:red")
    #plt.plot(dist3,e3,'bo',label="ock.",color="tab:green")
    plt.xlabel(xlabel,fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.ylabel('E (kcal/mol)',fontweight='bold')
    plt.legend()
    plt.grid(True)
    plt.show()
############################################################################%%


def main (mean_field_final_dir,reac_path_file_gas,out_file_solv,out_file_solv_error,out_file_solv_min,reac_path,n_images,n_atm,all_ener_pattern,solv_ener_pattern):

    e_gas,r,atm_names=mol.e_r_atm_names_from_xyz (reac_path_file_gas,n_images,n_atm)

    e_solv=np.zeros([0])
    e_solv_error=np.zeros([0])
    e_solv_min=np.zeros([0])


    for i in range(0,n_images):

        all_ener_project=str(i+1)+"_"+reac_path+"_all_ener"
        solv_ener_project=str(i+1)+"_"+reac_path+"_solv_ener"

        all_image_file=glob.glob(mean_field_final_dir+"/all_ener/"+all_ener_project+"/"+"slurm*.out")[-1]
        solv_image_file=glob.glob(mean_field_final_dir+"/solv_ener/"+solv_ener_project+"/"+"slurm*.out")[-1]



        e_all_list=mol.extract_ener(all_image_file,all_ener_pattern,-1)
        e_solv_list=mol.extract_ener(solv_image_file,solv_ener_pattern,-1)

        e_solv=np.append(e_solv,np.mean(e_all_list-e_solv_list))
        e_solv_error=np.append(e_solv_error,np.std(e_all_list-e_solv_list))
        e_solv_min=np.append(e_solv_min,np.min(e_all_list-e_solv_list))

        # print(np.min(e_all_list-e_solv_list))
        # print(e_all_list-e_solv_list)

    mol.write_trajectory(r,e_solv,atm_names,out_file_solv)
    mol.write_matrix_to_file(e_solv_error,"","",out_file_solv_error)
    mol.write_trajectory(r,e_solv_min,atm_names,out_file_solv_min)





###########################################################################%%


if __name__== "__main__":

    mean_field_final_dir=sys.argv[1]
    reac_path_file_gas=sys.argv[2]
    out_file_solv=sys.argv[3]

    reac_path=sys.argv[4]
    n_images=int(sys.argv[5])
    n_atm=int(sys.argv[6])

    all_ener_pattern=sys.argv[7]
    solv_ener_pattern=sys.argv[8]

    out_file_solv_error=sys.argv[9]
    out_file_solv_min=sys.argv[10]



    # mean_field_final_dir="/home/ar612/Documents/cp2k_organized/mean_field/r19_ts_p_reduced"
    # reac_path_file_gas="/home/ar612/Documents/cp2k_organized/reac_path/r19_ts_p_band_reduced/r19_ts_p_band_movie.xyz"
    # out_file_solv="/home/ar612/Documents/cp2k_organized/mean_field/r19_ts_p_reduced/r19_ts_p_band_solv_movie.xyz"
    # out_file_solv_error="/home/ar612/Documents/cp2k_organized/mean_field/r19_ts_p_reduced/r19_ts_p_band_solv_movie_std.txt"
    # out_file_solv_min="/home/ar612/Documents/cp2k_organized/mean_field/r19_ts_p_reduced/r19_ts_p_band_solv_movie_min.xyz"
    #
    # reac_path="r19_ts_p"
    # n_images=20
    # n_atm=33
    #
    #
    # all_ener_pattern="ENERGY| Total FORCE_EVAL ( QMMM ) energy (a.u.):"
    # solv_ener_pattern="ENERGY| Total FORCE_EVAL ( FIST ) energy (a.u.):"


    main(mean_field_final_dir,reac_path_file_gas,out_file_solv,out_file_solv_error,out_file_solv_min,reac_path,n_images,n_atm,all_ener_pattern,solv_ener_pattern)



    ### Gaussian data #####
    ecyclo,rcyclo,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/cyclohexanol_b3lyp_geo_opt_reduced/cyclohexanol_b3lyp_geo_opt-pos-1.xyz")
    epheny,rpheny,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/phenylisocyanate_b3lyp_geo_opt_reduced/phenylisocyanate_b3lyp_geo_opt-pos-1.xyz")

    eR1,rR1,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/R1_b3lyp_geo_opt_reduced/R1_b3lyp_geo_opt-pos-1.xyz")
    er20,rr20,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/r20_b3lyp_geo_opt_reduced/r20_b3lyp_geo_opt-pos-1.xyz")
    er3,rr3,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/r3_b3lyp_geo_opt_reduced/r3_b3lyp_geo_opt-pos-1.xyz")
    3*(eR1[-1]-(ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol
    (er20[-1]-(ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol
    3*(er3[-1]-(ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol


    edim,rdim,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/pheny_cyclo_dimer_b3lyp_geo_opt_reduced/pheny_cyclo_dimer_b3lyp_geo_opt-pos-1.xyz")
    e2phenyl1cyclo,r2phenyl1cyclo,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/2phenyl1cyclo_b3lyp_geo_opt_reduced/2phenyl1cyclo_b3lyp_geo_opt-pos-1.xyz")
    e2phenyl1cyclo2,r2phenyl1cyclo2,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/2phenyl1cyclo2_b3lyp_geo_opt_reduced/2phenyl1cyclo2_b3lyp_geo_opt-pos-1.xyz")
    (edim[-1]-(2*ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol
    (e2phenyl1cyclo[-1]-(ecyclo[-1]+2*epheny[-1]))*mol.Eh2kcalmol
    (e2phenyl1cyclo2[-1]-(ecyclo[-1]+2*epheny[-1]))*mol.Eh2kcalmol

    -11.186999596694605-10.482409399393873

    etrim2,rtrim2,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/pctreac2_b3lyp_geo_opt_reduced/pctreac2_b3lyp_geo_opt-pos-1.xyz")
    etrim1,rtrim1,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/pctreac1_b3lyp_geo_opt_reduced/pctreac1_b3lyp_geo_opt-pos-1.xyz")
    e2phenyl,r2phenyl,atm_names=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/geo_opt/2phenyl_b3lyp_geo_opt_reduced/2phenyl_b3lyp_geo_opt-pos-1.xyz")
    (etrim2[-1]-(3*ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol
    (etrim1[-1]-(3*ecyclo[-1]+epheny[-1]))*mol.Eh2kcalmol
    (e2phenyl[-1]-(2*epheny[-1]))*mol.Eh2kcalmol


    ### Reaction path data #####
    e1,r1,atm_names1=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/reac_path/pctreac1_pctts1_pctprod1_band_40rep_b3lyp_reduced/pctreac1_pctts1_pctprod1_band_40rep_b3lyp_movie.xyz")
    e2,r2,atm_names2=mol.e_r_atm_names_from_xyz(os.environ['CP2K_HOME']+"/reac_path/pctreac2_pctts2_pctprod2_band_40rep_b3lyp_reduced/pctreac2_pctts2_pctprod2_band_40rep_b3lyp_movie.xyz")

    dr1,dist1=mol.path_dist(r1)
    dr2,dist2=mol.path_dist(r2)
    xlabel="reac_path (A)"

    e2=(e2-e1[0])*mol.Eh2kcalmol
    e1=(e1-e1[0])*mol.Eh2kcalmol


    plot_e(dist1,e1,dist2,e2,xlabel)



    ### Gaussian data #####
    rpctreac1,atm_names,epctreac1=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/geo_opt/pctreac1_reduced/pctreac1_68058.out")
    rpctreac2,atm_names,epctreac2=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/geo_opt/pctreac2_reduced/pctreac2_68059.out")

    rpctprod1,atm_names,epctprod1=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/geo_opt/pctprod1_reduced/pctprod1_68061.out")
    rpctprod2,atm_names,epctprod2=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/geo_opt/pctprod2_reduced/pctprod2_68062.out")

    rpctts1,atm_names,epctts1=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/ts_opt/pctts1_reduced/pctts1_68057.out")
    rpctts2,atm_names,epctts2=mol.r_atm_names_e_from_opt_gauss("/media/ar612/multimedia_ale/Documents/gaussian/ts_opt/pctts2_reduced/pctts2_68056.out")

    (epctreac2[-1]-epctreac1[-1])*mol.Eh2kcalmol
    (epctts1[-1]-epctreac1[-1])*mol.Eh2kcalmol
    (epctts2[-1]-epctreac2[-1])*mol.Eh2kcalmol
    (epctprod1[-1]-epctreac1[-1])*mol.Eh2kcalmol
    (epctprod2[-1]-epctreac2[-1])*mol.Eh2kcalmol
