
#%%
import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['seaborn-whitegrid'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import islice
import glob
%matplotlib qt

import quantum as quant
#%%

##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%
abc=[chr(i) for i in range(97,123)]

def plot1(x,y,xlabel,ylabel,lsize,ax,plotlabel=" ",mark="-"):
    plt.rc('font', size=lsize)
    ax.plot(x,y,mark,linewidth=10.0,label=plotlabel)
    ax.set_xlabel(xlabel,size=lsize)
    ax.set_ylabel(ylabel,size=lsize)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2.5)
        ax.spines[axis].set_color("black")
    ax.grid(False)
    ax.tick_params(labelsize=size)
    ax.tick_params(direction='out', length=10, width=4)
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


def i_max(e_list,vmax):
    for i,e in enumerate(e_list):
        if e < vmax:
            i_max=i
    return i_max


def G(t,W,sig):
    if np.abs(t)<=W/2:
        return 1
    if np.abs(t)>W/2:
        return np.exp(-((np.abs(t)-W/2)**2)/(2*sig**2))

############## Ectract the ith element of a line matching the given pattern #################


def extract_pattern(inp_file,pattern,i,number=True):

    e=np.zeros([0])

    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if pattern in line:
               if number:
                   e=np.append(e,float(line.split()[i]))
               else:
                   e=np.append(e,line.split()[i])

    return e



def read_psopt_output(out_file):

    collocation=None
    nnodes=None
    solved=""
    solver=""
    niter=None
    cost=None
    end_cost=None
    run_cost=None
    local_err=None
    t=None

    with open(out_file) as file:
       for n, line in enumerate(file, 1):
           if "Collocation method:" in line:
               collocation=line.split()[-1]
           if "Number of nodes phase 1:" in line:
               nnodes=int(line.split()[-1])
           if "running with linear solver" in line:
               solver=line.split()[-1].replace('.','')
           if "Number of Iterations....:" in line:
               niter=int(line.split()[-1])
           if "Runtime is:" in line and "sec" in line:
               t=float(line.split()[-2])/60

           if "*** The problem has been solved!" in line:
               solved="solved"
           if "EXIT: Solved To Acceptable Level." in line:
               solved="almost_solved"
           if solved=="solved" or solved=="almost_solved":
               if "(unscaled) cost function value:" in line:
                   cost=float(line.split()[-1])
               if "Phase 1 endpoint cost function value:" in line:
                   end_cost=float(line.split()[-1])
               if "Phase 1 integrated part of the cost:" in line:
                   run_cost=float(line.split()[-1])
               if "Phase 1 maximum relative local error:" in line:
                   local_err=float(line.split()[-1])



    return collocation,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t



def read_irep_data_from_folder(dir,out_pattern):

    if not os.path.isdir(dir):
        print(f"{dir} doesn't exists.")


    par_opt=True


    eta0_list_list=[]
    etaf_list_list=[]
    t0_list_list=[]
    tf_list_list=[]
    cost_list_list=[]
    end_cost_list_list=[]
    run_cost_list_list=[]
    niter_list_list=[]
    comp_time_list_list=[]
    i_list_list=[]


    eta_dir_list=glob.glob(dir+"/*")
    eta_dir_list=[eta_dir for eta_dir in eta_dir_list if os.path.isdir(eta_dir)]
    eta_dir_list.sort()

    project=dir.split("/")[-1]

    if "no_par_opt" in dir:
        par_opt=False



    for eta_dir in eta_dir_list:

        i_dir_list=glob.glob(eta_dir+"/*")
        i_dir_list=[i_dir for i_dir in i_dir_list if os.path.isdir(i_dir)]

        i_dir_list.sort()




        eta0_list=[]
        etaf_list=[]
        t0_list=[]
        tf_list=[]
        cost_list=[]
        end_cost_list=[]
        run_cost_list=[]
        niter_list=[]
        i_list=[]
        comp_time_list=[]



        name_list=eta_dir.split("/")[-1].split("_")
        indx_t0=name_list.index("t0")
        indx_eta0=name_list.index("eta0")




        for i_dir in i_dir_list:





            out_file_list=glob.glob(i_dir+"/"+out_pattern)
            out_file_list=[ out_file for out_file in out_file_list if "slurm" not in out_file and "ipopt" not in out_file ]
            out_file_list.sort()
            eta_file=i_dir+"/p.dat"
            time_file=i_dir+"/t.dat"



            if len(out_file_list) != 0:
                #print(out_file_list)
                colloc,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,comp_time=read_psopt_output(out_file_list[-1])

                niter_list.append(niter)
                comp_time_list.append(comp_time)







                if solved=="solved" or solved=="almost_solved":



                    t0=float(name_list[indx_t0+1])
                    t0_list.append(t0)

                    eta0=float(name_list[indx_eta0+1])
                    eta0_list.append(eta0)


                    if par_opt:
                        etaf=float(np.genfromtxt(eta_file))
                    else:
                        etaf=eta0

                    tf=float(np.genfromtxt(time_file)[-1])


                    i_list.append(int(i_dir.split("/")[-1]))
                    etaf_list.append(etaf)
                    tf_list.append(tf)
                    cost_list.append(cost)
                    end_cost_list.append(end_cost)
                    run_cost_list.append(run_cost)



        i_list_list.append(i_list)
        eta0_list_list.append(eta0_list)
        etaf_list_list.append(etaf_list)
        t0_list_list.append(t0_list)
        tf_list_list.append(tf_list)
        cost_list_list.append(cost_list)
        end_cost_list_list.append(end_cost_list)
        run_cost_list_list.append(run_cost_list)
        niter_list_list.append(niter_list)
        comp_time_list_list.append(comp_time_list)



    return project,i_list_list,eta0_list_list,etaf_list_list,cost_list_list,end_cost_list_list,run_cost_list_list,t0_list_list,tf_list_list,niter_list_list,comp_time_list_list



eta_dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_molecular_1/N1_5_wc_w10_var_0.5_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced/eta0_0.050_t0_300/26"

def plot_field_dir(eta_dir,e0,mu0,sys_dir,W=25,sig=10,n_level=6,threshhold=0.1):

    plt.rc('font', size=40)

    par_opt=True


    if "no_par_opt" in eta_dir:
        par_opt=False


    project_name=eta_dir.split('/')[-1]


    out_file_list=glob.glob(eta_dir+"/reduced.out")
    out_file_list=[ out_file for out_file in out_file_list if "slurm" not in out_file and "ipopt" not in out_file ]
    out_file_list.sort()



    t_file=eta_dir+"/t.dat"
    u_file=eta_dir+"/u.dat"
    pop_file=eta_dir+"/popi.dat"
    #pop_file_s=eta_dir+"/pops.dat"
    eta_file=eta_dir+"/p.dat"
    x_file=eta_dir+"/xs.dat"


    if len(out_file_list) != 0:
        colloc,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t=read_psopt_output(out_file_list[-1])

        if solved=="solved" or solved=="almost_solved":
            name_list=project_name.split("_")
            # indx=name_list.index("eta0")
            #
            #
            # if par_opt:
            #     eta0=float(name_list[indx+1])
            #     etaf=float(np.genfromtxt(eta_file))
            # else:
            #     eta0=float(name_list[indx+1])
            #     etaf=eta0


            if os.path.exists(eta_file):
                etaf=np.genfromtxt(eta_file)
            else:
                inp_file=glob.glob(eta_dir+"/*inp")[0]
                with open(inp_file) as file:
                   for n, line in enumerate(file, 1):
                       if "##Initial_state" in line:
                           etaf=float(line.split("/")[-2].split("_")[-1])



            state_name_list=["molecular_1","symetric","antisymetric"]
            state_psi_list=[]

            if os.path.exists(sys_dir):
                sys_dir_list=glob.glob(sys_dir+"/eta_*")
                eta_list=[sys_dir.split("_")[-1] for sys_dir in sys_dir_list]
                eta_dif_list=[(float(eta)-etaf)**2 for eta in eta_list]

                min_val=min(eta_dif_list)
                min_indx=eta_dif_list.index(min_val)

                eta_dif_list=[float(eta) for eta in eta_list]
            else:
                raise RuntimeError("sys_dir doesn't exist")


            for state_name in state_name_list:
                state_file=sys_dir+"/eta_"+eta_list[min_indx]+"/"+state_name+".txt"
                state_vec=np.genfromtxt(state_file)
                state_psi=quant.vec_2_psi(quant.normalize_vec(state_vec))
                state_psi_list.append(state_psi)

            sys_dir


            t=np.genfromtxt(t_file)
            u=np.genfromtxt(u_file)
            pop_list=np.genfromtxt(pop_file).transpose()
            #pop_list_s=np.genfromtxt(pop_file_s).transpose()


            t_ps=(t/e0)*mol.au2fs/1000
            u_au=u*(e0/mu0)

            f=2*np.pi/(t[1]-t[0])

            n_w=1000

            spect_w_t=np.zeros([len(t)*n_w,3])

            i=0
            for t0 in t:
                utrans=np.array( [u[k]*G(t1-t0,W,sig) for k,t1 in enumerate(t)])
                spectrans,freqtrans,plottrans=plt.magnitude_spectrum(utrans,f,pad_to=2000)
                plt.close()
                for j in range(n_w):
                     spect_w_t[i,0]=freqtrans[j]
                     spect_w_t[i,1]=(t0/e0)*mol.au2fs/1000
                     spect_w_t[i,2]=spectrans[j]
                     i=i+1



            fig, ax = plt.subplots(2, 1,sharex=True,figsize=(16,18),gridspec_kw={'hspace': 0.05,'wspace': 0.2})
            ax[0].plot(t_ps,u_au,'o-',markersize=3)
            #ax[0].set_title(project+"_eta0_"+str(eta0)+"_etaf_"+str(etaf))
            #ax[0].set_xlabel("t(ps)")
            ax[0].set_ylabel("E(au)")
            ax[0].set_xlim(t_ps[0],t_ps[-1])
            ax[0].grid(True)
            ax[0].ticklabel_format(axis="y",style='sci', scilimits=(0,0))





            ax[1].tricontourf(spect_w_t[:,1],spect_w_t[:,0],spect_w_t[:,2],n_level,cmap='rainbow',alpha=.55)##,vmax=0.03337,vmin=0)
            ax[1].set_xlabel("t(ps)")
            ax[1].set_ylabel(r'$\omega (\omega_{10})$')
            ax[1].set_ylim(0,4)
            ax[1].set_xlim(t_ps[0],t_ps[-1])
            ax[1].grid(True)
            ax[1].ticklabel_format(axis="y",style='sci', scilimits=(0,0))

            fig.set_size_inches(16,18,forward=False)
            fig.savefig(eta_dir+"/plot_u.pdf",bbox_inches='tight')
            plt.close()


            x=np.genfromtxt(x_file)
            n_time=len(t_ps)
            n_states=len(state_psi_list)
            pop_state=np.zeros([n_states,n_time])

            matrix_file=[]
            name_file=[]

            matrix_file.append(t_ps)
            matrix_file.append(u_au)

            name_file.append("t(ps)")
            name_file.append("u(au)")


            for j in range(n_time):
                psi_t=quant.vec_2_psi(x[j])
                for k in range(n_states):
                    pop_state[k,j]=np.abs(np.dot(psi_t,state_psi_list[k]))**2



            fig, ax = plt.subplots(1, 1,figsize=(16,9))
            for k in range(n_states):
                # if state_name_list[k] == "symetric":
                #     ax.plot(t_ps,pop_state[k],'o-',markersize=3,label=r'$|1\rangle + |2\rangle$')
                # if state_name_list[k] == "antisymetric":
                #     ax.plot(t_ps,pop_state[k],'o-',markersize=3,label=r'$|1\rangle - |2\rangle$')
                if state_name_list[k] == "molecular_1":
                    ax.plot(t_ps,pop_state[k],'o-',markersize=6,label=state_name_list[k])
                # else:
                #     ax.plot(t_ps,pop_state[k],'o-',markersize=3,label=state_name_list[k])

                matrix_file.append(pop_state[k])
                name_file.append(state_name_list[k])



            sum_pop=np.zeros(len(t_ps))
            for i,pop in enumerate(pop_list):
                sum_pop=sum_pop+pop
                if max(pop) > threshhold:
                    ax.plot(t_ps,pop,'o-',markersize=3,label="|"+str(i)+r"$\rangle$")
                matrix_file.append(pop)
                name_file.append("|"+str(i)+">")

            ax.plot(t_ps,sum_pop,'o-',markersize=3,label="sum_pop")
            matrix_file.append(sum_pop)
            name_file.append("sum_pop")


            # for i,pop in enumerate(pop_list_s):
            #     /data/ar612/cavities/fixed_dipole/nodes_test_Emax_5_reducedif max(pop) > threshhold:
            #         ax.plot(t_ps,pop,'o-',markersize=3,label="sch|"+str(i)+r"$\rangle$")

            ax.legend(prop={'size': 20})
            #ax.set_title(project+"_eta0_"+str(eta0)+"_etaf_"+str(etaf))
            ax.set_xlabel("t(ps)")
            ax.set_ylabel("Population")
            ax.grid(True)
            ax.ticklabel_format(axis="y",style='sci', scilimits=(0,0))
            fig.set_size_inches(16,9,forward=False)
            fig.savefig(eta_dir+"/plot_pop.pdf",bbox_inches='tight')
            plt.close()


            mol.write_matrix_to_file(np.array(matrix_file).transpose(),"                      ".join(name_file),"",eta_dir+"/t_u_pop.txt")




def plot_field_dir_irep_prop(eta_dir,psi_file,e0,mu0,sys_dir,threshhold=0.1):


    x_file=eta_dir+"/"+psi_file

    state_name_list=["symetric","antisymetric"]
    state_psi_list=[]


    for state_name in state_name_list:
        state_file=sys_dir+"/"+state_name+".txt"
        state_vec=np.genfromtxt(state_file)
        state_psi=quant.vec_2_psi(quant.normalize_vec(state_vec))
        state_psi_list.append(state_psi)

    t=np.genfromtxt(x_file)[:,0]



    t_ps=(t/e0)*mol.au2fs/1000





    x=np.genfromtxt(x_file)[:,1:]
    n_time=len(t_ps)
    n_states=len(state_psi_list)
    n=len(x[0])//2
    pop_state=np.zeros([n_states,n_time])



    for j in range(n_time):
        psi_t=quant.vec_2_psi(x[j])
        for k in range(n_states):
            pop_state[k,j]=np.abs(np.dot(psi_t,state_psi_list[k]))**2




    fig, ax = plt.subplots(1, 1,figsize=(16,9))
    for k in range(n_states):
        if state_name_list[k] == "molecular_1":
            ax.plot(t_ps,pop_state[k],'o-',markersize=6,label=state_name_list[k])
        else:
            ax.plot(t_ps,pop_state[k],'o-',markersize=3,label=state_name_list[k])





    sum=np.zeros(n_time)
    for i in range(n):
        pop=np.zeros(n_time)
        for j in range(n_time):
            psi_t=quant.vec_2_psi(x[j])
            psi_n=psi_t[i]
            pop[j]=np.abs(np.dot(psi_n.conj(),psi_n))
        sum=sum+pop

        if max(pop) > threshhold:
            ax.plot(t_ps,pop,'o-',markersize=3,label="|"+str(i)+r"$\rangle$")


    #ax.plot(t_ps,sum,'o-',markersize=3,label="Total")



    # for i,pop in enumerate(pop_list_s):
    #     if max(pop) > threshhold:
    #         ax.plot(t_ps,pop,'o-',markersize=3,label="sch|"+str(i)+r"$\rangle$")

    ax.legend(prop={'size': 20})
    #ax.set_title(project+"_eta0_"+str(eta0)+"_etaf_"+str(etaf))
    ax.set_xlabel("t(ps)")
    ax.set_ylabel("Population")
    ax.grid(True)
    ax.ticklabel_format(axis="y",style='sci', scilimits=(0,0))
    fig.set_size_inches(16,9,forward=False)
    fig.savefig(eta_dir+"/plot_pop.pdf",bbox_inches='tight')
    plt.close()











##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%
e0=0.0005764776761753108
mu0=1.657
N1=5
wc="w10_var_1.0"
sys="N1_5_wc_"+wc+"_e0_"+str(e0)+"_mu0_"+str(mu0)+"_neta_11"

sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_"+wc+"_neta_11/"+sys

dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_scan/excitation_molecular_1/N1_5_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced/eta0_0.030/eta0_0.030_t0_10/2"
plot_field_dir(dir,e0,mu0,sys_dir,W=10,sig=1,n_level=6,threshhold=0.01)

eta_dir="/home/ar612/Documents/git_kraken/irep2_prop/results"
psi_file="t_Yt_0.txt"
eta="0.050"
sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_"+wc+"_neta_11/"+sys+"/eta_"+eta
threshhold=0.1
plot_field_dir_irep_prop(eta_dir,psi_file,e0,mu0,sys_dir,threshhold=0.1)
##################################################################################################################%%
################################ Ploting Collocation ##########################################################%%
##################################################################################################################%%
e0=0.0005764776761753108
#mu0=0.0295
mu0=1.657
wc="w10_var_0.95"
target_state="molecular_1"


e0_2_ps=mol.au2fs/(e0*1000)
ps_2_e0=1/e0_2_ps


sys="N1_5_wc_"+wc+"_e0_"+str(e0)+"_mu0_"+str(mu0)+"_neta_11"

base_dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_"+target_state+"/"+sys+"/"

sys_dir="/home/ar612/Documents/Work/cavities/H_transfer/wc_"+wc+"_neta_11/"+sys

dir_list=["random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced"]





# T_list=["500","400","300","200","100","50"]
# for T in T_list:
#     eta_dir="/home/ar612/Documents/Work/cavities/test2/really_works/eta0_0.030_works_kappa_0.02_mu_1.657_T_"+T
#     plot_field_dir(eta_dir,e0,mu0,sys_dir,W=10,sig=1,n_level=6,threshhold=0.01)
#
# eta_dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_antisymetric/N1_10_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_4_trapezoidal_500_reduced/eta0_0.035_t0_400/1"
# plot_field_dir(eta_dir,e0,mu0,sys_dir,W=10,sig=1,n_level=6,threshhold=0.01)


###%%
scan="eta"
plot_field_pop="no"
plt.rc('font', size=40)
marksize=20


i=0
#for eta_random_dir in eta_random_dir_list:
for i in range(1):
    dir=dir_list[i]

    name=dir

    dir=base_dir+dir

    project_3,i_list_3,eta0_list_3,eta_list_3,cost_list_3,end_cost_list_3,run_cost_list_3,t0_list_3,tf_list_3,niter_list,comp_time_list=read_irep_data_from_folder(dir,"reduced.out")


    tf_list_3=[ list(np.array(tf)*e0_2_ps) for tf in tf_list_3]
    t0_list_3=[ list(np.array(tf)*e0_2_ps) for tf in t0_list_3]


    optimal_par_list=[]
    optimal_cost_list=[]
    optimal_end_cost_list=[]
    optimal_run_cost_list=[]
    optimal_tf_list=[]
    optimal_etaf_list=[]
    optimal_i_list=[]



    if scan == "tf":
        par_list=list(np.arange(10, 110, 10, dtype=float)*e0_2_ps)
        read_par_list=t0_list_3

    if scan == "tf_free":
        par_list=list(np.arange(25, 30, 10, dtype=float)*e0_2_ps)
        read_par_list=t0_list_3

    if scan == "eta":
        par_list=list(np.arange(0.00, 0.055, 0.005, dtype=float))
        read_par_list=eta0_list_3

    if scan == "eta_free":
        par_list=list(np.arange(0.035, 0.036, 0.005, dtype=float))
        read_par_list=eta0_list_3


    for t,par_list in enumerate(read_par_list):
        min_val=np.inf
        i_min=0
        if len(par_list)!=0:
            par=par_list[0]
            for i,cost in enumerate(cost_list_3[t]):
                if cost_list_3[t][i] < min_val:
                    min_val=cost_list_3[t][i]
                    i_min=i




            optimal_par_list.append(par)
            optimal_cost_list.append(cost_list_3[t][i_min])
            optimal_end_cost_list.append(end_cost_list_3[t][i_min])
            optimal_run_cost_list.append(run_cost_list_3[t][i_min])
            optimal_tf_list.append(tf_list_3[t][i_min])
            optimal_etaf_list.append(eta_list_3[t][i_min])
            optimal_i_list.append(i_list_3[t][i_min])


    # optimal_par_list=np.genfromtxt(dir+"/total_cost.txt")[:,0]
    # optimal_cost_list=np.genfromtxt(dir+"/total_cost.txt")[:,1]
    # optimal_end_cost_list=np.genfromtxt(dir+"/end_cost.txt")[:,1]
    # optimal_run_cost_list=np.genfromtxt(dir+"/run_cost.txt")[:,1]
    # optimal_tf_list=np.genfromtxt(dir+"/tf.txt")[:,1]
    # optimal_etaf_list=np.genfromtxt(dir+"/etaf.txt")[:,1]
    # optimal_i_list=np.genfromtxt(dir+"/opt_i.txt")[:,1]


    t0_name=glob.glob(dir+"/*")[0].split("_")[-1]



    if (scan == "eta" or scan=="eta_free" or scan=="tf") and plot_field_pop=="yes":
        for k in range(len(optimal_par_list)):
            print(dir+"/eta0_"+"{:.3f}".format(optimal_par_list[k])+"_t0_"+t0_name+"/"+str(int(optimal_i_list[k])))
            plot_field_dir(dir+"/eta0_"+"{:.3f}".format(optimal_par_list[k])+"_t0_"+t0_name+"/"+str(int(optimal_i_list[k])),e0,mu0,sys_dir,W=10,sig=1,n_level=6,threshhold=0.01)


    #########################################################
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))



    if scan=="tf" or scan=="tf_free":
        matrix=np.array([np.array(optimal_par_list),optimal_cost_list]).transpose()
        ax1.plot(np.array(optimal_par_list),optimal_cost_list,'o',linewidth=marksize/4,markersize=1.3*marksize,label="optimal")

        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i]),cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"tf(ps)   total_cost","",dir+"/total_cost.txt")
        ax1.set_xlabel("tf (ps)")
        ax1.set_ylabel("total cost")

    if scan=="eta" or scan=="eta_free":
        matrix=np.array([optimal_par_list,optimal_cost_list]).transpose()
        ax1.plot(np.array(optimal_par_list),optimal_cost_list,'o',linewidth=marksize/4,markersize=1.3*marksize,label="optimal")

        # for i,par in enumerate(eta_list_3):
        #     ax1.plot(par,cost_list_3[i],'o',linewidth=marksize/4,markersize=marksize,label=str(i+1))
        #     for j in range(len(par)):
        #         ax1.text(par[j],cost_list_3[i][j],str(i_list_3[i][j]))
        mol.write_matrix_to_file(matrix,"eta0   total_cost","",dir+"/total_cost.txt")
        ax1.set_xlabel("eta0")
        ax1.set_ylabel("total cost")


    ax1.legend(prop={'size': 8})
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/total_cost.pdf")
    plt.close('all')

    #########################################################












    #########################################################
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))

    ax1.plot(optimal_par_list,optimal_end_cost_list,'o--',linewidth=marksize/4,markersize=marksize)
    matrix=np.array([optimal_par_list,optimal_end_cost_list]).transpose()

    if scan=="tf":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"tf(ps)   end_cost","",dir+"/end_cost.txt")
        ax1.set_xlabel("tf (ps)")
        ax1.set_ylabel("end cost")

    if scan=="eta" or scan=="eta_free":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"eta0   end_cost","",dir+"/end_cost.txt")
        ax1.set_xlabel("eta0")
        ax1.set_ylabel("end cost")


    #ax1.legend()
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/end_cost.pdf")
    plt.close('all')

    #########################################################


    #########################################################
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))

    ax1.plot(optimal_par_list,optimal_run_cost_list,'o--',linewidth=marksize/4,markersize=marksize)
    matrix=np.array([optimal_par_list,optimal_run_cost_list]).transpose()

    if scan=="tf":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"tf(ps)   run_cost","",dir+"/run_cost.txt")
        ax1.set_xlabel("tf (ps)")
        ax1.set_ylabel("run cost")

    if scan=="eta" or scan=="eta_free":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"eta0   run_cost","",dir+"/run_cost.txt")
        ax1.set_xlabel("eta0")
        ax1.set_ylabel("run cost")


    #ax1.legend()
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/run_cost.pdf")
    plt.close('all')

    #########################################################



    #########################################################    matrix=np.array([eta0_list_3,cost_list_3]).transpose()
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))

    matrix=np.array([optimal_par_list,optimal_tf_list]).transpose()

    if scan=="tf":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"tf(ps)   tf","",dir+"/tf_ps.txt")
        ax1.set_xlabel("tf (ps)")
        ax1.set_ylabel("tf (ps)")

    if scan=="eta" or scan=="eta_free":
        # for i in range(len(cost_list_3)-5):
        #     ax1.plot(eta_list_3[i],tf_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i+1))
        mol.write_matrix_to_file(matrix,"eta0   tf","",dir+"/tf_ps.txt")
        ax1.set_xlabel("eta0")
        ax1.set_ylabel("tf (ps)")

    ax1.plot(optimal_par_list,optimal_tf_list,'o--',linewidth=marksize/4,markersize=marksize,label="optimal")


    ax1.legend()
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/tf_ps.pdf")
    plt.close('all')



    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))
    matrix=np.array([optimal_par_list,np.array(optimal_tf_list)*ps_2_e0]).transpose()
    mol.write_matrix_to_file(matrix,"eta0   tf","",dir+"/tf.txt")
    ax1.plot(optimal_par_list,np.array(optimal_tf_list)*ps_2_e0,'o--',linewidth=marksize/4,markersize=marksize,label="optimal")
    ax1.legend()
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/tf.pdf")
    plt.close('all')


    #########################################################





    #########################################################
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))

    ax1.plot(optimal_par_list,optimal_etaf_list,'o--',linewidth=marksize/4,markersize=marksize)
    matrix=np.array([optimal_par_list,optimal_etaf_list]).transpose()

    if scan=="tf":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"tf(ps)   etaf","",dir+"/etaf.txt")
        ax1.set_xlabel("tf (ps)")
        ax1.set_ylabel("etaf")

    if scan=="eta" or scan=="eta_free":
        # for i in range(len(cost_list_3)):
        #     ax1.plot(np.array(tf_list_3[i])/e0*mol.au2fs/1000,cost_list_3[i],'o--',linewidth=marksize/4,markersize=marksize,label=str(i))
        mol.write_matrix_to_file(matrix,"eta0   /home/ar612/Documents/Work/cavities/no_par_opt_tf_scan/excitation_molecular_1/Emax1_10/neta_11/etaf","",dir+"/etaf.txt")
        ax1.set_xlabel("eta0")
        ax1.set_ylabel("etaf")


    #ax1.legend()
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(dir+"/etaf.pdf")
    plt.close('all')
    #########################################################

    mol.write_matrix_to_file(np.array([np.array(optimal_par_list)/e0_2_ps,optimal_i_list]).transpose(),"i","",dir+"/opt_i.txt")
    #

####%%
#
#
#
project_3,i_list_3,eta0_list_3,eta_list_3,cost_list_3

i_list_3
eta0_list_3

plt.plot(i_list_3[1],cost_list_3[1],'o')


##%%
#

#

# #

# #
# #
# matrix=np.array(np.array([optimal_etaf_list,optimal_cost_list,optimal_end_cost_list,optimal_run_cost_list])).transpose()
# mol.write_matrix_to_file(matrix,"eta_f  total_cost    end_cost    run_cost ","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/cost_eta_opt.txt")

matrix=np.array([optimal_par_list,optimal_cost_list,optimal_end_cost_list,optimal_run_cost_list]).transpose()
mol.write_matrix_to_file(matrix,"eta   total_cost","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/cost_eta.txt")


# matrix=np.array([par_list,optimal_cost_list,optimal_end_cost_list,optimal_run_cost_list]).transpose()
# mol.write_matrix_to_file(matrix,"tf(ps)   total_cost    end_cost    run_cost","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/cost_tf.txt")


# matrix=np.array([[optimal_tf_list[0],optimal_cost_list[0],optimal_end_cost_list[0],optimal_run_cost_list[0]]])
# mol.write_matrix_to_file(matrix,"tf(ps)  total_cost    end_cost    run_cost ","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/cost_tf_opt.txt")


# matrix=np.array([par_list,optimal_tf_list]).transpose()
# mol.write_matrix_to_file(matrix,"eta0    tf(ps)","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/tf_eta_kappa_0.02.txt")
#
#
# matrix=np.array([par_list,optimal_tf_list]).transpose()
# mol.write_matrix_to_file(matrix,"eta0    tf(ps)","","/home/ar612/Templates/Latex/work_update_07.12.2022/data/tf_eta_kappa_0.005.txt")





################################################################%%
dat_0_file="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_10_wc_w10_e0_0.0005764776761753108_mu0_0.0025174068699653565_neta_11/eta_0.000/h11.txt"
dat_1_file="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_10_wc_w10_e0_0.0005764776761753108_mu0_0.0025174068699653565_neta_11/eta_0.015/h11.txt"
dat_2_file="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_10_wc_w10_e0_0.0005764776761753108_mu0_0.0025174068699653565_neta_11/eta_0.035/h11.txt"
dat_3_file="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_10_wc_w10_e0_0.0005764776761753108_mu0_0.0025174068699653565_neta_11/eta_0.050/h11.txt"



h0=np.genfromtxt(dat_0_file).diagonal()
h1=np.genfromtxt(dat_1_file).diagonal()
h2=np.genfromtxt(dat_2_file).diagonal()
h3=np.genfromtxt(dat_3_file).diagonal()

n=4
xmin_list=[i*1/n for i in range(n)]
xmax_list=[i*1/n+(1/2/n) for i in range(n)]




for i in range(10):
    plt.axhline(h0[i], xmin =xmin_list[0], xmax =xmax_list[0], color = 'tab:blue')
    plt.axhline(h1[i], xmin = xmin_list[1], xmax =xmax_list[1], color = 'tab:red')
    plt.axhline(h2[i], xmin =xmin_list[2], xmax = xmax_list[2], color = 'tab:orange')
    plt.axhline(h3[i], xmin = xmin_list[3], xmax = xmax_list[3], color = 'tab:purple')






#################################################################################%%
par_list=list(np.arange(0.00, 0.055, 0.005, dtype=float))

d11_list=[]

for par in par_list:
    d11_file="/home/ar612/Documents/Work/cavities/H_transfer/wc_w10_neta_11/N1_5_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/eta_"+"{:.3f}".format(par)+"/d11.txt"
    d11_list.append(np.genfromtxt(d11_file))

d11_list=np.array(d11_list)


imax=4

for i in range(imax):
    for j in range(i):
        plt.plot(par_list,d11_list[:,i,j],'o-',label="d"+str(i)+str(j))

plt.legend()




#######%%
var_list=["0.5","0.55","0.7","0.9","0.95","1.0","1.05"]
file_list=["tf","tf_ps","run_cost","end_cost","total_cost"]
#file_list=["tf"]

base_dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_molecular_1/"

marksize=20

for file in file_list:
    fig1, ax1 = plt.subplots(1, 1,figsize=(16,9))

    for var in var_list:
        dir="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_molecular_1/N1_5_wc_w10_var_"+var+"_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced/"+file+".txt"

        dat=np.genfromtxt(dir)

        ax1.plot(dat[1:,0],dat[1:,1],'o--',linewidth=marksize/4,markersize=marksize,label=var)
    ax1.legend(loc="upper right")
    ax1.set_xlim(-0.002,0.07)
    fig1.set_size_inches(16,9,forward=False)
    fig1.savefig(base_dir+file+".pdf")
    plt.close('all')



####Compute time########%%%
dir1="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_scan/excitation_molecular_1/N1_5_wc_w10_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced/eta0_0.030"
dir2="/home/ar612/Documents/Work/cavities/fixed_dipole/no_par_opt_eta_scan_tf_free/excitation_molecular_1/N1_5_wc_w10_var_1.0_e0_0.0005764776761753108_mu0_1.657_neta_11/random_intcost_0.0001_kappa_0.005_umax_8_trapezoidal_2100_reduced"
dir_list=[dir1,dir2]

for i in range(2):
    project_3,i_list_3,eta0_list_3,eta_list_3,cost_list_3,end_cost_list_3,run_cost_list_3,t0_list_3,tf_list_3,niter_list,comp_time_list=read_irep_data_from_folder(dir_list[i],"reduced.out")

    flat_list = [item/60 for sublist in comp_time_list for item in sublist if isinstance(item, float)]

    avg=sum(flat_list)/len(flat_list)


    plt.hist(flat_list, density=True , bins = 20, histtype='step', fill=False,label=str(i))


    # plt.plot([avg]*len(flat_list),flat_list,'o',label=str(i))
    plt.xlabel("t(h)")
    plt.legend()



#%%%
