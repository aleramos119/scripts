#%%
import sys
#sys.path.append('$PYDIR')

import mol
import numpy as np
from itertools import islice

def main():

    inp_file=sys.argv[1]
    #out_file_temp=sys.argv[2]+"_temp"
    out_file=sys.argv[2]

    period=int(sys.argv[3])

    mol_i=int(sys.argv[4])
    mol_f=int(sys.argv[5])

    step=int(sys.argv[6])



    step_i=int(sys.argv[7])

    step_f=int(sys.argv[8])


    mol.molecule_trajectory(inp_file,out_file,period,mol_i,mol_f,step,step_i,step_f)

#########################################################%%

if __name__== "__main__":
     main()
