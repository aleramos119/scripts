
import numpy as np
import sys
sys.path.append('$PYTHON_SCRIPTS')
import mol
from itertools import islice
%matplotlib qt
import matplotlib.pyplot as plt




# ### These are the position of the atoms from which I calculate the new coordinates vectors.
# ### These position are taken from the file r19.xyz
# pn=np.array([7.022079, 11.467340, 8.252784])
# pc1=np.array([7.938811, 12.208834, 8.041826])
# pc2=np.array([6.659386, 10.383466, 9.063626])
#d/1.40$ ^C
# ### Here I calculate the vectors conecting the three atoms
# dnc1=pc1-pn
# dnc2=pc2-pn
#
# ### Calculating the new coordinate vectors
# ez=dnc1/np.linalg.norm(dnc1)
# ez
#
# ex=np.cross(ez,dnc2)
# ex=ex/np.linalg.norm(ex)
# ex
#
# ey=np.cross(ez,ex)
# ey
#
# ### Chequing if they are perpendicular
# np.dot(ex,ey)


def main(inp_file_pattern,dist_list,i,f):
    
    for dist in dist_list[i-1:f]:

        inp_file=inp_file_pattern.replace("$$",str("%.2f" % dist))
        d=np.genfromtxt(inp_file,unpack= True, skip_header = 1,usecols=1)
        plt.hist(d,bins=400,range=(1.20,4),alpha=0.5)


if __name__== "__main__":

    dist_list=np.append(np.append(np.append(np.append(np.arange(1.36,2.01,0.04),2.5),3.),2.25),1.38)
    dist_list=np.sort(dist_list)

    #phi_ind=np.array([1,2,9,10,11,12,13,14,15,28,29,30,31,32])
    #chol_ind=np.array([0,3,4,5,6,7,8,16,17,18,19,20,21,22,23,24,25,26,27])

    inp_file_pattern="/cluster/data/ar612/cp2k_organized/umbrella/co_dist_k_6000_umbrella_reduced/$$/colvar"


    main(inp_file_pattern,dist_list,1,len(dist_list))
