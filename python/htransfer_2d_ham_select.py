#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 11:17:23 2022

@author: eric fischer
"""

from importlib import reload
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import os
import mol
import quantum as quant
%matplotlib qt
plt.style.use(['ggplot'])
import math
quant = reload(quant)

au_to_fs=2.41888e-2
au_to_eV=27.2
au_to_cm=219474.63068
ang_to_au = 1.8897261
au_to_debye = 1/0.393456


out_dir="/home/ar612/Documents/Work/cavities/H_transfer"
data_dir="/home/ar612/Documents/Work/cavities/H_transfer/data"

e0=0.0005764776761753108
mu0=1.657


states = 5 # selected nr of states
N1=states
N2=1



wc="w10_var_0.5"


orig_N1=31


#-----------Read Data/ Define Grids ------------------
eta_list = np.linspace(0.000,0.05,11)

neta=len(eta_list)

emission_list=[]

for eta_indx,eta in enumerate(eta_list):

    state_grid = np.arange(0, states)

    orig_sys="N1_"+str(orig_N1)+"_wc_"+wc+"_e0_"+str(e0)+"_mu0_"+str(mu0)+"_neta_"+str(neta)
    orig_final_dir=out_dir+"/wc_"+wc+"_neta_"+str(neta)+"/"+orig_sys+"/eta_"+"{:.3f}".format(eta)

    sys="N1_"+str(N1)+"_wc_"+wc+"_e0_"+str(e0)+"_mu0_"+str(mu0)+"_neta_"+str(neta)
    final_dir=out_dir+"/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/eta_"+"{:.3f}".format(eta)


    os.makedirs(final_dir,exist_ok='True')


    orig_eval_mat=np.genfromtxt(orig_final_dir+"/h11.txt")
    orig_dip_mat=np.genfromtxt(orig_final_dir+"/d11.txt")
    orig_emission_diagonal=np.genfromtxt(orig_final_dir+"/t11.txt")
    orig_molecular_1=np.genfromtxt(orig_final_dir+"/molecular_1.txt")



    # ##%%
    # decay_plot_dir=out_dir+"/plots/wc_"+wc+"_neta_"+str(neta)+"/"+sys+"/decay"
    # os.makedirs(decay_plot_dir,exist_ok='True')
    # plt.figure()
    # plt.plot(orig_emission_diagonal.diagonal()[:states],'o')
    # plt.ylim(-0.01,2.01)
    # plt.savefig(decay_plot_dir+'/decay_eta_'+"{:.3f}".format(eta)+".png", dpi=100)
    # plt.close()
    # ##%%

    emission_list.append(orig_emission_diagonal.diagonal())

    np_list=np.array(emission_list)

    #plt.plot(np_list[:,8],'--o')



    ground_state=np.zeros(2*N1)
    ground_state[0]=1

    state1=np.zeros(2*N1)
    state1[1]=1

    state2=np.zeros(2*N1)
    state2[2]=1

    mix_symetric=np.zeros(2*N1)
    mix_symetric[1]=1
    mix_symetric[2]=1

    mix_antisymetric=np.zeros(2*N1)
    mix_antisymetric[1]=1
    mix_antisymetric[2]=-1

    mol.write_matrix_to_file(orig_eval_mat[:N1,:N1],"","",final_dir+"/h11.txt")
    mol.write_matrix_to_file(orig_dip_mat[:N1,:N1],"","",final_dir+"/d11.txt")
    mol.write_matrix_to_file(orig_emission_diagonal[:N1,:N1],"","",final_dir+"/t11.txt")

    mol.write_matrix_to_file([ground_state],"","",final_dir+"/state_0.txt")
    mol.write_matrix_to_file([state1],"","",final_dir+"/state_1.txt")
    mol.write_matrix_to_file([state2],"","",final_dir+"/state_2.txt")
    mol.write_matrix_to_file([mix_symetric],"","",final_dir+"/symetric.txt")
    mol.write_matrix_to_file([mix_antisymetric],"","",final_dir+"/antisymetric.txt")

    mol.write_matrix_to_file([orig_molecular_1[:2*N1]],"","",final_dir+"/molecular_1.txt")




    mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/d12.txt")
    mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/d21.txt")
    mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/d22.txt")

    mol.write_matrix_to_file(np.zeros([N1,N1]),"","",final_dir+"/q11.txt")
    mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/q12.txt")
    mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/q21.txt")
    mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/q22.txt")

    mol.write_matrix_to_file(np.zeros([N1,N2]),"","",final_dir+"/p12.txt")
    mol.write_matrix_to_file(np.zeros([N2,N1]),"","",final_dir+"/p21.txt")

    mol.write_matrix_to_file(np.zeros([N2,N2]),"","",final_dir+"/h22.txt")
