
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol


def main(inp_file,x_label,cx,y_label,cy):

    cv=np.genfromtxt(inp_file,unpack= True, skip_header = 2)


    ######################################################################################################################%%

    t = (cv[0]-cv[0,0])*mol.fs2ps
    data1 = cv[1]*cx
    data2 = cv[2]*cy


    fig, ax1 = plt.subplots()
    color = 'red'
    ax1.set_xlabel('t (ps)',fontweight='bold')
    ax1.set_ylabel(x_label, color=color,fontweight='bold')
    ax1.plot(t, data1, color=color,linewidth=1.5)
    ax1.tick_params(axis='y', labelcolor=color,labelsize=14)


    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'blue'
    ax2.set_ylabel(y_label, color=color,fontweight='bold')  # we already handled the x-label with ax1
    ax2.plot(t, data2,color=color,linewidth=0.5)
    ax2.tick_params(axis='y', labelcolor=color,labelsize=14)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()








if __name__== "__main__":
    if(sys.argv[3]=="ang"):
        cx=180/np.pi
    if(sys.argv[3]=="dist"):
        cx=mol.au2A
    if(sys.argv[5]=="ang"):
        cy=180/np.pi
    if(sys.argv[5]=="dist"):
        cy=mol.au2A
    main(sys.argv[1],sys.argv[2],cx,sys.argv[4],cy)
