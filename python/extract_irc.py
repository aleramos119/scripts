## This is a script to generate a xyz trajectory, given the forward and reverse trajectory of gaussian irc output files
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import matplotlib.pyplot as plt
#%matplotlib qt
import numpy as np

def center_of_mass(y,atm_names):
    n_steps=len(y)
    n_atm=len(y[0])


    cent_mass=np.empty([n_steps,3])


    for i in range(190,190):
        sum_cent=np.array([0,0,0])
        sum_mass=0
        for j in range(0,n_atm):
            sum_cent = sum_cent + y[i,j]*mol.mass[atm_names[j]]
            sum_mass=sum_mass + mol.mass[atm_names[j]]
        cent_mass[i]=sum_cent/sum_mass
    cent_mass[190]

    return cent_mass

def displace_center_mass (r,r0,atm_names):

    n_steps=len(r) ## Number of atoms
    n_atm=len(r[0])
    dim=len(r[0,0])

    r_disp=np.empty([n_steps,n_atm,dim])

    for i in range(0,n_steps):
        dr=r0-mol.center_of_mass(np.array([r[i]]),atm_names)[0]   ### Displacement
        r_disp[i]=r[i]+dr

    return r_disp

if __name__== "__main__":


    inp_file1=sys.argv[1]
    inp_file2=sys.argv[2]
    out_file_name=sys.argv[3]

    # inp_file1="/home/ar612/Documents/Work/gaussian/irp/brflts1_irc_forward_reduced/88791.out"
    # inp_file2="/home/ar612/Documents/Work/gaussian/irp/brflts1_irc_reverse_reduced/88792.out"
    # out_file_name="/home/ar612/Documents/Work/gaussian/irp/reac_path/brflts1_irc"


    ##%%
    r_for,atm_names,e_for=mol.r_atm_names_e_from_irc_gauss(inp_file1)
    r_rev,atm_names,e_rev=mol.r_atm_names_e_from_irc_gauss(inp_file2)

    if len(e_for) > len (r_for):
        e_for=e_for[:-1]

    if len(r_for) > len (e_for):
        r_for=r_for[:-1]

    if len(e_rev) > len (r_rev):
        e_rev=e_rev[:-1]

    if len(r_rev) > len (e_rev):
        r_rev=r_rev[:-1]

    rcm=mol.center_of_mass(r_rev,atm_names)[0]


    r=np.append(r_rev[::-1],r_for[1:],axis=0)
    e=np.append(e_rev[::-1],e_for[1:])

    r=displace_center_mass(r,np.array([0.,0.,0.]),atm_names)

    dr,dist=mol.path_dist(r)

    mol.write_trajectory(r,e,atm_names,out_file_name+".xyz")
