#%%
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules/')
import mol
import numpy as np
from itertools import islice
from timeit import default_timer as timer
#import matplotlib.pyplot as plt

def form_dD(r,dr,atm_list):

    n_images=len(r)
    n_atm=len(atm_list)
    n_cons=(n_atm**2-n_atm)/2


    D=np.zeros([n_images,n_cons])
    dD=np.zeros([n_images-1,n_cons])



    for k in range(0,n_images):

        n=0
        for i,img_i in enumerate(atm_list):
            for j,img_j in enumerate(atm_list):
                if img_i < img_j:
                    D[k,n]=np.linalg.norm(r[k,i]-r[k,j])  ## Here I calculate the elements of the distance matrix

                    if k < n_images-1:
                        dD[k,n]=np.sum( (r[k,i]-r[k,j])*(dr[k,i]-dr[k,j]) ) / D[k,n] ## Here I calculate the elements of the variation of the distance matrix
                                                                                         ## as a function of the variations of the position vectors

                    n=n+1

    return D,dD

def dD_lag_mult_projection(dD,lag_mult):

    T=len(lag_mult) ## The number of time steps

    dH_dchi=np.empty([T]) ## In this array I storage the force along the reaction path for each time step

    ## Calculating the projection
    for j in range(0,T):
        dH_dchi[j]=np.dot(dD,lag_mult[j])

    return dH_dchi

def compute_free_ener(dH_along_path,std_along_path,n_images):

    free_ener=np.zeros([n_images])
    std_free_ener=np.zeros([n_images])


    sum=0
    free_ener[0]=0
    std_free_ener[0]=0
    for i in range(1,n_images):
        sum= sum + dH_along_path[i-1]
        free_ener[i]=sum
        std_free_ener[i]=np.sqrt(np.sum(std_along_path[0:i]**2))


    return free_ener,std_free_ener







#########################################################%%

if __name__== "__main__":


        cp2k_dir="/data/ar612/cp2k_organized"

	job="solv"

	name_append="_pmf"

        lag_inp_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!-1.LagrangeMultLog"
        reac_path_file="&&/reac_path/r20_ts_p_80_rep/r20_ts_p_80_rep_movie.xyz"
        free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_free_ener.txt"
        std_free_ener_out_file="&&/pmf/r20_ts_p_80_rep/**/r20_ts_p_80_rep_**!!_std_free_ener.txt"
        dh_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!__dH__$$.txt"
        dh_avg_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!__dH_avg__$$.txt"  ## File pattern to which I'm going to save <dH(i)_dchi> as a function of the time step
        dh_avg_std_file_pattern="&&/pmf/r20_ts_p_80_rep/**/$$_r20_ts_p_80_rep_**!!/$$_r20_ts_p_80_rep_**!!__dH_avg_std__$$.txt"


        lag_inp_file_pattern=lag_inp_file_pattern.replace("&&",cp2k_dir)
        reac_path_file=reac_path_file.replace("&&",cp2k_dir)
        free_ener_out_file=free_ener_out_file.replace("&&",cp2k_dir)
        std_free_ener_out_file=std_free_ener_out_file.replace("&&",cp2k_dir)
        dh_file_pattern=dh_file_pattern.replace("&&",cp2k_dir)
        dh_avg_file_pattern=dh_avg_file_pattern.replace("&&",cp2k_dir)
        dh_avg_std_file_pattern=dh_avg_std_file_pattern.replace("&&",cp2k_dir)

	lag_inp_file_pattern=lag_inp_file_pattern.replace("**",job)
        reac_path_file=reac_path_file.replace("**",job)
        free_ener_out_file=free_ener_out_file.replace("**",job)
        std_free_ener_out_file=std_free_ener_out_file.replace("**",job)
        dh_file_pattern=dh_file_pattern.replace("**",job)
        dh_avg_file_pattern=dh_avg_file_pattern.replace("**",job)
        dh_avg_std_file_pattern=dh_avg_std_file_pattern.replace("**",job)

	lag_inp_file_pattern=lag_inp_file_pattern.replace("!!",name_append)
        reac_path_file=reac_path_file.replace("!!",name_append)
        free_ener_out_file=free_ener_out_file.replace("!!",name_append)
        std_free_ener_out_file=std_free_ener_out_file.replace("!!",name_append)
        dh_file_pattern=dh_file_pattern.replace("!!",name_append)
        dh_avg_file_pattern=dh_avg_file_pattern.replace("!!",name_append)
        dh_avg_std_file_pattern=dh_avg_std_file_pattern.replace("!!",name_append)

        # Collecting data
        # lag_inp_file_pattern=sys.argv[1]  ## This is the pattern of the files from which I exctract the Lagrange multipliers
        # reac_path_file=sys.argv[2]  ## This is the reaction path files from which I ectract the structures of the reaction path
        # free_ener_out_file=sys.argv[3]  ## File to where I'm going to save the free energy profile A(chi). This is the integral of the last value of <dH(\$\$)_dchi> as a function of the time step
        # std_free_ener_out_file=sys.argv[4]  ## File to where I'm going to save the standard deviation of A(chi). Taking into account the standar deviation of <dH(\$\$)_dchi> and tha fact that I'm integrating all this values.
        # n_images=int(sys.argv[5])
        # n_atm=int(sys.argv[6])
        # dh_avg_file_pattern=sys.argv[7]  ## File pattern to which I'm going to save <dH(i)_dchi> as a function of the time step
        # dh_avg_std_file_pattern=sys.argv[8]  ## File pattern to which I'm going to save the standard deviation of <dH(\$\$)_dchi> as a function of the time step


        ## Here I read the geometries of the reaction path to calculate latter dr and dist
        e,r,atm_names=mol.e_r_atm_names_from_xyz (reac_path_file)

        im_list=np.array(range(30,44))-1 ##The list of images that I will use to compute free Energy
        atm_list=np.array([1,9,2,27,28])-1 ##List of atom that are going to use to construct D and dD

        ## Keping only frames and atoms that I'm interested in
        e=e[im_list]
        atm_names=atm_names[atm_list]
        r=r[im_list][:,atm_list]




        n_atm=len(atm_names)
        n_images=len(e)
        dr,dist=mol.path_dist(r)
        n_cons=(n_atm**2-n_atm)/2  ## Number of constraint in the simulation. All distances between atoms

        ## Here I calculate the distance matrix D and the variation dD for each point in the reaction path.
        D,dD=form_dD(r,dr,atm_list)


        ## Declaring the list that are going to store the average force along the reaction path (<dH/dchi>) and its standard deviation.
        dH_list=np.zeros([n_images-1])  ## Here I storage the average energy difference along the reaction path
        std_list=np.zeros([n_images-1]) ## And the corresponding standard deviation

        j=0
        ## Running through all images and computing averages
        for i in im_list[:-1]:

            # if i%2==0:
            #     image_name=str((i+2)/2) ## Name of the image
            #
            # if i%2!=0:
            #     image_name=str((i+2.)/2) ## Name of the image
            image_name=str(i+1)

            ## Creating names for Lagrange multipliers file, averages <dH(i)_dchi> and standard deviation
            lag_inp_file=lag_inp_file_pattern.replace("$$",image_name)
            dh_file=dh_file_pattern.replace("$$",image_name)
            dh_avg_file=dh_avg_file_pattern.replace("$$",image_name)
            dh_avg_std_file=dh_avg_std_file_pattern.replace("$$",image_name)


            ## Reading Lagrange multipliers from file for every step for image i
            ##########################%%
            #start = timer()
            lag_mult=mol.read_lag_mult(lag_inp_file,n_cons)
            lag_mult=lag_mult*mol.Eh2kcalmol/mol.au2A
            #end = timer()
            #print(end-start)
            #################%%

            ## Computing the force along reac_path for step i and every time step
            dH=-dD_lag_mult_projection(dD[j],lag_mult)

            # %matplotlib inline
            # plt.style.use(['ggplot'])
            # fig=plt.figure()
            # plt.plot(np.arange(0,0.0005*len(lag_mult),0.0005),dH_dchi*mol.Eh2kcalmol)
            # fig.savefig("/cluster/data/ar612/cp2k_organized/thermo_int/r19_ts_p_reduced/all_ener/$$_r19_ts_p_all_ener/".replace("$$",image_name)+"dH_dchi.pdf")
            #
            dH_avg_t=mol.comp_avg(dH)
            dH_avg_std_t=mol.comp_std(dH_avg_t)

            mol.write_matrix_to_file(dH,"","",dh_file) ## This file sorages the
            mol.write_matrix_to_file(dH_avg_t,"","",dh_avg_file)
            mol.write_matrix_to_file(dH_avg_std_t,"","",dh_avg_std_file)


            dH_list[j]=dH_avg_t[-1]
            std_list[j]=dH_avg_std_t[-1]

            j=j+1



        free_ener,std_free_ener=compute_free_ener(dH_list,std_list,n_images)

        mol.write_matrix_to_file(free_ener,"","",free_ener_out_file)
        mol.write_matrix_to_file(std_free_ener,"","",std_free_ener_out_file)
