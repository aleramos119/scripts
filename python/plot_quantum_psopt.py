
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['ggplot'])
%matplotlib qt

def get_xf(inp_file):
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'xf' in line and 'pxf' not in line:
               xf=float(line.split()[0])
    return xf


def plot_overlap(project_name,cross_pattern,oct_states_pattern,qdq_pattern,project_pattern,out_dir_pattern,final_out_dir,t0,tf,m0,mf,eig0,eigf,a,xf):

    t_list=np.array(range(t0,tf+1))*1000
    m_list=range(m0,mf+1)
    final_overlap=np.zeros([len(t_list),len(m_list)])
    err=np.zeros([len(t_list),len(m_list)])

    for it,t in enumerate(t_list):
        for jm,m in enumerate(m_list):
            inp_file_pattern=out_dir_pattern+"/"+project_pattern+"/"+cross_pattern
            out_file_pattern=out_dir_pattern+"/"+project_pattern+"_overlap.pdf"
            out_file=out_file_pattern.replace("$$",str(m)).replace("%%",str(t))

            qdq_file=qdq_pattern.replace("$$",str(m)).replace("%%",str(t))

            quant_x=np.genfromtxt(qdq_file)[-1,1]

            err[it,jm]=np.abs(xf-quant_x)/a


    a=2
    vmax=0.01
    mH=1837
    T_3_2=np.array([ (3/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_4_2=np.array([ (4/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_5_2=np.array([ (5/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_6_2=np.array([ (6/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])
    T_7_2=np.array([ (7/2)*2*np.pi*np.sqrt(i*mH*a**2/(8*vmax)) for i in m_list])



    ##%%
    lsize=40
    plt.rc('font', size=lsize)
    t=(np.append(t_list,t_list[-1]+1000)-500)
    m=np.append(m_list,m_list[-1]+1)-0.5
    fig=plt.figure(figsize=(16, 9))
    ax = fig.add_subplot(111)
    bar=ax.pcolormesh(t,m,err.T,vmin=0,vmax=2,alpha=0.9,cmap='coolwarm')
    # ax.plot(T_3_2[2:],m_list[2:],color="tab:green",linewidth=5)
    # ax.plot(T_4_2[1:],m_list[1:],color="tab:red",linewidth=5)
    # ax.plot(T_5_2,m_list,color="tab:green",linewidth=5)
    # ax.plot(T_6_2,m_list,color="tab:red",linewidth=5)
    # ax.plot(T_7_2[:-1],m_list[:-1],color="tab:green",linewidth=5)
    fig.colorbar(bar)
    ax.set_xlabel(r'$t_f ~(au)$',size=lsize)
    #ax.set_xlabel('T_2',fontweight='bold')
    ax.set_ylabel(r'$m ~(m_H)$',size=lsize)
    ax.tick_params(labelsize=lsize)
    fig.savefig(final_out_dir+project_name+".pdf", bbox_inches='tight')
    ##%%

if __name__== "__main__":

    #quatics_dir="/home/ar612/Documents/quantics"
    ##%%
    quatics_dir=os.environ['QUANTICS_HOME']
    oct_dir=os.environ['OCTGAUSS_HOME']
    quant_psopt_dir=oct_dir+"/../quantum_psopt"

    cross_pattern="cross_eig_&&"
    project_pattern="$$_emax"
    time_pattern="%%T"

    project_name="linear_ma97"
    xf=1.932447 ## Xf for 1_hmass
    subdir="/control/double_well/"+project_name+"/"


    final_out_dir=quatics_dir+subdir

    out_dir_pattern=quatics_dir+subdir+time_pattern+"/"+project_pattern
    oct_states_pattern=quant_psopt_dir+subdir+time_pattern+"/"+project_pattern+"/x_expect.dat"
    xf_file_pattern=oct_dir+subdir+time_pattern+"/"+project_pattern+"/"+project_pattern+".inp"
    qdq_pattern=out_dir_pattern+"/"+project_pattern+"/qdq_1_1.pl"

    a=2

    t0=5
    tf=8

    m0=12
    mf=18

    eig0=1
    eigf=40
    ##%%
    plot_overlap(project_name,cross_pattern,oct_states_pattern,qdq_pattern,project_pattern,out_dir_pattern,final_out_dir,t0,tf,m0,mf,eig0,eigf,a,xf)
