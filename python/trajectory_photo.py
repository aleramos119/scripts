#%%
import sys
sys.path.append('$PYDIR')

import mol
import numpy as np
from itertools import islice





def main():

    inp_file=sys.argv[1]
    out_file=sys.argv[2]
    frame=int(sys.argv[3])-1  ##### Number of the frame I want to extract


    initial_index=int(sys.argv[4])-1  #### Initial index of the molecule I want to export
    final_index=int(sys.argv[5])-1  ##### Final index of the molecule I want to export


    # inp_file="/home/ar612/Documents/cp2k_organized/reac_path/r20_ts_p_band_reduced/r20_ts_p_band_movie.xyz"
    # out_file=sys.argv[2]
    # frame=5
    # n_images=20
    # n_atm=33
    #
    # initial_index=1-1  #### Initial index of the molecule I want to export
    # final_index=1482-1  ##### Final index of the molecule I want to export

    e,r,atm_names=mol.e_r_atm_names_from_xyz (inp_file)
    n_images=len(r)
    n_atm=len(r[0])
    if frame < n_images and initial_index < n_atm-1 and final_index <= n_atm-1:
        mol.xyz_file (r[frame,initial_index:final_index+1],atm_names[initial_index:final_index+1],out_file)
    else:
        print ("Something is out of range !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")


#########################################################%%

if __name__== "__main__":
     main()
