
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
%matplotlib qt


color_list=["yellow","red","green","blue","white","magenta","orange"]



class rectangle:
    def __init__(self,point_list):

        self.vertices=[point_list[0],point_list[1],[point_list[0][0],point_list[1][1]],[point_list[1][0],point_list[0][1]]]

        self.dx=np.abs(self.vertices[0][0]-self.vertices[1][0])
        self.dy=np.abs(self.vertices[0][1]-self.vertices[1][1])

        self.a=0
        self.b=0
        self.max="x"

        # print(self.dx,self.dy,self.a,self.b,self.max)


        ### Sets self.a as the maximum value
        if self.dx >= self.dy:

            self.a=self.dx
            self.b=self.dy
            self.max="x"
            # print(self.dx,self.dy,self.a,self.b,self.max)


        if self.dy > self.dx:
            self.a=self.dy
            self.b=self.dx
            self.max="y"
            # print(self.dx,self.dy,self.a,self.b,self.max)



        self.area=self.dx*self.dy

def find_max_index(rect_list):

    imax=0
    max_area=0

    for i,rect in enumerate(rect_list):
        if rect.area > max_area:
            imax=i
            max_area=rect.area

    return imax



##%%
n=4  ### Number of divisions. You will get n+1 rectangles
DX=100
DY=150
rect0=rectangle([[0,0],[DX,DY]]) ### Initial rectangle
div=0.9  ### Maximum allowed division in %

rect_list=[rect0]

for i in range(n):

    ## Finds the index of the largest area rectangle
    imax=find_max_index(rect_list)
    max_rect=rect_list[imax]

    ## Computes the displacement of the new division
    disp=int(np.random.uniform((1-div)*max_rect.a,(div)*max_rect.a,1)[0])

    print(max_rect.dx,max_rect.dy,max_rect.a,max_rect.b,max_rect.max)

    if max_rect.max=="x":
        new_point=min(max_rect.vertices[0][0],max_rect.vertices[1][0])+disp

        new_rect1=rectangle([[max_rect.vertices[0][0],max_rect.vertices[0][1]],[new_point,max_rect.vertices[1][1]]])
        new_rect2=rectangle([[new_point,max_rect.vertices[0][1]],[max_rect.vertices[1][0],max_rect.vertices[1][1]]])



    if max_rect.max=="y":
        new_point=min(max_rect.vertices[0][1],max_rect.vertices[1][1])+disp

        new_rect1=rectangle([[max_rect.vertices[0][0],max_rect.vertices[0][1]],[max_rect.vertices[1][0],new_point]])
        new_rect2=rectangle([[max_rect.vertices[0][0],new_point],[max_rect.vertices[1][0],max_rect.vertices[1][1]]])


    del rect_list[imax]

    rect_list.append(new_rect1)
    rect_list.append(new_rect2)


to_file=np.array([[rect.dx,rect.dy,rect.vertices[0][0],rect.vertices[0][1]] for rect in rect_list])
np.savetxt("/home/ar612/rectangles_n_"+str(n+1)+"_DX_"+str(DX)+"_DY_"+str(DY)+".txt", to_file,header="# dx   dy   x0   y0",delimiter='  ',fmt="%10d")



fig = plt.figure()
ax = fig.add_subplot(111)

for i in range(len(to_file)):
    rect_plot=matplotlib.patches.Rectangle((to_file[i,2], to_file[i,3]),to_file[i,0], to_file[i,1],edgecolor="black",facecolor = color_list[i%len(color_list)])
    ax.add_patch(rect_plot)


ax.set_aspect('equal')

plt.xlim([0, DX])
plt.ylim([0, DY])
plt.show()
plt.savefig("/home/ar612/rectangles_n_"+str(n+1)+"_DX_"+str(DX)+"_DY_"+str(DY)+".pdf")
plt.close()

#%%
