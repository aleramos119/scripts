
#%%
import numpy as np
import sys
sys.path.append('$PYDIR')
import mol
from scipy import interpolate


def even_space(inp_file_1,inp_file_2,out_file,n):

    t,efield=mol.merge(inp_file_1,inp_file_2,out_file).T

    efield_int=interpolate.interp1d(t, efield, kind='quadratic',bounds_error=False,fill_value="extrapolate")

    T=t[-1]-t[0]
    dt=T/n

    t_even=np.linspace(t[0]-2*dt,t[-1]+2*dt,n+4)

    e_even=np.array([efield_int(t) for t in t_even])

    m0=(efield[2]-efield[1])/(t[2]-t[1])
    mf=(efield[-2]-efield[-3])/(t[-2]-t[-3])

    e_even[0]=efield[0]+m0*(t_even[0]-t[0])
    e_even[1]=efield[0]+m0*(t_even[1]-t[0])

    e_even[-1]=efield[-1]+mf*(t_even[-1]-t[-1])
    e_even[-2]=efield[-1]+mf*(t_even[-2]-t[-1])

    file_even=np.column_stack((t_even,e_even))

    mol.write_matrix_to_file(file_even,"","",out_file)




if __name__== "__main__":

    # inp_file_1="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/t.dat"
    # inp_file_2="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/u.dat"
    # out_file="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/efield.txt"



    inp_file_1=sys.argv[1]
    inp_file_2=sys.argv[2]
    out_file=sys.argv[3]

    n=int(sys.argv[4])

    #n=500


    even_space(inp_file_1,inp_file_2,out_file,n)
