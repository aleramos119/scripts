
#%%
import numpy as np
#%matplotlib qt
import matplotlib.pyplot as plt
import sys
import mol

def main(inp_file,job):
    ##################################################################################################################%%
    #time,spath,zpath,sigma_spath,sigma_zpath,height,biasf=np.genfromtxt(inp_file,unpack= True, skip_header = 1)
    ht,hc1,hc2,hc3,hs1,hs2,hs3,hh,hb=np.genfromtxt(inp_file,unpack= True, skip_header = 1)

    # if (len(ht) > 100000):
    #     ht[100000:]=ht[100000:]+50000

    hc1_c2=[np.sqrt(hc1[i]**2+hc2[i]**2)  for i in range(0,len(hc1))]

    if(job=="colvar"):
        plt.style.use(['ggplot'])
        plt.errorbar(hc1_c2, hc3, xerr=hs1/2, yerr=hs2/2,fmt='o',color="tab:red",markersize=3)
        plt.xlabel("$hc_1^2 + hc_2^2$",fontweight='bold')
        plt.ylabel("colvar_3",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.show()
    elif(job=="time"):
        plt.style.use(['ggplot'])
        fig=plt.figure(figsize=(10,10))
        ax=plt.subplot(221)
        plt.errorbar(hc1_c2,hc3, xerr=hs1/2, yerr=hs3/2,fmt='o',color="tab:red",markersize=3)
        plt.plot([1.66],[0.14],'o',color="tab:blue",markersize=10)
        plt.xlabel("$hc_1^2 + hc_2^2$",fontweight='bold')
        plt.ylabel("colvar_3",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)


        ax=plt.subplot(222)
        plt.errorbar(hc1, hc2, xerr=hs1/2, yerr=hs2/2,fmt='o',color="tab:red",markersize=3)
        plt.xlabel("colvar_1",fontweight='bold')
        plt.ylabel("colvar_2",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)


        ax=plt.subplot(223)
        plt.errorbar(ht, hc3,yerr=hs3/2,fmt='o',color="tab:blue",markersize=3)
        plt.xlabel("t (ps)",fontweight='bold')
        plt.ylabel("colvar_3",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)

        ax=plt.subplot(224)
        plt.plot(ht,hh,'o',color="tab:blue",markersize=3)
        plt.xlabel("t (ps)",fontweight='bold')
        plt.ylabel("height (kcal/mol)",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.show()
    elif(job=="fes"):
        xmin=np.min(hc1)-2*hs1[0]
        xmax=np.max(hc1)+2*hs1[0]
        ymin=np.min(hc2)-2*hs2[0]
        ymax=np.max(hc2)+2*hs2[0]

        x, y = np.mgrid[xmin:xmax:100j,ymin:ymax:100j]
        z=np.zeros([100,100])

        for i in range(0,len(hc1)):
            z=z+mol.gauss(x,y,hc1[i],hs1[i],hc2[i],hs2[i],hh[i])
        z=-z
        zmin=np.min(z)
        z=z-zmin
        #%%
        plt.style.use(['ggplot'])
        fig=plt.figure(figsize=(10,10))
        ax=fig.add_subplot(111)
        plt.contourf(x,y,z,100,cmap='rainbow_r',alpha=.55)
        cbar=plt.colorbar()
        cbar.ax.set_title('kcal/mol')
        C=plt.contour(x,y,z,15, colors='black', linewidths=0.6)
        plt.clabel(C, inline=1, fontsize=10)
        plt.xlabel("colvar_1",fontweight='bold')
        plt.ylabel("colvar_2",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        #ax.set_aspect('equal')
        plt.show()
        #%%

if __name__== "__main__":

    # inp_file="/home/ar612/Documents/cp2k_organized/well_temp/dco_x_dco_y_dnh_ho_gamma_40_well_temp_reduced/HILLS"
    # job="time"


    inp_file=sys.argv[1]
    job=sys.argv[2]

    main(inp_file,job)


mol.k*mol.J2kcalmol*298
