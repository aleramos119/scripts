#! /bin/python3

#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import quaternion
#import matplotlib.pyplot as plt
#%matplotlib qt


def read_dihedral_atoms(dihedral_atoms_file,job):

    with open(dihedral_atoms_file) as file:
       for n, line in enumerate(file, 1):
           if job+"_path_step" in line:
               step=int(line.split()[-1])
           if job+"_angle" in line:
               a0=float(line.split()[-4])
               af=float(line.split()[-3])
               na=int(line.split()[-2])
               angle=np.linspace(a0,af,na,endpoint=False)
               if "deg" in line:
                   angle=np.radians(angle)
           if job+"_r0" in line:
               r0=[float(i) for i in line.split()[-3:]]
           if job+"_dihedral" in line:
               dihedral=[int(i) for i in line.split()[-4:]]
           if job+"_atoms" in line:
               atoms=[int(i) for i in line.split()[1:]]
    return step,angle,r0,dihedral,atoms








############################################################################%%

def main (reaction_path_inp_file,dihedral_atoms_file,job):

    ##%%
    step,angle_list,r0,dih_atms,atoms=read_dihedral_atoms(dihedral_atoms_file,job)
    n_angle=len(angle_list)


    e_irp,rirp,atm_names=mol.e_r_atm_names_from_xyz(reaction_path_inp_file)
    n_path=len(rirp)

    cent_coord=mol.center_of_coord(rirp[0])
    dr = r0 - cent_coord
    ## Reducing the path
    e_irp=np.array([ e for i,e in enumerate(e_irp) if i%step==0 or i==n_path])
    rirp=np.array([ r for i,r in enumerate(rirp) if i%step==0 or i==n_path])
    n_path=len(rirp)
    n_atm=len(rirp[0])


    dih=np.array([ mol.dihedral(rirp[i,dih_atms[0]],rirp[i,dih_atms[1]],rirp[i,dih_atms[2]],rirp[i,dih_atms[3]]) for i in range(len(rirp))])



    r=np.zeros([n_angle*n_path,n_atm,3])
    e=np.zeros(n_angle*n_path)


    for k,angle in enumerate(angle_list):

        dih0=angle
        ddih=dih-dih0

        for i in range(n_path):
            axis=rirp[i,dih_atms[1]]-rirp[i,dih_atms[2]]
            p=rirp[i,dih_atms[1]]
            q = quaternion.Quaternion.from_axisangle(ddih[i], axis, p)

            for j in range(n_atm):
                e[k*n_path+i]=e_irp[i]
                if j in atoms:
                    r[k*n_path+i,j] = q*rirp[i,j]+dr
                else:
                    r[k*n_path+i,j] = rirp[i,j]+dr

    #dih2=np.array([ mol.dihedral(r[i,dih_atms[0]],r[i,dih_atms[1]],r[i,dih_atms[2]],r[i,dih_atms[3]]) for i in range(len(r))])



    out_file='.'.join(reaction_path_inp_file.split(".")[:-1])+"_nirc_"+str(n_path)+"_nangle_"+str(n_angle)+"_"+job+".xyz"
    out_file
    mol.write_trajectory(r,e,atm_names,out_file)




###########################################################################%%


if __name__== "__main__":

    ##%%

    #reaction_path_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/trajectory/aptts1_mass_weight_nx_-2_2_90_ny_-1.5_2_90.xyz"
    #job="a"
    #dihedral_atoms_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/dihedral_atoms"



    reaction_path_inp_file=sys.argv[1]
    job=sys.argv[2]
    dihedral_atoms_file="dihedral_atoms"


    main(reaction_path_inp_file,dihedral_atoms_file,job)
