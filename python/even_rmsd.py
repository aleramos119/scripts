
#%%
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt
import os
import sys
from scipy import interpolate
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import rmsd
%matplotlib qt
plt.style.use(['ggplot'])





def spline(r_orig,e,imp_atms=[0,1,2,14,15,33,34,52,53],N=100):

    nframes=len(r_orig)
    natm=len(r_orig[0])
    dim=len(r_orig[0,0])

    r_irp=mol.remove_trans_rot(r_orig,r_orig[0],imp_atms)


    weights=np.zeros([natm,3])
    weights[imp_atms]=np.array([1,1,1])
    r_red=np.array([np.multiply(weights,r) for r in r_irp])


    path_rmsd=np.insert(np.array([rmsd.rmsd(r_red[i+1],r_red[i]) for i in range(nframes-1)]),0,0,axis=0)
    path_sum=np.cumsum(path_rmsd)

    xnew=np.arange(path_sum[0], path_sum[-1],(path_sum[-1]-path_sum[0])/N)

    f=interpolate.CubicSpline(path_sum, e)

    e_int=f(xnew)

    r_int=np.zeros([N,natm,dim])

    for i in range(0,natm):
        for j in range(0,dim):

            y=r_irp[::,i,j]
            f= interpolate.CubicSpline(path_sum, y)

            r_int[::,i,j]=f(xnew)


    return e_int,r_int


def SZ(r,r_irp,l):
    partition=np.sum([np.exp(-l*rmsd.rmsd(r,xi)**2) for i,xi in enumerate(r_irp) ])
    sss=np.sum([i*np.exp(-l*rmsd.rmsd(r,xi)**2) for i,xi in enumerate(r_irp) ])/partition
    zzz=(-1/l)*np.log(partition)
    return sss,zzz,partition


##%%%
#reaction_path_inp_file="/home/ar612/Documents/Work/cp2k_organized/reac_path/pcmreac1dftb_pcmts1dftb_pcmprod1dftb_band_40rep_dftb_reduced/pcmreac1dftb_pcmts1dftb_pcmprod1dftb_band_40rep_dftb_movie.xyz"
#imp_atms=[0,1,2,14,15]
reaction_path_inp_file="/home/ar612/Documents/Work/cp2k_organized/reac_path/pctreac1dftb_pctts1dftb_pctprod1dftb_band_40rep_dftb_reduced/pctreac1_complete_path.xyz"
imp_atms=[0,1,2,14,15,33,34,52,53]
e_irp,r_irp,atm_names=mol.e_r_atm_names_from_xyz(reaction_path_inp_file)

r_red=r_irp[:,imp_atms]
atm_names_red=atm_names[imp_atms]
r_rot=mol.remove_trans_rot(r_red,r_red[0],range(len(imp_atms)))
enew,rnew=spline(r_rot,e_irp,range(len(imp_atms)),N=80)
mol.write_trajectory(rnew,enew,atm_names_red,"/home/ar612/Documents/Work/cp2k_organized/xyz/reac_path/pctreac1_complete_red_path.xyz")




nframes=len(r_irp)
natm=len(atm_names)
weights=np.zeros([natm,3])
weights[imp_atms]=np.array([1,1,1])
r_red=np.array([np.multiply(weights,r) for r in rnew])


path_rmsd=np.array([rmsd.rmsd(r_red[i+1],r_red[i]) for i in range(nframes-1)])
path_sum=np.cumsum(path_rmsd)

plt.plot(dist1)

mat=np.zeros([nframes,nframes])
for i in range(nframes):
    for j in range(nframes):
        mat[i,j]=rmsd.rmsd(rnew[i],rnew[j])**2

plt.matshow(mat)





sd=[rmsd.rmsd(rnew[i+1],rnew[i])  for i in range(nframes-1)]
msd=[rmsd.rmsd(rnew[i+1],rnew[i])**2  for i in range(nframes-1)]
s=np.sum(msd)

plt.plot(msd)

lamda=2.3*(nframes-1)/s
lamda


SZ(r_irp[3],r_irp,1)


rmsd.rmsd(r_irp[2],r_irp[-1])**2

##%%
l=1
plt.plot([SZ(r,r_irp,l)[0] for r in r_irp],label="sss")
plt.plot([SZ(r,r_irp,l)[1] for r in r_irp],label="zzz")
plt.plot([SZ(r,r_irp,l)[2] for r in r_irp],label="partition")
plt.legend()
##%%

for l in [2000]:
    dat=np.genfromtxt("/home/ar612/Documents/git_kraken/plumed2/colvar_"+str(l))
    plt.plot(dat[:,0],dat[:,1],"o-",label="plumed_lambda="+str(l))
    plt.xlabel("frame_index")
    plt.ylabel("sss")
    plt.legend()



for l in [2000]:
    zzz=np.array([ SZ(r,r_irp,l)[0] for r in r_irp])
    plt.plot(zzz+1,"o-",label="python_lambda="+str(l))
    plt.xlabel("frame_index")
    plt.ylabel("sss")
    plt.legend()
