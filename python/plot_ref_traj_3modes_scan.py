
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
%matplotlib qt
plt.style.use(['ggplot'])
from matplotlib.widgets import Slider
############################################################################%%


def main (ref_traj_inp_file,m1,m2,n,saturation):

    ##%%
    ## Reading cartesian normal modes and calculating mass_weighted normal modes
    e,r,atm_names=mol.e_r_atm_names_from_xyz (ref_traj_inp_file)
    w,modes,r0,atm_names=mol.w_modes_r_atm_names_from_molden(normal_modes_inp_file)

    e=e*mol.Eh2kcalmol
    emin=min(e)
    e=e-emin


    for i,x in enumerate(e):
        if x > sat:
            e[i]=sat


    x=np.linspace(x_min, x_max, num=nx)
    y=np.linspace(y_min, y_max, num=ny)
    z=np.linspace(z_min, z_max, num=nz)




    e_x_y_z=np.asarray([np.split(mat,ny) for mat in np.split(e,nx)])

    # r=[[xi,yi,zi] for xi in x for yi in y for zi in z ]
    #
    # mat_e=[[ri[0],ri[1],ri[2],e[i]] for i,ri in enumerate(r)]
    #
    #
    # mol.write_matrix_to_file(mat_e,"","","/home/ar612/Documents/cp2k_organized/ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_10_10_eps_10e-6_ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_10_10_eps_10e-6_ref_traj_ener.txt")

    ##%%
    i=1
    l=20
    fig=plt.figure(figsize=(10,10))
    fig.suptitle("w_"+str(m1)+": "+str(x[i]), fontsize=10)
    ax=plt.subplot(111)
    plt.axis([y_min, y_max, z_min,z_max])
    plt.contourf(y,z,e_x_y_z[i],l,cmap='rainbow',alpha=.55,vmin=0, vmax=sat)
    cbar=plt.colorbar()
    cbar.ax.set_title('kcal/mol')
    C=ax.contour(y,z,e_x_y_z[i],l, colors='black', linewidths=0.6)
    ax.clabel(C, inline=1, fontsize=10)
    plt.xlabel("w_"+str(m2)+": " +str(w[m2]),fontweight='bold')
    plt.ylabel("w_"+str(m3)+": " +str(w[m3]),fontweight='bold')
    plt.grid(True)
    plt.tick_params(labelsize=14)

    ## Set slide axes
    axx_val = plt.axes([0.25, .02, 0.50, 0.02])
    # Slider
    sx_val = Slider(axx_val, "w_"+str(m1)+": " +str(w[m1]), 0, nx-1, valinit=i,valstep=1)
    ##%%
    def update(val):
        # amp is the current value of the slider
        i = int(sx_val.val)

        plt.figure(fig.number)
        plt.subplot(111)
        plt.cla()
        plt.axis([y_min, y_max, z_min,z_max])
        fig.suptitle("w_"+str(m1)+": "+str(x[i]), fontsize=10)
        #plt.figure(fig.number)
        ax.contourf(y,z,e_x_y_z[i],l,cmap='rainbow',alpha=.55,vmin=0, vmax=sat)
        # cbar=plt.colorbar()
        # cbar.ax.set_title('kcal/mol')
        C=ax.contour(y,z,e_x_y_z[i], l, colors='black', linewidths=0.6)
        ax.clabel(C, inline=1, fontsize=10)
        plt.xlabel("w_"+str(m2)+": " +str(w[m2]),fontweight='bold')
        plt.ylabel("w_"+str(m3)+": " +str(w[m3]),fontweight='bold')
        plt.grid(True)
        plt.tick_params(labelsize=14)

        fig.canvas.draw_idle()
    # call update function on slider value change

    sx_val.on_changed(update)

    plt.show()

    ##%%
    # # Plot scatter with mayavi
    # xi, yi, zi = np.mgrid[x_min:x_max:nx*1j, y_min:y_max:ny*1j, z_min:z_max:nz*1j]
    #
    # ##%%
    # figure = mlab.figure('DensityPlot')
    # grid = mlab.pipeline.scalar_field(xi, yi, zi, e_x_y_z)
    # min = density.min()
    # max=density.max()
    # mlab.pipeline.volume(grid, vmin=min, vmax=max)
    #
    # np.mgrid[-1:1]
    #
    # mlab.axes()
    # mlab.show()
    # ##%%
















###########################################################################%%


if __name__== "__main__":

    ##%%
    ref_traj_inp_file="/home/ar612/Documents/Work/cp2k_organized/ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_15_15_eps_10e-5_ref_traj/38_r20_ts_p_80_rep_vib_7_35_92_n_15_15_15_eps_10e-5_ref_traj-pos-1.xyz"
    normal_modes_inp_file="/home/ar612/Documents/Work/cp2k_organized/vibrational_analysis/38_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis/38_r20_ts_p_80_rep_movie_b3lyp_vibrational_analysis-VIBRATIONS-1.mol"

    m1=7
    m2=35
    m3=92

    x_max=2.5
    x_min=-1
    nx=15

    y_max=1
    y_min=-1
    ny=15

    z_max=1
    z_min=-0.5
    nz=15

    sat=150
    ##%%

    main(modes_inp_file,m1,m2,n,sat)
