
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol



############################################################################%%


def main (modes_inp_file,m1,m2,disp_max):


    ## Reading cartesian normal modes and calculating mass_weighted normal modes
    w,int,modes,r0,atm_names=mol.w_modes_r_atm_names_from_molden (modes_inp_file)
    modes=mol.normal(modes)

    modes_mass=mol.cartesian2mass_weighted(modes,atm_names)
    #modes_mass=modes


    n_atm=len(r0)
    n_vib=len(w)



    ## Taking the important normal modes
    mode1=modes[m1]
    mode2=modes[m2]
    mode3=modes[m3]

    mode1_mass=modes_mass[m1]
    mode2_mass=modes_mass[m2]
    mode3_mass=modes_mass[m3]


    ##%%
    ## Maximum displacement for every direction
    x_max=2.5
    x_min=-1
    nx=15

    y_max=1
    y_min=-1
    ny=15

    z_max=1
    z_min=-0.5
    nz=15

    x_list=np.linspace(x_min, x_max, num=nx)
    y_list=np.linspace(y_min, y_max, num=ny)
    z_list=np.linspace(z_min, z_max, num=nz)

    ## The first extructure needed for append to work
    r=np.zeros([1,n_atm,3])



    ## Escaning through the displacements in both directions
    for dx1 in x_list:
        for dx2 in y_list:
            #for dx3 in z_list:
            r=np.append(r,[r0+dx1*mode1+dx2*mode2],axis=0)

    ## Removing the first dummy structure
    r=r[1:]

    ## Naming the final file
    out_file=out_pattern.replace("$$",str(m1)+"_"+str(m2)).replace("&&",str(nx)+"_"+str(ny))
    out_file

    ## Writing the trajectory to the file
    mol.write_trajectory (r,np.zeros(len(r)),atm_names,out_file)



    ##%%




















###########################################################################%%


if __name__== "__main__":


    modes_inp_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/conf00-normal-modes.mldf"
    out_pattern="/home/ar612/Documents/Work/cp2k_organized/ref_traj/trajectory/0pctts1irc_vib_$$_n_&&.xyz"
    m1=137
    m2=211
    m3=92




    main(modes_inp_file,m1,m2,disp_max)
