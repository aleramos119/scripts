##%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib qt
plt.style.use(['ggplot'])
from scipy.integrate import simps
##%%


##%%
def V_double(x,vmax,a):
    return vmax*((x/a)**2-1)**2

def V_harm(x,vmax,a):
    k=8*vmax*a**-2
    return (0.5*k)*x**2

def w(vmax,a,m,conv=1):
    return np.sqrt(8*vmax/(a**2*m))*conv

def mu_lin(q,x):
    return q*x

def mu_quad(q,x):
    return q*x**2

##%%


def main(project_name,Emax):

    eigval_file=project_name+"_eigenenergies_Eh.txt"
    Hdiag_file=project_name+"_Hdiag.txt"
    eigvec_file=project_name+"_eigenstates_a0.txt"
    mu_lin_file=project_name+"_muij_qx.txt"
    mu_quad_file=project_name+"_muij_qx2.txt"
    ortho_file=project_name+"_ortho.txt"

    Em=np.genfromtxt(eigval_file,dtype=np.float128)
    Hdiag=np.diag(Em[:Emax])


    col=mol.file_col(eigvec_file)
    x=mol.fast_load(eigvec_file,[0])[:,0]
    m=mol.fast_load(eigvec_file,range(1,col)).T

    ##Normalizing the vector because fgh does not return the vector normilized for the integral
    for i in range(len(m)):
        m[i]=m[i]/np.sqrt(simps(m[i]*m[i],x))




    q=1
    ortho=np.empty([Emax,Emax])
    muij_lin=np.empty([Emax,Emax])
    muij_quad=np.empty([Emax,Emax])


    for i in range(Emax):
        for j in range(Emax):
            ortho[i,j]=simps(m[i]*m[j],x)
            muij_lin[i,j]=simps(mu_lin(q,x)*m[i]*m[j],x)
            muij_quad[i,j]=simps(mu_quad(q,x)*m[i]*m[j],x)


    mol.write_matrix_to_file(ortho,"","",ortho_file)
    mol.write_matrix_to_file(muij_lin,"","",mu_lin_file)
    mol.write_matrix_to_file(muij_quad,"","",mu_quad_file)
    mol.write_matrix_to_file(Hdiag,"","",Hdiag_file)

    #plt.matshow(muij_lin)
    #muij_lin[1,1]
##%%

if __name__== "__main__":

    ##%%
    project_name=sys.argv[1]
    # project_name="/home/ar612/Documents/Work/quantics/control/double_well/eigen_states/1_hmass/1_hmass"
    # Emax=30

    Emax=int(sys.argv[2])


    ##%%


    main(project_name,Emax)
