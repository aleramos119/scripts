## This is a script to generate a xyz trajectory, given the forward and reverse trajectory of gaussian irc output files
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np

###########################################################################%%


if __name__== "__main__":

    ## Reading the forward and reverse trajectory of gaussian irc output files
    inp_file="/home/ar612/Documents/Work/gaussian/ts_opt/pctts1_reduced/pctts1_233545.out"
    out_file="/home/ar612/Documents/Work/gaussian/ts_opt/pctts1_reduced/pctts1.xyz"
    job="opt"
    ##%%
    job=sys.argv[1]
    inp_file=sys.argv[2]
    out_file=sys.argv[3]



    if job == "opt":
        r,atm_names,e=mol.r_atm_names_e_from_opt_gauss(inp_file,"Input orientation:")
    if job == "irc":
        r,atm_names,e=mol.r_atm_names_e_from_irc_gauss(inp_file)

    r

    if len(e) > len (r):
        e=e[:-1]

    if len(r) > len (e):
        r=r[:-1]


    ##%%

    mol.write_trajectory(r,e,atm_names,out_file)
