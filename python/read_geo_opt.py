##%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np


inp_file="/home/ar612/Documents/Work/cp2k_organized/geo_opt/pcmts1dftb_dftb_geo_opt_reduced/pcmts1dftb_dftb_geo_opt-pos-1.xyz"
out_file="/home/ar612/Documents/git_kraken/plumed2/red_path_center.xyz"
ch_ener="yes"
r0=[0,0,0]


def main(inp_file,out_file,r0,ch_ener):

    e,r,atm_names=mol.e_r_atm_names_from_xyz(inp_file)

    r_disp=mol.displace_center_coord(np.array([r[-1]]),r0)

    my_dict=dict(zip(map(tuple,np.round(r_disp[0],5)),atm_names))

    r_disp,atm_names=np.array(list(map(np.array,my_dict.keys()))),list(my_dict.values())

    mol.write_trajectory(np.array([r_disp]),np.array([e[-1]]),atm_names,out_file)
#%%



#########################################################%%
#sys_list=["p", "p2", "p3", "r1", "r2", "r3_t", "r4_t", "r5_t", "r6_t", "r7_t", "r8_t", "r9_t", "r10_t", "r11_t", "r12_t", "r13", "r14", "r15"]

#sys_list=["rred13", "rred19", "rred20", "rred3", "pred"]

#sys_list=["1phenyl6cyclo"]
sys_list=["pctreac3dftb","pctprod3dftb"]
ch_ener="yes"
for sys in sys_list:

    inp_file="/home/ar612/Documents/Work/cp2k_organized/geo_opt/$$_b3lyp_geo_opt_reduced/$$_b3lyp_geo_opt-pos-1.xyz"
    #inp_file="/home/ar612/Downloads/cp2k/xyz/$$.xyz"
    #inp_file="/home/ar612/Documents/Work/cp2k_organized/geo_opt/$$_dftb_geo_opt_reduced/$$-pos-1.xyz"
    #inp_file=os.environ['CP2K_HOME']+"/XYZ/$$.xyz"
    #inp_file=os.environ['CP2K_HOME']+"/../gaussian/ts_opt/$$_reduced/$$.xyz"
    #inp_file=os.environ['CP2K_HOME']+"/XYZ/$$.xyz"

    #out_file="/home/ar612/Documents/git_kraken/plumed2/red_path_center.xyz"
    out_file="/home/ar612/Documents/Work/cp2k_organized/xyz/$$_b3lyp.xyz"

    pattern="i ="


    r0=[12.5,12.5,12.5] ## Where I want the center of coordinates to be


    inp_file=inp_file.replace("$$",sys)

    out_file=out_file.replace("$$",sys)

    main(inp_file,out_file,r0,ch_ener)
