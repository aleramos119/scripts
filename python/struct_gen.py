
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol


def mass_weight (y,n_vib,atm_names,n_atm):
    for i in range(0,n_vib):
        for j in range(0,n_atm):
            y[i,j]=y[i,j]*np.sqrt(mol.mass[atm_names[j]])

    return y

def normal (y,n_vib):
    for i in range(0,n_vib):
        y[i]=y[i]/np.linalg.norm(y[i].flatten())
    return y



def projection_matrix (y,n_vib):
    y_ij=np.empty([n_vib,n_vib])

    for i in range(0,n_vib):
        for j in range(0,n_vib):
            y_ij[i,j]=np.dot(y[i].flatten(),y[j].flatten())


    return y_ij




###################################################################%%
def projections(dr,n_images,y,n_vib):
    ## Creating the Matrix where I store the projection of the normal modes for each reaction path step
    dy=np.empty([n_images,n_vib])

    ## Calculating the projections. The displacement dr are normalize
    for i in range(0,n_images):
        for j in range(0,n_vib):
            dy[i,j]=np.dot(dr[i].flatten(),y[j].flatten())

    return dy




def struct_gen(r0,c,y):

    n=len(r0)
    n_vib=len(y)
    dr=np.zeros([n,3])

    for i in range(0,n_vib):
        dr=dr+c[i]*y[i]

    r_gen=r0+dr
    return r_gen

############################################################################%%


def main (xyz_inp_file,n_atm,n_images,ts_image,modes_inp_file,exc_out_file):

    n_vib=3*n_atm-6

    ## Reading energies and reaction path from movie file. e is in Hartrees and r in armstrong
    e,r,atm_names=mol.e_r_atm_names_from_xyz(xyz_inp_file,n_images,n_atm)

    ## Calculating the displacementes of each image of the reaction path with respect to the first image. The first displacement will be cero


    f,y=mol.f_y_from_molden(modes_inp_file,n_vib,n_atm)
    y=mass_weight(y,n_vib,atm_names,n_atm)  ### Mass weight normal modes
    y=normal(y,n_vib)  ### Normalize normal modes
    ### y_ij=projection_matrix(y,n_vib)  ### Calculate projections


    dr=r-r[0]

    ## Calculating projections
    dy=projections(dr,n_images,y,n_vib)


    r_gen=struct_gen(r[0],dy[ts_image],y)


    mol.xyz_file(r_gen,atm_names,exc_out_file)
















        #print (y_ij)









###########################################################################%%


if __name__== "__main__":


    n_images=20

    n_atm=33


    xyz_inp_file=sys.argv[1]
    ts_image=sys.argv[2]    ##The number of the transition state image. I only calculate the projection of the reaction path into the normal mode until this image

    project=xyz_inp_file.split('_')
    project=project[:-2]

    if(project[1]=='solvated'):
        vibra_mol='_'.join(project[:1])
    else:
        vibra_mol=project[0]

    vibra_folder="/media/ar612/Ale_HDD/Trabajo/cp2k/script_testing/gas_phase/vibrational_analysis/dft/b3lyp/"
    modes_inp_file=vibra_folder+"$$_b3lyp_dft_vibrational_analysis/$$_b3lyp_dft_vibrational_analysis-VIBRATIONS-4.mol"
    modes_inp_file=modes_inp_file.replace("$$",vibra_mol)

    exc_out_folder="/home/ar612/Documents/cp2k_organized/excited/XYZ/"
    exc_out_file=exc_out_folder+vibra_mol+"_e$$.xyz"
    exc_out_file=exc_out_file.replace("$$",ts_image)


    #xyz_inp_file="/media/ar612/Ale_HDD/Trabajo/cp2k/reac_path/r13_ts_p_band/r13_ts_p_band_movie.xyz"
    #modes_inp_file="/media/ar612/Ale_HDD/Trabajo/cp2k/script_testing/gas_phase/vibrational_analysis/dft/b3lyp/r13_b3lyp_dft_vibrational_analysis/r13_b3lyp_dft_vibrational_analysis-VIBRATIONS-4.mol"
    #exc_out_file="/home/ar612/Documents/cp2k_organized/excited/XYZ/r13_e1.xyz"
    #ts_image=4

    ts_image=int(ts_image)
    #print(xyz_inp_file,project,n_atm,n_images,ts_image,modes_inp_file,vibra_analysis)
    main(xyz_inp_file,n_atm,n_images,ts_image,modes_inp_file,exc_out_file)
