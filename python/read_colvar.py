#! /bin/python3

#%%
import numpy as np
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
import matplotlib.pyplot as plt
from statistics import mean
%matplotlib qt




def G(t,W,sig):
    if np.abs(t)<=W/2:
        return 1
    if np.abs(t)>W/2:
        return np.exp(-((np.abs(t)-W/2)**2)/(2*sig**2))



def power_time(t_e0,u_au,wmax,tmax,W=100,sig=20,n_t=500):

    f=2*np.pi/(t_e0[1]-t_e0[0])

    spectrans,freqtrans,plottrans=plt.magnitude_spectrum(u_au,f,pad_to=5000)

    plt.close()


    for i,w in enumerate(freqtrans):
        if w > wmax:
            n_w=i
            break;


    spect_w_t=np.zeros([n_t*n_w,3])

    tstride=len(t_e0)//n_t


    i=0
    for t0 in t_e0[:tmax:tstride]:
        utrans=np.array( [u_au[k]*G(t1-t0,W,sig) for k,t1 in enumerate(t_e0)])
        spectrans,freqtrans,plottrans=plt.magnitude_spectrum(utrans,f,pad_to=10000)
        plt.close()
        for j in range(n_w):
             spect_w_t[i,0]=freqtrans[j]
             spect_w_t[i,1]=t0
             spect_w_t[i,2]=spectrans[j]
             i=i+1

    return spect_w_t



######################################################################################%%
pctreac1_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctreac1_1_thf_196_300K_30A_qmmm_reduced/colvar")
pctdreac1_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctdreac1_1_thf_196_300K_30A_qmmm_reduced/colvar")
pctddreac1_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctddreac1_1_thf_196_300K_30A_qmmm_reduced/colvar")


pctreac1_oh_dist_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctreac1_oh_dist_1.05_1_thf_196_300K_30A_qmmm_reduced/colvar")
pctdreac1_oh_dist_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctdreac1_oh_dist_1.05_1_thf_196_300K_30A_qmmm_reduced/colvar")
pctddreac1_oh_dist_colvar=np.genfromtxt("/home/ar612/Documents/Work/cp2k_organized/equilibration/qmmm_2/pctddreac1_oh_dist_1.05_1_thf_196_300K_30A_qmmm_reduced/colvar")


plt.plot((pctreac1_oh_dist_colvar[:,1]-mean(pctreac1_oh_dist_colvar[:,1]))**2)
plt.plot((pctdreac1_oh_dist_colvar[:,1]-mean(pctdreac1_oh_dist_colvar[:,1]))**2)
plt.plot((pctddreac1_oh_dist_colvar[:,1]-mean(pctddreac1_oh_dist_colvar[:,1]))**2)



spect_w_t=power_time(pctreac1_oh_dist_colvar[:,0],pctreac1_oh_dist_colvar[:,1]-mean(pctreac1_oh_dist_colvar[:,1]),wmax=1,tmax=1000,W=100,sig=20,n_t=500)


nplotx=1
nploty=1
n_level=6

fig, ax = plt.subplots(nplotx, nploty,sharex=True,sharey=False,figsize=(16*nploty,9*nplotx),gridspec_kw={'hspace': 0.15,'wspace': 0.2})
ax.tricontourf(spect_w_t[:,1],spect_w_t[:,0],spect_w_t[:,2],n_level,cmap='rainbow',alpha=.55)##,vmax=0.03337,vmin=0)
ax.set_xlabel(xlabel_list[1])
ax.set_ylabel(ylabel_list[1])
ax.set_ylim(0,4)
ax.set_xlim(t_ps[0],t_ps[-1])
ax.grid(True)
ax.ticklabel_format(axis="y",style='sci', scilimits=(0,0))
ax.set_title("("+abc[1]+")")


x=np.linspace(pctreac1_oh_dist_colvar[0,0],pctreac1_oh_dist_colvar[-1,0],100)
y=np.array([G(t1-pctreac1_oh_dist_colvar[-1,0]/2,W=100,sig=20) for k,t1 in enumerate(x)])

ax.plot(x,4*y)
ax.plot(pctreac1_oh_dist_colvar[:,0],(pctreac1_oh_dist_colvar[:,1]-mean(pctreac1_oh_dist_colvar[:,1])))




spectrans,freqtrans,plottrans=plt.magnitude_spectrum((pctreac1_oh_dist_colvar[:,1]-mean(pctreac1_oh_dist_colvar[:,1])),2*np.pi/(pctreac1_oh_dist_colvar[1,0]-pctreac1_oh_dist_colvar[0,0]),pad_to=5000)
