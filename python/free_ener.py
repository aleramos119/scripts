
#%%
import numpy as np
import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol
from scipy import interpolate




def plot_comparisson(dist,e_gas,e_solv,e_solv_error,e_solv_min,project,n_images):
    plt.plot(dist,e_gas,'b-o',linewidth=2,label="reaction path in gas phase")
    plt.errorbar(dist, e_solv, yerr=e_solv_error,linewidth=2,fmt='r-o',capsize=3,label="same reaction path with solvent (averaged)")
    plt.plot(dist,e_solv_min,'g-o',linewidth=2,label="same reaction path with solvent (minimum)")
    plt.xlabel(project+"(A)",fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.ylabel('E (kcal/mol)',fontweight='bold')
    plt.grid(True)
    plt.style.use(['ggplot'])
    plt.legend(loc=3,ncol=1)
    plt.savefig(project+"_comp.pdf", bbox_inches='tight')
    plt.show()




############################################################################%%


def main (xyz_inp_file,interpolation_file,xyz_inp_file_solv,solv_error_file,solv_min_file,important_modes_file,project,n_atm,n_images,ts_image,modes_inp_file,job):

    n_vib=3*n_atm-6


    ## Reading energies and reaction path from movie file. e is in Hartrees and r in armstrong
    e_gas,r,atm_names=mol.e_r_atm_names_from_xyz(xyz_inp_file,n_images,n_atm)




    ## Expresing energies in kcal/mol and using the first image as reference
    e_gas_kcalmol=(e_gas-e_gas[0])*mol.Eh2kcalmol





    ## Calculating the displacementes of each image of the reaction path with respect to the first image. The first displacement will be cero
    dr=r-r[0]

    dist=path_dist(dr,n_images)


    ## plot simply the reac_path energy in gas phase
    if(job=='ener'):
        plot_e(dist,e_gas_kcalmol,project,n_images)
        e_int,r_int=spline(r,e_gas)
        mol.write_trajectory(r_int,e_int,atm_names,interpolation_file)

    ## comparisson of reac_path energy in gas and solvated phase with error bars
    if(job=='comp'):
        e_solv,r_solv,atm_names_solv=mol.e_r_atm_names_from_xyz(xyz_inp_file_solv,n_images,n_atm)

        e_solv_min,r_solv_min,atm_names_solv_min=mol.e_r_atm_names_from_xyz(solv_min_file,n_images,n_atm)

        free_ener=np.genfromtxt("/home/ar612/Documents/cp2k_organized/reac_path/r19_ts_p_band_reduced/r19_ts_p_free_ener.txt")
        e_solv_error=np.genfromtxt("/home/ar612/Documents/cp2k_organized/reac_path/r19_ts_p_band_reduced/r19_ts_p_std_free_ener.txt")

        plt.errorbar(free_ener, yerr=e_solv_error,linewidth=2,fmt='r-o',capsize=3,label="same reaction path with solvent (averaged)")

        e_solv_min_kcalmol=(e_solv_min-e_solv[0])*mol.Eh2kcalmol
        e_solv_kcalmol=(e_solv-e_solv[0])*mol.Eh2kcalmol



        e_solv_error_kcalmol=(e_solv_error)*mol.Eh2kcalmol


        plot_comparisson(dist,e_gas_kcalmol,e_solv_kcalmol,e_solv_error_kcalmol,e_solv_min_kcalmol,project,n_images)

    ## do vibrational analisis
    if(job=='vibra'):
        f,y=mol.f_y_from_molden(modes_inp_file,n_vib,n_atm)
        y=mass_weight(y,n_vib,atm_names,n_atm)  ### Mass weight normal modes
        y=normal(y,n_vib)  ### Normalize normal modes
        ##y_ij=projection_matrix(y,n_vib)  ### Calculate projections



        ## Calculating projections
        dy=projections(dr,n_images,y,n_vib)


        vib=np.genfromtxt(important_modes_file,dtype=int)-1



        plot_dy(dist,e_gas_kcalmol,dy[1:ts_image+1],project,n_images,n_vib,vib,ts_image)








        #print (y_ij)









###########################################################################%%


if __name__== "__main__":


    n_images=20
    ts_image=20  ##The number of the transition state image. I only calculate the projection of the reaction path into the normal mode until this image
    n_atm=33


    xyz_inp_file=sys.argv[1]
    job=sys.argv[2]


    # xyz_inp_file="r18_ts_p_band_movie.xyz"
    # job="vibra"



    project=xyz_inp_file.split('_')
    project=project[:-2]


    if(project[1]=='t'):
        vibra_mol='_'.join(project[:2])
    if(project[1]!='t'):
        vibra_mol=project[0]


    reac_path='_'.join(project)


    modes_inp_file="/home/ar612/Documents/cp2k_organized/vibrational_analysis/gas_phase/dft/b3lyp/$$_b3lyp_dft_vibrational_analysis_reduced/$$_b3lyp_dft_vibrational_analysis-VIBRATIONS-1.mol"
    modes_inp_file=modes_inp_file.replace("$$",vibra_mol)

    xyz_inp_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_band_movie.xyz"
    xyz_inp_file=xyz_inp_file.replace("$$",reac_path)

    interpolation_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_band_movie_int.xyz"
    interpolation_file=interpolation_file.replace("$$",reac_path)

    xyz_inp_file_solv="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_band_solv_movie.xyz"
    xyz_inp_file_solv=xyz_inp_file_solv.replace("$$",reac_path)

    solv_error_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_band_solv_movie_std.txt"
    solv_error_file=solv_error_file.replace("$$",reac_path)

    solv_min_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_band_solv_movie_min.xyz"
    solv_min_file=solv_min_file.replace("$$",reac_path)

    important_modes_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/$$_important_modes.txt"
    important_modes_file=important_modes_file.replace("$$",reac_path)

    print(reac_path,xyz_inp_file,interpolation_file,project,n_atm,n_images,ts_image,modes_inp_file)




    # modes_inp_file="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_b3lyp_dft_vibrational_analysis-VIBRATIONS-1.mol"
    # xyz_inp_file="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_ts_p_band_movie.xyz"
    # xyz_inp_file_solv="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_ts_p_band_movie_solv.xyz"
    # solv_error_file="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_ts_p_band_movie_std.txt"
    # solv_min_file="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_ts_p_band_movie_min.xyz"
    # important_modes_file="/home/ar612/Documents/cp2k_organized/reac_path/$$_band_reduced/r18_ts_p_important_modes.txt"
    # interpolation_file="/home/ar612/Documents/cp2k_organized/reac_path/r18_ts_p_band_reduced/r18_ts_p_band_movie_int.xyz"
    #
    # reac_path="r18_ts_p"
    #
    # project="r18_ts_p"
    #
    # job="ener"


    main(xyz_inp_file,interpolation_file,xyz_inp_file_solv,solv_error_file,solv_min_file,important_modes_file,reac_path,n_atm,n_images,ts_image,modes_inp_file,job)
