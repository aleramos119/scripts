#! /bin/python3
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol
from itertools import islice



def merge(inp_file_1,inp_file_2,out_file):
    ##################################################################################################################%%
    len1=mol.file_len(inp_file_1)
    col1=mol.file_col(inp_file_1)

    len2=mol.file_len(inp_file_2)
    col2=mol.file_col(inp_file_2)

    lenf=len1
    colf=col1+col2


    file_1=mol.fast_load(inp_file_1,np.array(range(0,col1)),1000)
    file_2=mol.fast_load(inp_file_2,np.array(range(0,col2)),1000)



    file_f=np.column_stack((file_1,file_2))

    mol.write_matrix_to_file(file_f,"","",out_file)

    return file_f







if __name__== "__main__":


    inp_file_1=sys.argv[1]
    inp_file_2=sys.argv[2]
    out_file=sys.argv[3]

    merge(inp_file_1,inp_file_2,out_file)
