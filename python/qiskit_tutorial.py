
##%%
import numpy as np
import qiskit
from qiskit import(IBMQ,QuantumCircuit,execute,Aer)
from qiskit.visualization import plot_histogram
##%%

qiskit.__qiskit_version__
##%%
from qiskit.providers.aer import AerSimulator
sim = AerSimulator()

# Use Aer's qasm_simulator
simulator = Aer.get_backend('qasm_simulator')

# Create a Quantum Circuit acting on the q register
qc = QuantumCircuit(2, 2)

qc.x(0)
qc.x(1)
qc.cx(0,1)
qc.measure([0,1],[0,1])


qc.draw()


test_qc = QuantumCircuit(4, 2)

# First, our circuit should encode an input (here '11')
test_qc.x(0)
test_qc.x(1)

# Next, it should carry out the adder circuit we created
test_qc.cx(0,2)
test_qc.cx(1,2)
test_qc.ccx(0,1,3)

# Finally, we will measure the bottom two qubits to extract the output
test_qc.measure(2,0)
test_qc.measure(3,1)
test_qc.draw()

job=sim.run(test_qc)
result=job.result()

result.get_counts()

## To run in IBMQ
from qiskit.tools.monitor import job_monitor
IBMQ.save_account('89906eb25d9d73641a3bed22a82a42312cd134d1b9335e0736d312b29400dc2c895855ad590e19ef5e69b1e2ed6c2dde334bc1ab70b999afbaaff2dc7703d9f3',overwrite=True)
provider = IBMQ.load_account()

device = provider.get_backend('ibmq_valencia')
job = execute(circuit, backend = device)

job_monitor(job)


job.

result = job.result()

counts= result.get_counts(circuit)

print (counts)


plot_histogram(counts)
