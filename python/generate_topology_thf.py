
#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np
from itertools import islice

#%%

def mass_list(atm_names_sys):
    return np.array([ mol.mass[atm] for atm in atm_names_sys])
def xyz_name2g96_name(xyz_name):

    g96_names=[]
    atom_types=[]
    atom_count=[]

    for name in xyz_name:
        if name in atom_types:
            index=atom_types.index(name)
            atom_count[index]+=1
            g96_names.append(name+str(atom_count[index]))
        else:
            atom_types.append(name)
            atom_count.append(1)
            g96_names.append(name+str(1))

    return np.array(g96_names)
def g96_name2xyz_name(g96_name):
    return np.array([mol.remove_digits(name) for name in g96_name])
def types_list(xyz_name,select):

    atom_types_list=[]
    for name in xyz_name:
        if name == "O" and select=="thf":
            atom_types_list.append(1)
        if name == "C":
            atom_types_list.append(2)
        if name == "H":
            atom_types_list.append(3)
        if name == "O" and select=="sys":
            atom_types_list.append(4)
        if name == "N":
            atom_types_list.append(5)

    return np.array(atom_types_list)
def read_topology(thf_top):

    nmat=-10
    cgm_line=0
    n_atm=0
    with open(thf_top) as file:
       for n, line in enumerate(file, 1):
           if 'NMAT' in line:
               nmat=n
           if n==nmat+1:
               n_atm=int(line.split()[0])
               cg=np.zeros(n_atm)
               ine=np.zeros(n_atm)
               icg=np.zeros(n_atm)
               jne=[]
           if 'CGM' in line:
               cgm_line=n
           if n>cgm_line and n<=cgm_line+n_atm:
               i=n-cgm_line-1
               split=line.split()

               cg[i]=float(split[4])
               icg[i]=int(split[5])
               ine[i]=int(split[6])
               jne.append(np.array([int(k) for k in split[7:]]))

    return cg,icg,ine,jne
def read_info_packmol(packmol_inp_file):
    nthf0=-1
    with open(packmol_inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'output' in line:
               packmol_out=line.split()[-1]
           if 'structure' in line and 'thf' in line:
               nthf0=n
           if n==nthf0+1:
               n_thf=int(line.split()[-1])
    return n_thf,packmol_out

def write_title(output_file):
    with open(output_file, 'w') as f:
        f.write("########################################################\n")
        f.write("######################  TITLE  #########################\n")
        f.write("########################################################\n")
        f.write("TITLE\n")
        f.write("This is the topology file "+output_file+" with THF solvent in  G96 format\n")
        f.write("END\n")
def write_topphyscon(output_file):
    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("######################  TOPPHYSCON  ####################\n")
        f.write("########################################################\n")
        f.write("TOPPHYSCON\n")
        f.write("# FPEPSI: 1.0/(4.0*PI*EPS0) (EPS0 is the permitivity of vacuum)\n")
        f.write("0.1389354E+03\n")
        f.write("# HBAR: Planck's constant HBAR = H/(2* PI)\n")
        f.write("0.6350780E-01\n")
        f.write("END\n")
def write_topversion(output_file):
    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   TOPVERSION    #####################\n")
        f.write("########################################################\n")
        f.write("TOPVERSION\n")
        f.write("1.7\n")
        f.write("END\n")
def write_atomtypename(output_file):

    lj_atom_type=["OA", "C", "HC", "O", "NR"]
    NRATT=len(lj_atom_type)

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   ATOMTYPENAME    ###################\n")
        f.write("########################################################\n")
        f.write("ATOMTYPENAME\n")
        f.write("#Van de Waals atom type sequence and name block\n")
        f.write("#NRATT number of van der Waals atom types\n")
        f.write(str(NRATT)+"\n\n")
        for type in lj_atom_type:
            f.write(type+"\n")
        f.write("END\n")

    return lj_atom_type
def write_ljparmeters(output_file):

    c6_sqrt=np.array([0.04756, 0.04838, 0.00920, 0.04756, 0.04936])
    c12_sqrt=np.array([1.1000e-03, 2.2220e-03, 1.2300e-04, 1.0000e-03, 1.5230e-03])
    cs6_sqrt=np.array([0.04756, 0.04838, 0.00920, 0.04756, 0.04936])
    cs12_sqrt=np.array([1.1250e-03, 1.8370e-03, 1.2300e-04, 8.6110e-04, 1.3010e-03])

    NRATT=len(c6_sqrt)
    NRATT2=NRATT*(NRATT+1)//2

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("##################   LJPARAMETERS    ###################\n")
        f.write("########################################################\n")
        f.write("LJPARAMETERS\n")
        f.write("#Van de Waals (Lennard-Jones) interaction block\n")
        f.write("# NRATT2= NRATT*(NRATT+1)/2 number of LJ interaction types\n")
        f.write(str(NRATT2)+"\n")

        f.write("#It's assumed that i<j and that j appears in ascending order\n")
        f.write("#IAC JAC     C12              C6            CS12          CS6\n")

        for j in range(NRATT):
            for i in range(j+1):
                #f.write(str(i+1)+"  "+str(j+1)+"  "+ str(c12_sqrt[i]*c12_sqrt[j])+"  "+ str(c6_sqrt[i]*c6_sqrt[j])+"  "+ str(cs12_sqrt[i]*cs12_sqrt[j])+"  "+ str(cs6_sqrt[i]*cs6_sqrt[j])+"\n")
                f.write("  %i   %i   %.6E   %.6E   %.6E   %.6E \n"%(i+1,j+1,c12_sqrt[i]*c12_sqrt[j],c6_sqrt[i]*c6_sqrt[j],cs12_sqrt[i]*cs12_sqrt[j],cs6_sqrt[i]*cs6_sqrt[j]))
        f.write("END\n")
def write_resname(output_file,n_atm_sys):

    NRAA2=n_atm_sys+1

    residue_name=[ "SYS"+str(i+1) for i in range (0,n_atm_sys)]
    residue_name.append("THF")

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   RESNAME    #####################\n")
        f.write("########################################################\n")
        f.write("RESNAME\n")
        f.write("#Solute residue sequence and name block\n")
        f.write("#NRAA2 number of residues\n")
        f.write(str(NRAA2)+"\n")
        f.write("#AANM: residue names\n")
        for residue in residue_name:
            f.write(residue+"\n")
        f.write("END\n")

    return residue_name
def write_soluteatom(output_file,lj_atm_type,atm_names_sys,n_sys,g96_atm_names_thf,n_thf,thf_top):

    n_atm_sys=len(atm_names_sys)
    n_atm_thf=len(g96_atm_names_thf)

    NRP_total=n_thf*n_atm_thf+n_sys*n_atm_sys

    panm_sys=xyz_name2g96_name(atm_names_sys)
    iac_sys=types_list(atm_names_sys,"sys")
    mass_sys=mass_list(atm_names_sys)
    cg_sys = np.zeros(n_atm_sys)
    icg_sys = np.ones(n_atm_sys)
    ine_sys = np.zeros(n_atm_sys)
    ine14_sys = np.zeros(n_atm_sys)
    jne_sys = [np.array([]) for i in range(0,n_atm_sys)]
    jne14_sys = [np.array([]) for i in range(0,n_atm_sys)]


    panm_thf=g96_atm_names_thf
    iac_thf=types_list(g96_name2xyz_name(g96_atm_names_thf),"thf")
    mass_thf=mass_list(g96_name2xyz_name(g96_atm_names_thf))
    cg_thf,icg_thf,ine_thf,jne_thf=read_topology(thf_top)


    ine14_thf = np.zeros(n_atm_sys)
    jne14_thf = [np.array([]) for i in range(0,n_atm_sys)]

    final_residue_index=[]#np.zeros(n_sys*n_atm_sys + n_thf*n_atm_thf)
    final_atom_name=[]


    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("###################  SOLUTEATOM    #####################\n")
        f.write("########################################################\n")
        f.write("SOLUTEATOM\n")
        f.write("#Solute atom information block\n")
        f.write("#Number of atoms\n")
        f.write(str(NRP_total)+"\n")
        f.write("# ATNM: atom number  MRES: residue number  PANM: atom name of solute atom\n")
        f.write("# IAC: integer (van der Waals) atom type code  MASS: mass of solute atom\n")
        f.write("#CG: charge of solute atom  IGC: charge group code (0 or 1)\n")
        f.write("#INE: number of excluded atoms for non-bonded interactions\n")
        f.write("#JNE: indexes of atoms excluded from non-bonded interactions for atom i (i<j and j in ascending order)\n")
        f.write("# JNE14: indexes of atoms included for 1-4 LJ interactions for atom i (i<j and j in ascending order)\n")
        f.write("#ATNM MRES  PANM    IAC      MASS         CG      IGC       INE      JNE         INE14   JNE14\n")

        for i in range(0,n_sys):
            for j in range(0,n_atm_sys):
                jne_atm = jne_sys[j]+i*n_atm_sys
                jne14_atm = jne14_sys[j]+i*n_atm_sys
                atm=i*n_atm_sys+j+1

                final_residue_index.append(atm)
                final_atom_name.append(panm_sys[j])
                f.write(" %i    %i      %s      %s      %s      %s       %i       %i     %s\n" % (atm,atm,panm_sys[j],iac_sys[j],mass_sys[j],cg_sys[j],icg_sys[j],ine_sys[j],' '.join([str(k) for k in jne_atm])))
                f.write("                                                                                  %i       %s  \n" % (ine14_sys[j],' '.join([str(k) for k in jne14_atm])))



        for i in range(0,n_thf):
            for j in range(0,n_atm_thf):
                jne_atm = jne_thf[j]+i*n_atm_thf+n_sys*n_atm_sys
                jne14_atm = jne14_thf[j]+i*n_atm_thf+n_sys*n_atm_sys
                atm=i*n_atm_thf+j+n_sys*n_atm_sys+1
                final_residue_index.append(n_atm_sys+1)
                final_atom_name.append(panm_thf[j])
                f.write(" %i    %i      %s      %s      %s      %s       %i       %i     %s\n" % (atm,n_atm_sys+1,panm_thf[j],iac_thf[j],mass_thf[j],cg_thf[j],icg_thf[j],ine_thf[j],' '.join([str(k) for k in jne_atm])))
                f.write("                                                                                  %i       %s  \n" % (ine14_thf[j],' '.join([str(k) for k in jne14_atm])))


        f.write("END\n")


    return final_residue_index,final_atom_name

def write_bondtype(output_file):

    NBTY=4

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("#####################   BONDTYPE   #####################\n")
        f.write("########################################################\n")
        f.write("BONDTYPE\n")
        f.write("#Bond interaction type block\n")
        f.write("# NBTY: number of covalent bond types\n")
        f.write(str(NBTY)+"\n")
        f.write("# CB: force constant  B0: Bond lenght\n")
        f.write("#     CB            B0\n")
        f.write("# H1-C1\n")
        f.write("1.2300000e+07	0.109000\n")
        f.write("# C1-C2\n")
        f.write("3.0818540e+06	0.156000\n")
        f.write("# C1-C4\n")
        f.write("4.0057345e+06	0.154000\n")
        f.write("# C4-O1\n")
        f.write("8.1800000e+06	0.143000\n")
        f.write("END\n")
def write_bondh(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys):

        h_bond_pairs=[[1,2],[2,3],[4,5],[4,6],[8,9],[8,10],[11,12],[11,13]]
        h_bond_pairs=[np.array(i) for i in h_bond_pairs]
        hbond_type=np.ones(len(h_bond_pairs))

        NBONH=len(hbond_type)

        NBONH_total=NBONH*n_thf


        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BONDH   #######################\n")
            f.write("########################################################\n")
            f.write("BONDH\n")
            f.write("#Solute bonds involving H atoms\n")
            f.write("# NBONH: number of bonds involving H atoms in solute\n")
            f.write(str(NBONH_total)+"\n")
            f.write("#  IBH, JBH: atom sequence numbers of atoms forming a bond ICBH: bond type code\n")
            f.write("#IBH JBH    ICBH\n")
            for i in range (0,n_thf):
                for j in range(0,NBONH):
                    bond = h_bond_pairs[j]+i*n_atm_thf+n_sys*n_atm_sys
                    f.write(" %i    %i        %i \n" % (bond[0],bond[1],hbond_type[j]))
            f.write("END\n")
def write_bond(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys):

        bond_pairs=[[2,4],[4,7],[7,8],[8,11],[2,11]]
        bond_pairs=[np.array(i) for i in bond_pairs]
        bond_type=np.array([3,4,4,3,2])

        NBON=len(bond_type)

        NBON_total=NBON*n_thf


        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BOND   ########################\n")
            f.write("########################################################\n")
            f.write("BOND\n")
            f.write("#Solute bonds NOT involving H atoms\n")
            f.write("# NBON: number of bonds involving H atoms in solute\n")
            f.write(str(NBON_total)+"\n")
            f.write("#  IBH, JBH: atom sequence numbers of atoms forming a bond  ICBH: bond type code\n")
            f.write("#IBH JBH    ICBH\n")
            for i in range (0,n_thf):
                for j in range(0,NBON):
                    bond = bond_pairs[j]+i*n_atm_thf+n_sys*n_atm_sys
                    f.write(" %i    %i        %i \n" % (bond[0],bond[1],bond_type[j]))
            f.write("END\n")

def write_bondangletype(output_file):

    NTTY=8

    angle_triad=[[ 1  ,  2  ,  3 ,  1 ],
                [  1  ,  2  ,  4 ,  2 ],
                [  1  ,  2  , 11 ,  3 ],
                [  3  ,  2  ,  4 ,  2 ],
                [  3  ,  2  , 11 ,  3 ],
                [  4  ,  2  , 11 ,  4 ],
                [  2  ,  4  ,  5 ,  2 ],
                [  2  ,  4  ,  6 ,  2 ],
                [  2  ,  4  ,  7 ,  5 ],
                [  5  ,  4  ,  6 ,  6 ],
                [  5  ,  4  ,  7 ,  7 ],
                [  6  ,  4  ,  7 ,  7 ],
                [  4  ,  7  ,  8 ,  8 ],
                [  7  ,  8  ,  9 ,  7 ],
                [  7  ,  8  , 10 ,  7 ],
                [  7  ,  8  , 11 ,  5 ],
                [  9  ,  8  , 10 ,  6 ],
                [  9  ,  8  , 11 ,  2 ],
                [ 10  ,  8  , 11 ,  2 ],
                [  2  , 11  ,  8 ,  4 ],
                [  2  , 11  , 12 ,  3 ],
                [  2  , 11  , 13 ,  3 ],
                [  8  , 11  , 12 ,  2 ],
                [  8  , 11  , 13 ,  2 ],
                [ 12  , 11  , 13 ,  1 ]]

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("###################   BONDANGLETYPE   ##################\n")
        f.write("########################################################\n")
        f.write("BONDANGLETYPE\n")
        f.write("#Bond angle interaction type block\n")
        f.write("# NTTY: number of covalent bond types\n")
        f.write(str(NTTY)+"\n")
        f.write("# CT: force constant T0: Bond angle lenght\n")
        f.write("#     CT            T0\n")
        f.write("# H2-C1-H1\n")
        f.write("1.6805112e+03	109.000000\n")
        f.write("# H2-C1-C4\n")
        f.write("5.3000000e+02	111.000000\n")
        f.write("# C2-C1-H2\n")
        f.write("1.5594146e+03	114.000000\n")
        f.write("# C2-C1-C4\n")
        f.write("1.7335469e+03	106.000000\n")

        f.write("# C1-C4-O1\n")
        f.write("1.6805112e+03	109.000000\n")
        f.write("# H7-C4-H8\n")
        f.write("4.4300000e+02	108.530000\n")
        f.write("# H7-C4-O1\n")
        f.write("5.0700000e+02	107.600000\n")
        f.write("# C4-O1-C3\n")
        f.write("4.5000000e+02	109.500000\n")
        f.write("END\n")

        return angle_triad
def write_bondangleh(output_file,angle_triad,n_thf,n_atm_thf,n_sys,n_atm_sys):

        #4, 5 and 8 are the angle types NOT involving H
        h_bond_triad=[ np.array(i) for i in angle_triad if i[3]!=4 and i[3]!=5 and i[3]!=8]

        NTHEH=len(h_bond_triad)

        NTHEH_total=NTHEH*n_thf


        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   BONDANGLEH   #####################\n")
            f.write("########################################################\n")
            f.write("BONDANGLEH\n")
            f.write("#Solute bond angles involving H atoms\n")
            f.write("# NTHEH: number of angle bonds involving H atoms in solute\n")
            f.write(str(NTHEH_total)+"\n")
            f.write("#ITH, JTH KTH: atom sequence numbers of atoms forming a bond angle  ICTH: bond angle type code\n")
            f.write("#ITH JTH KTH   ICTH\n")
            for i in range (0,n_thf):
                for j in range(0,NTHEH):
                    bond = h_bond_triad[j][:3]+i*n_atm_thf+n_sys*n_atm_sys
                    f.write(" %i    %i    %i     %i \n" % (bond[0],bond[1],bond[2],h_bond_triad[j][3]))
            f.write("END\n")
def write_bondangle(output_file,angle_triad,n_thf,n_atm_thf,n_sys,n_atm_sys):

        #4, 5 and 8 are the angle types NOT involving H
        bond_triad=[ np.array(i) for i in angle_triad if i[3]==4 or i[3]==5 or i[3]==8]

        NTHE=len(bond_triad)

        NTHE_total=NTHE*n_thf

        bond_triad

        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   BONDANGLE   #####################\n")
            f.write("########################################################\n")
            f.write("BONDANGLE\n")
            f.write("#Solute bond angles NOT involving H atoms\n")
            f.write("# NTHE: number of angle bonds NOT involving H atoms in solute\n")
            f.write(str(NTHE_total)+"\n")
            f.write("#  IT, JT KT: atom sequence numbers of atoms forming a bond angle  ICT: bond angle type code\n")
            f.write("#IT JT KT   ICT\n")
            for i in range (0,n_thf):
                for j in range(0,NTHE):
                    bond = bond_triad[j][:3]+i*n_atm_thf+n_sys*n_atm_sys
                    f.write(" %i    %i    %i     %i \n" % (bond[0],bond[1],bond[2],bond_triad[j][3]))
            f.write("END\n")

def write_impdihedraltype(output_file):

    NQTY=0

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("###################   IMPDIHEDRALTYPE   ################\n")
        f.write("########################################################\n")
        f.write("IMPDIHEDRALTYPE\n")
        f.write("#Improper (harmonic) dihedral angle interaction type block\n")
        f.write("# NQTY: number of covalent improper (harmonic) dihedral angle types\n")
        f.write(str(NQTY)+"\n")
        f.write("END\n")
def write_impdihedralh(output_file):

        NQHIH=0


        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   IMPDIHEDRALH   ##################\n")
            f.write("########################################################\n")
            f.write("IMPDIHEDRALH\n")
            f.write("#Improper (harmonic) dihedral angle interaction type block\n")
            f.write("# NQHIH: number of covalent improper (harmonic) dihedral angles involving H\n")
            f.write(str(NQHIH)+"\n")
            f.write("END\n")
def write_impdihedral(output_file):

        NQHI=0

        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   IMPDIHEDRAL   ##################\n")
            f.write("########################################################\n")
            f.write("IMPDIHEDRAL\n")
            f.write("#Improper (harmonic) dihedral angle interaction type block\n")
            f.write("# NQHI: number of covalent improper (harmonic) dihedral angles NOT involving H\n")
            f.write(str(NQHI)+"\n")
            f.write("END\n")

def write_dihedraltype(output_file):

    NPTY=2

    with open(output_file, 'a') as f:
        f.write("########################################################\n")
        f.write("####################   DIHEDRALTYPE   ##################\n")
        f.write("########################################################\n")
        f.write("DIHEDRALTYPE\n")
        f.write("#Proper (trigonometric) dihedral angle interaction type block\n")
        f.write("# NPTY: Number of proper dihedral interaction types \n")
        f.write(str(NPTY)+"\n")
        f.write("# CP: force constant  PD: cosine of the phase shift  NP: multiplicity\n")
        f.write("# CP    PD    NP\n")
        f.write("# 46   O,H,H|C|C|C,H,H|020\n")
        f.write("1.000	-1.0	3\n")
        f.write("# 23   -CHn-OA(no sugar)\n")
        f.write("1.260	+1.0	3\n")
        f.write("END\n")
def write_dihedralh(output_file):

        NPHIH=0

        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   DIHEDRALH   ###################\n")
            f.write("########################################################\n")
            f.write("DIHEDRALH\n")
            f.write("#Solute dihedrals involving H-atoms block\n")
            f.write("# NPHIH: Number of dihedral angles involving H\n")
            f.write(str(NPHIH)+"\n")
            f.write("END\n")
def write_dihedral(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys):

        bond_pairs=[[11,2,4,7],[4,2,11,8],[2,4,7,8],[4,7,8,11],[7,8,11,2]]
        bond_pairs=[np.array(i) for i in bond_pairs]
        bond_type=np.array([1,1,2,2,1])

        NPHI=len(bond_type)

        NPHI_total=NPHI*n_thf


        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("######################   BOND   ########################\n")
            f.write("########################################################\n")
            f.write("DIHEDRAL\n")
            f.write("#Solute bonds NOT involving H atoms\n")
            f.write("# NBON: number of bonds involving H atoms in solute\n")
            f.write(str(NPHI_total)+"\n")
            f.write("#  IBH, JBH: atom sequence numbers of atoms forming a bond  ICBH: bond type code\n")
            f.write("#IBH JBH    ICBH\n")
            for i in range (0,n_thf):
                for j in range(0,NPHI):
                    bond = bond_pairs[j]+i*n_atm_thf+n_sys*n_atm_sys
                    f.write(" %i    %i    %i    %i     %i \n" % (bond[0],bond[1],bond[2],bond[3],bond_type[j]))
            f.write("END\n")

def write_solventatom(output_file):

        NRAM=0

        with open(output_file, 'a') as f:
            f.write("########################################################\n")
            f.write("###################   SOLVENTATOM   ####################\n")
            f.write("########################################################\n")
            f.write("SOLVENTATOM\n")
            f.write("#NRAM: Number of solvent atoms in one molecule\n")
            f.write(str(NRAM)+"\n")
            f.write("END\n")
def write_g96_coordenates(final_residue_name,final_atom_name,r_full,n_atm_sys,n_thf,n_atm_thf,g96_output_file):

    r_full=r_full*mol.A2nm

    with open(g96_output_file, 'w') as f:
        f.write("TITLE\n")
        f.write("This is the coordinate file for "+g96_output_file + " in g96 format\n")
        f.write("END\n")
        f.write("POSITION\n")
        k=0
        for i in range(0,n_atm_sys):
            f.write(" %i    %s  %s  %i  % 3.8f  % 3.8f  % 3.8f \n" % (k+1,final_residue_name[k],final_atom_name[k],k+1,r_full[k,0],r_full[k,1],r_full[k,2]))
            k+=1
        for i in range(0,n_thf):
            for j in range (0,n_atm_thf):
                f.write(" %i    %s  %s  %i  % 3.8f  % 3.8f  % 3.8f \n" % (n_atm_sys+i+1,final_residue_name[k],final_atom_name[k],k+1,r_full[k,0],r_full[k,1],r_full[k,2]))
                k+=1
        f.write("END\n")

############################################################################%%


def main (sys_file,thf_file,thf_top,packmol_inp_file,output_file,g96_output_file):

    ## Sys data
    e_sys,r_sys,atm_names_sys=mol.e_r_atm_names_from_xyz (sys_file,ener="no")
    r_sys=r_sys[0]
    n_sys=1
    n_atm_sys=len(atm_names_sys)

    ## THF data
    e_thf,r_thf,atm_names_thf=mol.e_r_atm_names_from_xyz(thf_file)
    r_thf=r_thf[0]
    g96_atm_names_thf=xyz_name2g96_name(atm_names_thf)
    n_atm_thf=len(g96_atm_names_thf)

    ## Full system
    n_thf,packmol_out=read_info_packmol(packmol_inp_file)
    e_full,r_full,atm_names_full=mol.e_r_atm_names_from_xyz (packmol_out,ener="no")
    r_full=r_full[0]

    ##%%
    write_title(output_file)

    write_topphyscon(output_file)

    write_topversion(output_file)

    lj_atm_type=write_atomtypename(output_file)

    write_ljparmeters(output_file)

    residue_name=write_resname(output_file,n_atm_sys)

    final_residue_index,final_atom_name=write_soluteatom(output_file,lj_atm_type,atm_names_sys,n_sys,g96_atm_names_thf,n_thf,thf_top)
    final_residue_name=[residue_name[i-1] for i in final_residue_index]

    write_bondtype(output_file)

    write_bondh(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys)

    write_bond(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys)

    angle_triad=write_bondangletype(output_file)

    write_bondangleh(output_file,angle_triad,n_thf,n_atm_thf,n_sys,n_atm_sys)

    write_bondangle(output_file,angle_triad,n_thf,n_atm_thf,n_sys,n_atm_sys)

    write_impdihedraltype(output_file)

    write_impdihedralh(output_file)

    write_impdihedral(output_file)

    write_dihedraltype(output_file)

    write_dihedralh(output_file)

    write_dihedral(output_file,n_thf,n_atm_thf,n_sys,n_atm_sys)

    write_solventatom(output_file)

    write_g96_coordenates(final_residue_name,final_atom_name,r_full,n_atm_sys,n_thf,n_atm_thf,g96_output_file)
###########################################################################%%


if __name__== "__main__":

    # sys_name=sys.argv[1]
    # n_thf=sys.argv[2]

    # sys_file="/home/ar612/Documents/cp2k_organized/equilibration/md/test/r2.xyz"
    # thf_file="/home/ar612/Documents/cp2k_organized/equilibration/md/test/thf.xyz"
    # thf_top="/home/ar612/Documents/cp2k_organized/equilibration/md/test/thf_g96.top"
    # packmol_inp_file="/home/ar612/Documents/cp2k_organized/equilibration/md/test/packmol.inp"
    # out_file_pattern="/home/ar612/Documents/cp2k_organized/equilibration/md/test/r2_thf_$$_g96.top"
    # g96_out_file_pattern="/home/ar612/Documents/cp2k_organized/equilibration/md/test/r2_thf_$$.g96"

    sys_file=sys.argv[1]
    thf_file=sys.argv[2]
    thf_top=sys.argv[3]
    packmol_inp_file=sys.argv[4]
    output_file=sys.argv[5]
    g96_output_file=sys.argv[6]

    main(sys_file,thf_file,thf_top,packmol_inp_file,output_file,g96_output_file)
