#! /bin/python3
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('$PYDIR')
import mol
from itertools import islice

# %matplotlib qt
#
# # inp_file_1="/home/ar612/Documents/Work/cavities/par_opt/excitation_molecular_1/Emax1_10/neta_11/random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/18/eta0_0.000_t0_25/t.dat"
# # inp_file_2="/home/ar612/Documents/Work/cavities/par_opt/excitation_molecular_1/Emax1_10/neta_11/random_intcost_0.02_kappa_0.02_umax_2_trapezoidal_500/18/eta0_0.000_t0_25/u.dat"


def merge(inp_file_1,inp_file_2,out_file):
    ##################################################################################################################%%

    file_1=np.genfromtxt(inp_file_1)
    file_2=np.genfromtxt(inp_file_2)

    n=len(file_1)

    file_1_aug=[file_1[0]]
    file_2_aug=[file_2[0]]

    aug=5

    for i in range(aug):
        file_1_aug+=(file_1[1:]+i*file_1[-1]).tolist()
        if i == 0:
            file_2_aug+=(file_2[1:]).tolist()
        else:
            file_2_aug+=[0]*(n-1)

    file_1_aug+=[file_1_aug[-1]+(file_1_aug[-1]-file_1_aug[-2])]
    file_2_aug+=[0]



    file_f=np.array([file_1_aug,file_2_aug]).transpose()

    mol.write_matrix_to_file(file_f,"","",out_file)






if __name__== "__main__":


    inp_file_1=sys.argv[1]
    inp_file_2=sys.argv[2]
    out_file=sys.argv[3]

    merge(inp_file_1,inp_file_2,out_file)
