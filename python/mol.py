#############################################%%

import numpy as np
import os
import itertools
from itertools import islice
import glob
import rmsd
import matplotlib.pyplot as plt



## Masses of some atoms in atomic mass units

mass={}

mass["H"]=1.007825
mass["C"]=12
mass["O"]=15.999
mass["N"]=14.007
mass["Br"]=79.904
mass["F"]=18.9984


##### Some constants and conversion values####%%

## Length
au2A=0.52917721067
A2au=1./au2A

nm2A=10
A2nm=0.1

## Energy
Eh2J=4.359744650e-18
Eh2kcalmol=627.509391
kcalmol2Eh=1/Eh2kcalmol
J2Eh=1./Eh2J
J2kcalmol=J2Eh*Eh2kcalmol
J2cal=0.239006

Eh2cm_1=219474.6305
Eh2ev=27.211396
ev2cm_1=8065.54

cm_12Eh=1./Eh2cm_1

## Time
au2s=2.4188843265e-17
fs2ps=0.001
s2fs=1e+15
au2fs=au2s*s2fs
fs2au=1./au2fs

## Force
frc_au_2_Eh2kcalmol_A=Eh2kcalmol/au2A
mdyne2J_A=1e-18

## Mass
amu2au_mass=1.82289e+3
au_mass2amu=1./amu2au_mass


h=6.62607015e-34
h_bar=1.054571800e-34
k=1.38064852e-23 ### Boltzmann constant
c=299792458 	### Speed of light
Na=6.022140857e+23   ###Avogadro's number
T=298




######################################################################%%
def write_r_cp2k_inp (file_dir,line_number,r):

    n=len(r)  ## Numer of atoms

    dim=len(r[0])  ## This is the dimension of each element of r

    file_lines = open(file_dir, 'r').readlines()  ## Reading the file into an array of lines

    for i in range(0,n):
       temp=file_lines[line_number+i].split()
       temp[1:1+dim]=r[i]
       temp=np.append(temp,"\n")
       file_lines[line_number+i]="   ".join(temp)

    with open(file_dir,'w') as file:
        file.writelines(file_lines)




#########################################################%%
def read_residues (file_dir,line_number,n,pattern="&COORD",init_col=4):

    length=file_len(file_dir)

    names,numbers=np.genfromtxt(file_dir,unpack= True, skip_header = line_number,max_rows=n,usecols={init_col,init_col+1},dtype=str)

    return names,numbers







##### This function creates a new geometry from the original geometry, the vibrational eigenvectors and the amplitud of the oscilation####%%

def excite (r,disp,amp):

    r_excited=r+amp*disp

    return r_excited



#######################################################%%


##### This function displace the center of coordinates to a desire point in space ####%%

def center_of_coord (r):

    n=len(r) ## Number of atoms

    rcm=np.array([0,0,0])

    #### Calculating center of coordinates
    for i in range(0,n):

        rcm=(rcm+r[i]/n)


    return rcm



######################################################################################%%


##### This function displace the center of coordinates to a desire point in space ####%%

def displace_center_coord (r,r0):

    n_steps=len(r) ## Number of atoms
    n_atm=len(r[0])
    dim=len(r[0,0])

    r_disp=np.empty([n_steps,n_atm,dim])

    for i in range(0,n_steps):
        dr=r0-center_of_coord(r[i])   ### Displacement
        r_disp[i]=r[i]+dr


    return r_disp



#######################################################%%




##### This function creates a xyz file from the coordinates and atoms names ####%%

def xyz_file (r,atm_names,file_name):

    n=len(r)  ## Number of atoms

    file=open(file_name,"w+")
    file.write(str(n)+"\n\n")


    ### Writing to file
    for i in range(0,n):

        file.write(str(atm_names[i])+"  "+str(r[i,0])+"  "+str(r[i,1])+"  "+str(r[i,2])+"\n")

    file.close()


######### This function orders and old array with order_i, to have the new order order_f#####%%
def order_array (r,order_i,order_f,n,m,data_type="float"):

    if m == 1:
        r_order=np.zeros([n],dtype=data_type)
    else:
        r_order=np.zeros([n,m],dtype=data_type)


    for i in range(0,n):
        r_order[order_f[i]]=r[order_i[i]]

    return r_order




##########This function extracts the energies, trajectories and atom names from a xyz file ###############%%


def e_r_atm_names_from_xyz(inp_file):

    with open(inp_file) as finp:
        first_line = list(islice(finp, 1))  #### Reading the file in slices of 1
        n_atm=int(first_line[0].split()[0])


    period=n_atm+2
    lines=file_len(inp_file)

    ##%%
    if lines%period != 0:
        print("*********** ERROR *************   e_r_atm_names_from_xyz: lines is not multiple of n_atm+2. "+inp_file+" must be corrupted")
    else:
        n_steps=lines//period

        r=np.empty([n_steps,n_atm,3])
        i_list=[]
        time_list=[]
        e=[]

        ##%%
        i=0
        names_check=0
        with open(inp_file) as finp:
            while True:
                next_n_lines = list(islice(finp, n_atm+2))  #### Reading the file in slices of n_tot+2
                if not next_n_lines:
                    break
                if names_check==0:
                    atm_names=np.array([a.split()[0] for a in next_n_lines[2:]])
                    names_check=1
                if "E =" in next_n_lines[1]:
                    e.append(float(next_n_lines[1].split()[-1]))
                if "time =" in next_n_lines[1]:
                    time_list.append(float(next_n_lines[1].split()[-4][:-1]))
                if "i =" in next_n_lines[1] and "time =" in next_n_lines[1]:
                    i_list.append(int(next_n_lines[1].split()[-7][:-1]))
                if "i =" in next_n_lines[1] and not "time =" in next_n_lines[1]:
                    i_list.append(int(next_n_lines[1].split()[-4][:-1]))





                r[i]=np.array([[[float(b) for b in a.split()[1:]] for a in next_n_lines[2:]]])

                i=i+1
        ##%%

        return np.array(e),r,atm_names,time_list,i_list



##########This function extracts coordinates and atom names from a g96 file ###############%%
def r_atm_names_from_g96(inp_file):

    coord_check=0

    ## Reading the file line by line. To get the positions corresponding to the last job
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'POSITION' in line:
               coord_0=n
               coord_check=1
           if 'END' in line and coord_check==1:
               coord_f=n-1
               break

    n_atm=coord_f-coord_0

    ##%%
    r=np.genfromtxt(inp_file,skip_header=coord_0,max_rows=n_atm,usecols={4,5,6})*nm2A
    atm_names=np.genfromtxt(inp_file,skip_header=coord_0,max_rows=n_atm,usecols={2},dtype=str)


    return r,atm_names





############ This function takes the information every step steps from a periodic file of period period#############################################%%

def reduce_periodic_file (inp_file,out_file,period,step,step_i=1,step_f=10000000):

    fout=open(out_file,"w+")
    i=1
    with open(inp_file) as finp:
        while True:
            next_n_lines = list(islice(finp, period))
            if not next_n_lines or i> step_f:
                break
            if i <= step_f and i>= step_i and (i-1)%step==0:
                fout.write(''.join(next_n_lines)) ### Writing block to file

            i=i+1

    fout.close()
    #print i,next_n_lines[1]

#################### Saves the trajectory of a specific molecule from a xyz file#######################################################################%%
def molecule_trajectory (inp_file,out_file,period,mol_i,mol_f,step=1,step_i=1,step_f=10000000):


    fout=open(out_file,"w+")
    i=1
    with open(inp_file) as finp:
        while True:
            next_n_lines = list(islice(finp, period))  #### Reading the file in slices of n_tot+2
            if not next_n_lines or i> step_f:
                break
            if i <= step_f and i>= step_i and (i-1)%step==0:

                next_n_lines[mol_f+2:period]=[]  ### Deleting all atoms after the molecule!!!!!!!!)
                next_n_lines[2:mol_i+1]=[]  ### Deleting all atoms before the molecule!!!!!!!!)

                next_n_lines[0]=str(mol_f-mol_i+1)+"\n"   ### Chanching the number of atoms

                fout.write(''.join(next_n_lines)) ### Writing block to file

            i=i+1

    fout.close()

###################Takes the last frame from a xyz file##############################################################%%

def last_frame (inp_file,out_file,period):


    fout=open(out_file,"w+")
    i=1
    with open(inp_file) as finp:
        while True:
            next_n_lines = list(islice(finp, period))  #### Reading the file in slices of n_tot+2
            if not next_n_lines:
                fout.write(''.join(last)) ### Writing block to file
                break
            last=next_n_lines
            i=i+1

    fout.close()



############## Ectract all frequencies, vibrational modes and frequencies from a given molden file#################
def w_modes_r_atm_names_from_molden (inp_file):

    ## These are some variables to check whether I already asigned the initial and final lines for the coordinates and frequancy sections
    freq_check_0=0
    freq_check_f=0
    coord_check_0=0
    coord_check_f=0



    ## Reading the file line by line
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if '[FREQ]' in line and freq_check_0==0:
               freq_0=n
               freq_check_0=1
           if freq_check_0==1:
               if '[' in line and n > freq_0 and freq_check_f==0:
                   freq_f=n-1
                   freq_check_f=1

           if '[FR-COORD]' in line and coord_check_0==0:
               coord_0=n
               coord_check_0=1
           if coord_check_0==1:
               if '[' in line and n > coord_0 and coord_check_f==0:
                   coord_f=n-1
                   coord_check_f=1
           if "[FR-NORM-COORD]" in line:
                norm_0=n
           if "[INT]" in line:
                int_0=n


    ## Calculating the number of normal modes and atoms
    n_vib=freq_f-freq_0
    n_atm=coord_f-coord_0


    ## Declaring an array where I will storage the normal modes
    modes=np.empty([n_vib,n_atm,3]) ## Normal modes

    ## Reading the frequancies, atm_names and coordinates
    w=np.genfromtxt(inp_file,unpack= True, skip_header = freq_0,max_rows=n_vib, usecols={0},dtype=float)

    int=np.genfromtxt(inp_file,unpack= True, skip_header = int_0,max_rows=n_vib, usecols={0},dtype=float)

    atm_names=np.genfromtxt(inp_file,unpack= True, skip_header = coord_0,max_rows=n_atm, usecols={0},dtype=str)

    r=np.genfromtxt(inp_file, skip_header = coord_0,max_rows=n_atm, usecols={1,2,3},dtype=float)*au2A

    ## Reading normal modes
    for vib in range(0,n_vib):
        #print((n_atm+1)*n_vib + norm_0+1)
        modes[vib]=np.genfromtxt(inp_file, skip_header = (n_atm+1)*vib + norm_0+1,max_rows=n_atm, usecols={0,1,2},dtype=float)*au2A


    return w,int,modes,r,atm_names


############## Ectract all frequencies, vibrational modes and frequencies from a given irc gaussian file#################
def w_redm_f_intes_modes_r_atm_names_from_gauss (inp_file):

    ## Initializing arrays
    w=np.empty([0])
    redm=np.empty([0])
    f=np.empty([0])
    intes=np.empty([0])

    ## Reading the file line by line. To get the positions corresponding to the last job
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'Standard orientation:' in line:
               coord_0=n+4
           if 'Mulliken charges:' in line and n > coord_0:
               mull_0=n+1
           if 'Sum of Mulliken charges =' in line and n > mull_0:
               mull_f=n-1
           if 'normal coordinates:' in line and n > mull_f:
               normal_0=n+7

    ## Reading the file line by line. To get the frequencies of the last job
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'Frequencies --' in line and n > mull_f:
               w=np.append(w,np.array( [float(i) for i in line.split()[2:]] ))
           if 'Red. masses --' in line and n > mull_f:
               redm=np.append(redm,np.array( [float(i) for i in line.split()[3:]] ))
           if 'Frc consts  --' in line and n > mull_f:
               f=np.append(f,np.array( [float(i) for i in line.split()[3:]] ))
           if 'IR Inten    --' in line and n > mull_f:
               intes=np.append(intes,np.array( [float(i) for i in line.split()[3:]] ))


    ## Reading atom names and structure
    n_atm=mull_f-mull_0
    atm_names=np.genfromtxt(inp_file,unpack= True, skip_header = mull_0,max_rows=n_atm, usecols={1},dtype=str)
    r=np.genfromtxt(inp_file, skip_header = coord_0,max_rows=n_atm, usecols={3,4,5},dtype=float)

    ## Declaring an array where I will storage the normal modes
    n_vib=len(w)
    modes=np.empty([n_vib,n_atm,3])
    # Reading normal modes
    for vib in range(0,n_vib):

        ### Index, Displacement and Period in the vertical direction
        i=int(vib/3)
        di=normal_0
        ti=n_atm+7

        ### Index, Displacement and Period in the horizontal direction
        j=vib%3
        dj=2
        tj=3
        ### Saving the normal mode
        modes[vib]=np.genfromtxt(inp_file, skip_header = i*ti + di, max_rows=n_atm, usecols={j*tj + dj+0,j*tj + dj+1,j*tj + dj+2},dtype=float)



    return w,redm,f,intes,modes,r,atm_names






def thermo_from_gauss (inp_file):

    ## Electronic energy
    e0=0
    ## Electronic energy, plus zero point, thermal, enthalpy and gibbs
    point0=0
    thermal=0
    enthalpy=0
    gibbs=0

    ## Reading the file line by line. To get the positions corresponding to the last job
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'SCF Done:' in line:
               e0=float(line.split()[4])
           if 'Sum of electronic and zero-point Energies' in line:
               point0=float(line.split()[-1])
           if 'Sum of electronic and thermal Energies' in line:
               thermal=float(line.split()[-1])
           if 'Sum of electronic and thermal Enthalpies' in line:
               enthalpy=float(line.split()[-1])
           if 'Sum of electronic and thermal Free Energies' in line:
               gibbs=float(line.split()[-1])

    return e0,point0,thermal,enthalpy,gibbs




def r_atm_names_e_from_irc_gauss (inp_file):

    ## Reading the file line by line. To get the positions corresponding to the last job
    with open(inp_file) as file:
        ## Initializing arrays
        coord_0=np.empty([0])
        e=np.empty([0])
        ne=np.empty([0])
        for n, line in enumerate(file, 1):
           if 'SCF Done:' in line:
               ne=np.append(ne,int(n))
               e=np.append(e,np.array(float(line.split()[4])))
           if 'Input orientation:' in line:
               coord_0=np.append(coord_0,n+4)
           if 'Mulliken charges:' in line:
               mull_0=n+1
           if 'Sum of Mulliken charges =' in line and n > mull_0:
               mull_f=n-1

    coord_0=np.array([int(i) for i in coord_0[:-1]]) ### I remove one Input orientation because the last one doesnt have energy asociated

    ## Reading atom names and structure
    n_atm=mull_f-mull_0
    n_step=len(coord_0)
    r=np.empty([n_step,n_atm,3])


    atm_names=np.genfromtxt(inp_file,unpack= True, skip_header = mull_0,max_rows=n_atm, usecols={1},dtype=str)
    for i in range(0,len(coord_0)):
        r[i]=np.genfromtxt(inp_file, skip_header = coord_0[i],max_rows=n_atm, usecols={3,4,5},dtype=float)


    return r,atm_names,e


def r_atm_names_e_from_opt_gauss (inp_file,pattern='Input orientation:'):

    ## Reading the file line by line. To get the positions corresponding to the last job
    with open(inp_file) as file:
        ## Initializing arrays
        coord_0=np.empty([0])
        e=np.empty([0])
        for n, line in enumerate(file, 1):
           if 'SCF Done:' in line:
               e=np.append(e,np.array(np.array(float(line.split()[4]))))
           if pattern in line:
               coord_0=np.append(coord_0,n+4)
           if 'Mulliken charges:' in line or "Mulliken atomic charges:" in line:
               mull_0=n+1
           if ('Sum of Mulliken charges =' in line or "Sum of Mulliken atomic charges =" in line) and n > mull_0:
               mull_f=n-1



    coord_0=np.array([int(i) for i in coord_0])

    if len(e) > len(coord_0):
        e=e[:len(coord_0)]
    if len(coord_0) > len(e):
        coord_0=coord_0[:len(e)]

    ## Reading atom names and structure
    n_atm=mull_f-mull_0
    n_step=len(coord_0)
    r=np.empty([n_step,n_atm,3])


    atm_names=np.genfromtxt(inp_file,unpack= True, skip_header = mull_0,max_rows=n_atm, usecols={1},dtype=str)
    for i in range(0,len(coord_0)):
        r[i]=np.genfromtxt(inp_file, skip_header = coord_0[i],max_rows=n_atm, usecols={3,4,5},dtype=float)


    return r,atm_names,e



## Writes normal modes to a nmd file
def write_modes_2_nmd(nmd_out_file,atm_names,r,modes,name_list):
    n_atm=len(r)
    n_vib=len(modes)

    with open(nmd_out_file,"w+") as file:
        ## writing atom names
        file.write("atomnames ")
        for name in atm_names:
            file.write(name+" "+name+" "+name+" ")
        file.write("\n")

        ## writing coordinates
        file.write("coordinates ")
        for i in range(0,n_atm):
            file.write(str(r[i,0])+"  "+str(r[i,1])+"  "+str(r[i,2])+" ")
        file.write("\n")

        ## writing modes
        for i in range(0,n_vib):
            file.write("mode "+str(name_list[i])+" ")
            for j in range(0,n_atm):
                file.write(str(modes[i,j,0])+"  "+str(modes[i,j,1])+"  "+str(modes[i,j,2])+" ")
            file.write("\n")



################ Write a given matrix to a file ############################
def write_matrix_to_file(matrix,header,footer,out_file,format='%.20e'):

    with open(out_file,"w+") as file:
        np.savetxt(file, matrix,fmt=format,header=header,footer=footer,delimiter='  ')




############## Ectract the ith element of a line matching the given pattern #################


def extract_pattern(inp_file,pattern,i):

    e=np.zeros([0])

    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if pattern in line:
               e=np.append(e,float(line.split()[i]))

    return e



#### This function writes a trajectory given a list of frames r with energies e. r and e must have the same length##############
def write_trajectory (r,e,atm_names,time,indx,out_file):

    n_frames=len(r)
    n_atm=len(r[0])  ## Number of atoms

    if len(r) != len(e):
        print("******* ERROR ********   write_trajectory: len(r)= "+str(len(r))+"   len(e)= " + str(len(e)) + "   They don't have the same number of frames!")
    else:
        with open(out_file,"w+") as file:
            for j in range(0,n_frames):


                file.write(str(n_atm)+"\n")
                file.write( "i =      "+str(indx[j])+", time =     "+str(time[j])+", E =     "+str(e[j]) + "\n")
                ### Writing to file
                for i in range(0,n_atm):
                    file.write("  %s      % 4.8f     % 4.8f     % 4.8f \n" % (atm_names[i],r[j,i,0],r[j,i,1],r[j,i,2]))





#### This function finds the line numbers matching a given pattern in a file. start specifies where to start the counter ##############

def find_line_numbers(inp_file,pattern,start=0):

    line_numbers=np.zeros([0])

    with open(inp_file) as file:
       for num, line in enumerate(file,start):
           if pattern in line:
               line_numbers=np.append(line_numbers,num)

    return line_numbers



### This function converts a text file into a matrix of its words

def matrix_file(inp_file,delimiter):

    with open(inp_file, 'r') as f:
        matrix = [[str(word) for word in line.split(delimiter)] for line in f ]

    return matrix



### This function returns the number of lines in a file
def file_len(fname):
    i=0
    with open(fname) as f:
        for i, l in enumerate(f,1):
            pass
    return i

### This function returns the number of colums in the first line not commented by #
def file_col(file):
    with open(file) as finp:
        next_n_lines = list(islice(finp, 100))
        for line in next_n_lines:
            if "#" not in line:
                    n_col=len(line.split())
                    break
    return n_col


## Return the diferences of positions and the distance along the reaction path from a given set of images of the reaction path
def path_dist(r):
    n_images=len(r)
    n_atm=len(r[0])
    dim=len(r[0,0])

    dr=np.zeros([n_images-1,n_atm,3])
    dist=np.empty(n_images)

    for k in range(0,n_images-1):
            dr[k]=r[k+1]-r[k]


    dist[0]=0
    for i in range(1,n_images):
        dist[i]=np.linalg.norm(dr[i-1])
    sum=0
    for i in range(0,n_images):
        sum=sum+dist[i]
        dist[i]=sum


    return dr,dist



def comp_avg(x):

    n=len(x)
    avg=np.empty([n])

    avg[0]=x[0]

    for i in range(1,n):
        j=i+1
        avg[i]=(j-1.)*avg[i-1]/j + x[i]/j

    return avg

def comp_std(x):

    n=len(x)
    std=np.empty([n])
    avg=np.empty([n])

    std[0]=0
    avg[0]=x[0]

    for i in range(1,n):
        j=i+1
        avg[i]=(j-1.)*avg[i-1]/j + x[i]/j
        std[i]=np.sqrt(  (j-2)*std[i-1]**2/(j-1) + (avg[i-1]-x[i])**2/j   )

    return std


def fast_load(file,cols,chunck=1000):
    n_lines =file_len(file)
    n_cols=len(cols)

    dat=np.zeros([n_lines,n_cols])

    i=1
    lenght=0
    j=0
    with open(file) as finp:
        while True:
            next_n_lines = list(islice(finp, chunck))
            if not next_n_lines:
                break
            if i >= 1:
                for k,line in enumerate(next_n_lines):
                    if "#" not in line and line != "\n":
                        if lenght==0:
                            file_col=len(line.split())
                            lenght=1
                        if file_col == len(line.split()):
                            dat[j]=np.array([ float(element) for element in np.array(line.split())[cols]])
                            j=j+1
            i=i+1
    return dat[:j]



def gauss(x,y,x0=0,sx=1,y0=0,sy=1,h=1):
        return h*np.exp(-np.power((x - x0)/sx, 2.)/2.-np.power((y - y0)/sy, 2.)/2.)




def read_lag_mult(lag_inp_file,n_cons):

    l=file_len(lag_inp_file)  ## This is the lenght of the file
    chunk=int(np.ceil(n_cons/4.)) ## This is the number of line between one frame an the other
    lag_lenght=l/(2.*chunk)        ## This is the number of frames. If everything is all frames are complete this numer must be integer


    if (lag_lenght.is_integer()): ## Checking if lag_lenght.is_integer(). Otherwise there is a problem with the file

        ## Converting lag_lenght to int data type
        lag_lenght=int(lag_lenght)

        ## Creating array to storage lag multipliers values for every instant of time
        lag_mult_list=np.empty([lag_lenght,n_cons])

        ## This the number of values in the last line. It goes from 1 to 4
        reminder=(n_cons-1)%4+1

        with open(lag_inp_file) as finp:
            i=0
            while True:
                next_n_lines = list(islice(finp, chunk))  #### Reading the file in slices of n_tot+2
                if not next_n_lines:
                    break
                if "Shake" in next_n_lines[0]:  ## I'm only interested in Shake lag multipliers
                    lag_mult=np.empty([n_cons]) ## Here I storage the lag_mult for a instant of time
                    j=0
                    for line in next_n_lines[:-1]: ## Except for the last line there are 4 lagrange multipliers per line
                        line.split()
                        lag_mult[4*j:4*(j+1)]=np.array([float(k) for k in line.split()[-4:]])
                        j=j+1
                    lag_mult[4*j:4*j+reminder]=np.array([float(k) for k in next_n_lines[-1].split()[-reminder:]]) ## For the last line there are reminder lag_mult

                    ## Appending lag_mult for that frame to lag_mult_list
                    lag_mult_list[i]=lag_mult
                    i=i+1
        return lag_mult_list


    else:
        print("*******ERROR*******  read_lag_mult: lag_lenght must be integer. A frame must be corrupted in "+lag_inp_file+" file")




def merge(inp_file_1,inp_file_2,out_file):
    ##################################################################################################################%%
    len1=file_len(inp_file_1)
    col1=file_col(inp_file_1)

    len2=file_len(inp_file_2)
    col2=file_col(inp_file_2)

    lenf=len1
    colf=col1+col2


    file_1=fast_load(inp_file_1,np.array(range(0,col1)),1000)
    file_2=fast_load(inp_file_2,np.array(range(0,col2)),1000)

    file_1.shape

    file_f=np.column_stack((file_1,file_2))

    write_matrix_to_file(file_f,"","",out_file)

    return file_f


## This function returns a list of the average as a function of time
def mean_list(list):
    avg=list[0]
    avg_list=np.zeros([len(list)])
    avg_list[0]=avg
    for i in range(1,len(list)):
        avg=i*avg/(i+1)+list[i]/(i+1)
        avg_list[i]=avg

    return avg_list

## Convert from cartesian to mass_weighted coordinates
def cartesian2mass_weighted(y,atm_names):
    n_vib=len(y)
    n_atm=len(y[0])

    ymass=np.empty([n_vib,n_atm,3])

    for i in range(0,n_vib):
        for j in range(0,n_atm):
            ymass[i,j]=y[i,j]*np.sqrt(mass[atm_names[j]])
    return ymass

## Convert from mass_weighted to cartesian coordinates
def mass_weighted2cartesian(y,atm_names):
    n_vib=len(y)
    n_atm=len(y[0])

    ycart=np.empty([n_vib,n_atm,3])

    for i in range(0,n_vib):
        for j in range(0,n_atm):
            ycart[i,j]=y[i,j]/np.sqrt(mass[atm_names[j]])
    return ycart


def center_of_mass(y,atm_names):
    n_steps=len(y)
    n_atm=len(y[0])

    cent_mass=np.empty([n_steps,3])


    for i in range(0,n_steps):
        sum_cent=np.array([0,0,0])
        sum_mass=0
        for j in range(0,n_atm):
            sum_cent = sum_cent + y[i,j]*mass[atm_names[j]]
            sum_mass=sum_mass + mass[atm_names[j]]
        cent_mass[i]=sum_cent/sum_mass

    return cent_mass

## Normalize the normal modes
def normal (y):
    n_vib=len(y)
    n_atm=len(y[0])

    ynorm=np.empty([n_vib,n_atm,3])

    for i in range(0,n_vib):
        ynorm[i]=y[i]/np.linalg.norm(y[i].flatten())
    return ynorm

## Returns the projections for every normal mode in y
def projection_matrix (y):
    n_vib=len(y)
    y_ij=np.empty([n_vib,n_vib])

    for i in range(0,n_vib):
        for j in range(0,n_vib):
            y_ij[i,j]=np.dot(y[i].flatten(),y[j].flatten())

    return y_ij


## Converts a flatten position trajectory into a not flatten one
def no_flat(r,part=3):

    if len(r[0])%part != 0:
        print("*********** ERROR *************  no_flat: The length of each elemt in r must be devided by part (default=3). All the elements in r must have the same dimensions")
    else:
        return np.array([ np.array(np.array_split(x[:,0],len(x)/part)) for x in r])

## Converts a not flatten position trajectory into a flatten one
def flat(r):
    return np.array([ np.array([x.flatten()]).T for x in r])



## This function computes the difference between 2 structures, rotated to minimize the rmsd
def rmsd_sub(XR, XL,select):

    if select == 0: ## Do not rotate the molecules
        return XR - XL

    if select == 1: ## Rotate the molecules

        XR_cent = XR - rmsd.centroid(XR)
        XL_cent = XL - rmsd.centroid(XL)

        U_XR_XL = rmsd.kabsch(XR_cent,XL_cent)
        XR_rot=np.dot(XR_cent,U_XR_XL)

        return XR_rot - XL_cent


### This function removes all digits from a given string
def remove_digits(string):
    return ''.join([i for i in string if not i.isdigit()])


## This function compute the dihedral angle given for points in a 3d space
def dihedral(p0,p1,p2,p3):
    """Praxeolitic formula
    1 sqrt, 1 cross product"""

    b0 = -1.0*(p1 - p0)
    b1 = p2 - p1
    b2 = p3 - p2

    # normalize b1 so that it does not influence magnitude of vector
    # rejections that come next
    b1 /= np.linalg.norm(b1)

    # vector rejections
    # v = projection of b0 onto plane perpendicular to b1
    #   = b0 minus component that aligns with b1
    # w = projection of b2 onto plane perpendicular to b1
    #   = b2 minus component that aligns with b1
    v = b0 - np.dot(b0, b1)*b1
    w = b2 - np.dot(b2, b1)*b1

    # angle between v and w in a plane is the torsion angle
    # v and w may not be normalized but that's fine since tan is y/x
    x = np.dot(v, w)
    y = np.dot(np.cross(b1, v), w)
    return np.arctan2(y, x)


### This function returns the index corresponding to the maximum of a given list
def get_max_index(e_list):

    emax=e_list[0]
    imax=0

    for i,e in enumerate(e_list):
        if e>emax:
            emax=e
            imax=i
    return imax




############################################################################%%
## Returns the un-flatten position array
def no_flat(r,part=3):
    if len(r[0])%part != 0:
        print("*********** ERROR *************  no_flat: The length of each elemt in r must be devided by part (default=3). All the elements in r must have the same dimensions")
    else:
        return np.array([ np.array(np.array_split(x,int(len(x)/part))) for x in r])

def flat(r):
    return np.array([ np.array([x.flatten()]).T for x in r])




### This function is needed by ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss because in some cases the
### frequencies w come without spaces such as -1231.231-1342.2334
def return_number(s):
    try:
        float(s)
        return float(s)
    except ValueError:
        s1=-float(s.split("-")[1])
        s2=-float(s.split("-")[2])
        return np.array([s1,s2])

def ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss (inp_file):

    ## Everything obtained in this file will be corresponding to the last job
    ## Initializing arrays
    w=np.empty([0])
    redm=np.empty([0])
    fcnstmDA=np.empty([0])
    intes=np.empty([0])
    modes_lines=np.zeros([0])

    coord_0=0
    mull_0=0
    mull_f=0
    dip_0=0
    dip_der_0=0
    force_0=0

    ## Reading the file line by line to get the positions of some relevant keywords
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'SCF Done:  E(' in line:
               ener=float(line.split()[4])
           if 'Input orientation:' in line:
               coord_0=n+4
           if 'Mulliken charges:' in line and n > coord_0:
               mull_0=n+1
           if 'Sum of Mulliken charges =' in line and n > mull_0:
               mull_f=n-1
           if 'Coord Atom Element:' in line and n > mull_f:
               modes_lines=np.append(modes_lines, n)
           if 'After rot to inp orien Dipole Moment' in line:
               dip_0=n+1
           if 'After rot to inp orien Dipole Derivatives' in line:
               dip_der_0=n+1
           if 'Forces (Hartrees/Bohr)' in line:
               force_0=n+2

    ## Converting to int values returned by np.append
    modes_lines=np.array([ int(i) for i in modes_lines])

    ## Reading the file line by line to get the frequencies
    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if 'Frequencies ---' in line and n < modes_lines[-1]:
               w=np.append(w,np.array( [return_number(i) for i in line.split()[2:]] ))
           if 'Reduced masses ---' in line and n < modes_lines[-1]:
               redm=np.append(redm,np.array( [float(i) for i in line.split()[3:]] ))
           if 'Force constants ---' in line and n < modes_lines[-1]:
               fcnstmDA=np.append(fcnstmDA,np.array( [float(i) for i in line.split()[3:]] ))
           if 'IR Intensities ---' in line and n < modes_lines[-1]:
               intes=np.append(intes,np.array( [float(i) for i in line.split()[3:]] ))


    ### Flattening the frequencies array
    w=np.array(list(itertools.chain.from_iterable(np.asarray(a).ravel() for a in w)))

    ## Reading atom names, structure, forces and dipole
    n_atm=mull_f-mull_0
    n_dof=3*n_atm
    atm_names=np.genfromtxt(inp_file,unpack= True, skip_header = mull_0,max_rows=n_atm, usecols={1},dtype=str)
    r=np.genfromtxt(inp_file, skip_header = coord_0,max_rows=n_atm, usecols={3,4,5},dtype=float)
    f_au=np.genfromtxt(inp_file, skip_header = force_0,max_rows=n_atm, usecols={2,3,4},dtype=float)


    try:
        dip_0
    except NameError:
        print("*********** Warning *************   ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss:  No Dipole Information")
        dip_au=0
    else:
        dip_au=np.array([float(i.replace("D","e")) for i in np.genfromtxt(inp_file, skip_header = dip_0,max_rows=3, usecols={1},dtype=str)])


    try:
        dip_der_0
    except NameError:
        print("*********** Warning *************   ener_w_redm_fcnstmDA_intes_qcart_qmwc_fau_dipau_dipderau_r_atmnames_from_gauss:  No Dipole Derivatives Information")
        dip_der_au=0
    else:
        ## Array where I save the dipole derivatives in cartesian coordinates
        dip_der_au=np.empty([n_dof,3])
        # Reading dipole derivatives
        for dof in range(n_dof):

            ### Index, Displacement and Period in the vertical direction
            i=int(dof/5)
            di=dip_der_0
            ti=4

            ### Index, Displacement and Period in the horizontal direction
            j=dof%5
            dj=1
            tj=1

            ### Saving the three dipole components
            dip_der_au[dof]=np.array([float(i.replace("D","e")) for i in np.genfromtxt(inp_file, skip_header = i*ti + di, max_rows=3, usecols={j*tj + dj},dtype=str)])


    ## Declaring an array where I will storage the normal modes
    n_vib=len(w)
    modes=np.empty([n_vib,3*n_atm])
    # Reading normal modes
    for vib in range(n_vib):

        ### Index, Displacement and Period in the vertical direction
        i=int(vib/5)
        di=modes_lines[i]

        ### Index, Displacement and Period in the horizontal direction
        j=vib%5
        dj=3
        tj=1

        ### Saving the normal mode
        modes[vib]=np.genfromtxt(inp_file, skip_header = di, max_rows=3*n_atm, usecols={j*tj + dj},dtype=float)


    ##<a|x> The rows of this matrix are the normal modes in cartesian coordinates (not flattened). These are not orthonormal
    q_cart=no_flat(modes)

    ## This is a vector containing the forces in cartesian coordinates
    f_au_flat=flat(np.array([f_au]))[0]

    ## q_mwc=<x_mwc|a>. The columns of this matrix are the normal modes in MWC coordinates. These modes are orthonormal
    q_mwc=flat(normal(cartesian2mass_weighted(no_flat(modes),atm_names))).T[0]

    return ener,w,redm,fcnstmDA,intes,q_cart,q_mwc,f_au_flat,dip_au,dip_der_au,r,atm_names





### This function return the list of eigenvalues given a list of frequencies
def freq2eigenval(w_list):
    n=len(w_list)
    l=np.zeros([n])

    for i,w in enumerate(w_list):
        if w >= 0:
            l[i]=w**2
        if w< 0:
            l[i]=-w**2
    return l

def remove_trans_rot(rtraj,r0,imp_atms):

    rfinal=rtraj.copy()
    red_rtraj=rtraj[:,imp_atms].copy()
    red_r0=r0[imp_atms].copy()
    center=rmsd.centroid(red_r0)
    red_r0_center=red_r0-center

    for i,r in enumerate(rtraj):
        rfinal[i]=rfinal[i]-rmsd.centroid(red_rtraj[i])
        red_rtraj[i]=red_rtraj[i]-rmsd.centroid(red_rtraj[i])

        U = rmsd.kabsch(red_rtraj[i],red_r0_center)
        rfinal[i]=np.dot(rfinal[i],U)
        rfinal[i]=rfinal[i]+center


    return rfinal


## This function returns the reaction path in mass weighted coordinates from a given file with the reaction path
def XIRP_from_irp(reaction_path_inp_file):

    e_irp,rirp,atm_names=e_r_atm_names_from_xyz(reaction_path_inp_file)
    rirp=np.array([x-center_of_mass(np.array([x]),atm_names) for x in rirp])

    r0=rirp[0]

    rirp=remove_trans_rot(rirp,r0)

    its=get_max_index(e_irp)

    rr=rirp[0]
    rts=rirp[its]
    rp=rirp[-1]

    n_atm=len(rr)
    n_path=len(rirp)

    ##%%
    ## Calculating mass weighted structures and transposing to be consistent with the 1 column vector convention
    XR=np.array([cartesian2mass_weighted(np.array([rr]),atm_names)[0].flatten()]).T
    XTS=np.array([cartesian2mass_weighted(np.array([rts]),atm_names)[0].flatten()]).T
    XL=np.array([cartesian2mass_weighted(np.array([rp]),atm_names)[0].flatten()]).T
    XIRP=cartesian2mass_weighted(rirp,atm_names)
    XIRP=flat(XIRP)

    return XR,XTS,XL,XIRP,e_irp,its,rirp,atm_names




## This function returns the directions d1 and d2 and the projectors into that subspace given a file with the reaction path
def d1_d2_P1_P2_P12_I_P12_from_irp(XR,XTS,XL):

        ##%%
        ## Defining the reaction plane directions
        d1=(XR-XL)/np.linalg.norm((XR-XL))
        P1=np.matmul(d1,d1.T)


        d2=((XR-XTS)-np.matmul(P1,(XR-XTS)))/np.linalg.norm((XR-XTS)-np.matmul(P1,(XR-XTS)))
        P2=np.matmul(d2,d2.T)
        I=np.identity(len(P2))
        P12=P1+P2  ## The projector in MWC representation
        I_P12=I-P12

        return d1,d2,P1,P2,P12,I_P12



#### This function replaces a series of patterns in a file
def replace_pattern_file(inp_file,out_file,patterns,replaces):
    #input file
    fin = open(inp_file, "rt")
    #output file to write the result to
    fout = open(out_file, "wt")
    #for each line in the input file
    for line in fin:
        linef=line
        for i in range(len(patterns)):
            linef=linef.replace(patterns[i] , replaces[i])
        fout.write(linef)
    #close input and output files
    fin.close()
    fout.close()
