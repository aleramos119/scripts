
#%%
import numpy as np
import matplotlib.pyplot as plt
import sys
import mol





def main(inp_file1,col_1,inp_file2,col_2,mark,size):
    ##################################################################################################################%%

        if inp_file1=="dummy":
            y=mol.fast_load(inp_file2,np.array([col_2]),1000).T[0]
            x=np.array(range(len(y)))
            x_label="n"
            y_label=inp_file2
        else:
            x=mol.fast_load(inp_file1,np.array([col_1]),1000).T[0]
            y=mol.fast_load(inp_file2,np.array([col_2]),1000).T[0]
            y=x*y
            x_label=inp_file1
            y_label=inp_file2





        plt.style.use(['ggplot'])
        plt.plot(x,y,mark,markersize=size)
        plt.xlabel(x_label+" "+str(col_1),fontweight='bold')
        plt.ylabel(y_label+" "+str(col_2),fontweight='bold')

        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.show()
        ##%%



if __name__== "__main__":

    inp_file="/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/launch/out"
    col_1=9
    col_2=0
    mark='-'

    arg_len=len(sys.argv)

    if arg_len==4:
        inp_file1="dummy"
        col_1=""
        inp_file2=sys.argv[1]
        col_2=int(sys.argv[2])
        mark=sys.argv[3]


    if arg_len==5:
        inp_file1=sys.argv[1]
        col_1=int(sys.argv[2])
        inp_file2=inp_file1
        col_2=int(sys.argv[3])
        mark=sys.argv[4]

    if arg_len==6:
        inp_file1=sys.argv[1]
        col_1=int(sys.argv[2])
        inp_file2=sys.argv[3]
        col_2=int(sys.argv[4])
        mark=sys.argv[5]



    if mark == "-":
        size=0.05
    if mark != "-":
        size = 2

    main(inp_file1,col_1,inp_file2,col_2,mark,size)



    #t=mol.fast_load("/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/t.dat",np.array([0]),1000).T[0]
    #Et=mol.fast_load("/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/Et.dat",np.array([0]),1000).T[0]
    #u=mol.fast_load("/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/u.dat",np.array([0]),1000).T[0]

    #plt.plot(t,Et)
    #plt.plot(t,u)
    #plt.plot(t,Et/u)

    #T=3.57
    #2*np.pi/T

    #x2=mol.fast_load("/home/ar612/Documents/git_kraken/eigen_quantum_propagation/results/t_monitor_0.txt",np.array([0]),1000).T[0]
    #y2=mol.fast_load("/home/ar612/Documents/git_kraken/eigen_quantum_propagation/results/t_monitor_0.txt",np.array([1]),1000).T[0]

    #x3=mol.fast_load("/home/ar612/Documents/Work/irep/test/collocation/fermi_res_gamma_0.5_w1_2_w2_1_nx_3_ny_6_T_90/Chebyshev/Chebyshev_140/t.dat",np.array([0]),1000).T[0]
    #y3=mol.fast_load("/home/ar612/Documents/Work/irep/test/collocation/fermi_res_gamma_0.5_w1_2_w2_1_nx_3_ny_6_T_90/Chebyshev/Chebyshev_140/u.dat",np.array([0]),1000).T[0]

    #%matplotlib qt
    #plt.plot(x1,y1,label="lagrange")
    #plt.plot(x2,y2,label="spline")
    #plt.plot(x3,y3,'o')
    #plt.legend()
