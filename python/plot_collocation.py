
#%%
import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['seaborn-whitegrid'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
from itertools import islice
import glob
%matplotlib qt
plt.style.use(['ggplot'])




##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%
abc=[chr(i) for i in range(97,123)]

def plot1(x,y,xlabel,ylabel,lsize,ax,plotlabel=" ",mark="-"):
    plt.rc('font', size=lsize)
    ax.plot(x,y,mark,linewidth=10.0,label=plotlabel)
    ax.set_xlabel(xlabel,size=lsize)
    ax.set_ylabel(ylabel,size=lsize)
    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(2.5)
        ax.spines[axis].set_color("black")
    ax.grid(False)
    ax.tick_params(labelsize=size)
    ax.tick_params(direction='out', length=10, width=4)
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))


def i_max(e_list,vmax):
    for i,e in enumerate(e_list):
        if e < vmax:
            i_max=i
    return i_max

############## Ectract the ith element of a line matching the given pattern #################


def extract_pattern(inp_file,pattern,i,number=True):

    e=np.zeros([0])

    with open(inp_file) as file:
       for n, line in enumerate(file, 1):
           if pattern in line:
               if number:
                   e=np.append(e,float(line.split()[i]))
               else:
                   e=np.append(e,line.split()[i])

    return e


def read_psopt_output(out_file):

    collocation=None
    nnodes=None
    solved=False
    niter=None
    cost=None
    end_cost=None
    run_cost=None
    local_err=None
    t=None

    with open(out_file) as file:
       for n, line in enumerate(file, 1):
           if "Collocation method:" in line:
               collocation=line.split()[-1]
           if "Number of nodes phase 1:" in line:
               nnodes=int(line.split()[-1])
           if "running with linear solver" in line:
               solver=line.split()[-1].replace('.','')
           if "Number of Iterations....:" in line:
               niter=int(line.split()[-1])

           if "*** The problem has been solved!" in line:
               solved=True
           if solved:
               if "Optimal (unscaled) cost function value:" in line:
                   cost=float(line.split()[-1])
               if "Phase 1 endpoint cost function value:" in line:
                   end_cost=float(line.split()[-1])
               if "Phase 1 integrated part of the cost:" in line:
                   run_cost=float(line.split()[-1])
               if "Phase 1 maximum relative local error:" in line:
                   local_err=float(line.split()[-1])
               if "Runtime is:" in line and "sec" in line:
                   t=float(line.split()[-2])/60


    return collocation,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t



##################################################################################################################%%
################################ Ploting Functions ###############################################################%%
##################################################################################################################%%



##################################################################################################################%%
################################ Ploting Collocation ##########################################################%%
##################################################################################################################%%
test_dir="/home/ar612/Documents/Work/cavities/test/linear"


collocation_dir_list=glob.glob(test_dir+"/*")

collocation_list=[]
nodes_list_list=[]
cost_list_list=[]
end_cost_list_list=[]
run_cost_list_list=[]
local_error_list_list=[]
time_list_list=[]

for collocation_dir in collocation_dir_list:
    collocation=collocation_dir.split("/")[-1].split("_")[0]
    collocation_list.append(collocation)
    nodes_dir_list=glob.glob(collocation_dir+"/*")

    nodes_list=[]
    cost_list=[]
    end_cost_list=[]
    run_cost_list=[]
    local_error_list=[]
    time_list=[]

    for nodes_dir in nodes_dir_list:
        job_name=nodes_dir.split("/")[-1]
        out_file_list=glob.glob(nodes_dir+"/"+job_name+"*.out")
        if len(out_file_list) != 0:
            out_file=out_file_list[-1]
            colloc,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t=read_psopt_output(out_file)
            if solved:
                nodes_list.append(nnodes)
                cost_list.append(cost)
                end_cost_list.append(end_cost)
                run_cost_list.append(run_cost)
                local_error_list.append(local_err)
                time_list.append(t)
            else:
                print("Optimization failed for:",collocation,nnodes)


    nodes_list_list.append(nodes_list)
    cost_list_list.append(cost_list)
    end_cost_list_list.append(end_cost_list)
    run_cost_list_list.append(run_cost_list)
    local_error_list_list.append(local_error_list)
    time_list_list.append(time_list)

collocation_list

##%%
fig, ax = plt.subplots(1, 1,sharex=True,figsize=(16,9), gridspec_kw={'hspace': 0.3,'wspace': 0.18})
size=30

for i in range(len(collocation_list)):
    plot1(nodes_list_list[i],local_error_list_list[i],' nodes','err',size,ax,plotlabel=collocation_list[i],mark="o")
ax.grid(True)
ax.legend()
fig.savefig(test_dir+"/error.pdf", bbox_inches='tight')
##%%


##%%
fig, ax = plt.subplots(1, 1,sharex=True,figsize=(16,9), gridspec_kw={'hspace': 0.3,'wspace': 0.18})
size=30

for i in range(len(collocation_list)):
    plot1(nodes_list_list[i],time_list_list[i],' nodes','t(min)',size,ax,plotlabel=collocation_list[i],mark="o")
ax.grid(True)
ax.legend()
fig.savefig(test_dir+"/time.pdf", bbox_inches='tight')
##%%




##%%
fig, ax = plt.subplots(1, 1,sharex=True,figsize=(16,9), gridspec_kw={'hspace': 0.3,'wspace': 0.18})
size=30

for i in range(len(collocation_list)):
    plot1(nodes_list_list[i],cost_list_list[i],' nodes','cost',size,ax,plotlabel=collocation_list[i],mark="o")
ax.grid(True)
ax.legend()
fig.savefig(test_dir+"/cost.pdf", bbox_inches='tight')
##%%


##%%
fig, ax = plt.subplots(1, 1,sharex=True,figsize=(16,9), gridspec_kw={'hspace': 0.3,'wspace': 0.18})
size=30

for i in range(len(collocation_list)):
    plot1(nodes_list_list[i],end_cost_list_list[i],' nodes','end_cost',size,ax,plotlabel=collocation_list[i],mark="o")
ax.grid(True)
ax.legend()
fig.savefig(test_dir+"/end_cost.pdf", bbox_inches='tight')
##%%


##%%
fig, ax = plt.subplots(1, 1,sharex=True,figsize=(16,9), gridspec_kw={'hspace': 0.3,'wspace': 0.18})
size=30

for i in range(len(collocation_list)):
    plot1(nodes_list_list[i],run_cost_list_list[i],' nodes','run_cost',size,ax,plotlabel=collocation_list[i],mark="o")
ax.grid(True)
ax.legend()
fig.savefig(test_dir+"/run_cost.pdf", bbox_inches='tight')
##%%




##################################################################################################################%%
################################ Ploting Collocation ##########################################################%%
##################################################################################################################%%
test_dir="/home/ar612/Documents/Work/cavities/excitation_state_1/random_umax_0.2_trapezoidal_500"


project_dir_list=glob.glob(test_dir+"/*")

eta_list=[]
cost_list=[]
end_cost_list=[]
run_cost_list=[]


for project_dir in project_dir_list:
    collocation=collocation_dir.split("/")[-1].split("_")[0]
    collocation_list.append(collocation)
    nodes_dir_list=glob.glob(collocation_dir+"/*")

    nodes_list=[]
    cost_list=[]
    end_cost_list=[]
    run_cost_list=[]
    local_error_list=[]
    time_list=[]

    for nodes_dir in nodes_dir_list:
        job_name=nodes_dir.split("/")[-1]
        out_file_list=glob.glob(nodes_dir+"/"+job_name+"*.out")
        if len(out_file_list) != 0:
            out_file=out_file_list[-1]
            colloc,nnodes,solver,niter,solved,cost,end_cost,run_cost,local_err,t=read_psopt_output(out_file)
            if solved:
                nodes_list.append(nnodes)
                cost_list.append(cost)
                end_cost_list.append(end_cost)
                run_cost_list.append(run_cost)
                local_error_list.append(local_err)
                time_list.append(t)
            else:
                print("Optimization failed for:",collocation,nnodes)


    nodes_list_list.append(nodes_list)
    cost_list_list.append(cost_list)
    end_cost_list_list.append(end_cost_list)
    run_cost_list_list.append(run_cost_list)
    local_error_list_list.append(local_error_list)
    time_list_list.append(time_list)

collocation_list
