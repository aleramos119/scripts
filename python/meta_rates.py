
#%%
import numpy as np
%matplotlib qt
import matplotlib.pyplot as plt
import sys
from scipy import interpolate
from timeit import default_timer as timer
from itertools import islice
import mol


def gauss(x,y,x0=0,sx=1,y0=0,sy=1,h=1):
        return h*np.exp(-np.power((x - x0)/sx, 2.)/2.-np.power((y - y0)/sy, 2.)/2.)

def alfa(ct,cc1,cc2,ht,hc1,hc2,hs1,hs2,hh,hb,beta,t_final,nx=100,ny=100):
    #### Defining the grid
    x, y = np.mgrid[-np.pi:np.pi:complex(0,nx), -np.pi:np.pi:complex(0,ny)]
    dx=x[1,0]-x[0,0]
    dy=y[0,1]-y[0,0]
    dt=ct[1]-ct[0]

    #### Interpolation before the first gaussian deposition
    z=np.zeros([nx,ny])
    int=interpolate.RectBivariateSpline(x[:,0], y[0,:], z)


    #### Running through all elements of ct and calculating average dependent on time
    sum_list=np.empty([t_final])
    sum=0
    k=0
    for i in range(0,t_final):
        sum=sum+np.exp(beta*int(cc1[i],cc2[i]))*dt
        #sum_list[i]=sum/ct[i]
        sum_list[i]=int(cc1[i],cc2[i])
        if ct[i]==ht[k]:
            zgauss=gauss(x,y,hc1[k],hs1[k],hc2[k],hs2[k],hh[k])
            z=z+zgauss
            int=interpolate.RectBivariateSpline(x[:,0], y[0,:], z)
            k=k+1

    return sum_list




    # ##########%%
    # start = timer()
    # znew=np.zeros([125,125])
    # for i in range(0,len(spath)):
    #     znew=znew+gauss(xnew,ynew,spath[i],sigma_spath[i],zpath[i],sigma_zpath[i],height[i])
    # znew=-znew
    # min=np.min(znew)
    # znew=znew-min
    # end = timer()
    # print(end-start)
    # ##########%%
    # np.min(znew)
    #
    #
    # zgauss=gauss(x,y,hc1[2],hs1[2],hc2[2],hs2[2],hh[2])
    # zgauss.shape
    # z=z+zgauss
    #
    # int=interpolate.RectBivariateSpline(x[:,0], y[0,:], z)
    #
    # z_int=int(x[:,0], y[0,:])
    #
    #
    # ##%%
    # fig, ax = plt.subplots(1, 1, figsize=(7,7))
    # plt.pcolor(x, y, zgauss,cmap='rainbow_r')
    # cbar=plt.colorbar()
    # ax.set_aspect('equal')
    # ax.set_xlabel('colvar 1')
    # ax.set_ylabel('colvar 2')
    # plt.show()
    # #%%
    #
    # z=int(xnew[:,0],ynew[0,:])




if __name__== "__main__":

    hills="/cluster/data/ar612/cp2k_organized/meta_dyn/dih_hnco_dih_coch_meta_dyn_reduced/HILLS"
    colvar="/cluster/data/ar612/cp2k_organized/meta_dyn/dih_hnco_dih_coch_meta_dyn_reduced/colvar"
    T=298
    beta=1/(mol.k*mol.J2kcalmol*T)

    ht,hc1,hc2,hs1,hs2,hh,hb=np.genfromtxt(hills,unpack= True, skip_header = 1)
    ct,cc1,cc2=mol.fast_load(colvar,np.array([0,1,2]),1000).T

    n=len(ct)

    alfa_t=alfa(ct,cc1,cc2,ht,hc1,hc2,hs1,hs2,hh,hb,beta,1000000)

    mol.write_matrix_to_file(np.array([ct,alfa_t]).T,"# t(ps)  alfa(t)","  ","/cluster/data/ar612/cp2k_organized/meta_dyn/dih_hnco_dih_coch_meta_dyn_reduced/alfa_t")


    plt.plot(ct[:1000000],alfa_t)

ct[:100]

1.70/100000*len(ct)


20*np.pi/180



    #job="colvar"


    # inp_file=sys.argv[1]
    # job=sys.argv[2]

    main(inp_file,job)
