
#%%
import numpy as np
from numpy import linalg
from scipy import interpolate
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import quantum as quan
from scipy import integrate
import glob




############### Reading the data #######################
##%%
htrans_dir="/home/ar612/Documents/Work/cavities/H_transfer/data_polstates_2d"

sys="N1_3_e0_0.0005764776761753108_mu0_0.0025174068699653565"
sys_dir=htrans_dir+"/"+sys

eta_dir_list=sorted(glob.glob(sys_dir+"/*"))

eta_list=[]
h11_list=[]
d11_list=[]

for eta_dir in eta_dir_list:
    eta=eta_dir.split("/")[-1].split("_")[-1]
    eta_list.append(float(eta))


    h11_list.append(np.genfromtxt(eta_dir+"/h11.txt"))
    d11_list.append(np.genfromtxt(eta_dir+"/d11.txt"))


 eta_array=np.array(eta_list)
 h11_array=np.array(h11_list)
 d11_array=np.array(d11_list)


#%%
i=0
j=1

d11_int_model=interpolate.CubicSpline(eta_array, d11_array[:,i,j])
eta_int=np.linspace(eta_array[0],eta_array[-1],100)

d11_int=np.array([d11_int_model(eta) for eta in eta_int])


plt.plot(eta_array,d11_array[:,i,j],'o',label="data point")
plt.plot(eta_int,d11_int,'-',label="interpolation")
##%%

d11_int_model.c
