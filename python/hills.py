
#%%
import numpy as np
#%matplotlib qt
import matplotlib.pyplot as plt
import sys
import mol

#hills_file="/home/ar612/Documents/Work/cp2k_organized/well_temp/ntad1_1_chl_195_300K_30A_dho_ho5_dho_ho6_well_temp_reduced/HILLS"
def read_hills_data(hills_file):

    nthf0=-1
    dim=2
    name_list=[]


    with open(hills_file) as file:
       for n, line in enumerate(file, 1):
           if "FIELDS" in line:
               dim=int((len(line.split())-4)/2)
               name_list=line.split()[3:3+dim]

    return name_list


def main(inp_file,job):
    ##################################################################################################################%%
    #time,spath,zpath,sigma_spath,sigma_zpath,height,biasf=np.genfromtxt(inp_file,unpack= True, skip_header = 1)
    ht,hc1,hc2,hs1,hs2,hh,hb=np.genfromtxt(inp_file,unpack= True, skip_header = 1)
    name_list=read_hills_data(inp_file)



    if(job=="colvar"):
        plt.style.use(['ggplot'])
        plt.errorbar(hc1, hc2, xerr=hs1/2, yerr=hs2/2,fmt='o',color="tab:red",markersize=3)
        plt.xlabel(name_list[0],fontweight='bold')
        plt.ylabel(name_list[1],fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.show()
    elif(job=="time"):
        plt.style.use(['ggplot'])
        fig=plt.figure(figsize=(20,10))
        ax=plt.subplot(221)
        plt.errorbar(ht, hc1,yerr=hs1/2,fmt='o',color="tab:red",markersize=3)
        plt.xlabel("t (ps)",fontweight='bold')
        plt.ylabel(name_list[0],fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)

        ax=plt.subplot(222)
        plt.errorbar(ht, hc2,yerr=hs2/2,fmt='o',color="tab:blue",markersize=3)
        plt.xlabel("t (ps)",fontweight='bold')
        plt.ylabel(name_list[1],fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)

        ax=plt.subplot(223)
        plt.plot(ht,hh,'o',color="tab:blue",markersize=3)
        plt.xlabel("t (ps)",fontweight='bold')
        plt.ylabel("height (kcal/mol)",fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        plt.show()

    elif(job=="fes"):
        xmin=np.min(hc1)-2*hs1[0]
        xmax=np.max(hc1)+2*hs1[0]
        ymin=np.min(hc2)-2*hs2[0]
        ymax=np.max(hc2)+2*hs2[0]

        x, y = np.mgrid[xmin:xmax:100j,ymin:ymax:100j]
        z=np.zeros([100,100])

        for i in range(0,len(hc1)):
            z=z+mol.gauss(x,y,hc1[i],hs1[i],hc2[i],hs2[i],hh[i])
        z=-z
        zmin=np.min(z)
        z=z-zmin
        #%%
        plt.style.use(['ggplot'])
        fig=plt.figure(figsize=(10,10))
        ax=fig.add_subplot(111)
        plt.contourf(x,y,z,30,cmap='rainbow',alpha=.55)
        cbar=plt.colorbar()
        cbar.ax.set_title('kcal/mol')
        C=plt.contour(x,y,z,30, colors='black', linewidths=0.6)
        plt.clabel(C, inline=1, fontsize=10)
        plt.xlabel(name_list[0],fontweight='bold')
        plt.ylabel(name_list[1],fontweight='bold')
        plt.tick_params(labelsize=14)
        plt.grid(True)
        #ax.set_aspect('equal')
        plt.show()
        #%%

if __name__== "__main__":

    # inp_file="/cluster/data/ar612/cp2k_organized/meta_dyn/dih_hnco_dih_coch_meta_dyn_reduced/HILLS"
    # job="fes"


    inp_file=sys.argv[1]
    job=sys.argv[2]

    main(inp_file,job)


mol.k*mol.J2kcalmol*298
