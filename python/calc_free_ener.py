#%%
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules/')
import mol
import numpy as np
from itertools import islice
from timeit import default_timer as timer
import matplotlib.pyplot as plt

def read_lag_mult(lag_inp_file,n_cons):

    l=mol.file_len(lag_inp_file)
    chunk=int(np.ceil(n_cons/4))
    lag_lenght=l/(2*chunk)

    lag_mult_list=np.zeros([lag_lenght,n_cons])


    with open(lag_inp_file) as finp:
        i=0
        while True:
            next_n_lines = list(islice(finp, np.ceil(n_cons/4)))  #### Reading the file in slices of n_tot+2
            if not next_n_lines or i==lag_lenght:
                break
            if "Shake" in next_n_lines[0]:
                lag_mult=np.zeros([chunk,4])
                j=0
                for line in next_n_lines:
                    lag_mult[j]=np.array(map(float,line.split()[-4:]))
                    j=j+1
                flat_lag_mult=lag_mult.flatten()
                if len(flat_lag_mult) == n_cons:
                    lag_mult_list[i]=np.array(flat_lag_mult)
                i=i+1
    return lag_mult_list

def form_dD(r,dr,n_cons,n_images):

    n_atm=len(r[0])

    D=np.zeros([n_images,n_cons])
    dD=np.zeros([n_images-1,n_cons])



    for k in range(0,n_images):

        n=0
        for i in range(0,n_atm):
            for j in range(i+1,n_atm):
                D[k,n]=np.linalg.norm(r[k,i]-r[k,j])  ## Here I calculate the elements of the distance matrix

                if k < n_images-1 and i!= j:
                    dD[k,n]=np.sum( (r[k,i]-r[k,j])*(dr[k,i]-dr[k,j]) ) / D[k,n] ## Here I calculate the elements of the variation of the distance matrix
                                                                                     ## as a function of the variations of the position vectors

                n=n+1
        #if k < n_images-1:
        #    dD[k]=dD[k]/np.linalg.norm(dD[k])


    return D,dD

def force_along_reac_path(dD,lag_mult):

    T=len(lag_mult) ## The number of time steps

    dH_dchi=np.empty([T]) ## In this array I storage the force along the reaction path for each time step

    ## Calculating the projection
    for j in range(0,T):
        dH_dchi[j]=np.dot(dD,lag_mult[j])

    return dH_dchi



def compute_free_ener(f_along_path,std_f_along_path,n_images):

    free_ener=np.zeros([n_images])
    std_free_ener=np.zeros([n_images])


    sum=0
    free_ener[0]=0
    std_free_ener[0]=0
    for i in range(1,n_images):
        sum= sum + f_along_path[i-1]
        free_ener[i]=sum
        std_free_ener[i]=np.sqrt(np.sum(std_f_along_path[0:i]**2))


    return free_ener,std_free_ener







#########################################################%%

if __name__== "__main__":



        lag_inp_file_pattern="/cluster/data/ar612/cp2k_organized/thermo_int/r19_ts_p_reduced/all_ener/$$_r19_ts_p_all_ener/$$_r19_ts_p_all_ener-1.LagrangeMultLog"
        reac_path_file="/cluster/data/ar612/cp2k_organized/reac_path/r19_ts_p_band_b3lyp_40_rep/r19_ts_p_band_b3lyp_movie.xyz"
        free_ener_out_file="/cluster/data/ar612/cp2k_organized/thermo_int/r19_ts_p_reduced/r19_ts_p_free_ener.txt"
        std_free_ener_out_file="/cluster/data/ar612/cp2k_organized/thermo_int/r19_ts_p_reduced/r19_ts_p_std_free_ener.txt"
        n_images=40
        n_atm=33
        dh_avg_file_pattern="/cluster/data/ar612/cp2k_organized/reac_path/r19_ts_p\_band_reduced/r19_ts_p__dH_dchi__\$\$.txt"  ## File pattern to which I'm going to save <dH(i)_dchi> as a function of the time step
        dh_avg_std_file_pattern="/cluster/data/ar612/cp2k_organized/reac_path/r19_ts_p\_band_reduced/r19_ts_p_std__dH_dchi__\$\$.txt"


        # Collecting data
        # lag_inp_file_pattern=sys.argv[1]  ## This is the pattern of the files from which I exctract the Lagrange multipliers
        # reac_path_file=sys.argv[2]  ## This is the reaction path files from which I ectract the structures of the reaction path
        # free_ener_out_file=sys.argv[3]  ## File to where I'm going to save the free energy profile A(chi). This is the integral of the last value of <dH(\$\$)_dchi> as a function of the time step
        # std_free_ener_out_file=sys.argv[4]  ## File to where I'm going to save the standard deviation of A(chi). Taking into account the standar deviation of <dH(\$\$)_dchi> and tha fact that I'm integrating all this values.
        # n_images=int(sys.argv[5])
        # n_atm=int(sys.argv[6])
        # dh_avg_file_pattern=sys.argv[7]  ## File pattern to which I'm going to save <dH(i)_dchi> as a function of the time step
        # dh_avg_std_file_pattern=sys.argv[8]  ## File pattern to which I'm going to save the standard deviation of <dH(\$\$)_dchi> as a function of the time step


        n_cons=(n_atm**2-n_atm)/2  ## Number of constraint in the simulation. All distances between atoms


        ## Here I read the geometries of the reaction path to calculate latter dr and dist
        e,r,atm_names=mol.e_r_atm_names_from_xyz (reac_path_file,n_atm)
        dr,dist=mol.path_dist(r)


        ## Here I calculate the distance matrix D and the variation dD for each point in the reaction path.
        D,dD=form_dD(r,dr,n_cons,n_images)


        ## Declaring the list that are going to store the average force along the reaction path (<dH/dchi>) and its standard deviation.
        f_along_path_list=np.zeros([n_images-1])
        std_f_along_path_list=np.zeros([n_images-1])

        ## Running through all images and computing averages
        for i in range(0,n_images-1):

            # if i%2==0:
            #     image_name=str((i+2)/2) ## Name of the image
            #
            # if i%2!=0:
            #     image_name=str((i+2.)/2) ## Name of the image
            image_name=str(i+1)

            ## Creating names for Lagrange multipliers file, averages <dH(i)_dchi> and standard deviation
            lag_inp_file=lag_inp_file_pattern.replace("$$",image_name)
            dh_avg_file=dh_avg_file_pattern.replace("$$",image_name)
            dh_avg_std_file=dh_avg_std_file_pattern.replace("$$",image_name)


            ## Reading Lagrange multipliers from file for every step for image i
            ##########################%%
            #start = timer()
            lag_mult=read_lag_mult(lag_inp_file,n_cons)
            #end = timer()
            #print(end-start)
            #################%%

            ## Computing the force along reac_path for step i and every time step
            dH_dchi=force_along_reac_path(dD[i],lag_mult)

            %matplotlib inline
            plt.style.use(['ggplot'])
            fig=plt.figure()
            plt.plot(np.arange(0,0.0005*len(lag_mult),0.0005),dH_dchi*mol.Eh2kcalmol)
            fig.savefig("/cluster/data/ar612/cp2k_organized/thermo_int/r19_ts_p_reduced/all_ener/$$_r19_ts_p_all_ener/".replace("$$",image_name)+"dH_dchi.pdf")

            # dH_dchi_avg=mol.comp_avg(dH_dchi)
            # dH_dchi_avg_std=mol.comp_std(dH_dchi_avg)
            #
            # mol.write_matrix_to_file(dH_dchi_avg,"","",dh_avg_file)
            # mol.write_matrix_to_file(dH_dchi_avg_std,"","",dh_avg_std_file)
            #
            #
            # f_along_path_list[i]=dH_dchi_avg[-1]
            # std_f_along_path_list[i]=dH_dchi_avg_std[-1]



        free_ener,std_free_ener=compute_free_ener(f_along_path_list,std2_f_along_path_list,n_images)

        mol.write_matrix_to_file(free_ener,"","",free_ener_out_file)
        mol.write_matrix_to_file(std_free_ener,"","",std_free_ener_out_file)
