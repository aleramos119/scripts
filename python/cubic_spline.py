#! /bin/python3
#%%
import numpy as np
import sys
sys.path.append('$PYDIR')
import mol
from scipy import interpolate
import matplotlib.pyplot as plt


def even_space(inp_file_1,inp_file_2,out_file_1,out_file_2,n):

    t=np.genfromtxt(inp_file_1)
    efield=np.genfromtxt(inp_file_2)

    efield_int=interpolate.CubicSpline(t, efield)

    T=t[-1]-t[0]
    dt=T/n

    t_even=np.linspace(t[0]-2*dt,t[-1]+2*dt,n+4)

    e_even=np.array([efield_int(t) for t in t_even])

    m0=(efield[2]-efield[1])/(t[2]-t[1])
    mf=(efield[-2]-efield[-3])/(t[-2]-t[-3])

    e_even[0]=efield[0]+m0*(t_even[0]-t[0])
    e_even[1]=efield[0]+m0*(t_even[1]-t[0])

    e_even[-1]=efield[-1]+mf*(t_even[-1]-t[-1])
    e_even[-2]=efield[-1]+mf*(t_even[-2]-t[-1])


    mol.write_matrix_to_file(t_even,"","",out_file_1)
    mol.write_matrix_to_file(e_even,"","",out_file_2)


    plt.plot(t_even,e_even,'-')
    plt.plot(t,efield,'o')
    plt.tick_params(labelsize=14)
    plt.grid(True)
    plt.show()





if __name__== "__main__":

    # inp_file_1="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/t.dat"
    # inp_file_2="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/u.dat"
    # out_file="/home/ar612/Documents/Work/quantum_psopt/control/double_well/high_emax/emax_30/T_5000/efield.txt"



    inp_file_1="t.dat"
    inp_file_2="u.dat"
    out_file_1=inp_file_1.replace(".dat","_cubicspline.dat")
    out_file_2=inp_file_2.replace(".dat","_cubicspline.dat")

    n=10000

    #n=500


    even_space(inp_file_1,inp_file_2,out_file_1,out_file_2,n)
