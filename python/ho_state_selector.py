
#%%
import sys
# import os
# sys.path.append(os.environ['PYTHON_SCRIPTS'])
# import mol
#
# state_index_file="/home/ar612/Downloads/cp2k/scripts/cp_rest_states_index.txt"
# eigenvec_file="/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.025_w1_0.014_w2_0.007_m1_1837.18_m2_1837.18_nx_3_ny_6/eigenvec.txt"
# vec="column"

def read_matrix_from_file(file_name, nformat):
    with open(file_name, 'r') as fd_read:
        ret_mxt = [list(map(nformat, line.split())) for line in fd_read]
    return ret_mxt

def write_matrix_to_file(nmatrix, file_name):
    save_str = '\n'.join([' '.join(map(str, i)) for i in nmatrix])
    with open(file_name, 'w') as fd_write:
        fd_write.write(save_str)

def transpose(nmatrix):
    return list(map(list, zip(*nmatrix)))

def ho_state_selector(state_index_file,eigenvec_file,emax,vec="row"):

    state_index=read_matrix_from_file(state_index_file,str)
    state_index=[[state[0],int(state[1])] for state in state_index]
    eigenvec=read_matrix_from_file(eigenvec_file,float)

    nselect=len(state_index)
    
    selected_states=[]

    for i in range(nselect):
        if state_index[i][0] == "ho":
            selected_states.append(eigenvec[state_index[i][1]][:emax]+([0]*emax))
        if state_index[i][0] == "eigen":
            eigen_state=[0]*2*emax
            eigen_state[state_index[i][1]]=1
            selected_states.append(eigen_state)


    out_file=state_index_file.replace("_index","")
    write_matrix_to_file(selected_states,out_file)






if __name__== "__main__":

    # state_index_file="/home/ar612/Documents/git_kraken/psopt_oct_gauss/PSOPT/examples/irep/cp_rest_states_index.txt"
    # eigenvec_file="/home/ar612/Documents/Work/quantum_psopt/systems/fermi_res_gamma_0.1_w1_2_w2_1_m1_1_m2_1_nx_10_ny_20/eigenvec.txt"
    # emax=18
    # vec="row"

    state_index_file=sys.argv[1]
    eigenvec_file=sys.argv[2]
    emax=int(sys.argv[3])
    vec=sys.argv[4]



    ho_state_selector(state_index_file,eigenvec_file,emax,vec)
