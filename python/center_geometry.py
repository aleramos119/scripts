#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol

import numpy as np


###################################### FES P ########################################%%
molecule="pheny_cyclo_trimer"

file_temp="/data/ar612/cp2k_organized/XYZ/$$.xyz"

inp=file_temp.replace("$$",molecule)
out=file_temp.replace("$$",molecule)+"temp"


e,r,atm_names=mol.e_r_atm_names_from_xyz(inp,ener="n")

r_disp=mol.displace_center_coord(r,[10,10,10])

mol.write_trajectory(r_disp,range(0,len(r_disp)),atm_names,out)
