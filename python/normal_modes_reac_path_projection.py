
#%%
import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import glob
import matplotlib.pyplot as plt
%matplotlib qt
plt.style.use(['ggplot'])
import subprocess
import rmsd

plt.rc('font', size=15)


def plot_reac_path_projection(XIRP,XR,Q,e_irp,weights,cuttof,out_fig_file):

    n_vib=len(Q[0])
    n_path=len(XIRP)

    proj=np.zeros([n_vib,n_path-1])
    for i in range(n_vib):
        for j in range(1,n_path):
            dr=(XIRP-XR)[j][:,0]/np.linalg.norm((XIRP-XR)[j][:,0])
            dr=np.multiply(dr,weights)
            proj[i,j-1]=np.dot(dr,Q[:,i])

    ##%%
    fig, ax1 = plt.subplots(figsize=(16,9))
    i_select=[]
    for i in range(n_vib):
        if max(abs(proj[i])) > cuttof:
            ax1.plot(proj[i],"-",label=str(i))
            ax1.text(0,proj[i][0],str(i),horizontalalignment='right')
            ax1.text(n_path-2,proj[i][n_path-2],str(i),horizontalalignment='left')
            i_select.append(i)
    color2="tab:red"
    ax1.set_xlabel("step")
    ax1.set_ylabel("proj",color=color2)
    ax1.tick_params(axis='y', labelcolor=color2)
    ax1.ticklabel_format(style='plain')
    ax1.legend(loc="upper center",ncol=8)

    ax2 = ax1.twinx()
    color2="tab:blue"
    ax2.set_ylabel('E (kcal/mol)', color=color2)  # we already handled the x-label with ax1
    ax2.plot((e_irp-e_irp[0])*mol.Eh2kcalmol,'o', color=color2)
    ax2.ticklabel_format(style='plain')
    ax2.tick_params(axis='y', labelcolor=color2)


    ax1.legend(loc="upper center",ncol=16)

    fig.set_size_inches(16,9,forward=False)

    fig.savefig(out_fig_file)


    ##%%

    return proj,np.array(i_select)


def plot_reac_plane_projection(P12,q_mwc_reac):

    n_vib=len(q_mwc_reac[0])

    DYk=np.zeros([n_vib])
    for i in range(n_vib):
        DYk[i]=np.linalg.norm(np.matmul(P12,np.array([q_mwc_reac[:,i]]).T))

    ##%%
    plt.bar(range(1,n_vib+1),DYk)
    plt.xlabel("mode")
    plt.ylabel(r"$reac_plane_projection$")
    ##%%





def replace_pattern_file(inp_file,out_file,patterns,replaces):
    #input file
    fin = open(inp_file, "rt")
    #output file to write the result to
    fout = open(out_file, "wt")
    #for each line in the input file
    for line in fin:
        linef=line
        for i in range(len(patterns)):
            linef=linef.replace(patterns[i] , replaces[i])
        fout.write(linef)
    #close input and output files
    fin.close()
    fout.close()

def create_dir(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)

def flat(traj):
    return np.array([ r.flatten() for r in traj])

def modes2Q(modes,atm_names):
    return flat(mol.normal(mol.cartesian2mass_weighted(modes,atm_names))).T





def main (reac_path_file,reac_modes_file,modes_select_file):

    ##%%
    ### Reading reaction path in mwc
    w,int,modes,r0,atm_names=mol.w_modes_r_atm_names_from_molden(reac_modes_file)
    XR,XTS,XL,XIRP,e_irp,its,atm_names=mol.XIRP_from_irp(reac_path_file,r0)
    Q=modes2Q(modes,atm_names)

    ### Creating the weights vector depending on which atoms do I consider important
    n_atm=len(atm_names)
    list_important_atoms=[0,1,2,3,14,15,16,33,34,35,52,53,54]
    #list_important_atoms=list(range(n_atm))

    weights=np.zeros(3*n_atm)

    for i in list_important_atoms:
        weights[3*i+0]=1
        weights[3*i+1]=1
        weights[3*i+2]=1

    ### Computing projection in mwc
    proj,i_select_mwc=plot_reac_path_projection(XIRP,XR,Q,e_irp,weights,0.2,projections_mwc_file)
    select_modes_mwc=np.array([ modes[i] for i in i_select_mwc ])
    mol.write_modes_2_nmd(modes_mwc_select_file,atm_names,r0,select_modes_mwc,i_select_mwc)


    ### Reading reaction path in cc
    e_irp,rirp,atm_names=mol.e_r_atm_names_from_xyz(reac_path_file)
    rirp=mol.remove_trans_rot(rirp,r0)
    rirp=np.array([ np.array([r.flatten()]).T for r in rirp])
    normal_modes=flat(mol.normal(modes)).T

    ### Computing projection in cc
    proj,i_select_cc=plot_reac_path_projection(rirp,rirp[0],normal_modes,e_irp,weights,0.15,projections_cc_file)
    select_modes_cc=np.array([ modes[i] for i in i_select_cc ])
    mol.write_modes_2_nmd(modes_cc_select_file,atm_names,r0,select_modes_cc,i_select_cc)





###########################################################################%%


if __name__== "__main__":

    ##%%

    project_name="aptts2_nx_-2_2_50_ny_-1.5_2.5_50_dftb_eps_10e-6_ref_traj-pos-1_emax_80"

    reac_path_file="/home/ar612/Documents/Work/gaussian/irp/reac_path/pctts1_irc.xyz"
    reac_modes_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/conf00-normal-modes.mldf"
    modes_mwc_select_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/0pctts1irc_select_mwc.nmd"
    modes_cc_select_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/0pctts1irc_select_cc.nmd"
    projections_mwc_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/0pctts1irc_projections_mwc.pdf"
    projections_cc_file="/home/ar612/Documents/Work/normcor-1.0/b3lyp_0pctts1irc/0pctts1irc_projections_cc.pdf"



    main(reac_path_file,reac_modes_file,modes_select_file)
