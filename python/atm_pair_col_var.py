#%%
import os
import sys
sys.path.append(os.environ['PYTHON_SCRIPTS'])
import mol
import numpy as np


bond_cut=1.3  ## Bond length cuttof
var=0.2   ## The lenght of the bond variation that I will allowd (To be added to bond equilibrium distance)


xyz_inp_file="/home/ar612/Documents/Work/cp2k_organized/xyz/pctreac1_oh_dist_1.05.xyz"
## Reading positions from the reac_path file
e,r,atm_names=mol.e_r_atm_names_from_xyz(xyz_inp_file)
r=r[0]


natm=len(r)


## Pair of atoms with a distance less that bond_len in the first frame of the reac_path
pair0=[[i,j] for i,x in enumerate(r) for j,y in enumerate(r) if np.linalg.norm(x-y)<bond_cut and i < j and atm_names[i]=='C' and atm_names[j]=='H']


cont=1
for i,j in pair0:
    colvar="bond_ch_"+str(cont)
    print(colvar+":  DISTANCE ATOMS="+str(i+1)+","+str(j+1)+" NOPBC \n")
    cont=cont+1


cont=1
for i,j in pair0:
    colvar="bond_ch_"+str(cont)
    print(colvar+",")
    cont=cont+1
