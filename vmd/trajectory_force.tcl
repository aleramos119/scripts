proc frc_load {frc_file} {

	
	mol new $frc_file -autobonds off
	set frc_id [lindex [molinfo list] end]
	mol off $frc_id
	return $frc_id
	
}

proc xyz_load {xyz_file} {

	mol new $xyz_file 
	set xyz_id [lindex [molinfo list] end]
	return $xyz_id
}

proc vmd_draw_arrow {mol start end const} {
    # an arrow is made of a cylinder and a cone
    set middle [vecadd $start [vecscale 0.5 [vecsub $end $start]]]
    #graphics $mol color yellow
    graphics $mol cylinder $start $middle radius 0.02 resolution 10 filled yes
    graphics $mol cone $middle $end radius 0.05 resolution 10 
}



proc print_force {args} {

	global xyz_id
	global frc_id
        global const
        global vmd_frame
        
        set fr $vmd_frame($xyz_id)
        
        draw delete all
        draw color yellow 
	
	set xyz_sel [atomselect $xyz_id all frame $fr]
	set frc_sel [atomselect $frc_id all frame $fr]
# 	
# 	set atm_frame [$xyz_sel frame]
#  	puts "$atm_frame"
# 	puts "[lindex [$xyz_sel get {x y z}] 1]"
	
	 
	
	set n_atm [$xyz_sel num]
	
	
	
	for {set i 0} {$i < $n_atm} {incr i} { 	 
	  set xyz_atm_i [lindex [$xyz_sel get {x y z}] $i]
	  set frc_atm_i [lindex [$frc_sel get {x y z}] $i]
	  
	  set frc_atm_i [vecscale $const $frc_atm_i]
	  
	  set end [vecadd $xyz_atm_i $frc_atm_i]
	  
	  vmd_draw_arrow top $xyz_atm_i $end 1
	  
	 
	}
	
	draw color blue

}

proc enabletrace {} {
      global vmd_frame
      global xyz_id
      
      
      #puts "$vmd_frame($xyz_id)  $xyz_id "
      trace variable vmd_frame($xyz_id) w "print_force" 

}


# 
# proc print {args} {
# 
# global vmd_frame
# global xyz_id
# 
# puts "$vmd_frame($xyz_id)"
# }
# 
# proc disabletrace{} {
#       global vmd_frame
#       trace vdelete vmd_frame($xyz_id) w print_force
# 
# }





        set working_folder "/cluster/data/ar612/cp2k_organized/reac_path/r19_ts_p_band_reduced"


	set frc_file "r19_ts_p_e_dens-frc-1.xyz"
	set frc_file "$working_folder/$frc_file"

	
	set xyz_file "r19_ts_p_band_movie_int.xyz"
	set xyz_file "$working_folder/$xyz_file"
	
	
	
	
	set frc_id [frc_load $frc_file]
	set xyz_id [xyz_load $xyz_file]
	
	
	
	set frc_au_2_Eh2kcalmol_A 1185.8208901428313
	
	set const 0.5
	
	set const [expr $const*$frc_au_2_Eh2kcalmol_A] 
	
	

	
	
	#set xyz_sel [atomselect $xyz_id all]
	
	#set frc_sel [atomselect $frc_id all]
	
	
	
	
	
	
	
	



