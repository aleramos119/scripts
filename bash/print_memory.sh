#!/bin/bash

SLURMID=$1
PFAD=$2
SAMPLING=$3

if [[ $# -eq 3 ]]; then

echo "#Date | Virtual memory [MB] | Real memory [MB]" > $PFAD/JOB_${SLURMID}.mem
while true
do
	sleep $SAMPLING
	if [[ -e $PFAD/JOB_${SLURMID}.mem ]];then
		N=$(scontrol listpids $SLURMID 2>&1 | cat | grep 'does not exist on this node.' | wc -l)
		if [[ $N -eq 1 ]];then
			break
		fi
		DATUM=$(date)
		PIDGREP=$(scontrol listpids $SLURMID | awk '{print $1}' | awk '{if(NR==1) printf("grep "); else printf("-e %s ",$1)}')
		top -b -n1 | ${PIDGREP} > $PFAD/mem_${SLURMID}.tmp

		awk '{	if(substr($5,length($5),1)=="k") a=$5/1024; 
			else if(substr($5,length($5),1)=="m") a=$5;
			else if(substr($5,length($5),1)=="g") a=$5*1024;
			else a=$5/1024;
			if(substr($6,length($6),1)=="k") b=$6/1024; 
			else if(substr($6,length($6),1)=="m") b=$6;
			else if(substr($6,length($6),1)=="g") b=$6*1024;
			else b=$6/1024;
			print a,b}' $PFAD/mem_${SLURMID}.tmp | awk -v d="${DATUM}" 'BEGIN{v=0;r=0}{r+=$2;v+=$1}END{print d,v,r}' >> $PFAD/JOB_${SLURMID}.mem

		rm $PFAD/mem_${SLURMID}.tmp
	else
		exit 0
	fi
done

else
	echo "Error, wrong number of arguments. Usage: print_memory.sh JOBID FULL_QUALIFIED_OUTPUT_PATH SAMPLING_IN_SECONDS" 
fi
